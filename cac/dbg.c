#include <stdarg.h>
#include <stdio.h>
#include "dbg.h"

static  FILE * dbg_fp;
void dbg (const char * format, ...)
  {
    va_list args;
    va_start (args, format);
    //vfprintf (dbg_fp, format, args);
    vprintf (format, args);
    va_end (args);
  }

void set_dbg_file (const char * fn)
  {
    dbg_fp = fopen (fn, "w");
  }

