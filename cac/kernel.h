#include "muss.h"

// musl kernel interface

extern ADDR PW1,  PW2, PW3, PW4, PW5, PW6, PW7;
extern LOGICAL64 PWW1, PWW2, PWW3, PWW4; 

// 5.3 File System Commands
extern void           OPEN_DIR (ADDR_LOGICAL8);
#ifdef PDP
extern void           OPEN_FILE (LOGICAL64, LOGICAL64, INTEGER, INTEGER);
#else
extern void           OPEN_FILE (ADDR_LOGICAL8, LOGICAL64, INTEGER, LOGICAL);
#endif
// [cac] Sometimes OPENFILE, sometimes OPEN.FILE
#define OPENFILE OPEN_FILE
#ifndef _need_stdio
extern void           FILE (ADDR_LOGICAL8, LOGICAL64, INTEGER);
#endif
extern void           DELETE_FILE (ADDR_LOGICAL8, LOGICAL64);
extern void           RENAME_FILE (ADDR_LOGICAL8, LOGICAL64, ADDR_LOGICAL8);
extern void           CATALOGUE_FILES (ADDR_LOGICAL8, LOGICAL64);
extern void           READ_FILE_STATUS (ADDR_LOGICAL8, LOGICAL64);
extern void           PERMIT (ADDR_LOGICAL8, LOGICAL64, LOGICAL64, LOGICAL64);
extern void           CATALOGUE_PERMIT (ADDR_LOGICAL8, LOGICAL64);
extern void           NAME_DIR (LOGICAL64, LOGICAL64, LOGICAL64, ADDR_LOGICAL8);
extern void           RELEASE_DIR_NAME (LOGICAL64);
extern void           CREATE_SUBDIR (ADDR_LOGICAL8);
extern void           DELETE_SUBDIR (ADDR_LOGICAL8);
extern void           CREATE_FILE_SEGMENT (INTEGER, ADDR, ADDR_LOGICAL8, LOGICAL64);
extern void           CREATE_FILE_X_SEGMENT (INTEGER, ADDR_LOGICAL8, LOGICAL64);
extern void           CHANGE_ROOT_DIR (LOGICAL64, LOGICAL64);
extern void           CHANGE_FILE_SIZE (ADDR_LOGICAL8, LOGICAL64, INTEGER);

// 13.1  Virtual Store Management
extern void           CREATE_X_SEGMENT (INTEGER);
extern void           CHANGE_SIZE (INTEGER, ADDR);
extern void           CHANGE_ACCESS (INTEGER, LOGICAL);
extern void           INTERCHANGE (INTEGER, INTEGER);
extern void           READ_SEGMENT_STATUS (INTEGER);
extern void           MAP (INTEGER, INTEGER, INTEGER);
extern void           SECURE_SEGMENT (INTEGER);
extern void           COPY_BLOCK (INTEGER, INTEGER, INTEGER, INTEGER);
extern void           CHANGE_COMMON_SEGMENT (INTEGER, INTEGER);
extern void           PASS_SEGMENT (INTEGER, INTEGER, INTEGER, INTEGER);


