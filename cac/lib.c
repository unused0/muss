#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define _need_stdio
#include "kernel.h"
#include "lib.h"
#include "dbg.h"

#define NSTREAMS 16
// MUSS uses 0 for stdin, stdout; the output rontines will
// special case stdin.
static FILE * stream_table [NSTREAMS] = {[0 ... NSTREAMS - 1] = NULL};

static int current_input_stream = 0;
static int current_output_stream = 0;
static int lastch;

void SELECT_INPUT (INTEGER SN)
  {
    current_input_stream = SN;
  }

void SELECT_OUTPUT (INTEGER SN)
  {
    current_output_stream = SN;
  }

INTEGER DEFINE_INPUT (INTEGER SN, ADDR_LOGICAL8 FN, INTEGER MODE)
  {
    if (SN < -1 || SN >= NSTREAMS)
      {
        perror ("DEFINE_INPUT passed bad stream number");
        exit (1);
      }
    if (SN == -1)
      {
        int i;
        // Reserve 0 of default stdin/stdout
        for (i = 1; i < NSTREAMS; i ++)
          {
            if (stream_table[i] == NULL)
             break;
          }
        if (i >= NSTREAMS)
          {
            perror ("DEFINEINPUT out of streams");
            exit (1);
          }
        SN = i;
      }
    if (stream_table[SN] != NULL)
      {
        fclose (stream_table[SN]);
        stream_table[SN] = NULL;
      }
    vector * v = (vector *) FN; 
    char buf[v->l + 1];
    memcpy (buf, v->p, v->l);
    buf[v->l] = '\0';
    FILE *result = fopen (buf, "r");
    if (result == NULL)
      {
        perror ("Input file could not be opened");
        exit (1);
      }
    stream_table[SN] = result;
    return SN;
  }

INTEGER DEFINE_OUTPUT (INTEGER SN, ADDR_LOGICAL8 FN, INTEGER32 MODE, INTEGER32 P4)
  {
    if (SN < -1 || SN >= NSTREAMS)
      {
        perror ("DEFINE_OUTPUT passed bad stream number");
        exit (1);
      }
    if (SN == -1)
      {
        int i;
        // Reserve 0 of default stdin/stdout
        for (i = 1; i < NSTREAMS; i ++)
          {
            if (stream_table[i] == NULL)
             break;
          }
        if (i >= NSTREAMS)
          {
            perror ("DEFINEINPUT out of streams");
            exit (1);
          }
        SN = i;
      }
    if (stream_table[SN] != NULL)
      {
        fclose (stream_table[SN]);
        stream_table[SN] = NULL;
      }
    vector * v = (vector *) FN; 
    char buf[v->l + 1];
    memcpy (buf, v->p, v->l);
    buf[v->l] = '\0';
    FILE *result = fopen (buf, "w");
    if (result == NULL)
      {
        perror ("Output file could not be opened");
        exit (1);
      }
    stream_table[SN] = result;
    return SN;
  }

INTEGER CURRENT_OUTPUT (void)
  {
    return current_output_stream;
  }

INTEGER CURRENT_INPUT (void)
  {
    return current_input_stream;
  }

void END_INPUT (INTEGER SN, INTEGER MODE)
  {
    // XXX doesn't handle STRn, mode sign...
    if (SN < 0 || SN >= NSTREAMS)
    {
        printf ("warning: END.INPUT passed out of range stream number %d; ignoring\n", SN);
       return;
    }
    if (stream_table[SN] == NULL)
    {
        printf ("warning: END.INPUT closed stream number %d; ignoring\n", SN);
       return;
    }
    fclose(stream_table[SN]);
    stream_table[SN] = NULL;
   }



INTEGER IN_CH (void)
  {
    int result = fgetc (stream_table[current_input_stream]);
#if 0
    if (result == -1)
      return 4; // EOT
#endif
    return result;
  }

static FILE * current_input (void)
  {
    // If stream is non-zero, just return the table entry
    if (current_input_stream)
      return stream_table[current_input_stream];
    // If stream has been defined, return the table entry;
    if (stream_table[current_input_stream])
      return stream_table[current_input_stream];
    // Stream has not been defined, default to stdin
    return stdin;
  }

FILE * current_output (void)
  {
    // If stream is non-zero, just return the table entry
    if (current_output_stream)
      return stream_table[current_output_stream];
    // If stream has been defined, return the table entry;
    if (stream_table[current_output_stream])
      return stream_table[current_output_stream];
    // Stream has not been defined, default to stdout
    return stdout;
  }

void OUT_CH (INTEGER CH)
  {
    FILE * fp = current_output();
    lastch = CH;
    fputc (CH, fp);
  }

int NEXT_CH (void)
  {
    char result = IN_CH();
    IN_BACKSPACE (1);
    return result;
  }

void NEWLINES (INTEGER N)
  {
    FILE * fp = current_output ();
    if (lastch != '\n')
      N = N + 1;

    for (INTEGER i = 0; i < N; i++)
     fprintf(fp, "\n");
  }

void IN_BACKSPACE (INTEGER N)
  {
    FILE * fp = current_input ();
    fseek (fp, -N, SEEK_CUR);
  }

void PPC_CMD (void)
  {
    dbg ("XXX PPC.CMD\n");
  }


// OUT.LINE.NO (INTEGER32)
// This procedure prints the packed page/line number specified by P1 on the
// current output stream. The page and line numbers are printed in decimal,
// separated by ‘.’ with a total field width of 10 characters. The most
// significant 16 bits of P1 should give the page number and the rest of P1
// gives the line number.

void OUT_LINE_NO (INTEGER32 PAGE_LINE) // SUBLIB PROC
  {
    INTEGER32 PAGE = (PAGE_LINE >> 16) & 0xFFFF;
    INTEGER32 LINE = PAGE_LINE & 0xFFFF;
    char buf [11];
    snprintf (buf, 11, "%d.%d", PAGE, LINE);
    fprintf (current_output (), "%10s", buf);
  }


LOGICAL32 FIND_N (ADDR_LOGICAL8 NM, INTEGER TY)
  {
    vector * v = (vector *) NM;
    dbg ("XXX FIND_N NM.p %p NM.l %d TY %d\n", NM, TY);
    return 0;
  }

void OUT_HEX (LOGICAL32 P1, INTEGER P2)
  {
    if (P2 == 0)
      fprintf (current_output (), "%X", P1);
    else
      fprintf (current_output (), "%*X", P2, P1);
  }

void OUT_I (INTEGER32 P1, INTEGER P2)
  {
    if (P2 == 0)
      fprintf (current_output (), "%d", P1);
    else
      fprintf (current_output (), "%*d", P2, P1);
  }

void SPACES (INTEGER N)
  {
    FILE * fp = current_output ();
    for (int i = 0; i < N; i++)
      {
        fprintf (fp, " ");
      }
  }

void CAPTION (ADDR_LOGICAL8 STR)
  {
    vector * v = (vector *) STR; 
    char buf[v->l + 1];
    memcpy (buf, v->p, v->l);
    buf[v->l] = '\0';
    FILE * fp = current_output ();
    fprintf (fp, "%s", buf);
  }

INTEGER FIND_P (LOGICAL32 IDX, INTEGER PN, INTEGER WHAT)
  {
    dbg ("XXX FIND_P %u %d %d\n", IDX, PN, WHAT);
    return 0;
  }

void OUT_BIN_B (INTEGER P1)
  {
    FILE * fp = current_output ();
    uint8_t byte = P1 & 0xff;
    fwrite (& byte, 1, 1, fp);
  }

INTEGER32 I_POS (void)
  {
    dbg ("XXX I_POS\n");
    return 0;
  }

