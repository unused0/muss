#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>

#define _need_stdio
#include "kernel.h"
#include "libm.h"
#include "nmsl.h"
#include "mutl_link.h"
#include "dbg.h"

ADDR PART (ADDR PTR, INTEGER FIRST, INTEGER LAST)
  {
    //dbg ("XXX PART %p %d %d\n", PTR, FIRST, LAST);
    size_t n = LAST - FIRST + 1;
#if 0
    char * part = malloc (n + 1);
    memcpy (part, PTR + FIRST, n);
    part[n] = 0;
    return (ADDR) part;
#endif
    vector * v = malloc (sizeof (vector));
    if (v == NULL)
      {
        printf ("XXX PART malloc fail\n");
        exit (1);
      }
    v->p = PTR;
    v->l = n;
    return (ADDR) v;
  }

INTEGER SIZE (ADDR PTR)
  {
    //dbg ("XXX SIZE %p\n", PTR);
    return strlen ((const char *) PTR);
  }

int main (int argc, char * argv[])
  {
    set_dbg_file ("dbg.log");
    int CMPMODE = 0x200; /* 32 bit */
    if (argc <= 1)
      {
        printf("nmsl infile outfile [-i import] [-l logginglevel] [-lib]\n");
        printf("   -i is name of binary library to import");
        printf("   -l is bit mask of logging levels\n");
        printf("   -lib indicates a library is being compiled, otherwise it is a program\n");
        exit (1);
      }
    for (int arg = 3; arg < argc; arg ++)
      {
        if (strcmp (argv [arg], "-l") == 0)
          {
            arg ++;
            if (arg < argc)
              {
                set_logging (atoi (argv [arg ++]));
                continue;
              }
            printf ("Missing logging level");
            exit (1);
          }
        if (strcmp (argv[arg], "-lib") == 0)
          {
            CMPMODE |= 0x4;
            continue;
          }
        if (strcmp(argv[arg], "-i") == 0)
          {
            /* we import modules because we need additional data from modules for imported V-Store variables */
            arg ++;
            if (arg < argc)
              {
                import_module (argv [arg ++]);
                continue;
              }
            printf ("Missing module file name");
            exit (1);
          }
        printf ("Unknown argument %s", argv[arg]);
        exit (1);
      }
    MUSL ((ADDR_LOGICAL8) & vec (argv[1]), (ADDR_LOGICAL8) & vec (argv[2]), CMPMODE, 0);

    if ((CMPMODE & 0x4) == 0)
      {
        import_module (argv[2]);
        link_modules("test.bin");
      }
  }

