#ifndef MUSS_H
#define MUSS_H
#include <stdint.h>
#include <stddef.h>
#include <string.h>

typedef int          INTEGER;
typedef int8_t       INTEGER8;
typedef int16_t      INTEGER16;
typedef int32_t      INTEGER32;
typedef int64_t      INTEGER64;
typedef unsigned int LOGICAL;
typedef uint8_t      LOGICAL8;
typedef uint16_t     LOGICAL16;
typedef uint32_t     LOGICAL32;
typedef uint64_t     LOGICAL64;
typedef void *       ADDR;
typedef int *        ADDR_INTEGER;
typedef int8_t *     ADDR_INTEGER8;
typedef unsigned int * ADDR_LOGICAL;
typedef uint8_t *    ADDR_LOGICAL8;
typedef float        REAL32; // XXX assumption
typedef double       REAL64; // XXX assumption
typedef __float128   REAL128;

typedef struct vector_s vector;
struct vector_s
  {
    ADDR_LOGICAL8 p;
    size_t l;
  };
#define vec(s) ((vector) { .p = (ADDR_LOGICAL8) s, .l = strlen(s) })

#endif
