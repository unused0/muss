#include <stdio.h>
#include <stdlib.h>

#define _need_stdio

#include "kernel.h"
#include "mutl.h"
#include "dbg.h"

void TL_CYCLE (INTEGER P1)
  {
    dbg ("XXX TL_CYCLE %d\n", P1);
  }
void TL_CV_CYCLE (INTEGER P1, INTEGER P2, INTEGER P3)
  {
    dbg ("XXX TL_CVCYCLE %d %d %d\n", P1, P2, P3);
  }
void TL_CV_LIMIT (INTEGER P1)
  {
    dbg ("XXX TL_CV_LIMIT %d\n", P1);
  }
void TL_REPEAT (void)
  {
    dbg ("XXX TL_REPEAT\n");
  }


