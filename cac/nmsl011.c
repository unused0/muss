#include "muss.h"
#include "kernel.h"
#include "lib.h"
#include "sys.h"
#include "vax.h"

#include "nmsl.h"


// MODULE (CON.STK,CON.PTR,CONSTKZ,MUSL,IX,DIRYZ,CEXIT,RESTART,RESFILE,MSTR,MUTLSTR
  PLIST_ENT PLIST[PLISTZ] = {[0 ... PLISTZ-1] = {0, 0, 0, 0, 0}};

// );
// ::PDP *CODE 1; 
#ifdef PDP
     // *CODE 1 // XXX
#endif

// *GLOBAL 7; 
    // *GLOBAL 7; // XXX

// $IN PW0,PW1;
    // [cac] in kernel.h

// *GLOBAL 5;
    // *GLOBAL 5; // XXX

// ::PDP $IN CUR.OLAY;
// ::PDP $PS OVERLAY($IN);
// ::PDP $PR OVERLAY(N);
// ::PDP IF N = 0, -> OUT;
// ::PDP IF N /= CUR.OLAY THEN
// ::PDP MAP (N=>CUR.OLAY +78, 4, 0)FI
// ::PDP OUT: END
#ifdef PDP
    INTEGER CUR_OLAY = 0;
    extern void OVERLAY (INTEGER);
    void OVERLAY (INTEGER) {
    if (N == 0) goto OUT;
    if (N != CUR_OLAY)
      {
        MAP ((N = CUR_OLAY) + 78, 4, 0); 
      }
    OUT: ;
    }
#endif



// $IN OLDIN,MSTR,ISTR,MUTL.STR;
    static INTEGER OLDIN = 0;
    INTEGER  MSTR = 0;
    static INTEGER ISTR = 0, MUTL = 0, STR = 0;

// $IN [CON.STKZ]CON.STK;
    INTEGER CON_STK[CON_STKZ] = {[0 ... CON_STKZ - 1] = 0};

// $IN CON.PTR, TEMP,IX,DIRYZ;
    INTEGER CON_PTR = 0;
    static INTEGER TEMP = 0;
    INTEGER IX = 0;
    INTEGER DIRYZ = 0;

// ADDR[$LO8] RES.FILE;
    ADDR_LOGICAL8 RES_FILE = (ADDR_LOGICAL8) 0 /* NULL */;

// $LA RESTART,CEXIT;
    jmp_buf RESTART, CEXIT;

// @BOX 4.1
// CHARTS IN MODULE
// MSL01.1 MAIN.LOOP
// $LS MUSL(ADDR [$LO8],ADDR [$LO8],$IN,$IN);
    // [cac] in nmsl.h

// $PR MUSL(FILE,PROG,CMP.MODE,DIR);
    void MUSL (ADDR_LOGICAL8 FILE, ADDR_LOGICAL8 PROG, INTEGER CMP_MODE, INTEGER DIR)
      {

// ::PDP $IN SNO1,SNO2;
#ifdef PDP
    INTEGER SNO1 = 0, SNO1 = 0;
#endif

// @BOX 5.1
// INITIALISATION FOR
// ENTRY TO MAIN.LOOP
// ::THE LAYOUT HERE IS TEMPORARY TO AVOID FORWARD REFS
// ->TEMP.LAB.1;
    goto TEMP_LAB_1;

// TEMP.LAB.2:
    TEMP_LAB_2:

// :: MAIN LOOP
LB00011:

// START>:
    START:
// ITEMISE(0);
    ITEMISE (0);

// IS IT A LABEL

// $IF TAG OF LBUFF[1] = 1
// $AN [LBUFF[2] = COLON
// $OR LBUFF[2] = XCOLON],-> LB00028;
    if ((LBUFF[1].TAG == 1 &&
        ITYPE_eq (LBUFF[2], COLON)) ||
         ITYPE_eq (LBUFF[2], XCOLON)) goto LB00028;

// $IF CON.STK[CON.PTR] >= 9 $TH
// ALT.LABEL() $FI
    if (CON_STK[CON_PTR] >= 9) {
      ALT_LABEL (); }

// $IF TAG OF LBUFF[1]=0
// $AN IND OF LBUFF[1]=<40,-> LB00027;

    if (LBUFF[1].TAG == 0
        && LBUFF[1].IND <= 40) goto LB00027;

// IND OF LBUFF[1] => TEMP;
    TEMP = LBUFF[1].IND;

// $IF TAG OF LBUFF[1] /= 1,-> LB00020;
    if (LBUFF[1].TAG != 1) goto LB00020;

// $IF MUTLN(TEMP) => TEMP /= 0 /= %1002 $AN
// [K OF PLIST[TEMP] = KTYPE
// OR K OF PLIST[TEMP] = KTREF],-> LB00029;
    if (((TEMP = MUTLN (TEMP)) != 0 && TEMP != 0x1002 &&
        PLIST[TEMP].K == KTYPE) ||
         PLIST[TEMP].K == KTREF) goto LB00029;

// LB00017:
    LB00017: ;

// ::PDP OVERLAY(2);
#ifdef PDP
    OVERLAY (2);
#endif

// TRANS.COMP(0);
    TRANS_COMP (0);
// LB00018:
    LB00018: ;

// $IF LBUFF[1+>IPTR] = EOS,-> LB00011;
    if (ITYPE_eq (LBUFF[++ IPTR], EOS)) goto LB00011;

// MONITOR(0);
    MONITOR (0);
//  -> LB00011;
    goto LB00011;


    LB00020: ;

// IS IT A CONST
// $IF TAG OF LBUFF[1]=3,-> LB00017;
    if (LBUFF[1].TAG == 3) goto LB00017;

// IS IT '+,-,^,('

// $IF TAG OF LBUFF[1] = 0
// $AN DLIST[TEMP] & 2/= 0,-> LB00017;
    if (LBUFF[1].TAG == 0 
        && (DLIST[TEMP] & 2) != 0) goto LB00017;
// $IF LBUFF[1=>IPTR] /= GOTO,-> LB00024;
    if (ITYPE_ne (LBUFF[IPTR = 1], GOTO)) goto LB00024;
// TRANS.GOTO();
    TRANS_GOTO ();
//  -> LB00018;
    goto LB00018;

// LB00024
    LB00024:

// IS IT '*'
// $IF LBUFF[1] /= ASTERISK,-> LB00026;
    if (ITYPE_ne (LBUFF[1], ASTERISK))
      goto LB00026;
#ifdef PDP
    OVERLAY (1);
#endif // PDP

// TRANS.DIRECTIVE();
    TRANS_DIRECTIVE ();
// -> LB00018;
    goto LB00018;

// LB00026:
    LB00026: ;
// MONITOR(0);
    MONITOR (0);
//  -> LB00011;
    goto LB00011;

// LB00027:
    LB00027: ;

#if defined (MU6) || defined (VAX)
// ALTERNATIVE IND OF LBUFF[1=>IPTR] -1 FROM
    switch (LBUFF[IPTR = 1].IND - 1) {
// BEGIN.ST();PROC.HEAD();MOD.HEAD();
        case 0: BEGIN_ST (); break;
        case 1: PROC_HEAD (); break;
        case 2: MOD_HEAD (); break;
// END.ST();TRANS.IF();MONITOR(0);
        case 3: END_ST (); break;
        case 4: TRANS_IF (); break;
        case 5: MONITOR (0); break;
// TRANS.ELSE();TRANS.FI();TRANS.WHILE();
        case 6: TRANS_ELSE (); break;
        case 7: TRANS_FI (); break;
        case 8: TRANS_WHILE (); break;
// TRANS.FOR();MONITOR(0);TRANS.OD();
        case 9: TRANS_FOR (); break;
        case 10: MONITOR (0); break;
        case 11: TRANS_OD (); break;
// TRANS.SWITCH();TRANS.ALT();DECL.SPACE();
        case 12: TRANS_SWITCH (); break;
        case 13: TRANS_ALT (); break;
        case 14: DECL_SPACE (); break;
// TRANS.EXIT();DECL.PROC(0);DECL.LIT();
        case 15: TRANS_EXIT (); break;
        case 16: DECL_PROC (0); break;
        case 17: DECL_LIT (); break;
// DECL.DVEC();DECL.TYPE(0);DECL.FIELD();
        case 18: DECL_DVEC (); break;
        case 19: DECL_TYPE (0); break;
        case 20: DECL_FIELD (); break;
// DECL.VAR();DECL.VAR();DECL.VAR();
        case 21: DECL_VAR (); break;
        case 22: DECL_VAR (); break;
        case 23: DECL_VAR (); break;
// DECL.VAR();DECL.VSTORE();DECL.IMP();
        case 24: DECL_VAR (); break;
        case 25: DECL_VSTORE (); break;
        case 26: DECL_IMP (); break;
// DECL.PROC(8);TRNS.CMP: TRANS.COMP(0=>IPTR);->TRNS.CMP;
        case 27: DECL_PROC (8); break;
        case 28: TRNS_CMP: TRANS_COMP (IPTR = 0); break;
        case 29: goto TRNS_CMP;
// ->TRNS.CMP;->TRNS.CMP;->TRNS.CMP;
        case 30: goto TRNS_CMP;
        case 31: goto TRNS_CMP;
        case 32: goto TRNS_CMP;
// ->TRNS.CMP;->TRNS.CMP;->TRNS.CMP;
        case 33: goto TRNS_CMP;
        case 34: goto TRNS_CMP;
        case 35: goto TRNS_CMP;
// ->TRNS.CMP;DECL.PROC(4);DECL.TYPE (%3000);
        case 36: goto TRNS_CMP;
        case 37: DECL_PROC (4); break;
        case 38: DECL_TYPE (0x3000); break;
// DECL.VAR();
        case 39: DECL_VAR (); break;
// END
    }
#endif // MU6 || VAX
#ifdef PTV
#endif // PTV
#ifdef MC68000
#endif // MC68000
#ifdef PDP
// $DA OLN($LO8)
//  0 1 1 0 2 2 0 0 2 2
//  2 0 2 2 1 0 1 1 1 1
//  2 0 0 0 0 1 1 1 2 2
//  2 2 2 2 2 2 2 2
// END
    LOGICAL8 OLN[] = {
        0, 1, 1, 0, 2, 2, 0, 0, 2, 2,
        2, 0, 2, 2, 1, 0, 1, 1, 1, 1,
        2, 0, 0, 0, 0, 1, 1, 1, 2, 2,
        2, 2, 2, 2, 2, 2, 2, 2
      };
// OVERLAY(OLN[IND OF LBUFF[1]-1]);
    OVERLAY (OLN [LBUFF[1].IND - 1]);

// ALTERNATIVE IND OF LBUFF[1=>IPTR] -1 FROM
    switch (LBUFF[IPTR=1].IND - 1)
      {
// BEGIN.ST();PROC.HEAD();MOD.HEAD();
        case 0: BEGIN_ST (); break;
        case 1: PROC_HEAD (); break;
        case 2: MOD_HEAD (); break;
// END.ST();TRANS.IF();MONITOR(0);
        case 3: SND_ST (); break;
        case 4: TRANS_IF (); break;
        case 5: MONITOR (0); break;
// TRANS.ELSE();TRANS.FI();TRANS.WHILE();
        case 6: TRANS_ELSE (); break;
        case 7: TRANS_FI (); break;
        case 8: TRANS_WHILE (); break;
// TRANS.FOR();MONITOR(0);TRANS.OD();
        case 9: TRANS_FOR (); break;
        case 10: MONITOR (0); break;
        case 11: TRANS_WHILE (); break;
// TRANS.SWITCH();TRANS.ALT();DECL.SPACE();
        case 12: TRANS_SWITCH (); break;
        case 13: TRANS_ALT (); break;
        case 14: DECL_SPACE (); break;
// TRANS.EXIT();DECL.PROC(0);DECL.LIT();
        case 15: DECL_EXIT (); break;
        case 16: DECL_PROC (0); break;
        case 17: DECL_LIT (); break;
// DECL.DVEC();DECL.TYPE(0);DECL.FIELD();
        case 18: DECL_DVEC (); break;
        case 19: DECL_TYPE (0); break;
        case 20: DECL_FIELD (); break;
// DECL.VAR();DECL.VAR();DECL.VAR();
        case 21: DECL_VAR (); break;
        case 22: DECL_VAR (); break;
        case 23: DECL_VAR (); break;
// DECL.VAR();DECL.VSTORE();DECL.IMP();
        case 24: DECL_VAR (); break;
        case 25: DECL_VSTORE (); break;
        case 26: DECL_IMP (); break;
// DECL.PROC(8);TRNS.CMP: TRANS.COMP(0=>IPTR);->TRNS.CMP;
        case 27: DECL_PROC (8); break;
        case 28: TRNS_CMP: TRANS_COMP (IPTR = 0); break;
        case 29: goto TRNS_CMP;
// ->TRNS.CMP;->TRNS.CMP;->TRNS.CMP;
        case 30: goto TRNS_CMP;
        case 31: goto TRNS_CMP;
        case 32: goto TRNS_CMP;
// ->TRNS.CMP;->TRNS.CMP;->TRNS.CMP;
        case 33: goto TRNS_CMP;
        case 34: goto TRNS_CMP;
        case 35: goto TRNS_CMP;
// ->TRNS.CMP;DECL.PROC(4);DECL.TYPE (%3000);
        case 36: goto TRNS_CMP;
        case 37: DECL_PROC (4); break;
        case 38: DECL_TYPE (0x3000); break;
// DECL.VAR();
        case 39: DECL_VAR (); break;
// END
      }
#endif // PDP

// -> LB00018;
    goto LB00018;


LB00028:
// IF CONTEXT IS ALT PLANT JUMP TO END THEN DECLARE LABEL

// $IF CONSTK[CONPTR]=10$TH
    if (CONSTK[CONPTR] == 10) {
// TLPL(%4F,CONSTK[CONPTR-1]);
        TL_PL (0x4F, CONSTK[CONPTR-1]);
// 11 => CONSTK[CONPTR]$FI
        CONSTK[CONPTR] = 11; }
// DECL.LAB();
    DECL_LAB ();
//  -> LB00018;
    goto LB00018;



LB00029:
// PROCESS VAR DEC

// 1 => IPTR;
    IPTR = 1;
// DECL.VAR();
    DECL_VAR ();
// -> LB00018;
    goto  LB00018;

// OUT>:
    OUT:
// ->TEMP.LAB.3;
    goto TEMP_LAB_3;
// TEMP.LAB.1:
    TEMP_LAB_1: ;

// ::MU6 RELEASE.SEGMENT(61);
// ::MU6 CREATE.SEGMENT(61,%10000);
#ifdef MU6
    RELEASE_SEGMENT (61);
    CREATE_SEGMENT (61, (ADDR) 0x10000);
#endif
// ::PTV RELEASE.SEGMENT(30);
// ::PTV CREATE.SEGMENT(30,%D000);
#ifdef PTV
    RELEASE_SEGMENT (30);
    CREATE_SEGMENT (30, (ADDR) 0xD000);
#endif
// ::MC68000 RELEASE.SEGMENT(59);
// ::MC68000 RELEASE.SEGMENT(60);
// ::MC68000 RELEASE.SEGMENT(61);
// ::MC68000 CREATE.SEGMENT(59,%4000);
// ::MC68000 CREATE.SEGMENT(60,%4000);
// ::MC68000 CREATE.SEGMENT(61,%4000);
// ::MC68000 MAP(59, -1, 0);
// ::MC68000 MAP(60, -1, 0);
// ::MC68000 MAP(61, -1, 0);
#ifdef MC68000
    RELEASE.SEGMENT (59);
    RELEASE.SEGMENT (60);
    RELEASE.SEGMENT (61);
    CREATE.SEGMENT (59, 0x4000);
    CREATE.SEGMENT (60, 0x4000);
    CREATE.SEGMENT (51, 0x4000);
    MAP (59, -1, 0);
    MAP (60, -1, 0);
    MAP (61, -1, 0);
#endif
// ::PDP OPEN.FILE("NMSL5",0,-1,%E);
// ::PDP IF PW0 /= 0, -> GETOUT;
// ::PDP MAP(PW1 => SNO1,5,0);
// ::PDP OPEN.FILE("NMSL6",0,-1,%E);
// ::PDP IF PW0 /= 0, -> GETOUT;
// ::PDP MAP(PW1 => SNO2,6,0);
// ::PDP OPENFILE("NMSL3",%0,28,%D);
// ::PDP IF PW0 /= 0, ->GETOUT;
// ::PDP OPEN.FILE("NMSL4",%0,29,%D);
// ::PDP IF PW0 /= 0, ->GETOUT;
// ::PDP 0 => CUR.OLAY;
#ifdef PDP
    OPEN_FILE (& vec ("NMSL5"), 0, -1, 0xE);
    if (PW0 != 0) goto GETOUT;
    SNO1 = PW1;
    MAP(SNO1, 5, 0);
    OPEN_FILE (& vec ("NMSL6"), 0, -1, 0xE);
    if (PW0 != 0) goto GETOUT;
    SNO2 = PW1;
    MAP(SNO2, 6, 0);
    OPENFILE (& vec ("NMSL3"), 0x0, 28, 0xD);
    if (PW0 != 0) goto GETOUT;
    OPEN_FILE (& vec ("NMSL4"), 0x0, 29, 0xD);
    if (PW0 != 0) goto GETOUT;
    0 => CUR.OLAY; goto GETOUT;
#endif

// PROG => RES.FILE;
    RES_FILE = PROG;
#ifdef VAX
/*
ADDR_LOGICAL8 p = ((vector *) FILE)->p;
size_t l = ((vector *) FILE)->l;
muslfilename = malloc (l + 1);
strncpy (muslfilename, (const char *)p, l);
muslfilename[l] = 0;
*/
    set_musl_filename (FILE);
#endif
// DIR => DIRYZ;
    DIRYZ = DIR;
// START => RESTART;
    if (setjmp (RESTART))
      goto START;
// OUT => CEXIT;
    if (setjmp (CEXIT))
      goto OUT;
// 0 => CON.PTR;
    CON_PTR = 0;
// 7 => CON.STK[0];
    CON_STK[0] = 7;
// %8000 => IX;
    IX = 0x8000;
// CMP.MODE ->> 8 => C.MODE;
    C_MODE = CMP_MODE >> 8;
// INIT.S3();
    INIT_S3 ();
// INIT.S4();
    INIT_S4 ();
// INIT.S5();
    INIT_S5 ();
// INIT.S8();
    INIT_S8 ();
// INIT.S9();
    INIT_S9 ();
// CURRENT.INPUT() => OLDIN;
    OLDIN = CURRENT_INPUT ();
// CURRENT.OUTPUT() => MSTR;
    MSTR = CURRENT_OUTPUT ();
// DEFINE.INPUT(-1,FILE,0) => ISTR;
    ISTR = DEFINE_INPUT (-1, FILE, 0);
// SELECT.INPUT(ISTR);
    SELECT_INPUT (ISTR);
// ::PDP DEFINE.OUTPUT(-1,PROG,1,64,100,0) => MUTL.STR;
#ifdef PDP
    MUTL_STR = DEFINE_OUTPUT (-1, PROG, 1, 64, 100, 0);
#endif



// IF CMP.MODE & %800 = 0 THEN
    if ((CMP_MODE & 0x800) == 0) {
// TL(CMP.MODE&%FF,PROG,DIR) FI
      TL (CMP_MODE & 0xFF, PROG, DIR); }
// TL.MODULE();
    TL_MODULE ();

// ::THIS IS WHERE #MSL01.1 SHOULD BE
// ->TEMP.LAB.2;
    goto TEMP_LAB_2;


// TEMP.LAB.3:
    TEMP_LAB_3:
// SELECT.INPUT(OLDIN);
    SELECT_INPUT (OLDIN);
// ::PDP GETOUT:
#ifdef PDP
    GETOUT:
#endif
// SELECTOUTPUT(MSTR);
    SELECTOUTPUT (MSTR);
// END.INPUT(ISTR,0);
    END_INPUT (ISTR, 0);
// ::PDP END.OUTPUT(MUTL.STR,0);
#ifdef PDP
    END_OUTPUT (MUTL_STR, 0);
#endif
// ::MU6 RELEASE.SEGMENT(61);
#ifdef MU6
    RELEASE_SEGMENT (61);
#endif
// ::PTV RELEASE.SEGMENT(30);
#ifdef MU6
    RELEASE_SEGMENT (30);
#endif
// ::MC68000 RELEASE.SEGMENT(59);
// ::MC68000 RELEASE.SEGMENT(60);
// ::MC68000 RELEASE.SEGMENT(61);
#ifdef MC68000
    RELEASE_SEGMENT (59);
    RELEASE_SEGMENT (60);
    RELEASE_SEGMENT (61);
#endif
// ::PDP RELEASE.SEGMENT(28);
// ::PDP RELEASE.SEGMENT(29);
// ::PDP RELEASE.SEGMENT(SNO1);
// ::PDP RELEASE.SEGMENT(SNO2);
#ifdef PDP
    RELEASE_SEGMENT (28);
    RELEASE_SEGMENT (28);
    RELEASE_SEGMENT (SNO1);
    RELEASE_SEGMENT (SNO2);
#endif
// END
  } // MUSL
