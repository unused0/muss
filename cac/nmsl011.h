#include <setjmp.h>

//
// nmsl011
//

//   $LI CON.STKZ = 500;
#define CON_STKZ 500
#define CONSTKZ CON_STKZ
extern INTEGER CON_STK[CON_STKZ];
#define CONSTK CON_STK
extern INTEGER CON_PTR;
// [cac] Sometimes CON.PTR, sometimes CONPTR
#define CONPTR CON_PTR
void MUSL (ADDR_LOGICAL8, ADDR_LOGICAL8, INTEGER, INTEGER);
extern INTEGER IX;
extern INTEGER DIRYZ;
extern jmp_buf CEXIT;
extern jmp_buf RESTART;
extern ADDR_LOGICAL8 RES_FILE;
#define RESFILE RES_FILE
extern INTEGER MSTR;
extern INTEGER MUTL_STR;
#define MUTLSTR MUTL_STR
extern void TL_PROC_SPEC (ADDR_LOGICAL8, INTEGER);

