#include <stddef.h>
#include "muss.h"
#include "kernel.h"
#include "lib.h"
#include "libm.h"
#include "nmsl.h"

// 
// MODULE(LBUFFZ,CLISTZ,PLISTZ,PARAMSZ,TLISTZ,NLISTZ,
// NLIST,NLISTENT,PLIST,PLISTENT,CHLIST,XNAME, ::TEMP MOD
// KLAB,KLABREF,KVAR,KDUMMY,KGLAB,KIMPLAB,KTREF,KTYPE, ::TEMP MOD
// *ifdef PDP
// DIF,DTHEN,DELSE,DDO,DVSTORE,DTY,DEND,DDATAVEC,DADDR,
// DLA,DFROM,DOF,DIS,DAND,DOR,LTHAN,EQUALS,GTHAN,PLUS,MINUS,
// SLASH,GOTO,DAMPAND,DBANG,DEREF,HASH,REF,COMMA,LSB,RSB,LB,
// RB,BSLASH,XCOLON,COLON,TLAB,TLO32,
// *endif // PDP
// CHLISTZ,NIL);

// EXIT; ::ESCAPE FROM AUTOMATIC INITIALISATION OF PRIVATE LIBS
    // EXIT; // XXX

#ifdef PDP
ITYPE DIF;
ITYPE DTHEN;
ITYPE DELSE;
ITYPE DDO;
ITYPE DVSTORE;
ITYPE DTY;
ITYPE DEND;
ITYPE DDATAVEC;
ITYPE DADDR;
ITYPE DLA;
// [cac] PDP doesn't have DWITHIN?
ITYPE DFROM;
ITYPE DOF;
ITYPE DIS;
ITYPE DAND;
ITYPE DOR;
ITYPE LTHAN;
ITYPE EQUALS;
ITYPE GTHAN;
ITYPE PLUS;
ITYPE MINUS;
ITYPE SLASH;
ITYPE GOTO;
ITYPE DAMPAND;
ITYPE DBANG;
ITYPE DEREF;
ITYPE HASH;
ITYPE REF;
ITYPE COMMA;
ITYPE LSB;
ITYPE RSB;
ITYPE LB;
ITYPE RB;
ITYPE BSLASH;
ITYPE XCOLON;
ITYPE COLON;
#endif
ADDR_LOGICAL8 NIL;
NLIST_ENT NLIST[NLISTZ];
PLIST_ENT PLIST[PLISTZ];
LOGICAL8 CHLIST[CHLISTZ];
LOGICAL8 XNAME[64];
#if defined (MU6) || defined (VAX)
// ::MU6 $LI/ITYPE DIF = 0\0\%5, DTHEN = 0\0\%6,
ITYPE DIF;
ITYPE DTHEN;

// ::MU6 DELSE = 0\0\%7, DDO = 0\0\%B,DVSTORE = 0\0\%1A,DTY = 0\0\%14,
ITYPE DELSE;
ITYPE DDO;
ITYPE DVSTORE;
ITYPE DTY;

// ::MU6 DEND = 0\0\%4, DDATAVEC = 0\0\%13, DADDR = 0\0\%16, DLA = 0\%30\%28,DWITHIN = 0\0\%27,
ITYPE DEND;
ITYPE DDATAVEC;
ITYPE DADDR;
ITYPE DLA;
ITYPE DWITHIN;

// ::MU6 DFROM = 0\0\%2D,DOF = 0\0\%29, DIS = 0\0\%2A, DAND = 0\0\%2B,
ITYPE DFROM;
ITYPE DOF;
ITYPE DIS;
ITYPE DAND;

// ::MU6 DOR = 0\0\%2C, LTHAN = 0\0\%31, EQUALS = 0\0\%33, GTHAN = 0\0\%30,
ITYPE DOR;
ITYPE LTHAN;
ITYPE EQUALS;
ITYPE GTHAN;

// ::MU6 PLUS = 0\0\%34, MINUS = 0\0\%35,
ITYPE PLUS;
ITYPE MINUS;

// ::MU6 SLASH = 0\0\%37, GOTO = 0\0\%3F,DAMPAND = 0\0\%3A, DBANG = 0\0\%3B,
ITYPE SLASH;
ITYPE GOTO;
ITYPE DAMPAND;
ITYPE DBANG;

// ::MU6 DEREF = 0\0\%4A, HASH = 0\0\%56, REF = 0\0\%4A, COMMA = 0\0\%4C,
ITYPE DEREF;
ITYPE HASH;
ITYPE REF;
ITYPE COMMA;

// ::MU6 LSB = 0\0\%4D, RSB = 0\0\%4E, LB = 0\0\%4F,
ITYPE LSB;
ITYPE RSB;
ITYPE LB;

// ::MU6 RB = 0\0\%50, BSLASH = 0\0\%51, XCOLON = 0\0\%52,
ITYPE RB;
ITYPE BSLASH;
ITYPE XCOLON;

// ::MU6 COLON = 0\0\%4B;
ITYPE COLON;

#endif // MU6 || VAX
