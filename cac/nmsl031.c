#include <stddef.h>
#include "muss.h"
#include "kernel.h"
#include "lib.h"
#include "libm.h"
#include "nmsl.h"


// MODULE(ITEMISE,ITYPE,TINT,TADPROC,TRE0,TINT0,TLO8,TVST,TLO0,TLO1,TLO,TLAB,
// TDELIM,ITEM,LBUFF,IPTR,CLIST,DLIST,SBUFF,SPTR,SDLIST,STRINGTYPE,TRE,TBYADDR,
// TINT16,TINT32,TINT64,TLO32,EOS,LSHIFT,ASTERISK,EOL,DIMPORT,
// *ifdef MU6
// DIF,DTHEN,DELSE,DDO,DVSTORE,DTY,DEND,DDATAVEC,DADDR,DWITHIN,
// *endif // MU6
// *ifdef MU6
// DLA,DFROM,DOF,DIS,DAND,DOR,LTHAN,EQUALS,GTHAN,PLUS,MINUS,
// *endif // MU6
// *ifdef MU6
// SLASH,GOTO,DAMPAND,DBANG,DEREF,HASH,REF,COMMA,LSB,RSB,LB,
// *endif // MU6
// *ifdef MU6
// RB,BSLASH,XCOLON,COLON,
// *endif // MU6
// UMINUS,
// TDELIM,TNAME,TSTRING,TCONST,IEOS,IBYTE,IMIN.COMP,ILOAD,ISTORE,ICOMP,INIT.S3);

// $PS ADDN()/$LO16;
// $PS GENN($LO16)/ADDR[$LO8];
// $PS MONITOR($IN);
// $LO8[CHLISTZ] CHLIST;
LOGICAL8 CHLIST [CHLISTZ];
// $IN CWORD,LASTCH,CMODE;
INTEGER CWORD;
INTEGER C_MODE;
// $IN32 LINE.NO;
// $IN MSTR,MUTLSTR
INTEGER MSTR, MUTLSTR;



// GLOBAL 5; // XXX

// $LI MAX.KEYWORD = 45;
#define MAX_KEYWORD 45

// $LI TINT0=%40,TLO8=%80,TLO0=%80,TLO1=%20,TADPROC=%24,STRING.TYPE=%83;
// $LI TINT16=%44,TINT32=%4C,TINT64=%5C,TLO32=%8C,TRE0=%00,TLAB=%30;
// ITYPE [LBUFFZ]LBUFF;
ITYPE LBUFF[LBUFFZ];

// $LO8 [CLISTZ]CLIST;
LOGICAL8 CLIST[CLISTZ];

// $IN TINT,TLO,TRE,TBYADDR,TVST;
INTEGER TINT, TLO, TRE, TBYADDR, TVST;

// $IN IPTR,SPTR;
INTEGER IPTR, SPTR;

/* [rj] */
/* Manual format of the documentation
         TYPE---------------- ----------------STARTER
   BASIC TYPE-------------- | | --------------FINISHER
  BUILT IN FN------------ | | | | ------------CONDITIONAL
  COMP TERMTR---------- | | | | | | ----------OPERATOR
   SHIFT OP-------- | | | | | | | | --------COMPARATOR
             ------ | | | | | | | | | | ------STORE OP
             ---- | | | | | | | | | | | | ----INIT COMP OP
             -- | | | | | | | | | | | | | | --COND TERMTR
              | | | | | | | | | | | | | | | | 
          EOS | | | | |X| | | | | | | | | | |X|
        BEGIN | | | | | | | | |X|X| | | | | | |
         PROC | | | | | | | | |X| | | | | | | |
       MODULE | | | | | | | | |X| | | | | | | |
          END | | | | | | | | |X|X| | | | | | |
           IF | | | | | | | | |X| |X| | | | | |
         THEN | | | | |X| | | | |X|X| | | | |X|
         ELSE | | | | |X| | | |X|X|X| | | | | |
           FI | | | | |X| | | |X|X|X| | | | | |
        WHILE | | | | | | | | |X| | | | | | | |
          FOR | | | | | | | | |X| | | | | | | |
           DO | | | | |X| | | | |X| | | | | |X|
           OD | | | | | | | | |X|X| | | | | | |
       SWITCH | | | | | | | | |X| | | | | | | |
   ALTERNATIVE| | | | | | | | |X| | | | | | | |
        SPACE | | | | | | | | |X| | | | | | | |
         EXIT | | | | | | | | |X|X| | | | | | |
        PSPEC | | | | | | | | |X| | | | | | | |
      LITERAL | | | | | | | | |X| | | | | | | |
      DATAVEC | | | | | | | | |X| | | | | | | |
         TYPE | | | | | | | | |X| | | | | | | |
       SELECT | | | | | | | | |X| | | | | | | |
         ADDR | | | | | | | |X| | | | | | | | |
       INTEGER| | | | | | |X|X| | | | | | | | |
       LOGICAL| | | | | | |X|X| | | | | | | | |
         REAL | | | | | | |X|X| | | | | | | | |
       VSTORE | | | | | | | | |X| | | | | | | |
       IMPORT | | | | | | | | |X| | | | | | | |
        LSPEC | | | | | | | | |X| | | | | | | |
         BYTE | | | | | |X| | | | | | | | | | |
         MAKE | | | | | |X| | | | | | | | | | |
         POSN | | | | | |X| | | | | | | | | | |
         PART | | | | | |X| | | | | | | | | | |
         SIZE | | | | | |X| | | | | | | | | | |
         LLST | | | | | |X| | | | | | | | | | |
         LPST | | | | | |X| | | | | | | | | | |
         LENT | | | | | |X| | | | | | | | | | |
         X1XX | | | | | |X| | | | | | | | | | |
        BSPEC | | | | | | | | |X| | | | | | | |
       WITHIN | | | | | | | | |X| | | | | | | |
        LABEL | | | | | | |X|X| | | | | | | | |
          OF  | | | | | | | | | | | | | | | | |
          IS  | | | | | | | | | | | | | | | | |
          AND | | | | |X| | | | | | | | | | |X|
         TYPE---------------- ----------------STARTER
   BASIC TYPE-------------- | | --------------FINISHER
  BUILT IN FN------------ | | | | ------------CONDITIONAL
  COMP TERMTR---------- | | | | | | ----------OPERATOR
     SHIFT OP-------- | | | | | | | | --------COMPARATOR
             ------ | | | | | | | | | | ------STORE OP
             ---- | | | | | | | | | | | | ----INIT COMP OP
             -- | | | | | | | | | | | | | | --COND TERMTR
              | | | | | | | | | | | | | | | | 
           OR | | | | |X| | | | | | | | | | |X|
         FROM | | | | |X| | | | |X| | | | | | |
           >= | | | | |X| | | | | | | |X| | | |
           =< | | | | |X| | | | | | | |X| | | |
            > | | | | |X| | | | | | | |X| | | |
            < | | | | |X| | | | | | | |X| | | |
           /= | | | | |X| | | | | | | |X| | | |
            = | | | | |X| | | | | | | |X| | | |
            + | | | | | | | | | | | |X| | |X| |
            - | | | | | | | | | | | |X| | |X| |
            * | | | | | | | | | | | |X| | | | |
            / | | | | | | | | | | | |X| | | | |
           -: | | | | | | | | | | | |X| | | | |
           /: | | | | | | | | | | | |X| | | | |
            & | | | | | | | | | | | |X| | | | |
            ! | | | | | | | | | | | |X| | | | |
           -= | | | | | | | | | | | |X| | | | |
           => | | | | | | | | | | | |X| |X| | |
           +> | | | | | | | | | | | |X| |X| | |
           -> | | | | | | | | | | | |X| |X| | |
           *> | | | | | | | | | | | |X| |X| | |
           /> | | | | | | | | | | | |X| |X| | |
          -:> | | | | | | | | | | | |X| |X| | |
          /:> | | | | | | | | | | | |X| |X| | |
           &> | | | | | | | | | | | |X| |X| | |
           !> | | | | | | | | | | | |X| |X| | |
          -=> | | | | | | | | | | | |X| |X| | |
           << | | | | | | | | | | | | | | | | |
          ->> | | | |X| | | | | | | |X| | | | |
          <<- | | | |X| | | | | | | |X| | | | |
            ^ | | | | | | | | | | | | | | |X| |
            ; | | | | | | | | | |X| | | | | | |
            , | | | | |X| | | | | | | | | | |X|
            [ | | | | | | | | | | | | | | | | |
            ] | | | | |X| | | | | | | | | | |X|
            ( | | | | | | | | | | | | | | |X| |
            ) | | | | |X| | | | | | | | | | | |
            \ | | | | |X| | | | | | | | | | | |
           >: | | | | | | | | | |X| | | | | | |
            : | | | | | | | | | |X| | | | | | |
       FLTSYM | | | | | | | | | | | | | | | | |
   UNARY MINUS| | | | | | | | | | | |X| | | | |
       (hash) | | | | | | | | |X| | | | | | | |
          EOL | | | | | | | | | | | | | | | | |
My interpretation of the bits:
0x1000 - Shift Operator
0x0800 - Terminator (as in terminates a set of delimiters I think)
0x0010 - Computation terminator?????
0x0080 - Initial delimiter
0x0040 - Teminal delimiter
0x0020 - Conditional ?
0x0004 - Store operator
*/
// $LI SDLIST = 87;

// ::PDP *CODE 2;
#ifdef PDP
    // *CODE 2; // XXX
#endif

// *GLOBAL 1;
    // *GLOBAL 1; // XXX

// $DA DLIST($LO16)
// %801 %C0 %80 %2080 %C0 %A0 %861 %8E0
// %8E0 %80 %80 %841 %C0 %80 %80 %80
// %C0 %80 %2080 %2080 %2080 %80 %100 %300
// %300 %300 %80 %80 %80 %400 %400 %400
// %400 %400 %400 %400 %400 %400 %80 %80
// %300 %0 %0 %801 %801 %840 %808 %808
// %808 %808 %808 %808 %12 %12 %10
// %10 %10 %10 %10 %10 %10 %14 %14
// %14 %14 %14 %14 %14 %14 %14 %14
// %0 %1010 %1010 %2 %40 %801 %0 %801
// %2 %800 %800 %40 %40 %0 %10 %80
// $EN
    LOGICAL16 DLIST[] = {
        0x801,   0xC0,   0x80, 0x2080,   0xC0,   0xA0,  0x861,  0x8E0,
        0x8E0,   0x80,   0x80,  0x841,   0xC0,   0x80,   0x80,   0x80,
         0xC0,   0x80, 0x2080, 0x2080, 0x2080,   0x80,  0x100,  0x300,
        0x300,  0x300,   0x80,   0x80,   0x80,  0x400,  0x400,  0x400,
        0x400,  0x400,  0x400,  0x400,  0x400,  0x400,   0x80,   0x80,
        0x300,    0x0,    0x0,  0x801,  0x801,  0x840,  0x808,  0x808,
        0x808,  0x808,  0x808,  0x808,   0x12,   0x12,   0x10,
         0x10,   0x10,   0x10,   0x10,   0x10,   0x10,   0x14,   0x14,
         0x14,   0x14,   0x14,   0x14,   0x14,   0x14,   0x14,   0x14,
          0x0, 0x1010, 0x1010,    0x2,   0x40,  0x801,    0x0,  0x801,
          0x2,  0x800,  0x800,   0x40,   0x40,    0x0,   0x10,   0x80
    };

// *GLOBAL 5;
    // *GLOBAL 5; // XXX

// ::PDP *CODE 1;
#ifdef PDP
    // *CODE 1; // XXX
#endif

// $LI/ITYPE  UMINUS=0\0\%55,EOS=0\0\%0,LSHIFT=0\0\%47,ASTERISK=0\0\%36,EOL=0\0\%57,DIMPORT=0\0\%1B;
ITYPE UMINUS = {0, 0, 0x55};
ITYPE EOS = {0, 0, 0x0};
ITYPE LSHIFT = {0, 0, 0x47};
ITYPE ASTERISK = {0, 0, 0x36};
ITYPE EOL = {0, 0, 0x57};
ITYPE DIMPORT = {0, 0, 0x1B};

#if defined (MU6) || defined (VAX)
// ::MU6 $LI/ITYPE DIF = 0\0\%5, DTHEN = 0\0\%6,
ITYPE DIF = {0, 0, 0x5};
ITYPE DTHEN = {0, 0, 0x6};

// ::MU6 DELSE = 0\0\%7, DDO = 0\0\%B,DVSTORE = 0\0\%1A,DTY = 0\0\%14,
ITYPE DELSE = {0, 0, 0x7};
ITYPE DDO = {0, 0, 0xb};
ITYPE DVSTORE = {0, 0, 0x1a};
ITYPE DTY = {0, 0, 0x14};

// ::MU6 DEND = 0\0\%4, DDATAVEC = 0\0\%13, DADDR = 0\0\%16, DLA = 0\%30\%28,DWITHIN = 0\0\%27,
ITYPE DEND = {0, 0, 0x4};
ITYPE DDATAVEC = {0, 0, 0x13};
ITYPE DADDR = {0, 0, 0x16};
ITYPE DLA = {0, 0x30, 0x28};
ITYPE DWITHIN = {0, 0, 0x27};

// ::MU6 DFROM = 0\0\%2D,DOF = 0\0\%29, DIS = 0\0\%2A, DAND = 0\0\%2B,
ITYPE DFROM = {0, 0, 0x2d};
ITYPE DOF = {0, 0, 0x29};
ITYPE DIS = {0, 0, 0x2a};
ITYPE DAND = {0, 0, 0x2b};

// ::MU6 DOR = 0\0\%2C, LTHAN = 0\0\%31, EQUALS = 0\0\%33, GTHAN = 0\0\%30,
ITYPE DOR = {0, 0, 0x2c};
ITYPE LTHAN = {0, 0, 0x31};
ITYPE EQUALS = {0, 0, 0x33};
ITYPE GTHAN = {0, 0, 0x30};

// ::MU6 PLUS = 0\0\%34, MINUS = 0\0\%35,
ITYPE PLUS = {0, 0, 0x34};
ITYPE MINUS = {0, 0, 0x35};

// ::MU6 SLASH = 0\0\%37, GOTO = 0\0\%3F,DAMPAND = 0\0\%3A, DBANG = 0\0\%3B,
ITYPE SLASH = {0, 0, 0x37};
ITYPE GOTO = {0, 0, 0x3f};
ITYPE DAMPAND = {0, 0, 0x3a};
ITYPE DBANG = {0, 0, 0x3b};

// ::MU6 DEREF = 0\0\%4A, HASH = 0\0\%56, REF = 0\0\%4A, COMMA = 0\0\%4C,
ITYPE DEREF = {0, 0, 0x4a};
ITYPE HASH = {0, 0, 0x56};
ITYPE REF = {0, 0, 0x4a};
ITYPE COMMA = {0, 0, 0x4c};

// ::MU6 LSB = 0\0\%4D, RSB = 0\0\%4E, LB = 0\0\%4F,
ITYPE LSB = {0, 0, 0x4d};
ITYPE RSB = {0, 0, 0x4e};
ITYPE LB = {0, 0, 0x4f};

// ::MU6 RB = 0\0\%50, BSLASH = 0\0\%51, XCOLON = 0\0\%52,
ITYPE RB = {0, 0, 0x50};
ITYPE BSLASH = {0, 0, 0x51};
ITYPE XCOLON = {0, 0, 0x52};

// ::MU6 COLON = 0\0\%4B;
ITYPE COLON = {0, 0, 0x4b};
#endif // MUF || VAX


// $LI KEYWORD = 44;
// [cac] Unused? // XXX
#define KEYWORD 44  

// $LI TDELIM = 0, TNAME = 1, TSTRING = 2, TCONST = 3;

// $LI IEOS = 0, IBYTE = 29, IMIN.COMP = 46, ILOAD = 51, ISTORE = 61, ICOMP = 71;

// $LO8[2000]SBUFF;
LOGICAL8 SBUFF[2000];

// $LI NL=10, NEWP=12;
#define NL 10
#define NEWP 12

// $LO CFWDPTR, CBWDPTR;
    static LOGICAL CFWDPTR, CBWDPTR;

// ITYPE SAVEDELIM;
    static ITYPE SAVEDELIM;

// @BOX 4.1
// PROCEDURES IN MODULE:
// ITEMISE [MSL03.1]

// ::PDP *CODE 1;
#ifdef PDP
    // *CODE 1; // XXX
#endif

// #MSL03.1.2

// PSPEC XSYM() /$LO;
// PSPEC YSYM()/$LO;
// PSPEC XINT($LO)/$LO32;
// PSPEC XHEX() /$LO64;
// PSPEC XBTYPE($LO,$IN)/$IN;
static LOGICAL XSYM (void);
static LOGICAL YSYM (void);
static LOGICAL32 XINT (LOGICAL);
static LOGICAL64 XHEX (void);
static INTEGER XBTYPE (LOGICAL, INTEGER);

// @BOX 3.1
// $PR XSYM;
static LOGICAL XSYM (void) {
    LOGICAL ret;

// $LI EOT = 4;
    int EOT = 4;

// INCH() => XSYM;
    ret = INCH ();

// $IF CWORD & 2 = 2 $TH
    if ((CWORD & 2) == 2) {

// $IF XSYM /= EOT $TH
        if (ret != EOT) {

// OUTCH(XSYM)
            OUTCH (ret);

// $FI
        }

// $FI
    }

// XSYM => SBUFF[1+>SPTR];
    SBUFF[++ SPTR] = ret;

// $EN
    return ret;
    }

// $PR XINT(INIT.VAL);
static LOGICAL32 XINT (LOGICAL INIT_VAL) {

// $LO32 VAL;$IN SYM;
    LOGICAL32 VAL;
    INTEGER SYM;

// INIT.VAL => VAL;
    VAL = INIT_VAL;

// $WH NEXTCH()=>SYM >= '0 $AN SYM =< '9 $DO
    while ((SYM = NEXTCH()) >= '0' && SYM <= '9')

// VAL * 10 - '0 + XSYM() => VAL $OD
      VAL = VAL * 10 - '0' + XSYM ();

// VAL => XINT;
    return VAL;
// $EN
    }

// $PR XHEX;
static LOGICAL64 XHEX (void) {

// $LO64 ANS;$LO ISYM;
    LOGICAL64 ANS;
    LOGICAL ISYM;
// 0 => ANS;
    ANS = 0;
// $WH NEXTCH()=>ISYM >= '0 $AN ISYM =< '9 $OR
    while (((ISYM = NEXTCH ()) >= '0' && ISYM <= '9') ||
// ISYM >= 'A $AN ISYM =< 'F $OR ISYM = '( $DO
            (ISYM >= 'A' && ISYM <= 'F') || ISYM == '(') {
// $IF ISYM = '( $TH
        if (ISYM == '(') {
// ANS & %F => ISYM;
            ISYM = ANS & 0xf;
// XSYM();
            XSYM ();
// $FO XINT(XSYM()-'0)-1 $DO
            { INTEGER limit = XINT(XSYM () - '0') - 1;
            for (INTEGER i = 0; i < limit; i ++) {
// ANS <<- 4 ! ISYM => ANS $OD
                ANS = (ANS << 4) | ISYM; } }
// $IF XSYM() /= ') $TH MONITOR(14) $FI
            if (XSYM () != ')') { MONITOR (14); }
// $EL $IF XSYM() >= 'A $TH
        } else { if (XSYM () >= 'A') {
// ANS <<- 4 ! (ISYM-'A+10) => ANS
            ANS = (ANS << 4) | (ISYM - 'A' + 10);
// $EL ANS <<-4 ! (ISYM-'0) => ANS;
        } else { ANS = (ANS << 4) | ISYM - '0';
// $FI $FI $OD
       }
       }
       } // while do
// ANS => XHEX;
    return ANS;
// $EN
    }

// $PR XBTYPE(TSIZE,PTR);
static INTEGER XBTYPE (LOGICAL TSIZE, INTEGER PTR) {

// $IN T;
    INTEGER T;
// IF PTR>22<26 THEN
    if (PTR > 22 && PTR < 26) {
// ALTERNATIVE PTR-23 FROM
      switch (PTR-23) {
// BEGIN ::INTEGER
      case 0: { 
// $IF TSIZE=0 $TH TINT => T
        if (TSIZE == 0) { T = TINT; }
// $EL $IF TSIZE & %7 = 0 $AN TSIZE =< 32 $TH
        else { if (((TSIZE & 0x7) == 0) && (TSIZE <= 32)) {
// TSIZE ->> 1 + 60 => T
          T = (TSIZE >> 1) + 60;
// $EL MONITOR(3) $FI $FI
        } else { MONITOR (3); } }
// END
      break;
      }

// BEGIN ::LOGICAL
      case 1: {
// $IF TSIZE=0 $TH TLO => T
        if (TSIZE == 0) { T = TLO; }
// $EL $IF TSIZE & %7 = 0 $AN TSIZE = <32 $OR TSIZE = 64 $TH
        else { if ((((TSIZE & 0x7) == 0) && (TSIZE <= 32)) || TSIZE == 64) {
// TSIZE ->> 1 + 124 => T
          T = (TSIZE >> 1) + 124;
// $EL IF TSIZE = 1 $TH TLO1 => T
        } else { if (TSIZE == 1) { T = TLO1; 
// $EL MONITOR(3) $FI $FI $FI
        } else { MONITOR (3); } } }
// END
      break;
    }

// BEGIN ::REAL
      case 2: {
// $IF TSIZE=0 $TH TRE => T
        if (TSIZE == 0) { T = TRE; }
// $EL $IF TSIZE = 32 $OR TSIZE = 64 $OR TSIZE = 128 $TH
        else { if (TSIZE == 32 || TSIZE == 64 || TSIZE == 128) {
// TSIZE ->> 1 -4 => T
          T = (TSIZE >> 1) - 4;
// $EL MONITOR(13) $FI $FI
        } else { MONITOR (13); } }
// END
      break;
      }
// END
    } // switch
// $EL IF TSIZE = 0 $TH
    } else { if (TSIZE == 0) {
// TLAB=>T;
      T = TLAB;
// $EL MONITOR(3) FI FI
    } else { MONITOR (3); } }
// T => XBTYPE;
    return T;
// $EN
    }


// $PR YSYM;
static LOGICAL YSYM (void) {
  LOGICAL ret;

// $IF XSYM() => YSYM = '$$ $TH
  if ((ret = XSYM ()) == '$') {
// $IF XSYM() => YSYM == 'L $TH NL => YSYM
    if ((ret = XSYM ()) == 'L') { ret = NL;
// $EL $IF YSYM = 'P $TH NEWP => YSYM
    } else { if (ret == 'P') { ret = NEWP;
// $EL $IF YSYM = 'N $TH 0 => YSYM
    } else { if (ret == 'N') { ret = 0;
// $EL $IF YSYM /= '$$ /= '$"
    } else { if (ret != '$' && ret != '"')
// $TH MONITOR (142);
    { MONITOR (142); 
// $FI $FI $FI $FI
    } } } } 
// 128 !> YSYM;
    ret |= 128;
// $FI
    }
// $EN
    return ret;
  }


// $PS PRINT.ITEM.LINE();
static void PRINT_ITEM_LINE (void);
#define PRINT_ITEMLINE PRINT_ITEM_LINE
// $PR PRINT.ITEM.LINE;
static void PRINT_ITEM_LINE (void) {

// $IN I;
  INTEGER I;
// SELECTOUTPUT(MSTR);
  SELECTOUTPUT (MSTR);
// NEWLINES(0);
  NEWLINES (0);
// 0 =>I;
  I = 0;
// WHILE LBUFF[1+>I] /= EOS DO
  while (ITYPE_ne (LBUFF[++ I], EOS)) {
// OUTHEX(TAG OF LBUFF[I] <<- 8
    OUTHEX ((LBUFF[I].TAG << 8)
// ! ST OF LBUFF[I] <<- 16
    | (LBUFF[I].ST << 16)
// ! IND OF LBUFF[I], 8);
    | LBUFF[I].IND, 8);
// SPACES(2);
  SPACES (2);
// IF I & 7 = 0 THEN
  if ((I & 7) == 0) {
// NEWLINES(1) FI
    NEWLINES (1); }
// $OD
  }
// END
  }

// $PS ITEMISE($IN);

// #MSL03.1

// @BOX 5.1
// MODULE INITIALIZATION

// $PS INITS3();
// $PR INITS3;
// ::PDP $LI/$LO8 INT = %44, DEFAD = %44;
// ::MU6 $LI/$LO8 INT = %4C, DEFAD = %4C;
// $AL CMODE&3 FROM
//   $BE INT => TINT; DEFAD => TBYADDR;
//   ::MC TINT16=> TINT;
//   $EN
//   TINT16 => TINT => TBYADDR;
//   TINT32 => TINT => TBYADDR;
//   $BE TINT16 => TINT; TINT32 => TBYADDR $EN
// END
// TINT+ %40 => TLO => TVST;
// (IF CMODE & %30 = 0 THEN %0C ELSE
// (IF CMODE & %10 /= 0 THEN
// %0C ELSE %1C)) => TRE;
// $FO IPTR < LBUFFZ $DO
// EOS => LBUFF[IPTR] $OD
// EOS => SAVEDELIM;
// $EN
// @BOX 6.1
// END

// 
// ::END OF MODULE MSL031
// 
// *END
// @END









// @TITLE MSL03/1(1,10)
// [cac] T_2










// $PR ITEMISE(MODE);
    void ITEMISE (MODE) {
// $LO STEM,PTR,RPTR,NPTR,ISYM,SYM,
//     PPOS,BCOUNT,CSIZE,CTYPE,I,J,TY,SW;
    LOGICAL STEM, PTR, RPTR, NPTR, ISYM, SYM, 
            PPOS, BCOUNT, CSIZE, CTYPE,I, J, TY, SW;
// $LO32 INT; $LO64 L64;
    LOGICAL32 INT; LOGICAL64 L64;
// $RE64 FP,POWERS;
    REAL64 FP, POWERS;
// $LI/$IN TEN = 10, ZERO = 0;
    const INTEGER TEN = 10; const INTEGER ZERO = 0;
// #MSL03.1.1
// DATAVEC SWCODE ($LO8)
// 0[10]     ::CONTROL CHARS
// 17    ::END OF LINE
// 0 18
// 0[20] ::CONTROL CHARS
// 8 6 8 16 10 8 7 3          :: ! " # $ % & ' (
// 4 8 8 8 14 5 8             ::) * + , - . /
// 2 2 2 2 2 2 2 2 2 2        ::0 1 2 3 4 5 6 7 8 9
// 9 15 13 11 12 1 8          ::: ; < = > ? @
// 1 1 1 1 1 1 1 1 1 1 1 1 1  ::LETTERS
// 1 1 1 1 1 1 1 1 1 1 1 1 1
// 8 8 8 8 8 8                  ::[ \ ]  <- `
// 1[26] ::LC LETTERS
// 8 8 8 8 8                :: { | } ~ ERASE
// $EN
    static LOGICAL8 SWCODE[] = {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,          // 0[10]	::CONTROL CHARS
    17,                                    // 17    ::END OF LINE
    0, 18,                                 // 0 18
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,                         // 0[20] ::CONTROL CHARS
    8, 6, 8, 16, 10, 8, 7, 3,              // 8 6 8 16 10 8 7 3          :: ! " # $ % & ' (
    4, 8, 8, 8, 14, 5, 8,                  // 4 8 8 8 14 5 8             ::) * + , - . /
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,          // 2 2 2 2 2 2 2 2 2 2        ::0 1 2 3 4 5 6 7 8 9
    9, 15, 13, 11, 12, 1, 8,               // 9 15 13 11 12 1 8          ::: ; < = > ? @
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // 1 1 1 1 1 1 1 1 1 1 1 1 1  ::LETTERS
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // 1 1 1 1 1 1 1 1 1 1 1 1 1
    8, 8, 8, 8, 8, 8,                      // 8 8 8 8 8 8                  ::[ \ ]  <- `
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // 1[26] ::LC LETTERS
    8, 8, 8, 8, 8                          // 8 8 8 8 8                :: { | } ~ ERASE
    };
// DATAVEC STEMS($LO32)
// 0
// "BE"
// "PR"
// "MO"
// "EN"
// "IF"
// "TH"
// "EL"
// "FI"
// "WH"
// "FO"
// "DO"
// "OD"
// "SW"
// "AL"
// "SP"
// "EX"
// "PS"
// "LI"
// "DA"
// "TY"
// "SE"
// "AD"
// "IN"
// "LO"
// "RE"
// "VS"
// "IM"
// "LS"
// "BY"
// "MA"
// "PO"
// "PA"
// "SI"
// "LL"
// "LP"
// "LE"
// "X1"
// "BS"
// "WI"
// "LA"
// "OF"
// "IS"
// "AN"
// "OR"
// "FR"
// END
    static LOGICAL32 STEMS[] = {
       0, // 0
       'B' << 8 | 'E', // "BE"
       'P' << 8 | 'R', // "PR"
       'M' << 8 | 'O', // "MO"
       'E' << 8 | 'N', // "EN"
       'I' << 8 | 'F', // "IF"
       'T' << 8 | 'H', // "TH"
       'E' << 8 | 'L', // "EL"
       'F' << 8 | 'I', // "FI"
       'W' << 8 | 'H', // "WH"
       'F' << 8 | 'O', // "FO"
       'D' << 8 | 'O', // "DO"
       'O' << 8 | 'D', // "OD"
       'S' << 8 | 'W', // "SW"
       'A' << 8 | 'L', // "AL"
       'S' << 8 | 'P', // "SP"
       'E' << 8 | 'X', // "EX"
       'P' << 8 | 'S', // "PS"
       'L' << 8 | 'I', // "LI"
       'D' << 8 | 'A', // "DA"
       'T' << 8 | 'Y', // "TY"
       'S' << 8 | 'E', // "SE"
       'A' << 8 | 'D', // "AD"
       'I' << 8 | 'N', // "IN"
       'L' << 8 | 'O', // "LO"
       'R' << 8 | 'E', // "RE"
       'V' << 8 | 'S', // "VS"
       'I' << 8 | 'M', // "IM"
       'L' << 8 | 'S', // "LS"
       'B' << 8 | 'Y', // "BY"
       'M' << 8 | 'A', // "MA"
       'P' << 8 | 'O', // "PO"
       'P' << 8 | 'A', // "PA"
       'S' << 8 | 'I', // "SI"
       'L' << 8 | 'L', // "LL"
       'L' << 8 | 'P', // "LP"
       'L' << 8 | 'E', // "LE"
       'X' << 8 | '1', // "X1"
       'B' << 8 | 'S', // "BS"
       'W' << 8 | 'I', // "WI"
       'L' << 8 | 'A', // "LA"
       'O' << 8 | 'F', // "OF"
       'I' << 8 | 'S', // "IS"
       'A' << 8 | 'N', // "AN"
       'O' << 8 | 'R', // "OR"
       'F' << 8 | 'R'  // "FR"
    };
// DATAVEC REST($LO8)
// 0
// "GIN" 0
// "OC" 0
// "DULE" 0
// "D" 0
// "EN" 0
// "SE" 0
// "ILE" 0
// "R" 0
// "ITCH" 0
// "TERNATIVE" 0
// "ACE" 0
// "IT" 0
// "PEC" 0
// "TERAL" 0
// "TAVEC" 0
// "PE" 0
// "LECT" 0
// "DR" 0
// "TEGER" 0
// "GICAL" 0
// "AL" 0
// "TORE" 0
// "PORT" 0
// "PEC" 0
// "TE" 0
// "KE" 0
// "SN" 0
// "RT" 0
// "ZE" 0
// "ST" 0
// "ST" 0
// "NT" 0
// "XX" 0
// "XX" 0
// "XX" 0
// "BEL" 0
// "D" 0
// "OM" 0
// 'T 'H 'I 'N 0
// END
    static LOGICAL8 REST[] = {
    0,                     // 0
    'G', 'I', 'N', 0,      // BE "GIN" 0
    'O', 'C', 0,           // PR "OC" 0
    'D', 'U', 'L', 'E', 0, // MO "DULE" 0
    'D', 0,                // EN "D" 0
                           // IF
    'E', 'N', 0,           // TH "EN" 0
    'S', 'E', 0,           // EL "SE" 0
                           // FI
    'I', 'L', 'E', 0,      // WH "ILE" 0
    'R', 0,                // FO "R" 0
                           // DO
                           // OD
    'I', 'T', 'C', 'H', 0, // SW "ITCH" 0
    'T', 'E', 'R', 'N', 'A', 'T', 'I', 'V', 'E', 0, // AL "TERNATIVE" 0
    'A', 'C', 'E', 0,      // SP "ACE" 0
    'I', 'T', 0,           // EX "IT" 0
    'P', 'E', 'C', 0,      // PS "PEC" 0
    'T', 'E', 'R', 'A', 'L', 0, // LI "TERAL" 0
    'T', 'A', 'V', 'E', 'C', 0, // DA "TAVEC" 0
    'P', 'E', 0,           // TY "PE" 0
    'L', 'E', 'C', 'T', 0, // SE "LECT" 0
    'D', 'R', 0,           // AD "DR" 0
    'T', 'E', 'G', 'E', 'R', 0, // IN "TEGER" 0
    'G', 'I', 'C', 'A', 'L', 0, // "GICAL" 0
    'A', 'L', 0,           // RE "AL" 0
    'T', 'O','R', 'E', 0,  // VS "TORE" 0
    'P', 'O', 'R', 'T', 0, // IM "PORT" 0
    'P', 'E', 'C', 0,      // LS "PEC" 0
    'T', 'E', 0,           // BY "TE" 0
    'K', 'E', 0,           // MA "KE" 0
    'S', 'N', 0,           // PO "SN" 0
    'R', 'T', 0,           // PA "RT" 0
    'Z', 'E', 0,           // SI "ZE" 0
    'S', 'T', 0,           // LL "ST" 0
    'S', 'T', 0,           // LP "ST" 0
    'N', 'T', 0,           // LE "NT" 0
    'X', 'X', 0,           // X1 "XX" 0
    'X', 'X', 0,           // BS "XX" 0
    'X', 'X', 0,           // WI "XX" 0
    'B', 'E', 'L', 0,      // LA "BEL" 0
                           // OF
                           // IS
    'D', 0,                // AN "D" 0
                           // OR 
    'O', 'M', 0,           // FR "OM" 0
    'T', 'H', 'I', 'N', 0  // 'T 'H 'I 'N 0
// END
    };
// DATAVEC PTRS($LO8)
// 0
// 0
// 4
// 7
// 12
// 3
// 14
// 17
// 3
// 20
// 24
// 3
// 3
// 26
// 31
// 41
// 45
// 48
// 52
// 58
// 64
// 67
// 72
// 75
// 81
// 87
// 90
// 95
// 100
// 104
// 107
// 110
// 113
// 116
// 119
// 122
// 125
// 128
// 48
// 146
// 137
// 3
// 3
// 141
// 3
// 143
// END
    static LOGICAL8 PTRS[] = {
      0,   // 0
      0,   // 0
      4,   // 4
      7,   // 7
      12,  // 12
      3,   // 3
      14,  // 14
      17,  // 17
      3,   // 3
      20,  // 20
      24,  // 24
      3,   // 3
      3,   // 3
      26,  // 26
      31,  // 31
      41,  // 41
      45,  // 45
      48,  // 48
      52,  // 52
      58,  // 58
      64,  // 64
      67,  // 67
      72,  // 72
      75,  // 75
      81,  // 81
      87,  // 87
      90,  // 90
      95,  // 95
      100, // 100
      104, // 104
      197, // 107
      110, // 110
      113, // 113
      116, // 116
      119, // 119
      122, // 122
      125, // 125
      128, // 128
      48,  // 48
      146, // 146
      137, // 137
      3,   // 3
      3,   // 3
      141, // 141
      3,   // 3
      143  // 143
   };
// DATAVEC VALCODE ($LO8)
// 0[33]     ::CONTROL CHARS
// 59 0 86 0 0 58 0 79        :: ! " # $ % & ' (
// 80 54 52 76 53 0 55        ::) * + , - . /
// 126[10]                    ::0 1 2 3 4 5 6 7 8 9
// 75 0 49 51 48 127 84        :::; < = > ? @
// 127[13]                    ::LETTERS
// 127[13]
// 77 81 78 74 84 84          ::[ \ ] ^ <- .
// 127[13]                    ::LETTERS
// 127[13]
// 84 84 84 84 84             ::{ 1 } ~ ERASE
// $EN
    static LOGICAL8 VALCODE[] = {
    0, 0, 0, 0, 0, 0, 0, 0, 
    0, 0, 0, 0, 0, 0, 0, 0, 
    0, 0, 0, 0, 0, 0, 0, 0, 
    0, 0, 0, 0, 0, 0, 0, 0, 
    0,                                        // 0[33]	::CONTROL CHARS
    59, 0, 86, 0, 0, 58, 0, 79,               // 59 0 86 0 0 58 0 79        :: ! " # $ % & ' (
    80, 54, 52, 76, 53, 0, 55,                // 80 54 52 76 53 0 55        ::) * + , - . /
    126, 126, 126, 126, 126, 126, 126, 126, 
    126, 126,                                 // 126[10]                    ::0 1 2 3 4 5 6 7 8 9
    75, 0, 49, 51, 48, 127, 84,               // 75 0 49 51 48 127 84        :::; < = > ? @
    127, 127, 127, 127, 127, 127, 127, 127,
    127, 127, 127, 127, 127,                 // 127[13]                    ::LETTERS
    127, 127, 127, 127, 127, 127, 127, 127,
    127, 127, 127, 127, 127,                 // 127[13]
    77, 81, 78, 74, 84, 84, // 77 81 78 74 84 84          ::[ \ ] ^ <- .
    127, 127, 127, 127, 127, 127, 127, 127,
    127, 127, 127, 127, 127,                 // 127[13]                    ::LETTERS
    127, 127, 127, 127, 127, 127, 127, 127,
    127, 127, 127, 127, 127,                 // 127[13]
    84, 84, 84, 84, 84 // 84 84 84 84 84             ::{ 1 } ~ ERASE
    };
// INITIALIZE LBUFF AND BCOUNT
// BX3:
    BX3: ;
// 0 => CFWDPTR; CLISTZ => CBWDPTR;
    CFWDPTR = 0;
    CBWDPTR = CLISTZ;
// 0 => IPTR => PTR => BCOUNT => SPTR  => TY;
    TY = SPTR = BCOUNT = PTR = IPTR = 0;
// INSERT DELIMITER SAVED FROM
// PREVIOUS ITEMISED LINE INTO
// NEW ITEMISED LINE
// IF FINISHER,END THE LINE
// $IF SAVEDELIM /= EOS $TH
    if (ITYPE_ne (SAVEDELIM, EOS)) {
// SAVEDELIM => LBUFF[1=>IPTR];
        LBUFF[IPTR = 1] = SAVEDELIM;
// EOS => SAVEDELIM
        SAVEDELIM = EOS;
// $IF DLIST[IND OF LBUFF[1]] & 64 /= 0, -> SCOLON
        if ((LBUFF[1].IND & 64) != 0) goto SCOLON;
// $FI
    }
// IF MODE IS = 0 // IGNORE SP NL
// (CALL PPC.SEQ // IF **ENCOUNTERED)
// B4:
    B4: ;
// $WH INCH() => ISYM = '; $AN MODE=0 $OR ISYM = '$L
// $OR ISYM = '  $OR ISYM = %9 $DO  $OD
    while (((ISYM = INCH ()) == ';' && MODE == 0) || ISYM == '\n'
           || ISYM == ' ' || ISYM == 0x9) { ; }
// $IF INCH() = '* $AN ISYM = '* $TH
    if (INCH () == '*' && ISYM == '*') {
// SELECT.OUTPUT(MSTR);
        SELECT_OUTPUT (MSTR);
// PPC.CMD(); -> B4;
        PPC_CMD (); goto B4;
// ELSE INBACKSPACE(1)
    } else { INBACKSPACE (1);
// $FI
    }
// INBACKSPACE(1);
    INBACKSPACE (1);
// SET LINE.NO
// 1 +> LINE.NO;
    LINE_NO ++;
// TL.LINE(LINE.NO);
    TL_LINE (LINE_NO);
// $IF CWORD & 2 = 2 $TH
    if ((CWORD & 2) == 2) {
// SELECTOUTPUT(MSTR);
        SELECTOUTPUT (MSTR);
// NEWLINES(1);OUTLINENO(LINE.NO);
        NEWLINES(1); OUTLINENO (LINE_NO);
// OUTCH(9)
        OUTCH (9);
// $FI
    }
// LB00013:
LB00013: ;
// @BOX 5.1
// SWITCH ON CH
// IG: $IF SBUFF[SPTR]&%80 = 0 $TH
    IG: if ((SBUFF[SPTR] & 0x80) == 0) {
// 128 + IPTR => SBUFF[1+>SPTR] $FI
        SBUFF[++SPTR] = 128 + IPTR; }
// XSYM() => ISYM;
    ISYM = XSYM ();
// VALCODE[ISYM] => SYM;
    SYM = VALCODE[ISYM];
// SWITCH SWCODE[ISYM]=>SW
// IG,ALPHA,DD,LBR,RBR,DOT,DQ,SQ,DELIM,COLON,PCENT,
// COMP,COMP,COMP,MINUS,SCOLON,DOLLAR,NLINE,NPAGE;
    switch (SW = SWCODE[ISYM]) {
        case 0: goto IG;
        case 1: goto ALPHA;
        case 2: goto DD;
        case 3: goto LBR;
        case 4: goto RBR;
        case 5: goto DOT;
        case 6: goto DQ;
        case 7: goto SQ;
        case 8: goto DELIM;
        case 9: goto COLON;
        case 10: goto PCENT;
        case 11: goto COMP;
        case 12: goto COMP;
        case 13: goto COMP;
        case 14: goto MINUS;
        case 15: goto SCOLON;
        case 16: goto DOLLAR;
        case 17: goto NLINE;
        case 18: goto NPAGE;
    }
// @BOX 6.1
// PROCESS CONSTANTS
// SQ:
    SQ: ;
// YSYM() & 127 => INT;
    INT = YSYM () & 127;
// 2 => CTYPE;
    CTYPE = 2;
// LB00093:
    LB00093: ;
// INT => L64;
    L64 = INT;
// LB00083:
    LB00083: ;
// 0 => CSIZE
    CSIZE = 0;
// $WH L64 /= 0 OR [CTYPE = 0 AND CSIZE < 8] $DO
    while (L64 != 0 || (CTYPE == 0 && CSIZE < 8)) {
// L64 & %FF => CLIST[1->CBWDPTR];
      CLIST[-- CBWDPTR] = L64 & 0xff;
// L64 ->> 8 => L64;
      L64 = L64 >> 8;
// 1 +> CSIZE;
      ++ CSIZE;
// $OD
    }
// $IF CSIZE = 0 $TH
    if (CSIZE == 0) {
// 0 => CLIST[1->CBWDPTR];
      CLIST[--CBWDPTR] = 0;
// 1 => CSIZE $FI
      CSIZE = 1; }
// 3 => TAG OF LBUFF[1+>IPTR];
    LBUFF[++ IPTR].TAG = 3;
// CTYPE <<- 4 + CSIZE - 1<<- 2 => ST OF LBUFF[IPTR];
    LBUFF[IPTR].ST = (((CTYPE << 4) + CSIZE) - 1) << 2;
// CBWDPTR => IND OF LBUFF[IPTR];
    LBUFF[IPTR].IND = CBWDPTR;
//  -> LB00085;
    goto LB00085;

// DQ:
    DQ: ;
// $IF MODE = 2,-> LB00079;
    if (MODE == 2) goto LB00079;
// 0 => L64;
    L64 = 0;
// $WH YSYM() => ISYM /= '$" $DO
    while ((ISYM = YSYM ()) != '"') {
// $IF ISYM = NL $TH MONITOR(14) $FI
      if (ISYM == NL) { MONITOR (14); }
// L64 <<- 8 ! (ISYM&127) => L64 $OD
      L64 = ((L64 << 8) | (ISYM & 127)); }
// 2 => CTYPE;
    CTYPE = 2;
 // -> LB00083;
    goto LB00083;

// PCENT:
    PCENT: ;
// $IF NEXTCH()=>ISYM = '"
// $AN XSYM() = ISYM,-> LB00079;
    if ((ISYM = NEXTCH ()) == '"'
        && XSYM () == ISYM) goto LB00079;
// XHEX() => L64;
    L64 = XHEX ();
// 2 => CTYPE;
    CTYPE = 2;
 // -> LB00083;
    goto LB00083;

#if 0 // flip fix
// DD:
    DD: ;
// XINT(ISYM - '0) => INT;
    INT = XINT(ISYM - '0');
// $IF NEXTCH() => ISYM = '@ OR
// [ISYM = '. $AN XSYM() = ISYM],-> LB00088;
// [rj] XXX
/* another condition inversion? */
    if ((ISYM = NEXTCH ()) == '@' ||
        (ISYM == '.' && XSYM () == ISYM)) goto LB00088;
// LB00092:
    LB00092: ;
#else
// DD:
    DD:
// XINT(ISYM - '0) => INT;
    INT = XINT(ISYM - '0');
// $IF NEXTCH() => ISYM = '@ OR
// [ISYM = '. $AN XSYM() = ISYM],-> LB00092;
    if ((ISYM = NEXTCH ()) == '@' ||
        (ISYM == '.' && XSYM () == ISYM)) goto LB00092;
// $IF LBUFF[IPTR] = UMINUS $TH
    if (ITYPE_eq (LBUFF[IPTR], UMINUS)) {
// 1->IPTR;1=>CTYPE;
      --IPTR; CTYPE = 1;
// $IF INT<128 $TH 256-INT => INT
      if (INT < 128) { INT = 256 - INT; 
// $EL $IF INT=<%7FFF $TH %10000-INT=>INT
      } else { if (INT <= 0x7fff) { INT = 0x10000 - INT; 
// $EL 0-INT => INT $FI $FI
      } else { INT = 0 - INT; } }
// $EL 2 => CTYPE $FI
      } else { CTYPE = 2; }
// -> LB00093;
    goto LB00093;
// 
// DOT:
    DOT: ;
// 0 => INT;
    INT = 0;
// LB00092:
    LB00092: ;
#endif

// (INT) => FP;
    FP = INT;
// 0 => PPOS
    PPOS = 0;
// $IF ISYM ='. $TH
    if (ISYM == '.') {
// $WH VALCODE[NEXTCH()=>ISYM] = 126 $DO
    while (VALCODE[ISYM = NEXTCH ()] == 126) {
// FP * (TEN) + (XSYM() - '0) => FP;
    FP = ((FP * TEN) + (XSYM() - '0'));
// 1 -> PPOS $OD
    -- PPOS; }
// $FI
    }
// $IF ISYM ='@ $TH XSYM();
    if (ISYM == '@') { XSYM ();
// (IF XSYM()=>ISYM = '- $TH 0 -XINT(0)
// $EL XINT(ISYM - '0))+>PPOS
      PPOS += ((ISYM=XSYM()) == '-' ? (0 - XINT(0))
      : XINT (ISYM - '0'));
// FI
    }
// TEN => POWERS;
    POWERS = 10;
// IF PPOS < 0 THEN 1 => J; 0 -:> PPOS;
    if (PPOS < 0) { J = 1; PPOS = 0 - PPOS; 
// ELSE 0 => J FI
    } else { J = 0; }
// $WH PPOS /= 0 DO
    while (PPOS != 0) {
// IF PPOS & 1 /= 0 THEN
    if ((PPOS & 1) != 0) {
// IF J = 0 THEN
      if (J == 0) {
// POWERS *> FP;
        FP *= POWERS;
// ELSE POWERS /> FP;
     } else { FP /= POWERS; 
// FI FI
    } }
// PPOS ->> 1 => PPOS;
    PPOS = PPOS >> 1;
// POWERS *> POWERS;
    POWERS *= POWERS;
// $OD
    }
// IF LBUFF[IPTR] = UMINUS $TH
    if (ITYPE_eq (LBUFF[IPTR], UMINUS)) {
// ZERO -:>FP;
    FP = ZERO - FP;
// 1->IPTR
    --IPTR;
// FI
    }
// FP => L64;
    L64 = FP;
// 0 => CTYPE;
    CTYPE = 0;
//  -> LB00083;
    goto LB00083;

#if 0 // flip fix
// DOT:
    DOT: ;
// 0 => INT;
    INT = 0;
//  -> LB00092;
    goto LB00092;

// LB00088:
    LB00088: ;

// $IF LBUFF[IPTR] = UMINUS $TH
    if (ITYPE_eq (LBUFF[IPTR], UMINUS)) {
// 1->IPTR;1=>CTYPE;
      --IPTR; CTYPE = 1;
// $IF INT<128 $TH 256-INT => INT
      if (INT < 128) { INT = 256 - INT; 
// $EL $IF INT=<%7FFF $TH %10000-INT=>INT
      } else { if (INT <= 0x7fff) { INT = 0x10000 - INT; 
// $EL 0-INT => INT $FI $FI
      } else { INT = 0 - INT; } }
// $EL 2 => CTYPE $FI
      } else { CTYPE = 2; }
//  -> LB00093;
    goto LB00093;
#endif

//LB00079:
    LB00079: ;

// 2 => TAG OF LBUFF[1+>IPTR];
    LBUFF[++ IPTR].TAG = 2;
// CFWDPTR + 1 => IND OF LBUFF[IPTR];
    LBUFF[IPTR].IND = CFWDPTR + 1;
// $WH YSYM() => ISYM /= '$" $DO
    while ((ISYM = YSYM ()) != '"') {
// $IF ISYM = NL $TH MONITOR(142) $FI
      if (ISYM == NL) { MONITOR (142); }
// ISYM&127 => CLIST[1+>CFWDPTR] $OD
    CLIST[++ CFWDPTR] = ISYM & 127; }
// 0 => CLIST[1+>CFWDPTR];
    CLIST[++ CFWDPTR] = 0;
//  -> LB00085;
    goto LB00085;

// FN:
    FN: ;
// FINDN(PART(^CHLIST,
// LASTCH+2,PTR-1),0)=>INT;
    INT = FINDN (PART (& CHLIST, LASTCH + 2, PTR - 1), 0);
// 2 => CTYPE;
    CTYPE = 2;
//  -> LB00093;
    goto LB00093;

// LB00085:
    LB00085: ;

//  -> LB00013;
    goto LB00013;

// :: PROCESS NAMES
// :: AND DELIMITERS
ALPHA:
// ISYM => CHLIST[LASTCH+1=>PTR];
    CHLIST[PTR = (LASTCH + 1)] = ISYM;
// NEXTCH() => ISYM;
    ISYM = NEXTCH ();
// $WH VALCODE[ISYM] >= 126 $OR ISYM = %2E $DO
    while (VALCODE [ISYM] >= 126 || ISYM == 0x2e) {
// $IF XSYM() /= %2E $TH
      if (XSYM () != 0x2e) {
// ISYM => CHLIST[1+>PTR] $FI
        CHLIST[++ PTR] = ISYM; }
// NEXTCH() => ISYM;
      ISYM = NEXTCH ();
// $OD
    }
// 0 => CHLIST[1+>PTR];
    CHLIST [++ PTR] = 0;
// $IF CHLIST[LASTCH+1] = '? ,->FN;
    if (CHLIST[LASTCH + 1] == '?') goto FN;
// CHLIST[LASTCH+1] <<- 8 + CHLIST[LASTCH+2] => STEM;
    STEM = (CHLIST[LASTCH + 1] << 8) + CHLIST[LASTCH + 2];
// 0 => PTR => INT
    INT = PTR = 0;
// $WH 1 +> PTR =< MAX.KEYWORD $DO
    while ((++ PTR) <= MAX_KEYWORD) {
// $IF STEMS[PTR] = STEM $TH
      if (STEMS[PTR] == STEM) {
// PTRS[PTR] => RPTR;
        RPTR = PTRS[PTR];
// LASTCH+2 => NPTR;
        NPTR = LASTCH + 2;
// $WH REST[1+>RPTR] = CHLIST[1+>NPTR]
// $AN REST[RPTR] /= 0 $DO $OD
        while (REST[++ RPTR] == CHLIST[++ NPTR]
               && REST[RPTR] != 0) { ; };
// $IF REST[RPTR]=0 $AN DLIST[PTR] & %200 /= 0 $TH
        if (REST[RPTR] == 0 && (DLIST[PTR] & 0x200) != 0) {
// 1 -> NPTR
            --NPTR;
// $WH VALCODE[CHLIST[1+>NPTR]] = 126 $DO
            while (VALCODE[CHLIST[++ NPTR]] == 126) {
// INT *10 + CHLIST[NPTR] - '0 => INT $OD
                INT = INT * 10 + CHLIST[NPTR] - '0'; }
// XBTYPE(INT,PTR) => TY $FI
            TY = XBTYPE (INT, PTR); }
// -> B3 $FI $OD
        goto B3; } }
#if 0
// B3:
    B3: ;
// $IF PTR =< MAX.KEYWORD $AN
// CHLIST[NPTR] = 0 $AN REST[RPTR] = 0,-> LB00046;
// [rj] XXX
#if 0
    if (PTR <= MAX_KEYWORD &&
        CHLIST[NPTR] == 0 && REST[RPTR] == 0) goto  LB00046;
#else
/**/ /* The condition in the following line is inverted with respect to how it should be translated, not sure why */
     if (PTR > MAX_KEYWORD || CHLIST[NPTR] != 0 || REST[RPTR] != 0) goto LB00046;
#endif
#else
// B3:
    B3: ;
// $IF PTR =< MAX.KEYWORD $AN
// CHLIST[NPTR] = 0 $AN REST[RPTR] = 0,-> LB00047;
    if (PTR <= MAX_KEYWORD &&
    CHLIST[NPTR] == 0 && REST[RPTR] == 0) goto LB00047;
// 1 => TAG OF LBUFF[1+>IPTR];
    LBUFF[++IPTR].TAG = 1;
// 0 => ST OF LBUFF[IPTR] => TY;
    TY = LBUFF[IPTR].ST = 0;
// ADDN() => IND OF LBUFF[IPTR];
    LBUFF[IPTR].IND = ADDN ();
//  -> LB00062;
    goto LB00062;
#endif

// LB00047:
    LB00047: ;

// PTR => IND OF LBUFF[1+>IPTR];
    LBUFF[++ IPTR].IND = PTR;
// TY => ST OF LBUFF[IPTR];
    LBUFF[IPTR].ST = TY;
// 0 => TY;
    TY = 0;
// TDELIM => TAG OF LBUFF[IPTR];
    LBUFF[IPTR].TAG = TDELIM;
// $IF BCOUNT /= 0 $AN DLIST[PTR] & %20 /= 0,-> LB00062;
    if (BCOUNT != 0 && (DLIST[PTR] & 0x20) != 0) goto LB00062;
// $IF DLIST[PTR] & %80 /= 0,-> LB00051;
    if ((DLIST[PTR] & 0x80) != 0) goto LB00051;
// LB00050:
    LB00050: ;

// $IF DLIST[PTR] & %40 = 0,-> LB00062;
    if ((DLIST[PTR] & 0x40) == 0) goto LB00062;
// LB00053:
    LB00053: ;

// -> SCOLON;
    goto SCOLON;
// LB00051:
    LB00051: ;

// IF [IPTR = 1 AND MODE=0]
// OR LBUFF[1] = ASTERISK OR LBUFF[1] = DIMPORT,-> LB00050;
    if ((IPTR == 1 && MODE == 0)
        || ITYPE_eq (LBUFF[1], ASTERISK) || ITYPE_eq (LBUFF[1], DIMPORT)) goto LB00050;
// LBUFF[IPTR] => SAVEDELIM;
    SAVEDELIM = LBUFF[IPTR];
// 1 -> IPTR;
    --IPTR;
//  -> LB00053;
    goto LB00053;

#if 0 /// flip fix
// LB00046:
    LB00046: ;

// 1 => TAG OF LBUFF[1+>IPTR];
    LBUFF[++ IPTR].TAG = 1;
// 0 => ST OF LBUFF[IPTR] => TY;
    TY = LBUFF[IPTR].ST = 0;
// ADDN() => IND OF LBUFF[IPTR];
    LBUFF[IPTR].IND = ADDN ();
//  -> LB00062;
    goto LB00062;
#endif

// DOLLAR:
    DOLLAR: ;
// XSYM() <<- 8 + XSYM() => STEM;
    STEM = (XSYM () << 8) + XSYM ();
// 0 => PTR
    PTR = 0;
// $WH 1 +> PTR =< MAX.KEYWORD $DO
    while (++ PTR <= MAX_KEYWORD) {
// $IF STEMS[PTR] = STEM $TH
      if (STEMS[PTR] == STEM) {
// $IF DLIST[PTR] & %200 /= 0 $TH
        if ((DLIST[PTR] & 0x200) != 0) {
// XBTYPE(XINT(0),PTR) => TY $FI
          TY = XBTYPE (XINT (0), PTR); }
// -> B17 $FI $OD
          goto B17; } }

#if 0 // flip fix
B17:
// $IF PTR =< MAX.KEYWORD,-> LB00058;
// [rj] XXX
#if 0
    if (PTR <= MAX_KEYWORD) goto LB00058;
#else
    if (PTR > MAX_KEYWORD) goto LB00058;
#endif
//  -> LB00047;
    goto LB00047;

// LB00058:
    LB00058: ;
#else
// B17:
    B17: ;
// $IF PTR =< MAX.KEYWORD,-> LB00047;
    if (PTR <= MAX_KEYWORD) goto LB00047;
#endif

// MONITOR(0);
    MONITOR (0);
// :: MULTI-CHARACTER DELIMITERS
// $LI/$LO8
// GE = 46,
    const LOGICAL8 GE = 46;
// LE = 47,
    const LOGICAL8 LE = 47;
// GT = 48,
    const LOGICAL8 GT = 48;
//  LT = 49,
    const LOGICAL8 LT = 49;
// NE = 50,
    const LOGICAL8 NE = 50;
// EQ = 51,
    const LOGICAL8 EQ = 51;
// SUBTRACT = 53,
    const LOGICAL8 SUBTRACT = 53;
// DIVIDE = 55,
    const LOGICAL8 DIVIDE = 55;
// REVERSE.SUBTRACT = 56,
    const LOGICAL8 REVERSE_SUBTRACT = 56;
// REVERSE.DIVIDE = 57,
    const LOGICAL8 REVERSE_DIVIDE = 57;
// NEQ = 60,
    const LOGICAL8 NEQ = 60;
// MINUS.STORE = 63,
    const LOGICAL8 MINUS_STORE = 63;
// LT.LT = 71,
    const LOGICAL8 LT_LT = 71;
// RIGHT.SHIFT = 72,
    const LOGICAL8 RIGHT_SHIFT = 72;
// LEFT.SHIFT = 73,
    const LOGICAL8 LEFT_SHIFT = 73;
// RESTART.LABEL = 82,
    const LOGICAL8 RESTART_LABEL = 82;
// UNARY.MINUS = 85;
    const LOGICAL8 UNARY_MINUS = 85;
// COLON:
    COLON: ;
// IF NEXTCH()=': THEN
    if (NEXTCH () == ':') {
// WHILE XSYM()/='$L DO OD
      while (XSYM () != '\n') {;}
// IF IPTR=0, ->BX3
      if (IPTR == 0) goto BX3;
// IF MODE/=0, ->NLINE;
      if (MODE != 0) goto NLINE;
// ->SCOLON FI
      goto SCOLON; }
// 11=>SW;
    SW = 11;
// COMP:
    COMP: ;
// IF TAG OF LBUFF[IPTR]/=0, ->DELIM;
    if (LBUFF[IPTR].TAG != 0) goto DELIM;
// IND OF LBUFF[IPTR]=>I;
    I = LBUFF[IPTR].IND;
// SWITCH SW-11
// EQ0,GT0,LT0;
    switch (SW - 11) {
    case 0: goto EQ0;
    case 1: goto GT0;
    case 2: goto LT0;
    }
// EQ0: ;
    EQ0: ;
// DATAVEC PREVIOUS($LO8)
// SUBTRACT
// DIVIDE
// GT
// END;
    LOGICAL8 PREVIOUS[] = { SUBTRACT, DIVIDE, GT };
// DATAVEC EQ.ETC($LO8)
// NEQ
// NE
// GE
// END;
    LOGICAL8 EQ_ETC[] = { NEQ, NE, GE };
// DATAVEC COLON.ETC($LO8)
// REVERSE.SUBTRACT
// REVERSE.DIVIDE
// RESTART.LABEL
// END;
    LOGICAL8 COLON_ETC[] = { REVERSE_SUBTRACT, REVERSE_DIVIDE, RESTART_LABEL };
// -1=>J;
    J = -1;
// WHILE 1+>J<3 AND PREVIOUS[J]/=I
    while ((++ J) < 3 && PREVIOUS [J] != I)
// DO OD
      { ; }
// IF J=3,->DELIM;
    if (J == 3) goto DELIM;
// (IF SYM=EQ THEN EQ.ETC[J]
// ELSE COLON.ETC[J])=>SYM;
    SYM = (SYM == EQ) ? EQ_ETC[J] : COLON_ETC[J];
//  -> LB00073;
    goto LB00073;

// GT0:
    GT0: ;
// IF I>=EQ=<NEQ THEN
    if (I >= EQ && I <= NEQ) {
// I+10=>SYM ELSE
      SYM = I + 10; } else {
// IF I=MINUS.STORE THEN
      if (I == MINUS_STORE) {
// RIGHT.SHIFT=>SYM ELSE
        SYM = RIGHT_SHIFT; } else {
// ->DELIM
       goto DELIM;
// FI FI
       } }
//  -> LB00073;
    goto LB00073;

// LT0:
    LT0: ;
// IF I=LT THEN
    if (I == LT) {
// LT.LT=>SYM ELSE
      SYM = LT_LT; } else {
// IF I=EQ THEN
      if (I == EQ) {
// LE=>SYM ELSE
        SYM = LE; } else {
// ->DELIM
        goto DELIM;
// FI FI
     } }
//  -> LB00073;
    goto LB00073;

// MINUS:
    MINUS: ;
// IF LBUFF[IPTR]=LSHIFT THEN
    if (ITYPE_eq (LBUFF[IPTR], LSHIFT)) {
// LEFT.SHIFT=>SYM;->MULTI
      SYM = LEFT_SHIFT; goto MULTI;
// ELSE
    } else {
// IF TAG OF LBUFF[IPTR]=0
// AND DLIST[IND OF LBUFF[IPTR]]&%18/=0
// OR MODE=1 THEN
    if (((LBUFF[IPTR].TAG == 0) 
        && ((DLIST[LBUFF[IPTR].IND] & 0x18) != 0))
        || MODE == 1) {
// UNARY.MINUS=>SYM FI FI
      SYM = UNARY_MINUS; } }
// ->DELIM;
    goto DELIM;

// LB00073:
    LB00073: ;

// MULTI:
    MULTI: ;
// 1->IPTR;
    --IPTR;
// SBUFF[SPTR]=>SBUFF[1->SPTR];
    SBUFF[SPTR-1] = SBUFF[SPTR]; -- SPTR;
// DELIM:
    DELIM: ;
// SYM=>PTR
    PTR = SYM;
 // -> LB00047;
    goto LB00047;

// LB00062:
    LB00062: ;

//  -> LB00013;
    goto LB00013;



// LBR:
    LBR: ;
// @BOX 9.1
// 1 +> BCOUNT;
    BCOUNT ++;

// LB00020:
    LB00020: ;

// ->DELIM;
    goto DELIM;
// RBR:
    RBR: ;
// 1 -> BCOUNT;
    --BCOUNT;
//  -> LB00020;
    goto LB00020;


// @BOX13.1

#if 0 // flip fix
// NLINE:
    NLINE: ;
// IS MODE LINE BY LINE OR STATEMENT $LI $DA $MO $TY
// $IF MODE =0 AND [TAG OF LBUFF[1] /= 0
// $OR DLIST[IND OF LBUFF[1]] & %2000=0],-> LB00026;
    if ((MODE == 0 && (LBUFF[1].TAG != 0))
        || (DLIST[LBUFF[1].TAG] & 0x2000) == 0) goto LB00026;
//  -> LB00013;
      goto LB00013;

// @BOX15.1
// ;

// LB00023:
    LB00023: ;
#else
// IS MODE LINE BY LINE OR STATEMENT $LI $DA $MO $TY
NLINE:
// $IF MODE =0 AND [TAG OF LBUFF[1] /= 0
// $OR DLIST[IND OF LBUFF[1]] & %2000=0],-> LB00013;
    if ((MODE == 0 && (LBUFF[1].TAG != 0))
        || (DLIST[LBUFF[1].TAG] & 0x2000) == 0) goto LB00013;
// EOL => LBUFF[1+>IPTR];
    LBUFF[++ IPTR] = EOL;
#endif

// SCOL:
    SCOL: ;
// SCOLON:
    SCOLON: ;
// @BOX16.1
// STORE EOS ITEM
// PRINT IBUFF IF CWORD SET
// SET IPTR TO 0

// EOS => LBUFF[1+IPTR];
    LBUFF[1 + IPTR] = EOS;

// 0 => IPTR
    IPTR = 0;

// $IF CWORD & 1 /= 0 $TH
    if ((CWORD & 1) != 0) {
// PRINT.ITEMLINE() $FI
      PRINT_ITEMLINE (); }
// -> LB00025;
    goto LB00025;


#if 0 // flip fix
// LB00026:
    LB00026: ;

// @BOX17.1
// END.


// @BOX 18.1
// STORE EOL ITEM

// EOL => LBUFF[1+>IPTR];
    LBUFF[++ IPTR] = EOL;
// -> LB00023;
    goto LB00023;
#endif



// @BOX 21.1
// NEWPAGE

// NPAGE:
    NPAGE: ;
// INC PAGE NO AND RESET LINE NO
// LINE.NO & %FFFF0000 + %10000 => LINE.NO;
    LINE_NO = (LINE_NO & 0xFFFF0000) + 0x10000;
// -> LB00013;
    goto  LB00013;

// LB00025:
    LB00025: ;
// $EN
  } // ITEMISE





// $PS INITS3();
void INITS3 (void);
// $PR INITS3;
void INITS3 (void) {
// ::PDP $LI/$LO8 INT = %44, DEFAD = %44;
#ifdef PDP
    const LOGICAL8 INT = 0x44;
    const LOGICAL8 DEFAD = 0x44;
#endif
// ::MU6 $LI/$LO8 INT = %4C, DEFAD = %4C;
#if defined (MU6) || defined (VAX)
    const LOGICAL8 INT = 0x4C;
    const LOGICAL8 DEFAD = 0x4C;
#endif // MU6 || VAX
// $AL CMODE&3 FROM
    switch (CMODE & 3) {
//   $BE INT => TINT; DEFAD => TBYADDR;
      case 0: TINT = INT; TBYADDR = DEFAD; 
//   ::MC TINT16=> TINT;
#ifdef MC
              TINT = TINT16;
#endif
//   $EN
              break;
//   TINT16 => TINT => TBYADDR;
      case 1: TBYADDR = TINT = TINT16; break;
//   TINT32 => TINT => TBYADDR;
      case 2: TBYADDR = TINT = TINT32; break;
//   $BE TINT16 => TINT; TINT32 => TBYADDR $EN
      case 3: TINT = TINT16; TBYADDR = TINT32; break;
// END
    }
// TINT+ %40 => TLO => TVST;
    TVST = TLO = TINT+ 0x40;
// (IF CMODE & %30 = 0 THEN %0C ELSE
// (IF CMODE & %10 /= 0 THEN
// %0C ELSE %1C)) => TRE;
    TRE = (((CMODE & 0x30) == 0) ? 0x0c :
           (((CMODE & 0x10) != 0) ?
           0x0C : 0x1ci)); 
// $FO IPTR < LBUFFZ $DO
// EOS => LBUFF[IPTR] $OD
    { INTEGER limit = LBUFFZ;
    for (IPTR = 0; IPTR < limit; IPTR ++)
      LBUFF[IPTR] = EOS; }
// EOS => SAVEDELIM;
    SAVEDELIM = EOS;
// $EN
    }
// ::END OF MODULE MSL031
// *END
