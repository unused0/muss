#include "muss.h"
#include "kernel.h"
#include "lib.h"
#include "sys.h"
#include "nmsl.h"

// $LS LIB(ADDR[$LO8],$IN);
// $LS TL($IN,ADDR[$LO8],$IN);
// $LS TL.MODULE();
// $LS TLSEG($IN,ADDR,$LO32,ADDR,$IN);
// $LS TLLOAD($IN,$IN);
// $LS TL.MODE($IN,$IN);
// $LS TL.PRINT($IN);
// $LS TL.CODE.AREA($IN);
// $LS TL.DATA.AREA($IN);
// $LS TL.END.MODULE($IN);
// $LS TL.END();
// $LS TL.INSERT($IN);
// $PS COMP.TYPE()/$IN;
// $PS ADDN()/$LO16;
// $PS GENN($LO16)/ADDR[$LO8];
// $PS PRINT.STATS();
// $IN CONPTR;
// $IN IPTR;
// $PS END.CHECKS();
// $IM $LI CONSTKZ;
// $IN [CONSTKZ] CONSTK;
// $IN IX,DIRYZ;
// $IM $LI LBUFFZ;
// $TY ITYPE $IS $LO8 TAG,ST $LO16 IND;
// ITYPE[LBUFFZ] LBUFF;
// $IM $LI CLISTZ;
// $LO8[CLISTZ] CLIST;
// $PS EVAL.CONST()/$IN32;
// $PS MONITOR($IN);
// $IN FAULTS;
// ADDR[$LO8] RES.FILE;
// $IN TINT,TBYADDR,TVST,TLO,TRE;
// $IM $LI TADPROC;
// $LA CEXIT;
// $LI/ADDR[$LO8] NIL=;
// MODULE(CMODE,TRANS.DIRECTIVE,CWORD,CUR.GLB,INIT.S4);
// *GLOBAL 5;
    // GLOBAL 5; // XXX
// $LI/ITYPE DEND = 0\0\%4,HASH = 0\0\%56,EOS=0\0\%0;
// ::PDP *CODE 2;
#ifdef PDP
    // *CODE 2; // XXX
#endif
// *GLOBAL 1;
    // GLOBAL 1; // XXX
// $LI NO.DNAMES = 11;
#define NO_DNAMES 11
// $DA DNAMES($LO8)
// "INFORM"
// 0
// "CODE"
// 0
// "GLOBAL"
// 0
// "INIT"
// 0
// "CMAP"
// 0
// "STOPD"
// 0
// "STOPC"
// 0
// "TLSEG"
// 0
// "TLLOAD"
// 0
// "TLMODE"
// 0
// "VTYPE"
// 0
// $EN
    LOGICAL8 DNAMES[] = {
        'I', 'N', 'F', 'O', 'R', 'M', 0,
        'C', 'O', 'D', 'E', 0,
        'G', 'L', 'O', 'B', 'A', 'L', 0,
        'I', 'N', 'I', 'T', 0,
        'C', 'M', 'A', 'P', 0,
        'S', 'T', 'O', 'P', 'D', 0,
        'S', 'T', 'O', 'P', 'C', 0,
        'T', 'L', 'S', 'E', 'G', 0,
        'T', 'L', 'L', 'O', 'A', 'D', 0,
        'T', 'L', 'M', 'O', 'D', 'E', 0,
        'V', 'T', 'Y', 'P', 'E', 0
    };

// *GLOBAL 5;
  // GLOBAL 5; // XXX

// $IN CWORD,CMODE,CURGLB;
INTEGER CWORD, CMODE, CURGLB;
// $PS TRANS.DIRECTIVE();
extern void TRANS_DIRECTIVE (void);
// ::PDP *CODE 2;
#ifdef PDP
  // *CODE 2; // XXX
#endif

// [cac] It looks like INIT is a nested procedure ('subproc'); moved out of TRANS.DIRECTIVE to here

// $PS INIT($IN);
static void INIT (INTEGER);
// $PR INIT(I);
static void INIT (INTEGER I) {
// IF I & %4000 /= 0 THEN
   if ((I & 0x4000) != 0) {
// TL(I->>8,RESFILE,DIRYZ) FI
    TL (I >> 8, RESFILE, DIRYZ); }
// END
  }

// $PR TRANS.DIRECTIVE;
void TRANS_DIRECTIVE (void) {
// $IN I,J,K,C;
    INTEGER I, J, K, C;
// $AD[$LO8] NAME,LIBN;
    ADDR_LOGICAL8 NAME, LIBN;
// ::SUBPROCS OF
// ::TRANS DIRECTIVE
// ::END 4.1.1
// $IF TAG OF LBUFF[1+>IPTR] = 0,-> LB00015;
    if (LBUFF[++ IPTR].TAG == 0) goto LB00015;
// $IF TAG OF LBUFF[IPTR] /= 1,-> LB00019;
    if (LBUFF[IPTR].TAG != 1) goto LB00019;
// %8000 &> IX;
    IX &= 0x8000;
// GENN(IND OF LBUFF[IPTR]) => NAME;
    NAME = GENN (LBUFF[IPTR].IND);
// 0 => I;-1 => J;
    I = 0; J = -1;
// DIR.LOOP:
    DIR_LOOP: ;
// -1 => K;
    K = -1;
// CHAR.LOOP:
    CHAR_LOOP: ;
// IF DNAMES[1+>J] = 0,->DONE;
    if (DNAMES[++ J] == 0) goto DONE;
// IF DNAMES[J] = NAME^[1+>K],->CHAR.LOOP;
    if (DNAMES[J] == NAME[++K]) goto CHAR_LOOP;
// IF 1 +> I = NO.DNAMES,->DONE;
    if ((++ I) == NO_DNAMES) goto DONE;
// $WH DNAMES[1+>J] /= 0 DO OD
    while (DNAMES[++ J] != 0) { ; }
// ->DIR.LOOP;
    goto DIR_LOOP;
// DONE:
    DONE: ;
// IF I = NO.DNAMES,-> LB00019;
    if (I == NO_DNAMES) goto LB00019;
// $IF I > 3,-> LB00014;
    if (I > 3) goto LB00014;
// EVAL.CONST() => C
    C = EVAL_CONST ();
// LB00014:
    LB00014:
// 
// $AL I $FR
    switch (I) {
// TL.PRINT(C => CWORD ->>8);
    case 0: TL_PRINT ((CWORD = C) >> 8); break;
// TL.CODE.AREA(C);
    case 1: TL_CODE_AREA (C); break;
// TL.DATA.AREA(C=>CUR.GLB);
    case 2: TL_DATA_AREA (CUR_GLB = C); break;
// INIT(C);
    case 3: INIT (C); break;
// %DF &> CWORD;
    case 4: CWORD &= 0xdf; break;
// %EF &> CWORD;
    case 5: CWORD &= 0xef; break;
// %20 !> CWORD;
    case 6: CWORD |= 0x20; break;
// TLSEG(EVALCONST(),EVALCONST(),EVALCONST(),EVALCONST(),EVALCONST());
    case 7: {
      INTEGER c1 = EVALCONST ();
      ADDR c2 = (ADDR) EVALCONST ();
      LOGICAL32 c3 = (LOGICAL32) EVALCONST ();
      ADDR c4 = (ADDR) EVALCONST ();
      INTEGER c5 = EVALCONST ();
      TLSEG (c1, c2, c3, c4, c5); } break;
// TLLOAD(EVALCONST(),EVALCONST());
    case 8: {
      INTEGER32 c1 = EVALCONST ();
      INTEGER32 c2 = EVALCONST ();
      TLLOAD (c1, c2); } break;
// TLMODE(EVALCONST(),EVALCONST());
    case 9: {
      INTEGER32 c1 = EVALCONST ();
      INTEGER32 c2 = EVALCONST ();
      TLMODE (c1, c2); } break;
// IF COMP.TYPE () => TVST
//    & %8000 /= 0 $TH
// TADPROC => TVST;
// FI
    case 10: if (((TVST = COMP_TYPE ()) & 0x8000) != 0) TVST = TADPROC; break;
// $EN
    }
//  -> LB00018;
    goto LB00018;
// 
// LB00015:
    LB00015: ;
// 
// $IF LBUFF[IPTR] /= DEND,-> LB00023;
    if (ITYPE_ne (LBUFF[IPTR], DEND)) goto LB00023;
// END.CHECKS();
    END_CHECKS ();
// $IF CONSTK[CONPTR] = 6,-> LB00022;
    if (CONSTK[CONPTR] == 6) goto LB00022;
// MONITOR(138);
    MONITOR (138);
// LB00022:
    LB00022: ;
// 
// TL.END.MODULE(0);
    TL_END_MODULE (0);
// IF CMODE & 4 = 0 AND FAULTS = 0 THEN
    if (((CMODE & 4) == 0) && FAULTS == 0) {
// TL.END();
      TL_END ();
// FI
    }
// MONITOR(201);
    MONITOR (201);
// IF CWORD & %4 = 0 THEN
    if ((CWORD & 0x4) == 0) {
// PRINT.STATS() FI
    PRINT_STATS (); }
// -> CEXIT;
    longjmp (CEXIT, 1);
// LB00019:
    LB00019: ;
// 
// MONITOR(0);
    MONITOR (0);
// LB00023:
    LB00023: ;
// 
// $IF LBUFF[IPTR] /= HASH,-> LB00019;
    if (ITYPE_ne (LBUFF[IPTR], HASH)) goto LB00019;
// $WH LBUFF[IPTR+1] /= EOS DO
    while (ITYPE_ne (LBUFF[IPTR + 1], EOS)) {
// TL.INSERT(EVAL.CONST()) OD
      TL_INSERT (EVAL_CONST ()); }
// LB00018:
    LB00018: ;
// 
// $EN
  }
// ::PDP *CODE 1;
#ifdef PDP
    // *CODE 1; // XXX
#endif
// $PS INIT.S4();
extern void INIT_S4 (void);
// $PR INIT.S4;
void INIT_S4 (void) {
// 0 => CWORD => CUR.GLB;
    CUR_GLB = CWORD = 0;
// $EN
  }
// *END
