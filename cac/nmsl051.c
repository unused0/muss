#include "muss.h"
#include "kernel.h"
#include "lib.h"
#include "libm.h"
#include "sys.h"
#include "nmsl.h"

// 
// $LS SELECT.OUTPUT($IN);
// $LS NEWLINES($IN);
// $LS FIND.N(ADDR[$LO8],$IN)/$LO32;
// $LS FIND.P($LO32,$IN,$IN)/$IN;
// $LS CAPTION($AD[$LO8]);
// $LS TL.PROC.KIND($IN);
// $LS TL.PROC.PARAM($IN,ADDR);
// $LS TL.PROC.RESULT($IN);
// $LS TL.PROC($IN);
// $LS TL.DATA.AREA($IN);
// $LS TL.BLOCK();
// $LS TL.TYPE.COMP($IN,ADDR,ADDR[$LO8]);
// $LS TL.END.TYPE($IN);
// $LS TL.S.DECL(ADDR[$LO8],$IN,ADDR);
// $LS TL.PL($IN,$IN);
// $LS TL.SELECT.VAR();
// $LS TL.SELECT.FIELD($IN,$IN,$IN);
// $LS TL.SPACE(ADDR);
// $LS TL.C.LIT.32($IN,$IN32);
// $LS TL.C.LIT.16($IN,$IN16);
// $LS TL.ASS($IN,$IN);
// $LS TL.C.LIT.S($IN,$AD[$LO8]);
// $LS TL.ASS.VALUE($IN,$IN);
// $LS TL.ASS.END();
// $LS TL.LABEL($IN);
// $LS TL.PROC.SPEC(ADDR[$LO8],$IN);
// $LS TL.TYPE(ADDR[$LO8],$IN);
// $LS TL.C.NULL($IN);
// $LS TL.LIT(ADDR[$LO8],$IN);
// $LS TL.END.PROC();
// $LS TL.END.BLOCK();
// $LS TL.LABEL.SPEC($AD[$LO8],$IN);
// $LS TL.V.DECL(ADDR[$LO8],$LO32,$IN,$IN,$IN,ADDR);
// $PS MONITOR($IN);
// $PS ADDN()/$LO16;
// $PS GENN($LO16)/ADDR[$LO8];
// $PS ITEMISE($IN);
// $PS TRANS.COMP($IN)/$IN;
// $PS TRANS.END.ALT();
// $PS TRANS.OD();
// $PS TRANS.FI();
// $PS DECLARE.N($LO16,$IN,$IN)/$LO16;
// $PS DUMMY()/$LO16;
// $PS MUTLN($LO16)/$LO16;
// $PS EXPN($LO16);
// $TY ITYPE IS $LO8 TAG,ST $LO16 IND;
// ITYPE LB,COMMA,RB,SLASH,EQUALS,DOR,EOS,REF,LSB,RSB,DLA,DWITHIN,
// BSLASH,COLON,DEND,DTY,DVSTORE,LTHAN,GTHAN,DADDR,DIS,EOL;
// $IN XNPTR,CONPTR,IX,IPTR,CUR.GLB,MSTR,MUTLSTR;
// $IN TINT;
// $IM $LI CONSTKZ,PARAMSZ,TLISTZ;
// $IN [CONSTKZ] CONSTK;
// $IM $LI CHLISTZ;
// $LO8 [CHLISTZ] CHLIST;
// $IM $LI LBUFFZ;
// ITYPE [LBUFFZ] LBUFF;
// $IM $LI TDELIM,TLO1;
// $IM $LI TCONST,TNAME;
// $IM $LI NLISTZ,PLISTZ;
// $TY NLIST.ENT IS $LO16 MUTLN,MUSLN,HASH;
// NLIST.ENT[NLISTZ] NLIST;
// $TY PLISTENT IS $LO16 INTID,PREVN,K,T $IN32 DETAIL; ::TEMP MOD
// PLISTENT [PLISTZ] PLIST; ::TEMP MOD
// $IN LASTN,LASTCH;
// $LO8 [64] XNAME;
extern LOGICAL8 XNAME[64];
// $IM $LI TADPROC,TRE0,TINT0;
// $IM $LI CLISTZ;
// $LO8 [CLISTZ] CLIST;
// $LI/ADDR[$LO8] NIL =;
// $IM $LI TLO8,TLAB,TINT16,TINT32;
// $IN TVST,TBYADDR;
// $IM $LI SDLIST;
// $LO16[SDLIST] DLIST;
// $IN MAXMUTLN,MAXN,MAXCH;
// $IM $LI TLO0;
// $LI AOPD = %3000;
// MODULE(MOD.HEAD,DECL.PROC,ADD.LSPEC,DECL.FN,PROC.HEAD,BEGIN.ST,END.ST,INITS5,
// DECL.TYPE,DECL.VAR,DECL.FIELD,DECL.SPACE,DECL.LIT,DECL.LAB,DECL.DVEC,
// DECL.IMP,DECL.VSTORE,TLIST,PARAMS,KSPACE,KPROC,KFIELD,KPSPEC,COMP.TYPE,
// ::TEMP MOVE TO SEC 2 PLISTENT,PLIST,KLAB,KLABREF,KVAR,KDUMMY,KGLAB,KTREF,KTYPE,KIMPLAB,
// LASTMN,CURLEV,CURRES,CURPROC,CURBLK,GLEV,EVAL.CONST,END.CHECKS,PROCLEV,EVAL.LIT);
// ::TEMP MOVE TO SEC 2 $TY PLIST.ENT $IS $LO16 INTID,PREVN,K,T $IN32 DETAIL;
// *GLOBAL 5;
    // * GLOBAL 5; // XXX
// ::TEMP MOVE TO SEC 2 PLIST.ENT [PLISTZ] PLIST;
// $IN [PARAMSZ] PARAMS;
INTEGER PARAMS [PARAMSZ];
// $IN [TLISTZ] TLIST;
INTEGER T_LIST [TLISTZ];
#define TLIST T_LIST

// $IN LASTMN,CURLEV,CURRES,CURPROC,CURBLK,GLEV;
// PLISTZ => GLEV;
INTEGER LASTMN, CUR_LEV, CURRES, CURPROC, CUR_BLK, GLEV = PLISTZ;
// $LI KPSPEC = 1,KPROC = 3,KVAR=4,KFIELD=5,
// KTYPE = 6,KLIT = 7,KSPACE = 8,
// KTREF = 9,KEX =10,KDVEC =11,KDUMMY = 0,
// KLAB = 14,KLABREF=15,KGLAB=13,KIMPLAB=16;
// $IN PROCLEV,LASTT,LASTP;
#define KLIT 7
#define KEX 10
#define KDVEC 11
INTEGER PROC_LEV;
// PLISTZ => GLEV;
// $PS MOD.HEAD();
extern void MOD_HEAD (void);
// $PS DECL.PROC($IN);
extern void DECL_PROC (INTEGER);
// $PS ADD.LSPEC($LO16)/$LO16;
extern LOGICAL16 ADD_LSPEC (LOGICAL16);
// $PS PROC.HEAD();
extern void PROC_HEAD (void);
// $PS BEGIN.ST();
extern void BEGIN_ST (void);
// $PS DECL.TYPE($LO);
extern void DECL_TYPE (LOGICAL);
// $PS DECL.VAR();
extern void DECL_VAR (void);
// $PS DECL.FIELD();
extern void DECL_FIELD (void);
// $PS DECL.SPACE();
extern void DECL_SPACE (void);
// $PS DECL.LIT();
extern void DECL_LIT (void);
// $PS DECL.LAB();
extern void DECL_LAB (void);
// $PS DECL.DVEC();
extern void DECL_DVEC (void);
// $PS DECL.IMP();
extern void DECL_IMP (void);
// $PS END.ST();
extern void END_ST (void);
// $PS DECL.VSTORE();
extern void DECL_VSTORE (void);
// $PS ENDCHECKS();
extern void ENDCHECKS (void);
// $PS CHECK.LIT($IN,$IN)/$IN;
static INTEGER CHECK_LIT (INTEGER, INTEGER);
// $PS EVAL.LIT($IN,$IN)/$IN32;
extern INTEGER32 EVAL_LIT (INTEGER, INTEGER);
// $PS EVAL.CONST()/$IN32;
extern INTEGER32 EVAL_CONST (void);
// $PS COMP.TYPE()/$IN;
extern INTEGER COMP_TYPE (void);
// $PS COMP.DIM()/ADDR;
static ADDR COMP_DIM (void);
// ::PDP *CODE 2;
#ifdef PDP
    // *CODE 2; /// XXX
#endif

// BOX 1.1
// $PR MOD.HEAD;
void MOD_HEAD (void) {

// BOX 2.1
// $IN I,J,K;
    INTEGER I, J, K;

// BOX 9.1
// $IF CONSTK[CONPTR] /= 7,-> LB00016;
    if (CONSTK[CONPTR] != 7) goto LB00016;


// BOX 14.1
// -1 => XNPTR;
    XNPTR = -1;
// 6 => CONSTK[CONPTR];
    CONSTK[CONPTR] = 6;
// MONITOR(200);
    MONITOR (200);


// BOX 12.
// $IF TAG OF LBUFF[1+IPTR] /= 1,-> LB00009;
    if (LBUFF[1 + IPTR].TAG != 1) goto LB00009;
// MUSLN OF NLIST[IND OF LBUFF[1+>IPTR] => K] => I
    I = NLIST[K = LBUFF[++ IPTR].IND].MUSLN;
// $FO J < HASH OF NLIST[K] & %3F $DO
    { INTEGER limit = NLIST[K].HASH & 0x3f;
    for (J = 0; J < limit; J ++) {
// CHLIST [I+J] => XNAME [1 +> XNPTR] $OD
      XNAME[++ XNPTR] = CHLIST [I + J]; }}


// BOX 5.1

// LB00009:
    LB00009: ;
// 
// $IF LBUFF[1+>IPTR] /= LB,-> LB00023;
    if (ITYPE_ne (LBUFF[++ IPTR], LB)) goto LB00023;
// LB00010:
    LB00010: ;
// 



// BOX 13.1

// $IF TAG OF LBUFF[1+>IPTR]=1,-> LB00011;
    if (LBUFF[++ IPTR].TAG == 1) goto LB00011;
#if 0 // flip fix
// $IF LBUFF[IPTR] /= EOL,-> LB00022;
    if (ITYPE_ne (LBUFF[IPTR], EOL)) goto LB00022;
// LB00016:
    LB00016: ;
// 
// MONITOR(0);
    MONITOR (0);
#else
// $IF LBUFF[IPTR] /= EOL,-> LB00016;
    if (ITYPE_ne (LBUFF[IPTR], EOL)) goto LB00016;
// ITEMISE(1);
    ITEMISE(1);
// -> LB00010;
    goto LB00010;
#endif


// LB00011:
    LB00011: ;
// 
// EXPN(IND OF LBUFF[IPTR]);
    EXPN (LBUFF[IPTR].IND);
// $IF LBUFF[1+>IPTR] = COMMA,-> LB00010;
    if (ITYPE_eq (LBUFF[++ IPTR], COMMA)) goto LB00010;
// $IF LBUFF[IPTR]/=RB,-> LB00016;
    if (ITYPE_ne (LBUFF[IPTR], RB)) goto LB00016;
// LB00017:
    LB00017: ;
// 
// $IF LBUFF[IPTR+1] = EOL $TH 1+>IPTR $FI
    if (ITYPE_eq (LBUFF[IPTR + 1], EOL)) { ++ IPTR; }
// 0 => IX;
    IX = 0;
//  -> LB00014;
    goto LB00014;
// 

#if 0 // flip fix
// LB00022:
    LB00022: ;
// 
// ITEMISE(1);
    ITEMISE (1);
//  -> LB00010;
    goto LB00010;
#else
LB00016: ;

MONITOR(0);
#endif
// LB00023:
    LB00023: ;
// 
// 1 -> IPTR;
    --IPTR;
//  -> LB00017;
    goto LB00017;
// 
// LB00014:
    LB00014: ;
// 
// $EN
  }
// $PR DECL.PROC(NAT);
void DECL_PROC (INTEGER NAT) {
// $IN ID,N,N1,T.RES,DUPLN,TEMP,I;
    INTEGER ID, N, N1, T_RES, DUPLN, TEMP, I;
// $IF PROCLEV=1 $TH
    if (PROCLEV == 1) {
// TL.PROC.KIND(2) $FI
      TL_PROC_KIND (2); }
// $IF TAG OF LBUFF[1+>IPTR]/=1,-> LB00042;
    if (LBUFF[++ IPTR].TAG != 1) goto LB00042;
// DECLARE.N(IND OF LBUFF[IPTR]=>ID,KPSPEC,0)=>N;
    N = DECLARE_N (ID = LBUFF[IPTR].IND, KPSPEC, 0);
// IX +> NAT;
    NAT += IX;
// IF NAT&8 /= 0 THEN %8000 &> IX FI
    if ((NAT & 8) != 0) { IX &= 0x8000; }
// TL.PROC.SPEC(GENN(ID),NAT);
    TL_PROC_SPEC (GENN (ID), NAT);
// $IF LBUFF[1+>IPTR]/=LB,-> LB00038;
    if (ITYPE_ne (LBUFF[++ IPTR], LB)) goto LB00038;
// 1+>LASTP => DETAIL OF PLIST[N] => I
    I = PLIST[N].DETAIL = ++ LASTP;
// $IF LBUFF[1+IPTR]=RB,-> LB00044;
    if (ITYPE_eq (LBUFF[1 + IPTR], RB)) goto LB00044;
// LB00030:
    LB00030: ;
// 
// IF COMP.TYPE()=>PARAMS[1+>LASTP] => TEMP & %8000 /=0 $TH
// TADPROC => TEMP FI
    if ((TEMP = PARAMS[++ LASTP] = COMP_TYPE ()) & 0x8000) {
      TEMP = TADPROC; }
// TL.PROCPARAM(TEMP,0);
    TL_PROCPARAM (TEMP, 0);
// IF LASTP >= PARAMSZ THEN
    if (LASTP >= PARAMSZ) {
// SELECTOUTPUT(MSTR);NEWLINES(0);
    SELECTOUTPUT (MSTR); NEWLINES (0);
// CAPTION(%"PARAM LIST");MONITOR(9);FI
    CAPTION((ADDR_LOGICAL8) & vec ("PARAM LIST")); MONITOR (9); }
// $IF LBUFF[1+>IPTR]=COMMA,-> LB00030;
    if (ITYPE_eq (LBUFF[++ IPTR], COMMA)) goto LB00030;
// 1 -> IPTR;
    --IPTR;
// $IF LBUFF[IPTR + 1]/= RB,-> LB00042;
    if (ITYPE_ne (LBUFF[IPTR + 1], RB)) goto LB00042;
// LB00044:
    LB00044: ;
// 
// LASTP-I=>PARAMS[I];
    PARAMS[I] = LASTP - I;
// $IF LBUFF[2+>IPTR]/=SLASH,-> LB00037;
    if (ITYPE_ne (LBUFF[IPTR += 2], SLASH)) goto LB00037;
// COMP.TYPE()=>T.RES;
    T_RES = COMP_TYPE ();
// LB00035:
    LB00035: ;
// 
// IF T.RES=>T OF PLIST[N] & %8000 /=0 $TH
    if (((PLIST[N].T = T_RES) & 0x8000) != 0) {
// TADPROC => T.RES FI
      T_RES = TADPROC; }
// TL.PROC.RESULT(T.RES);
    TL_PROC_RESULT (T_RES);
//  -> LB00036;
    goto LB00036;
// 
// LB00037:
   LB00037: ;
// 
// 0 => T.RES;
    T_RES = 0;
// 1 -> IPTR;
    --IPTR;
//  -> LB00035;
    goto LB00035;
// 
// LB00038:
    LB00038: ;
// 
// $IF LBUFF[IPTR]/=EQUALS,-> LB00042;
    if (ITYPE_ne (LBUFF[IPTR], EQUALS)) goto LB00042;
// $IF TAG OF LBUFF[1+>IPTR]/=1
    if ((LBUFF[++ IPTR].TAG != 1)
// $OR MUTLN(IND OF LBUFF[IPTR])
// => DUPLN = 0 $OR K OF PLIST[DUPLN]>2,-> LB00042;
        || (MUTLN (DUPLN = LBUFF[IPTR].IND) == 0) || (PLIST[DUPLN].K > 2)) goto LB00042;
// DETAIL OF PLIST[DUPLN]
// =>DETAIL OF PLIST[N] => N1;
    N1 = PLIST[N].DETAIL = PLIST[DUPLN].DETAIL;
// $FO PARAMS[N1] DO
    { INTEGER LIMIT = PARAMS[N1]; for (INTEGER i = 0; i < PARAMS[N1]; i ++) {
// TL.PROC.PARAM(PARAMS[1+>N1],0)OD
      TL_PROC_PARAM (PARAMS[++ N1], 0); } }
// T OF PLIST[DUPLN] => T.RES;
    T_RES = PLIST[DUPLN].T;
//  -> LB00035;
    goto LB00035;
// 
// LB00042:
    LB00042: ;
// 
// MONITOR(0);
    MONITOR (0);
// LB00036:
    LB00036: ;
// 
// $EN
  }




// PROC ADD.LSPEC(LIBN);
    LOGICAL16 ADD_LSPEC(LOGICAL16 LIBN) {
    LOGICAL16 ret;
// $IN J,K,M,N;
    INTEGER J, K, M, N;
// $LO32 F;
    LOGICAL32 F;
// ADDR[$LO8] LNAME;
    ADDR_LOGICAL8 LNAME;
// GENN(LIBN) => LNAME;
    LNAME = GENN (LIBN);
// IF FIND.N(LNAME,0) => F = 0,-> LB00054;
    if ((F = FIND_N (LNAME, 0)) == 0) goto LB00054;
// DECLARE.N(LIBN,KPROC,0) => N;
    N = DECLARE_N (LIBN, KPROC, 0);
// TL.PROC.SPEC(LNAME,%8008);
    TL_PROC_SPEC (LNAME, 0x8008);
// FIND.P(F,-1,0) => M;
    M = FIND_P (F, -1, 0);
// 1 +> LAST.P => DETAIL OF PLIST[N] => J;
    J = PLIST[N].DETAIL = ++ LAST_P;
// FOR K < M DO
    { INTEGER limit = M;
    for (K = 0; K < M; K ++) {
// TL.PROC.PARAM(FIND.P(F,-1,K+1) => PARAMS[1 +> LAST.P],0) OD
      TL_PROC_PARAM (PARAMS[++ LAST_P] = FIND_P (F, -1, K + 1), 0); } }
// M => PARAMS[J];
    PARAMS[J] = M;
// IF LAST.P > PARAMS.Z THEN
    if (LAST_P > PARAMS_Z) {
// SELECT.OUTPUT(MSTR);
      SELECT_OUTPUT (MSTR);
// NEWLINES(0);CAPTION(%"PARAM LIST");
      NEWLINES (0); CAPTION((ADDR_LOGICAL8) & vec ("PARAM LIST"));
// MONITOR(9) FI
      MONITOR (9); }
// FIND.P(F,-1,M+1) => T OF PLIST[N] => J;
    J = PLIST[N].T = FIND_P (F, -1, M + 1);
// TL.PROC.RESULT(J);
    TL_PROC_RESULT (J);
// N => ADD.LSPEC;
    ret = N;
//  -> LB00053;
    goto LB00053;
// 
// LB00054:
    LB00054: ;
// 
// 0 => ADD.LSPEC;
    ret = 0;
// LB00053:
    LB00053: ;
// 
// END
    return ret;
    }



// $PR PROC.HEAD;
void PROC_HEAD (void) {
// $IN PROCN,N,I,J;
    INTEGER PROCN, N, I, J;
// $IF TAG OF LBUFF[1+>IPTR]/=1,-> LB00067;
    if (LBUFF[++ IPTR].TAG != 1) goto LB00067;
// $IF MUTLN(IND OF LBUFF[IPTR])=>PROCN<CURLEV
    if ((PROCN = MUTLN(LBUFF[IPTR].IND) < CURLEV)
// $OR K OF PLIST[PROCN] /= KPSPEC,-> LB00068;
        || (PLIST[PROCN].K != KPSPEC)) goto LB00068;
// MONITOR(200);
    MONITOR (200);
// 1 +> PROC.LEV;
    ++ PROC_LEV;
// CUR.GLB => CONSTK[1+>CONPTR];
    CONSTK[++ CONPTR] = CUR_GLB;
// CUR.LEV => CONSTK[1+>CONPTR];
    CONSTK[++ CONPTR] = CURLEV;
// CURRES => CONSTK[1+>CONPTR];
    CONSTK[++ CONPTR] = CURRES;
// CURPROC => CONSTK[1+>CONPTR];
    CONSTK[++ CONPTR] = CURPROC;
// CURBLK => CONSTK[1+>CONPTR];
    CONSTK[++ CONPTR] = CURBLK;
// GLEV => CONSTK[1+>CONPTR];
    CONSTK[++ CONPTR] = GLEV;
// LASTN => CONSTK[1+>CONPTR];
    CONSTK[++ CONPTR] = LASTN;
// LASTCH => CONSTK[1+>CONPTR];
    CONSTK[++ CONPTR] = LASTCH;
// 4 => CONSTK[1 +> CONPTR];
    CONSTK[++ CONPTR] = 4;
// 1 + LASTMN => CUR.LEV => CURBLK;
    CURBLK = CUR_LEV = 1 + LASTMN;
// PROCN => CURPROC; -1 => CURRES;
    CURPROC = PROCN;
    CURRES = -1;
// $IF PROCLEV < 3 $TH CURLEV => GLEV $FI
    if (PROCLEV < 3) { GLEV = CURLEV; }
// KPROC => K OF PLIST[PROCN];
    PLIST[PROCN].K = KPROC;
// TL.PROC(PROCN);
    TL_PROC (PROCN);
// TLDATAAREA(0=>CUR.GLB);
    TL_DATA_AREA (CUR_GLB = 0);
// $IF PARAMS[DETAIL OF PLIST[PROCN]=>I]=>J=0,-> LB00070;
    if ((J = PARAMS[I = PLIST[PROCN].DETAIL]) == 0) goto LB00070;
// $IF LBUFF[1+>IPTR]/= LB,-> LB00067;
    if (ITYPE_ne (LBUFF[++ IPTR], LB)) goto LB00067;
// LB00063:
    LB00063: ;
// 
// $IF TAG OF LBUFF[1+>IPTR]/=1,-> LB00067;
    if (LBUFF[++ IPTR].TAG != 1) goto LB00067;
// DECLAREN(IND OF LBUFF[IPTR],KVAR,PARAMS[1+>I])=>N;
    N = DECLARE_N (LBUFF[IPTR].IND, KVAR, PARAMS[++ I]);
// 0 => DETAIL OF PLIST[N];
    PLIST[N].DETAIL = 0;
// $IF 1->J=0,-> LB00069;
    if ((--J) == 0) goto LB00069;
// $IF LBUFF[1+>IPTR]=COMMA,-> LB00063;
    if (ITYPE_eq (LBUFF[++ IPTR], COMMA)) goto LB00063;
// LB00067:
    LB00067: ;
// 
// MONITOR(0);
    MONITOR (0);
// LB00068:
    LB00068: ;
// 
// MONITOR(1);
    MONITOR (1);
// LB00069:
   LB00069: ;
// 
// $IF LBUFF[1+>IPTR]=RB,-> LB00070;
   if (ITYPE_eq (LBUFF[++ IPTR], RB)) goto LB00070;
//  -> LB00067;
   goto LB00067;
// 
// LB00070:
    LB00070: ;
// 
// $IF T OF PLIST[PROCN]=>N = 0,-> LB00072;
    if ((N = PLIST[PROCN].T) == 0) goto LB00072;
// DUMMY() => CURRES;
    CURRES = DUMMY ();
// N=>T OF PLIST[CURRES];
    PLIST[CURRES].T = N;
// $IF N&%8000 /= 0 $TH
    if ((N & 0x8000) != 0) {
// TADPROC => N$FI
      N = TADPROC; }
// TL.S.DECL(NIL,N,0);
    TL_S_DECL (NIL, N, 0);
// LB00072:
    LB00072: ;
// 
// $EN
    }

// ::PDP *CODE 1;
#ifdef PDP
    // *CODE 1; // XXX
#endif




// $PR BEGIN.ST;
void BEGIN_ST (void) {
// CUR.BLK => CON.STK[1+>CON.PTR];
    CON_STK[++ CON_PTR] = CUR_BLK;
// LASTN => CONSTK[1+>CON.PTR];
    CON_STK[++ CON_PTR] = LASTN;
// LASTCH => CONSTK[1+>CON.PTR];
    CON_STK[++ CON_PTR] = LASTCH;
// 5 => CON.STK[1+>CON.PTR];
    CON_STK[++ CON_PTR] = 5;
// LASTMN + 1 => CUR.BLK;
    CUR_BLK = LASTMN + 1;
// TL.BLOCK();
    TL_BLOCK ();
// $EN
  }
// ::PDP *CODE 2;




// $PR DECL.TYPE(MODE);
void DECL_TYPE (LOGICAL MODE) {
// $IN N,TT,MTT,FIRST.T,ID;
    INTEGER N, TT, MTT, FIRST_T, ID;
// ADDR D;
    ADDR D;
// $IF TAG OF LBUFF[1+>IPTR]/=1,-> LB00092;
    if (LBUFF[++ IPTR].TAG != 1) goto LB00092;
// $IF LBUFF[1+>IPTR]/=DIS,-> LB00097;
    if (ITYPE_ne (LBUFF[++ IPTR], DIS)) goto LB00097;
// IND OF LBUFF[IPTR-1] => ID;
    ID = LBUFF[IPTR - 1].IND;
// DECLARE.N(ID,KTYPE,0)=>N;
    N = DECLARE_N (ID, KTYPE, 0);
// TL.TYPE(GENN(ID),MODE!IX);
    TL_TYPE (GENN (ID), MODE | IX);
// 1+>LAST.T=>DETAIL OF PLIST[N]
// => FIRST.T;
    FIRST_T = PLIST[N].DETAIL = ++ LAST_T;
// LB00093:
     LB00093: ;
// 
// $IF LBUFF[IPTR+1] = EOL,-> LB00094;
    if (ITYPE_eq (LBUFF[IPTR + 1], EOL)) goto LB00094;
// IF COMP.TYPE()=>TT=> MTT & %8000 /=0 $TH
    if (((MTT = TT = COMP_TYPE ()) & 0x8000) != 0) {
// TADPROC => TT FI
      TT = TADPROC; }
// IF COMP.DIM()=>D /= 0 THEN
    if ((D = COMP_DIM ()) != 0) {
// %4000 !> MTT
      MTT |= 0x4000;
// FI
    }
// LB00084:
    LB00084: ;
// 
// $IF TAG OF LBUFF[1+>IPTR]/=1,-> LB00092;
    if (LBUFF[++ IPTR].TAG != 1) goto LB00092;
// MTT=>TLIST[1+>LAST.T];
    TLIST[++ LAST_T] = MTT;
// IND OF LBUFF[IPTR]=>TLIST[1+>LAST.T]=>ID;
    ID = TLIST[++ LAST_T] = LBUFF[IPTR].IND;
// TL.TYPE.COMP(TT,D,GENN(ID));
    TL_TYPE_COMP (TT, D, GENN (ID));
// IF LAST.T >= TLISTZ THEN
    if (LAST_T >= TLISTZ) {
// SELECTOUTPUT(MSTR);NEWLINES(0);
      SELECTOUTPUT (MSTR); NEWLINES (0);
// CAPTION(%"TYPE LIST");MONITOR(9);FI
      CAPTION((ADDR_LOGICAL8) & vec ("TYPE LIST")); MONITOR (9); }
// $IF LBUFF[1+>IPTR]=COMMA,-> LB00084;
    if (ITYPE_eq (LBUFF[++ IPTR], COMMA)) goto LB00084;
// LB00087:
     LB00087: ;
// 
// $IF LBUFF[IPTR]/ = DOR,-> LB00089;
    if (ITYPE_ne (LBUFF[IPTR], DOR)) goto LB00089;
// LAST.T - FIRST.T ->> 1
//  ! %8000 => T.LIST[FIRST.T];
    T_LIST[FIRST_T] = ((LAST_T - FIRST_T) >> 1) | 0x8000;
// 1 +> LAST.T => FIRST.T;
    FIRST_T = ++ LAST_T;
// TL.END.TYPE(1);
    TL_END_TYPE (1);
//  -> LB00093;
    goto LB00093;
// 
// LB00089:
    LB00089: ;
// 
// $IF LBUFF[1->IPTR+1] /= EOS,-> LB00093;
    if (ITYPE_ne (LBUFF[(--IPTR) + 1], EOS)) goto LB00093;
// LAST.T - FIRST.T
// ->> 1 => TLIST[FIRST.T];
    T_LIST[FIRST_T] = ((LAST_T - FIRST_T) >> 1);
// TL.END.TYPE(0);
    TL_END_TYPE (0);
//  -> LB00091;
    goto LB00091;
// 
// LB00092:
    LB00092: ;
// 
// MONITOR(0);
    MONITOR (0);
// LB00094:
    LB00094: ;
// 
// ITEMISE(1);
    ITEMISE (1);
// 1 +>IPTR;
    ++ IPTR;
//  -> LB00087;
    goto LB00087;
// 
// LB00095:
    LB00095: ;
// 
// IF LBUFF[IPTR] = EOS,-> LB00096;
    if (ITYPE_eq (LBUFF[IPTR], EOS)) goto LB00096;
//  -> LB00092;
    goto LB00092;
// 
// LB00096:
    LB00096: ;
// 
// DECLAREN (IND OF LBUFF[1->IPTR]=>ID,KTREF,0);
    DECLARE_N ((ID = LBUFF[-- IPTR].IND), KTREF, 0);
// TL.TYPE(GENN(ID),IX!%2000);
    TL_TYPE (GENN (ID), IX | 0x2000);
//  -> LB00091;
    goto LB00091;
// 
// LB00097:
    LB00097: ;
// 
// IF LBUFF[IPTR] = EQUALS,-> LB00098;
    if (ITYPE_eq (LBUFF[IPTR], EQUALS)) goto LB00098;
//  -> LB00095;
   goto LB00095;
// 
// LB00098:
    LB00098: ;
// 
// DECLARE.N (IND OF LBUFF[IPTR-1] => ID, KTREF, 0);
    DECLARE_N ((ID = LBUFF[IPTR - 1].IND), KTREF, 0);
// TL.TYPE (GENN(ID),IX);
    TL_TYPE (GENN (ID), IX);
// TL.TYPE.COMP (TLO8,EVAL.CONST(),NIL);
    TL_TYPE_COMP (TLO8, (ADDR) EVAL_CONST (), NIL);
// TL.END.TYPE(2);
    TL_END_TYPE (2);
// LB00091:
    LB00091: ;
// 
// $EN
  }


// ::PDP *CODE 1;
#ifdef PDP
  // *CODE 1;
#endif





// $PR DECL.VAR;
void DECL_VAR (void) {
// $IN VART,VT,MUTLN,N;
    INTEGER VART, VT, MUTLN, N;
// ADDR VDIM;
    ADDR VDIM;
// 1 -> IPTR;
    --IPTR;
// IF COMP.TYPE()=>VART=>VT & %8000 /=0 $TH
    if (((VT = VART = COMP_TYPE ()) & 0x8000) != 0) {
// TADPROC => VT FI
      VT = TADPROC; }
// IF COMP.DIM() => VDIM /= 0 THEN
    if ((VDIM = COMP_DIM ()) != 0) {
// %4000 !> VART FI
    VART |= 0x4000; }
// LB00102:
    LB00102: ;
// 
// $IF TAG OF LBUFF[1+>IPTR]/=1,-> LB00106;
    if (LBUFF[++ IPTR].TAG != 1) goto LB00106;
// DECLARE.N(IND OF LBUFF[IPTR] => N,KVAR,VART)=>MUTLN;
    MUTLN = DECLARE_N (N = LBUFF[IPTR].IND, KVAR, VART);
// TL.S.DECL(GENN(N),VT ! IX,VDIM);
    TL_S_DECL (GENN (N), VT | IX, VDIM);
// VDIM => DETAIL OF PLIST[MUTLN];
    PLIST[MUTLN].DETAIL = (INTEGER32) VDIM;
// $IF LBUFF[1+>IPTR] = COMMA,-> LB00102;
    if (ITYPE_eq (LBUFF[++ IPTR], COMMA)) goto LB00102;
//  -> LB00105;
    goto LB00105;
// 
// LB00106:
    LB00106: ;
// 
// MONITOR(0);
    MONITOR (0);
// LB00105:
    LB00105: ;
// 
// 1 -> IPTR;
    --IPTR;
// $EN
  }


// ::PDP *CODE 3;
#ifdef PDP
   // *CODE 3; // XXX
#endif




// $PR DECL.FIELD;
void DECL_FIELD (void) {
// $IN CON.N,FIELD.N,PTR,I,ID,C.TYPE,ALT,SW;
    INTEGER CON_N, FIELD_N, PTR, I, ID, C_TYPE, ALT, SW;
// KDUMMY => K OF PLIST[1+>LASTMN=>CON.N];
    PLIST[CON_N = ++ LASTMN].K = KDUMMY;
// TL.SELECT.VAR();
    TL_SELECT_VAR ();
// REF => LBUFF[IPTR];
    LBUFF[IPTR] = REF;
// 1 -> IPTR;
    --IPTR;
// TRANS.COMP(0)=>C.TYPE;
    C_TYPE = TRANS_COMP (0);
// TL.PL(%60,CON.N);
    TL_PL (0x60, CON_N);
// $IF C.TYPE ->> 2 -64 => PTR < 0,-> LB00119;
    if ((PTR = ((C_TYPE >> 2) - 64)) < 0) goto LB00119;
// DETAIL OF PLIST[PTR] => PTR;
    PTR = PLIST[PTR].DETAIL;
// 0 => ALT;
    ALT = 0;
// LB00113:
    LB00113: ;
// 
// TLIST[PTR]&%8000 => SW;
    SW = TLIST[PTR] & 0x8000;
// $FO I < T.LIST[PTR]&%FF $DO
    { INTEGER limit =  T_LIST[PTR] & 0xFF;
      for (I = 0; I < limit; I ++) {
// DECLARE.N(TLIST[2+>PTR],KFIELD,TLIST[PTR-1]);
      PTR += 2;
      DECLARE_N (TLIST[PTR], KFIELD, TLIST[PTR - 1]);
// TL.SELECT.FIELD(CON.N,ALT,I);
      TL_SELECT_FIELD (CON_N, ALT, I);
// $OD
    } }
// 1 +> ALT; 1 +> PTR;
    ++ ALT; ++ PTR;
// $IF SW /= 0,-> LB00113;
    if (SW != 0) goto LB00113;
//  -> LB00118;
    goto LB00118;
// 
// LB00119:
    LB00119: ;
// 
// MONITOR(3);
    MONITOR (3);
// LB00118:
    LB00118: ;
// 
// $EN
  }

// ::PDP *CODE 2;
#ifdef PDP
   // *CODE 2; // XXX
#endif




// $PR DECL.SPACE;
void DECL_SPACE (void) {
// ADDR SZ;
    ADDR SZ;
// $IF TAG OF LBUFF[1+>IPTR]/=1,-> LB00129;
    if (LBUFF[++ IPTR].TAG != 1) goto LB00129;
// $IF LBUFF[1+>IPTR]/=LSB,-> LB00129;
    if (ITYPE_ne (LBUFF[++IPTR], LSB)) goto LB00129;
// EVAL.CONST()=>SZ;
    SZ = (ADDR) EVAL_CONST ();
// $IF LBUFF[1+>IPTR]/=RSB,-> LB00129;
    if (ITYPE_ne (LBUFF[++ IPTR], RSB)) goto LB00129;
//  DECLARE.N(IND OF LBUFF[IPTR-3],KSPACE,TBYADDR);
    DECLARE_N (LBUFF[IPTR-3].IND, KSPACE, TBYADDR);
// TL.SPACE(SZ);
    TL_SPACE (SZ);
//  -> LB00128;
    goto LB00128;
// 
// LB00129:
    LB00129: ;
// 
// MONITOR(0);
    MONITOR (0);
// LB00128:
    LB00128: ;
// 
// $EN
}


// [cac] subproc FIND moved out do DECL_LIT

// $PS FIND($IN,$AD[$IN])/$IN;
// $PR FIND(V,L);
static INTEGER FIND_ (INTEGER V, ADDR_INTEGER L) {
// $IN I;
    INTEGER I;
// $FO I < 6 DO
    for (I = 0; I < 6; I ++) {
// $IF V = L^[I] , ->OUT;
      if (V == L[I]) goto OUT;
// $OD
    }
// OUT:I=>FIND;
    OUT: return I;
// $EN
}

// $PR DECL.LIT;
void DECL_LIT (void) {
// $PS FIND($IN,$AD[$IN])/$IN;
// $PR FIND(V,L);
// $IN I;
// $FO I < 6 DO
// $IF V = L^[I] , ->OUT;
// $OD
// OUT:I=>FIND;
// $EN

// $IN LIT.T,MN,ID;
    INTEGER LIT_T, MN, ID;
// $IF LBUFF[1+>IPTR]/= SLASH,-> LB00141;
    if (ITYPE_ne (LBUFF[++ IPTR], SLASH)) goto LB00141;
// COMP.TYPE()=>LIT.T;
    LIT_T = COMP_TYPE ();
// LB00132:
    LB00132: ;
// 
// $IF TAG  OF LBUFF[1+>IPTR]/=1,-> LB00142;
    if (LBUFF[++ IPTR].TAG != 1) goto LB00142;
// $IF LBUFF[1+>IPTR]/= EQUALS,-> LB00138;
    if (ITYPE_ne (LBUFF[++ IPTR], EQUALS)) goto LB00138;
// DECLARE.N(IND OF LBUFF[IPTR-1]=>ID,KLIT,LIT.T) => MN;
    MN = DECLARE_N (ID = LBUFF[IPTR - 1].IND, KLIT, LIT_T);
// $BE
    {
// $IN32 RES,VAL;
      INTEGER32 RES, VAL;
// $IN FN;
      INTEGER FN;
// $DA FNLIST($IN)
// 52 ::PLUS
// 53 ::MINUS
// 58 ::AMPAND
// 59 ::BANG
// 54 ::ASTERISK
// 55 ::SLASH
// $EN
      INTEGER FNLIST[] = { 52, 53, 58, 59, 54, 55 };
// $IF LIT.T & %8003 /= 0,-> LB00159;
      if ((LIT_T & 0x8003) != 0) goto LB00159;
// IF LIT.T & %FF00 /= 0
      if (((LIT_T & 0xFF00) != 0)
// OR CHECK.LIT(LIT.T,TINT32) /= 0,-> LB00154;
          || CHECK_LIT (LIT_T, TINT32) != 0) goto LB00154;
// 0 => RES;0 => FN;
      RES = 0; FN = 0;
// LB00148:
      LB00148: ;
// 
// EVAL.LIT(0,TINT32) => VAL;
      VAL = EVAL_LIT (0, TINT32);
// $AL FN $FR
      switch (FN) {
// VAL +> RES;
        case 0: RES += VAL; break;
// VAL -> RES;
        case 1: RES -= VAL; break;
// VAL &> RES;
        case 2: RES &= VAL; break;
// VAL !> RES;
        case 3: RES |= VAL; break;
// VAL *> RES;
        case 4: RES *= VAL; break;
// VAL /> RES;
        case 5: RES /= VAL; break;
// $EN
      }
// $IF FIND(IND OF LBUFF[IPTR+1],^FNLIST) => FN = 6,-> LB00152;
      if (FIND_ ((FN = LBUFF[IPTR + 1].IND), (ADDR_INTEGER) & FNLIST) == 6) goto LB00152;
// 1 +> IPTR;
      ++ IPTR;
// ::FN SET ABOVE
//  -> LB00148;
      goto LB00148;
// 
// LB00152:
      LB00152: ;
// 
// RES => DETAIL OF PLIST[MN];
      PLIST[MN].DETAIL = RES;
// TL.C.LIT.32(LIT.T,RES);
      TL_C_LIT_32 (LIT_T, RES);
// LB00161:
      LB00161: ;
// 
// TL.LIT(GENN(ID),IX);
      TL_LIT (GENN (ID), IX);
//  -> LB00153;
      goto LB00153;
// 
// LB00154:
      LB00154: ;
// 
// $IF LIT.T < 256,-> LB00156;
      if (LIT_T < 256) goto LB00156;
// $BE
      {
// $IN PTR,N,FCOUNT;
        INTEGER PTR, N, FCOUNT;
// LIT.T - 256 ->> 2 => PTR;
        PTR = (LIT_T - 256) >> 2;
// DETAIL OF PLIST[PTR]=>PTR;
        PTR = PLIST[PTR].DETAIL;
// TLIST[PTR] => FCOUNT;
        FCOUNT = TLIST[PTR];
// 1 -> PTR;
        --PTR;
// TL.S.DECL(GENN(ID),IX+LIT.T,-4096);
        TL_S_DECL (GENN (ID), IX + LIT_T, (ADDR) -4096); // XXX
// TL.ASS(MN,-1);
        TL_ASS (MN, -1);
// LB00165:
        LB00165: ;
// 
// EVAL.LIT(1,TLIST[2+>PTR]) => N;
        N = EVAL_LIT (1, TLIST[PTR = PTR + 2]);
// TL.ASS.VALUE(N,1);
        TL_ASS_VALUE (N, 1);
// $IF 1 -> FCOUNT = 0,-> LB00170;
        if ((-- FCOUNT) == 0) goto LB00170;
// $IF LBUFF[1+>IPTR] = BSLASH,-> LB00165;
        if (ITYPE_eq (LBUFF[++ IPTR], BSLASH)) goto LB00165;
// MONITOR(0);
        MONITOR (0);
// LB00170:
        LB00170:
// 
// TL.ASS.END()
        TL_ASS_END ();
// $EN
      }
//  -> LB00153;
      goto LB00153;
// 
// LB00156:
      LB00156: ;
// 
// IF EVAL.LIT(1,LIT.T) = 0,-> LB00161;
      if (EVAL_LIT (1, LIT_T) == 0) goto LB00161;
// MONITOR(0);
      MONITOR (0);
// LB00159:
      LB00159: ;
// 
// TL.C.NULL(LIT.T);
      TL_C_NULL (LIT_T);
//  -> LB00161;
      goto LB00161;
// 
// LB00153:
      LB00153: ;
// 
// $EN
    }
// $IF LBUFF[1+>IPTR] = COMMA,-> LB00132;
    if (ITYPE_eq (LBUFF[++ IPTR], COMMA)) goto LB00132;
// $IF LBUFF[IPTR] = EOL $TH 1+>IPTR $FI
    if (ITYPE_eq (LBUFF[IPTR], EOL)) { ++ IPTR; };
//  -> LB00137;
    goto LB00137;
// 


// LB00138:
    LB00138: ;
// 
// MONITOR(0);
    MONITOR (0);

// LB00141:
    LB00141: ;
// 
// 1 -> IPTR;
    --IPTR;
// TINT => LIT.T;
    LIT_T = TINT;
//  -> LB00132;
    goto LB00132;
// 
// LB00142:
    LB00142: ;
// 

#if 0 // flip fix
// $IF LBUFF[IPTR] /= EOL,-> LB00143;
    if (ITYPE_ne (LBUFF[IPTR], EOL)) goto LB00143;
//  -> LB00138;
    goto LB00138;
// 
// LB00143:
    LB00143: ;
#else
// $IF LBUFF[IPTR] /= EOL,-> LB00138;
    if (ITYPE_ne (LBUFF[IPTR], EOL)) goto LB00138;
#endif

// 
// ITEMISE(1);
    ITEMISE (1);
//  -> LB00132;
   goto LB00132;
// 
// LB00137:
    LB00137: ;
// 
// 1 -> IPTR;
    --IPTR;
// $EN
}




// ::PDP *CODE  1;
#ifdef PDP
   // *CODE 1; // XXX
#endif









// $PR DECL.LAB;
void DECL_LAB (void) {
// $IN MNAME,KIND;
    INTEGER MNAME, KIND;
// KLAB => KIND;
    KIND = KLAB;
// $IF LBUFF[IPTR+2] = COLON,-> LB00173;
    if (ITYPE_eq (LBUFF[IPTR + 2], COLON)) goto LB00173;
// KGLAB => KIND;
    KIND = KGLAB;
// LB00173:
    LB00173: ;
// 
// DECLARE.N(IND OF LBUFF[1+>IPTR],KIND,0)=>MNAME;
    MNAME = DECLARE_N (LBUFF[++ IPTR].IND, KIND, 0);
// TL.LABEL(MNAME);
    TL_LABEL (MNAME);
// 1+>IPTR;
    ++ IPTR;
// $EN
}

// ::PDP *CODE 2;
#ifdef PDP
   // *CODE 2; // XXX
#endif









// $PR DECL.DVEC;
void DECL_DVEC (void) {
// $IN DVT,N,ID,MN,NPTR;
    INTEGER DVT, N, ID, MN, NPTR;
// 0 => CONSTK[1+>CONPTR];
    CONSTK[++ CONPTR] = 0;
// $IF TAG OF LBUFF[1+>IPTR=>NPTR]/=1,-> LB00189;
    if ((LBUFF[NPTR = ++ IPTR].TAG) != 1) goto LB00189;
// $IF LBUFF[1+>IPTR]/=LB,-> LB00189;
    if (ITYPE_ne (LBUFF[++IPTR], LB)) goto LB00189;
// COMP.TYPE()=>DVT
    DVT = COMP_TYPE ();
// $IF LBUFF[1+>IPTR] /= RB,-> LB00189;
    if (ITYPE_ne (LBUFF[++IPTR], RB)) goto LB00189;
// DECLARE.N(IND OF LBUFF[NPTR]=>ID,KDVEC,DVT ! %4000) => MN;
    MN = DECLARE_N (ID = LBUFF[NPTR].IND, KDVEC, DVT | 0x4000);
// $IF DVT&%8000 /= 0$TH
    if (DVT & 0x8000) {
// TADPROC => DVT
      DVT = TADPROC;
// ELSE IF DVT = TLAB THEN
    } else { if (DVT == TLAB) {
// %2C => DVT $FI$FI
      DVT = 0x2c; } }
// TL.S.DECL(GENN(ID), DVT!IX, -1);
    TL_S_DECL (GENN (ID), DVT | IX, (ADDR) -1); // XXX
// TL.ASS(MN,(IF PROCLEV = 0 THEN -2 ELSE -1 ));
    TL_ASS (MN, (PROCLEV == 0) ? -2 : -1);
// $BE
    {
// $IN CH.PTR,CH,C;
      INTEGER CH_PTR, CH, C;
// ADDR D;
      ADDR D;
// $AD[$LO8]ADLIT;
      ADDR_LOGICAL8 ADLIT;
// LB00195:
      LB00195: ;
// XXX
// [rj]
/**/ /* There seems to be a bug here because ITEMISE parses all the way to the END, so the literals are already in LBUFF */
#if 1
// $IF DVT = TLO8 $TH
      if (DVT == TLO8) {
// ITEMISE(2);
        ITEMISE (2);
// $EL
      } else {
// ITEMISE(1)
        ITEMISE (1);
// $FI
      }
#endif
// LB00196:
      LB00196: ;
// 
// $IF LBUFF[1+>IPTR] = EOS,-> LB00200;
      if (ITYPE_eq (LBUFF[++IPTR], EOS)) goto LB00200;
// $IF TAG $OF LBUFF[IPTR] = 2 $AN DVT = TLO8,-> LB00202;
      if (LBUFF[IPTR].TAG == 2 && DVT == TLO8) goto LB00202;
// BEGIN
      {
// IF DVT = %2C,-> LB00209;
        if (DVT == 0x2c) goto LB00209;
// IF TAG OF LBUFF[IPTR] = 1
        if (LBUFF[IPTR].TAG == 1
// AND MUTLN(IND OF LBUFF[IPTR]) => N > 1,-> LB00214;
            && (N = MUTLN (LBUFF[IPTR].IND)) > 1) goto LB00214;
// LB00207:
        LB00207: ;
// 
// 1 -> IPTR;
        --IPTR;
// EVAL.LIT(1,DVT) => N;
        N = EVAL_LIT (1, DVT);
//  -> LB00208;
        goto LB00208;
// 
// LB00214:
        LB00214: ;
// 
// IF [DVT = TADPROC OR DVT = TBYADDR]
        if ((DVT == TADPROC || DVT == TBYADDR)
// AND K OF PLIST[N] =< KPROC,-> LB00216;
            && (PLIST[N].K <= KPROC)) goto LB00216;
// IF K OF PLIST[N] /= KDVEC
        if ((PLIST[N].K != KDVEC)
// OR T OF PLIST[N] & %3FFF ! 3 /= DVT,-> LB00207;
            || (((PLIST[N].T & 0x3fff) | 3) != DVT)) goto LB00207;
// LB00216:
        LB00216: ;
// 
// ::ALREADY IN N
//  -> LB00208;
        goto LB00208;
// 
// LB00209:
        LB00209: ;
// 
// IF TAG OF LBUFF[IPTR] /= 1,-> LB00213;
        if (LBUFF[IPTR].TAG != 1) goto LB00213;
// IND OF LBUFF[IPTR] => ID;
        ID = LBUFF[IPTR].IND;
// IF MUTLN(ID) => N >= CURBLK,-> LB00212;
        if ((N = MUTLN (ID)) >= CURBLK) goto LB00212;
// DECLARE.N(ID,KLAB.REF,0) => N;
        N = DECLARE_N (ID, KLAB_REF, 0);
//  -> LB00208;
        goto LB00208;
// 
// LB00212:
        LB00212: ;
// 
// IF K OF PLIST[N] >= KLAB,-> LB00208;
        if (PLIST[N].K >= KLAB) goto LB00208;
// LB00213:
        LB00213: ;
// 
// MONITOR(0);
        MONITOR (0);
// LB00208:
        LB00208: ;
// 
// END
      }
// $IF COMP.DIM() => D = 0 $TH 1=>D $FI
      if ((D = COMP_DIM ()) == 0) { D = (ADDR) 1; } // XXX
// TL.ASS.VALUE(N,D);
      TL_ASS_VALUE (N, (INTEGER) D); // XXX
// LB00203:
      LB00203: ;
// 
// $IF LBUFF[1+IPTR] /= EOL,-> LB00196;
      if (ITYPE_ne (LBUFF [1 + IPTR], EOL)) goto LB00196;
//  -> LB00195;
      goto LB00195;
// 
//  -> LB00200;
      goto LB00200;
// 
// LB00202:
      LB00202: ;
// 
// IND OF LBUFF[IPTR] => CH.PTR;
      CH_PTR = LBUFF[IPTR].IND;
// 0 => C;
      C = 0;
// $WH CLIST[CH.PTR + C] /= 0 $DO 1 +> C $OD
      while (CLIST[CH_PTR + C] != 0) { ++ C; }
// TL.C.LIT.S(TLO8,PART(^CLIST,CH.PTR,CH.PTR + C -1));
      TL_C_LIT_S (TLO8, PART (& CLIST, CH_PTR, CH_PTR + C - 1));
// TL.ASS.VALUE(0,1);
      TL_ASS_VALUE (0, 1);
//  -> LB00203;
      goto LB00203;
// 
// LB00200:
      LB00200: ;
// 
// $EN
    }


// ::BOX TO BE REMOVED
// ITEMISE(0);
    ITEMISE (0);
// $IF LBUFF[1=>IPTR] /= DEND,-> LB00189;
    if (ITYPE_ne (LBUFF[IPTR = 1], DEND)) goto LB00189;
// $IF LBUFF[1+>IPTR] /= EOL $TH 1->IPTR $FI
    if (ITYPE_ne (LBUFF[++IPTR], EOL)) { -- IPTR; }
// TL.ASS.END();
    TL_ASS_END ();
// 1 -> CON.PTR;
    --CON_PTR;
//  -> LB00188;
    goto LB00188;
// 
// LB00189:
    LB00189: ;
// 
// MONITOR(0);
    MONITOR (0);
// LB00188:
    LB00188: ;
// 
// $EN
}









// $PR DECL.IMP;
void DECL_IMP (void) {
// $IN IMP.KIND,ID,NAME,I,TV;
    INTEGER IMP_KIND, ID, NAME, I, TV;
// $IF LBUFF[1+>IPTR] = DTY
    if (ITYPE_eq (LBUFF[++IPTR], DTY))
// $TH 0 => IMP.KIND
    { IMP_KIND = 0;
// $EL $IF LBUFF[IPTR] = DLA
    } else { if (ITYPE_eq (LBUFF[IPTR], DLA))
// $TH 3 => IMP.KIND
    { IMP_KIND = 3;
// $EL $IF LBUFF[IPTR] = DVSTORE
    } else { if (ITYPE_eq (LBUFF[IPTR], DVSTORE))
// $TH 1 => IMP.KIND $EL 2 => IMP.KIND FI
    { IMP_KIND = 1; } else { IMP_KIND = 2; } 
// IF TAG OF LBUFF[IPTR+1] = TDELIM AND
      if (LBUFF [IPTR + 1].TAG == TDELIM &&
// DLIST[IND OF LBUFF[IPTR+1]] & %200 /= 0 THEN
        (DLIST[LBUFF[IPTR + 1].IND] & 0x200) != 0) {
// 1 +> IPTR;
        ++IPTR;
// ST OF LBUFF[IPTR] => TV;
        TV = LBUFF[IPTR].ST;
// ELSE IF LBUFF[IPTR+1] = DADDR THEN
      } else { if (ITYPE_eq (LBUFF[IPTR+1], DADDR)) {
//        1 +> IPTR;
        ++ IPTR;
//        TBYADDR => TV;
        TV = TBYADDR;
//      ELSE
        } else {
//        TVST => TV FI FI
       TV = TVST; } }
//  $FI $FI
    } }
// LB00220:
    LB00220:
// 
// $IF TAG OF LBUFF[1 +> IPTR] /= 1,-> LB00224;
    if (LBUFF[++IPTR].TAG != 1) goto LB00224;
// IND OF LBUFF[IPTR] => ID;
    ID = LBUFF[IPTR].IND;
// $AL IMP.KIND $FR
    switch (IMP_KIND) {
// BEGIN
//  DECLARE.N(ID,KTREF,0);
// TL.TYPE(GENN(ID),%9000);
// END
      case 0:
        DECLARE_N (ID, KTREF, 0);
        TL_TYPE (GENN (ID), 0x9000);
        break;
// BEGIN
//  DECLARE.N(ID,KVAR,TV) => NAME;
// $IF LBUFF[IPTR+1] = LSB
// $AN LBUFF [IPTR+2] = RSB $TH
// 2 +> IPTR;
// %4000 !> T OF PLIST[NAME];
// -1 => I;
// ELSE
// 0 => I;
// $FI
// TL.VDECL(GENN(ID),0,0,0,TV!%8000,I);
// END
      case 1:
        NAME = DECLARE_N (ID, KVAR, 0);
        if (ITYPE_eq (LBUFF[IPTR + 1], LSB)
            && ITYPE_eq (LBUFF[IPTR + 2], RSB)) {
          IPTR += 2;
          PLIST[NAME].T |= 0x4000;
          I = -1;
        } else {
          I = 0;
        }
        TL_VDECL (GENN (ID), 0, 0, 0, TV | 0x8000, (ADDR) I);
        break;
// BEGIN
//  DECLARE.N(ID,KLIT,TV) => NAME;
// 0-NAME => DETAIL OF PLIST[NAME];
// TL.LIT(GENN(ID),TV!%8000);
// END
      case 2:
        NAME = DECLARE_N (ID, KLIT, TV);
        PLIST[NAME].DETAIL = 0 - NAME;
        TL_LIT (GENN (ID), TV | 0x8000);
        break;
//  DECLARE.N(ID,KIMP.LAB,0);
      case 3:
        DECLARE_N (ID, KIMP_LAB, 0);
        break;
// END
     }
// $IF LBUFF[1 +> IPTR] = COMMA,-> LB00220;
    if (ITYPE_eq (LBUFF[++IPTR], COMMA)) goto LB00220;
//  -> LB00223;
      goto LB00223;
// 
// LB00224:
    LB00224: ;
// 
// MONITOR(0);
    MONITOR (0);
// LB00223:
    LB00223: ;
// 
// 1 -> IPTR;
    --IPTR;
// $EN
}


// ::PDP *CODE 1;
#ifdef PDP
   // *CODE 1; // XXX
#endif









// $PR ENDST;
void ENDST (void) {
// $IN I;
    INTEGER I;
// $IF CONSTK[CONPTR] = 0,-> LB00239;
    if (CONSTK[CONPTR] == 0) goto LB00239;
// END.CHECKS();
    END_CHECKS ();
// $IF CON.STK[CON.PTR] >= 9,-> LB00235;
    if (CON_STK[CON_PTR] >= 9) goto LB00235;
// $IF CON.STK[CON.PTR] = 5,-> LB00236;
    if (CON_STK[CON_PTR] >= 5) goto LB00236;
// $IF CONSTK[CONPTR] /= 4,-> LB00234;
    if (CONSTK[CONPTR] != 4) goto LB00234;
// IF T OF PLIST[CURPROC] => I /=  0 THEN
    if ((I = PLIST[CURPROC].T) != 0) {
// IF I & %8000 /= 0 THEN
      if ((I & 0x8000) != 0) {
// TADPROC => I;
        I = TADPROC;
// FI
      }
// TL.PL(%46,I);
    TL_PL (0x46, I);
// TL.PL(%22,CURRES);
    TL_PL (0x22, CURRES);
// AOPD=>I;
    I = (INTEGER) AOPD; // XXX
// FI
    }
// TL.PL(%43,I);
    TL_PL (0x43, I);
// 1 -> PROCLEV;
    --PROCLEV;
// CONSTK[1->CONPTR] => LASTCH;
    LASTCH = CONSTK[--CONPTR];
// CONSTK[1->CONPTR]=> LASTN;
    LASTN = CONSTK[--CONPTR];
// CONSTK[1->CONPTR] => GLEV;
    GLEV = CONSTK[--CONPTR];
// CONSTK[1->CONPTR] => CURBLK;
    CURBLK = CONSTK[--CONPTR];
// CONSTK[1->CONPTR] => CURPROC;
    CURPROC = CONSTK[--CONPTR];
// CONSTK[1->CONPTR] => CURRES;
    CURRES = CONSTK[--CONPTR];
// CONSTK[1->CONPTR] => CURLEV;
    CURLEV = CONSTK[--CONPTR];
// TL.DATA.AREA(CONSTK[1->CONPTR]=>CUR.GLB);
    TL_DATA_AREA (CUR_GLB=CONSTK[--CONPTR]);
// 1 -> CONPTR;
    -- CONPTR;
// MONITOR(201);
    MONITOR (201);
// TL.END.PROC();
    TL_END_PROC ();
//  -> LB00233;
    goto LB00233;
// 
// LB00234:
    LB00234:
// 
// MONITOR(10);
    MONITOR (10);
//  -> LB00233;
    goto LB00233;
// 
// LB00235:
    LB00235: ;
// 
// TRANS.END.ALT();
    TRANS_END_ALT ();
// LB00236:
    LB00236: ;
// 
// CONSTK[1->CONPTR] => LASTCH;
    LASTCH = CONSTK[--CONPTR];
// CONSTK[1->CONPTR] => LASTN;
    LASTN = CONSTK[--CONPTR];
// CONSTK[1->CONPTR] => CURBLK;
    CURBLK = CONSTK[--CONPTR];
// 1 -> CONPTR;
    --CONPTR;
// TL.END.BLOCK();
    TL_END_BLOCK ();
//  -> LB00233;
    goto LB00233;
// 
// LB00239:
    LB00239: ;
// 
// 1 -> CONPTR;
    --CONPTR;
//  -> LB00234;
    goto LB00234;
// 
// LB00233:
    LB00233: ;
// 
// $EN
}
// ::PDP *CODE 2;
#ifdef PDP
   // *CODE 2; // XXX
#endif









// $PR DECL.VSTORE;
void DECL_VSTORE (void) {
// $IN VNAME,PREP,POSTP,NAME,VT;
    INTEGER VNAME, PREP, POSTP, NAME, VT;
// ADDR DIM;
    ADDR DIM;
// 0 => PREP => POSTP => VT;
    VT = POSTP = PREP = 0;
// $IF TAG OF LBUFF[1+>IPTR] /= 1,-> LB00253;
    if (LBUFF[++IPTR].TAG != 1) goto LB00253;
// IND OF LBUFF[IPTR] => NAME;
    NAME = LBUFF[IPTR].IND;
// IF COMP.DIM() => DIM /= 0 $TH
    if ((DIM = COMP_DIM ()) != 0) {
// %4000 => VT FI
      VT = 0x4000; }
// $IF TAG OF LBUFF[IPTR+1] = 1,-> LB00255;
    if (LBUFF[IPTR + 1].TAG == 1) goto LB00255;
// EVAL.LIT(1,TBYADDR) => VNAME;
    VNAME = EVAL_LIT (1, TBYADDR);
// TVST !> VT;
    VT |= TVST;
// LB00245:
    LB00245: ;
// 
// $IF LBUFF[1 + IPTR] /= LTHAN,-> LB00248;
    if (ITYPE_ne (LBUFF[1 + IPTR], LTHAN)) goto LB00248;
// $IF TAG OF LBUFF[2+>IPTR] /= 1,-> LB00253;
    if (LBUFF[IPTR += 2].TAG != 1) goto LB00253;
// DECLARE.N(IND OF LBUFF[IPTR],KPSPEC,0) => PREP;
    PREP = DECLARE_N(LBUFF[IPTR].IND, KPSPEC,0);
// 0=>DETAIL OF PLIST[PREP];
    PLIST[PREP].DETAIL = 0;
// TL.PROC.SPEC(NIL,%4002);
    TL_PROC_SPEC (NIL, 0x4002);
// TL.PROC.RESULT(0);
    TL_PROC_RESULT (0);
// LB00248:
    LB00248: ;
// 
// $IF LBUFF[1+IPTR] /= GTHAN,-> LB00251;
    if (ITYPE_ne (LBUFF[1 + IPTR], GTHAN)) goto LB00251;
// $IF TAG OF LBUFF[2+>IPTR] /= 1,-> LB00253;
    if (LBUFF[IPTR += 2].TAG != 1) goto LB00253;
// DECLARE.N(IND OF LBUFF[IPTR],KPSPEC,0) => POSTP;
    POSTP = DECLARE_N (LBUFF[IPTR].IND, KPSPEC, 0);
// TL.PROC.SPEC(NIL,%4002);
    TL_PROC_SPEC (NIL, 0x4002);
// 0 => DETAIL OF PLIST[POSTP];
    PLIST[POSTP].DETAIL = 0;
// TL.PROC.RESULT(0);
    TL_PROC_RESULT (0);
// LB00251:
    LB00251: ;
// 
// DECLARE.N(NAME,KVAR,VT);
    DECLARE_N (NAME, KVAR, VT);
// TL.V.DECL(GENN(NAME),VNAME,PREP,POSTP,VT!IX,DIM);
    TL_V_DECL (GENN (NAME), VNAME, PREP, POSTP, VT | IX, DIM);
//  -> LB00252;
    goto LB00252;
// 
// LB00253:
    LB00253: ;
// 
// MONITOR(0);
    MONITOR (0);
// LB00255:
    LB00255: ;
// 
// MUTLN(IND OF LBUFF[1+>IPTR]) => VNAME;
    VNAME = MUTLN (LBUFF[++IPTR].IND);
// T OF PLIST[VNAME] !> VT;
    VT |= PLIST[VNAME].T;
//  -> LB00245;
    goto LB00245;
// 
// LB00252:
    LB00252: ;
// 
// $EN
}

// ::PDP *CODE 1;
#ifdef PDP
   // *CODE 1; // XXX
#endif









// $PR COMP.TYPE;
INTEGER COMP_TYPE (void) {
    INTEGER ret;
// $IN VTYPE,N,KK; 0 => VTYPE;
    INTEGER VTYPE, N, KK; VTYPE = 0;
// $IF LBUFF[1+>IPTR]=DADDR,-> LB00269;
    if (ITYPE_eq (LBUFF[++IPTR], DADDR)) goto LB00269;
// LB00259:
    LB00259: ;
// 
// $IF TAG OF LBUFF[IPTR]=0
   if (LBUFF[IPTR].TAG == 0
// $AN DLIST[IND OF LBUFF[IPTR]] & %200 /= 0,-> LB00273;
       && (DLIST[LBUFF[IPTR].IND] & 0x200) != 0) goto LB00273;
// IF LBUFF[IPTR] = DADDR,-> LB00277;
    if (ITYPE_eq (LBUFF[IPTR], DADDR)) goto LB00277;
// $IF TAG OF LBUFF[IPTR]/=1,-> LB00274;
    if (LBUFF[IPTR].TAG != 1) goto LB00274;
// $IF MUTLN(IND OF LBUFF[IPTR]) => N =< 1,-> LB00274;
    if ((N = MUTLN (LBUFF[IPTR].IND)) <= 1) goto LB00274;
// $IF K OF PLIST[N] => KK = KTYPE OR KK = KTREF,-> LB00265;
    if ((KK = PLIST[N].K) == KTYPE || KK == KTREF) goto LB00265;
// IF KK /= KPSPEC AND KK /= KPROC
    if ((KK != KPSPEC && KK != KPROC)
// OR VTYPE /= 1,-> LB00274;
        || VTYPE != 1) goto LB00274;
// %8000 ! N => VTYPE;
    VTYPE = N | 0x8000;
//  -> LB00268;
   goto LB00268;
// 
// LB00265:
    LB00265: ;
// 
// N+64<<- 2!>VTYPE;
    VTYPE |= (N + 64) << 2;
// LB00267:
    LB00267: ;
// 
// $IF VTYPE & 3 /= 3,-> LB00268;
    if ((VTYPE & 3) != 3) goto LB00268;
// $IF LBUFF[1+>IPTR]/=RSB,-> LB00272;
    if (ITYPE_ne (LBUFF[++IPTR], RSB)) goto LB00272;
//  -> LB00268;
    goto LB00268;
// 
// LB00269:
    LB00269: ;
// 
// 1 => VTYPE;
    VTYPE = 1;
// $IF LBUFF[1+>IPTR] /= LSB,-> LB00259;
    if (ITYPE_ne (LBUFF[++IPTR], LSB)) goto LB00259;
// 3 => VTYPE;
    VTYPE = 3;
// 1+> IPTR;
    ++ IPTR;
//  -> LB00259;
    goto LB00259;
// 
// LB00272:
    LB00272: ;
// 
// MONITOR(0);
    MONITOR (0);
// LB00273:
    LB00273: ;
// 
// ST OF LBUFF[IPTR] !> VTYPE;
    VTYPE |= LBUFF[IPTR].ST;
//  -> LB00267;
    goto LB00267;
// 
// LB00274:
    LB00274: ;
// 
// IF VTYPE&1 /= 1,-> LB00272;
    if ((VTYPE & 1) != 1) goto LB00272;
// VTYPE ->> 1 + 1 -> IPTR;
    IPTR -= ((VTYPE >> 1) + 1);
// TBYADDR => VTYPE;
    VTYPE = TBYADDR;
//  -> LB00268;
    goto LB00268;
// 
// LB00277:
    LB00277: ;
// 
#if 0 // XXX  [rj]
IPTR--; /**/ /* Possible bug in the sources. If the parameter is just "ADDR" with nothing else IPTR is not left on the last part of the parameter */
#endif

// TBYADDR !> VTYPE;
    VTYPE |= TBYADDR;
//  -> LB00267;
    goto LB00267;
// 
// LB00268:
    LB00268: ;
// 
// VTYPE => COMP.TYPE;
   ret = VTYPE; 
// $EN
    return ret;
}








// $PR COMP.DIM;
static ADDR COMP_DIM (void) {
    ADDR ret;
// ADDR CONST;
    ADDR CONST;
// $IF LBUFF[1+>IPTR]/=LSB,-> LB00285;
    if (ITYPE_ne (LBUFF[++IPTR], LSB)) goto LB00285;
// EVAL.CONST()=>CONST;
    CONST = (ADDR) EVAL_CONST (); // XXX
// $IF LBUFF[1+>IPTR]/=RSB,-> LB00286;
    if (ITYPE_ne (LBUFF[++ IPTR], RSB)) goto LB00286;
// CONST => COMP.DIM;
    ret = CONST;
//  -> LB00284;
    goto LB00284;
// 
// LB00285:
    LB00285: ;
// 
// 1 -> IPTR;
    --IPTR;
// 0 => COMP.DIM;
    ret = 0;
//  -> LB00284;
    goto LB00284;
// 
// LB00286:
    LB00286: ;
// 
// MONITOR(0);
    MONITOR (0);
// LB00284:
    LB00284: ;
// 
// $EN
    return ret;
  }








// $PR CHECK.LIT(LT,REQDT);
static INTEGER CHECK_LIT (INTEGER LT, INTEGER REQDT) {
    INTEGER ret;
// IF LT & %3F =< REQDT & %3F
    if ((((LT & 0x3f) <= (REQDT & 0x3F)
// AND [LT & %C0 = REQDT & %C0
      && (LT & 0xc0) == (REQDT & 0xc0))
// OR LT & %C1 = TLO0 AND REQDT & %C0 = TINT0
      || ((LT & 0xc1) == TLO0 && (REQDT & 0xc0) == TINT0))
// OR LT = TLO0 AND REQDT = TLO1]
      || (LT == TLO0 && REQDT == TLO1))
// THEN 0 => CHECK.LIT
      { ret = 0;
// ELSE IF  LT & %FFC3 = TRE0 AND REQDT & %FFC3 = TRE0
      } else { if ((LT & 0xffc3) == TRE0 && 0xffc3 == TRE0)
// THEN 0 => CHECK.LIT
      { ret = 0;
// ELSE 1 => CHECK.LIT FI FI
      } else { ret = 1; } }
// END
    return 0;
}



// $PR EVAL.LIT(MODE,LIT.TYPE);
INTEGER32 EVAL_LIT (INTEGER MODE, INTEGER LIT_TYPE) {
    INTEGER32 ret;
// $IN I,ST,N;
    INTEGER I, ST, N;
// IF TAG OF LBUFF[1+>IPTR] => I /= TCONST,-> LB00300;
    if ((I = LBUFF[++IPTR].TAG) != TCONST) goto LB00300;
// ST OF LBUFF[IPTR] => ST ->>2 & %F => N;
    N = ((ST = LBUFF[IPTR].ST) >> 2) & 0xf;
// IND OF LBUFF[IPTR] => I;
    I = LBUFF[IPTR].IND;
// IF CHECK.LIT(ST,LIT.TYPE) /= 0,-> LB00302;
    if (CHECK_LIT (ST, LIT_TYPE) != 0) goto LB00302;
// IF MODE /= 0,-> LB00299;
    if (MODE != 0) goto LB00299;
// IF ST & %C0 = %40 THEN
    if ((ST & 0xc0) == 0x40) {
// -1 => EVAL.LIT ELSE
      ret = -1; } else {
// 0 => EVAL.LIT FI
      ret = 0; }
// 1 -> I;
    --I;
// FOR N + 1 DO
    { INTEGER limit = N + 1;
      for (INTEGER i = 0; i < limit; i ++) {
// EVAL.LIT <<- 8 + CLIST[1+>I] => EVAL.LIT OD
      ret = (ret << 8) + CLIST[++ I]; } }
// ::DONE IN BOX 7
//  -> LB00298;
    goto LB00298;
// 
// LB00299:
    LB00299: ;
// 
// TL.C.LIT.S(ST,PART(^CLIST,I,I+N));
    TL_C_LIT_S (ST, PART (& CLIST, I, I + N));
// 0 => EVAL.LIT;
    ret = 0;
//  -> LB00298;
    goto LB00298;
// 
// LB00300:
    LB00300: ;
// 
// IF I /= TNAME
    if (I != TNAME
// OR MUTLN(IND OF LBUFF[IPTR]) => I =< 1
        || MUTLN (I = LBUFF[IPTR].IND) <= 1
// OR K OF PLIST[I] /= KLIT,-> LB00302;
        || PLIST[I].K != KLIT) goto LB00302;
// IF CHECK.LIT(T OF PLIST[I],LIT.TYPE) = 0,-> LB00306;
    if (CHECK_LIT (PLIST[I].T, LIT_TYPE) == 0) goto LB00306;
// LB00302:
    LB00302: ;
// 
// -1 => EVAL.LIT;
    ret = -1;
// IF MODE = 2,-> LB00304;
    if (MODE == 2) goto LB00304;
// MONITOR(3);
    MONITOR (3);
// LB00305:
     LB00305: ;
// 
// DETAIL OF PLIST[I] => EVAL.LIT;
    ret = PLIST[I].DETAIL;
//  -> LB00298;
    goto LB00298;
// 
// LB00304:
    LB00304: ;
// 
// 1 -> IPTR;
    --IPTR;
//  -> LB00298;
    goto LB00298;
// 
// LB00306:
    LB00306:
// 
// IF MODE = 0,-> LB00305;
    if (MODE == 0) goto LB00305;
// I => EVAL.LIT;
    ret = I;
// LB00298:
    LB00298:
// 
// END
    return ret;
}



// $PR END.CHECKS;
void END_CHECKS (void) {
// $IN I;
    INTEGER I;
// 0 => I;
    I = 0;
// IF LASTMN > MAXMUTLN THEN
    if (LASTMN > MAXMUTLN) {
// LASTMN => MAXMUTLN FI
      MAXMUTLN = LASTMN; }
// IF LASTN > MAXN THEN
    if (LASTN > MAXN) {
// LASTN => MAXN FI
      MAXN = LASTN; }
// IF LASTCH > MAXCH THEN
    if (LASTCH > MAXCH) {
// LASTCH => MAXCH FI
      MAXCH = LASTCH; }
// LB00309:
    LB00309: ;
// 
// $IF CONSTK[CONPTR] /= 3 /= 9,-> LB00311;
    if (CONSTK[CONPTR] != 3 && CONSTK[CONPTR] != 9) goto LB00311;
// MONITOR(143);
    MONITOR (143);
// TRANS.OD();
    TRANS_OD ();
// LB00311:
    LB00311: ;
// 
// $IF CONSTK[CONPTR] /= 1,-> LB00313;
    if (CONSTK[CONPTR] != 1) goto LB00313;
// MONITOR(141);
    MONITOR (141);
// TRANS.FI();
    TRANS_FI ();
//  -> LB00309;
    goto LB00309;
// 
// LB00313:
    LB00313: ;
// 
// $IF LASTMN < CURBLK,-> LB00317;
    if (LASTMN < CURBLK) goto LB00317;
// $IF K OF PLIST[LASTMN] /= KPSPEC
    if (PLIST[LASTMN].K != KPSPEC
// $AN K OF PLIST[LASTMN] /= KLABREF,-> LB00316;
        && PLIST[LASTMN].K != KLABREF) goto LB00316;
// IF I = 0 THEN
    if (I == 0) {
// MONITOR(129);
     MONITOR (129);
// 1 => I;
      I = 1;
// FI
   }
// SELECTOUTPUT(MSTR);
    SELECTOUTPUT (MSTR);
// NEWLINES(1);
    NEWLINES (1);
// CAPTION(GENN(INTID OF PLIST[LASTMN]));
    CAPTION (GENN (PLIST[LASTMN].INTID));
// LB00316:
    LB00316: ;
// 
// $IF K OF PLIST[LASTMN] /= KDUMMY $TH
    if (PLIST[LASTMN].K != KDUMMY) {
// PREVN OF PLIST[LASTMN] => MUTLN
// OF NLIST[INTID OF PLIST[LASTMN]] $FI
    NLIST[PLIST[LASTMN].INTID].MUTLN = PLIST[LASTMN].PREVN; }
// 1 -> LASTMN;
    -- LASTMN;
//  -> LB00313;
    goto LB00313;
// 
// LB00317:
    LB00317: ;
// 
// $EN
}

// $PR EVAL.CONST;
INTEGER32 EVAL_CONST (void) {
// EVAL.LIT(0,TINT32) => EVAL.CONST;
    return EVAL_LIT (0, TINT32);
// END
}

// $PS INIT.S5();
extern void INIT_S5 (void);
// $PR INIT.S5;
void INIT_S5 (void) {
// 0  => PROCLEV => LASTT => LASTP;
    LASTP = LASTT = PROCLEV = 0;
// -1 => CURPROC => CURRES;
    CURRES = CURPROC = -1;
// 1 => LASTMN => CURBLK => CURLEV;
    CURLEV = CURBLK = LASTMN = 1;
// 0 => K OF PLIST[0];
    PLIST[0].K = 0;
// KEX => K OF PLIST[1];
    PLIST[1].K = KEX;
// $EN
}
// *END
