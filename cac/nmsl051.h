//
// nmsl051
//

extern void MOD_HEAD (void);
extern void DECL_PROC (INTEGER);
extern LOGICAL16 ADD_LSPEC (LOGICAL16);
// DECL.FN in MODULE decl., but not defined.
extern void PROC_HEAD (void);
extern void BEGIN_ST (void);
extern void END_ST (void);
#define ENDST END_ST
extern void INIT_S5 (void);
extern void DECL_TYPE (LOGICAL);
extern void DECL_VAR (void);
extern void DECL_FIELD (void);
extern void DECL_SPACE (void);
extern void DECL_LIT (void);
extern void DECL_LAB (void);
extern void DECL_DVEC (void);
extern void DECL_IMP (void);
extern void DECL_VSTORE (void);
extern INTEGER T_LIST [];
#define TLIST T_LIST
extern INTEGER PARAMS [];
//$LI KPSPEC = 1,KPROC = 3,KVAR=4,KFIELD=5,
//KTYPE = 6,KLIT = 7,KSPACE = 8,
//KTREF = 9,KEX =10,KDVEC =11,KDUMMY = 0,
//KLAB = 14,KLABREF=15,KGLAB=13,KIMPLAB=16;
#define KPSPEC 1
#define KPROC 3
#define KFIELD 5 
#define KSPACE 8
extern INTEGER COMP_TYPE (void);
extern INTEGER LASTMN, CUR_LEV, CUR_RES, CURPROC, CUR_BLK, GLEV;
#define CURLEV CUR_LEV
#define CURBLK CUR_BLK
#define CURRES CUR_RES
extern INTEGER32 EVAL_CONST (void);
#define EVALCONST EVAL_CONST
extern void END_CHECKS (void);
#define ENDCHECKS END_CHECKS
extern INTEGER PROC_LEV;
#define PROCLEV PROC_LEV
extern INTEGER32 EVAL_LIT (INTEGER, INTEGER);

