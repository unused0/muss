#include <stddef.h>
#include "muss.h"
#include "kernel.h"
#include "lib.h"
#include "libm.h"
#include "mutl.h"
#include "nmsl.h"


// 
// $LS TL.LABEL.SPEC(ADDR[$LO8],$IN);
// $LS TL.LABEL($IN);
// $LS TL.S.DECL(ADDR[$LO8],$IN,ADDR);
// $LS TL.PL($IN,$IN);
// $LS TL.C.LIT.32($IN,$IN32);
// $LS TL.C.LIT.16($IN,$IN16);
// $LS TL.REG($IN);
// $LS TL.ASS($IN,$IN);
// $LS TL.ASS.VALUE($IN,$IN);
// $LS TL.ASS.END();
// $LS TL.CYCLE($IN);
// $LS TL.CV.CYCLE($IN,$IN,$IN);
// $LS TL.CV.LIMIT($IN);
// $LS TL.REPEAT();
// $IN LASTMN;
// $IN AP;
// $PS PREPROCESS.COMP($IN)/$IN;
// $IN CWORD;
// $PS PRINT.AR();
// $IN IPTR;
// $PS COMPILE.COMP($IN,$IN)/$IN;
// $TY ITYPE IS $LO8 TAG,ST $LO16 IND;
// ITYPE EOS,DTHEN,COMMA,GOTO,DDO,LTHAN,BSLASH,DFROM,DEND;
// $IM $LI LBUFFZ;
// ITYPE[LBUFFZ] LBUFF;
// $IM $LI CONSTKZ;
// $IN [CONSTKZ] CONSTK;
// $IN CONPTR;
// $TY PLIST.ENT IS $LO16 INTID,PREVN,K,T $IN32 DETAIL;
// $IM $LI PLISTZ;
// PLIST.ENT[PLISTZ] PLIST;
// $PS MONITOR($IN);
// $IN CUR.LEV;
// $IN GLEV;
// $IN FPTR;
// $IN REGS.IU;
// $LI/ADDR[$LO8] NIL=;
// $IM $LI KLAB;
// $IM $LI KLABREF;
// $IM $LI KVAR;
// $IN TINT,TBYADDR;
// $IM $LI TLAB,TADPROC,TINT32;
// $IN CURBLK;
// $PS BEGIN.ST();
// $PS PREPROCESS.COND();
// $IM $LI KGLAB;
// $IN ST.PTR;
    INTEGER ST_PTR;
// $IN CURPROC;
// $IN CURRES;
// $PS COMPILE.COND($IN,$IN,$IN);
// $PS DECLARE.N($LO16,$IN,$IN)/$LO16;
// $PS DUMMY()/$LO16;
// $PS MUTLN($LO16)/$LO16;
// $PS EXPN($LO16);
// $PS EVAL.LIT($IN,$IN)/$IN32;
// $LI BREQD = -1, DIND = %1004, AOPD = %3000;
// MODULE(TRANS.COMP,TRANS.IF,TRANS.WHILE,
// TRANS.FOR,TRANS.GOTO,TRANS.SWITCH,
// TRANS.ALT,TRANS.ELSE,TRANS.FI,
// TRANS.OD,TRANS.EXIT,TRANS.END.ALT,
// ALT.LABEL,INIT.S6);
// *GLOBAL 5;
//     ::
// $PS TRANS.COMP($IN)/$IN;
// $PS TRANS.IF();
// $PS TRANS.WHILE();
// $PS TRANS.FOR();
// $PS TRANS.GOTO();
// $PS TRANS.SWITCH();
// $PS TRANS.ALT();
// $PS TRANS.ELSE();
// $PS TRANS.FI();
// $PS TRANS.OD();
// $PS TRANS.EXIT();
// $PS TRANS.END.ALT();
// $PS ALT.LABEL();
// $PS TRANS.COND($IN,$IN);
static void TRANS_COND (INTEGER, INTEGER);


#ifdef PDP
    // *CODE 3; // XXX
#endif // PDP

// $PR TRANS.COMP(REQD.TYPE);
INTEGER TRANS_COMP (INTEGER REQD_TYPE) {
// ::DECL & INIT
// 0 => AP => FPTR;
    FPTR = AP = 0;
// PREPROCESS.COMP(0);
    PREPROCESS_COMP (0);
// $IF CWORD & %10 /= 0 $TH
    if ((CWORD & 0x10) != 0) {
// PRINTAR();FI
      PRINTAR (); }
// 1 -> IPTR;
    --IPTR;
// 0 => AP => REGS.IU => ST.PTR;
    ST_PTR = REGS_IU = AP = 0;
// COMPILE.COMP(0,REQD.TYPE)=>TRANS.COMP;
    return COMPILE_COMP (0, REQD_TYPE);
// $EN
}




#ifdef PDP
   // *CODE 1; // XXX
#endif // PDP

// $PR TRANS.IF;
void TRANS_IF (void) {
// $IN DEST.LAB,COND.PTR,ID,END.PTR,IF.OR.UN;
    INTEGER DEST_LAB, COND_PTR, ID, END_PTR, IF_OR_UN;
// IPTR => COND.PTR;
    COND_PTR = IPTR;
// $WH LBUFF[1+>IPTR] /= EOS $DO $OD
    while (ITYPE_ne (LBUFF[++IPTR], EOS)) { ; }
// $IF LBUFF[1->IPTR] /= DTHEN,-> LB00025;
    if (ITYPE_ne (LBUFF[--IPTR], DTHEN)) goto LB00025;
// IPTR - 1 => END.PTR;
    END_PTR = IPTR - 1;
// DUMMY() => DEST.LAB;
    DEST_LAB = DUMMY ();
// TL.LABEL.SPEC(NIL,3);
    TL_LABEL_SPEC (NIL, 3);
// DEST.LAB => CON.STK[1+>CON.PTR];
    CON_STK[++CON_PTR] = DEST_LAB;
// 1 => CON.STK[1+>CON.PTR];
    CON_STK[++CON_PTR] = 1;
// 1 => IF.OR.UN;
    IF_OR_UN = 1;
// LB00020:
    LB00020: ;
// 
// COND.PTR => IPTR;
    IPTR = COND_PTR;
// TRANS.COND(IF.OR.UN,DEST.LAB)
    TRANS_COND (IF_OR_UN, DEST_LAB);
// $IF IPTR /= END.PTR,-> LB00030;
    if (IPTR != END_PTR) goto LB00030;
// $WH LBUFF[IPTR+1] /= EOS $DO
    while (ITYPE_ne (LBUFF[IPTR + 1], EOS)) {
// EOS => LBUFF[1+>IPTR] $OD
      LBUFF[++IPTR] = EOS; }
//  -> LB00024;
    goto LB00024;
// 
// LB00025:
    LB00025: ;
// 
// IPTR - 3 => IPTR => END.PTR
    END_PTR = IPTR = IPTR - 3;
// $IF LBUFF[1+>IPTR] /= COMMA,-> LB00030;
    if (ITYPE_ne (LBUFF[++IPTR], COMMA)) goto LB00030;
// $IF LBUFF[1+>IPTR] /= GOTO,-> LB00030;
    if (ITYPE_ne (LBUFF[++IPTR], GOTO)) goto LB00030;
// $IF TAG $OF LBUFF[1+>IPTR] /= 1,-> LB00030;
    if (LBUFF[++IPTR].TAG != 1) goto LB00030;
// IND $OF LBUFF[IPTR] => ID;
    ID = LBUFF[IPTR].IND;

#if 0 // flip fix
// $IF MUTLN(ID) => DEST.LAB < CURLEV,-> LB00032;
// [rj] XXX
#if 0
/* another condition inversion */
    if ((DEST_LAB = MUTLN (ID)) >= CURLEV) goto LB00032;
#else
    if ((DEST_LAB = MUTLN (ID)) < CURLEV) goto LB00032;
#endif
// DECLARE.N (ID,KLAB.REF,0) => DEST.LAB;
    DEST_LAB = DECLARE_N (ID, KLAB_REF, 0);
#else
// $IF MUTLN(ID) => DEST.LAB < CURLEV,-> LB00033;
    if ((DEST_LAB = MUTLN(ID)) < CURLEV) goto LB00033;
// $IF K $OF PLIST[DEST.LAB] >= KLAB,-> LB00029;
    if (PLIST[DEST_LAB].K >= KLAB) goto LB00029;
// LB00030:
    LB00030: ;
// 
// MONITOR(0);
    MONITOR(0);
#endif
// LB00029:
    LB00029: ;
// 
// 0 => IF.OR.UN;
    IF_OR_UN = 0;
//  -> LB00020;
    goto LB00020;
// 

#if 0 // flip fix
// LB00030:
    LB00030: ;
// 
// MONITOR(0);
    MONITOR (0);
// LB00032:
    LB00032: ;
// 
// $IF K $OF PLIST[DEST.LAB] >= KLAB,-> LB00029;
    if (PLIST[DEST_LAB].K >= KLAB) goto LB00029;
//  -> LB00030;
    goto LB00030;
#else
// LB00033:
    LB00033: ;
// 
// DECLARE.N (ID,KLAB.REF,0) => DEST.LAB;
    DEST_LAB = DECLARE_N (ID,KLAB_REF,0);
//  -> LB00029;
    goto LB00029;
#endif
// 
// LB00024:
    LB00024: ;
// 
// $EX;
     return; // XXX
// $EN
}



#ifdef PDP
    // *CODE 3; // XXX
#endif // PDP



// $PR TRANS.WHILE;
void TRANS_WHILE (void) {
// $IN ST.LAB,END.LAB;
    INTEGER ST_LAB, END_LAB;
// DUMMY() => ST.LAB;
    ST_LAB = DUMMY ();
// DUMMY() => END.LAB;
    END_LAB = DUMMY ();
// TL.LABEL.SPEC(NIL,2);
    TL_LABEL_SPEC (NIL, 2);
// TL.LABEL.SPEC(NIL,3);
    TL_LABEL_SPEC (NIL, 3);
// TL.LABEL(ST.LAB);
    TL_LABEL (ST_LAB);
// TRANS.COND(1,END.LAB);
    TRANS_COND (1,END_LAB);
// $IF LBUFF[1+>IPTR] /= DDO,-> LB00041;
    if (ITYPE_ne (LBUFF[++IPTR], DDO)) goto LB00041;
// ST.LAB => CON.STK[1+>CON.PTR];
    CON_STK[++CON_PTR] = ST_LAB;
// END.LAB => CON.STK[1+>CON.PTR];
    CON_STK[++CON_PTR] = END_LAB;
// 3 => CON.STK [1 +> CON.PTR];
    CON_STK[++CON_PTR] = 3;
//  -> LB00040;
    goto LB00040;
// 
// LB00041:
    LB00041: ;
// 
// MONITOR(0);
    MONITOR (0);
// LB00040:
    LB00040: ;
// 
// $EN
}

// [cac] EVAL.LIMIT subproc copied from below

// $PS EVAL.LIMIT($IN)/$IN;
// $PR EVAL.LIMIT(T);
static INTEGER EVAL_LIMIT (INTEGER T) {
    INTEGER ret;
// IF LBUFF[IPTR+2] /= DDO
    if (ITYPE_ne (LBUFF[IPTR + 2], DDO)
// OR EVAL.LIT(2,T) => EVAL.LIMIT = -1,-> LB00057;
        || (ret = EVAL_LIT (2, T)) == -1) goto LB00057;
// ::ALREADY DONE IN EVAL.LIT
// ::ALREADY DONE (BOX 3)
//  -> LB00059;
    goto LB00059;
// 
// LB00057:
    LB00057: ;
// 
// TRANS.COMP(0);
    TRANS_COMP (0);
// TL.PL(%45,T);
    TL_PL (0x45, T);
// %3000 => EVAL.LIMIT;
    ret = 0x3000;
// LB00059:
    LB00059: ;
// 
// END
    return ret;
}



// $PR TRANS.FOR;
 void TRANS_FOR (void) {
// $PS EVAL.LIMIT($IN)/$IN;
// $IN CV,T;
    INTEGER CV, T;
// $PR EVAL.LIMIT(T);
// IF LBUFF[IPTR+2] /= DDO
// OR EVAL.LIT(2,T) => EVAL.LIMIT = -1,-> LB00057;
// ::ALREADY DONE IN EVAL.LIT
// ::ALREADY DONE (BOX 3)
//  -> LB00059;
// 
// LB00057:
// 
// TRANS.COMP(0);
// TL.PL(%45,T);
// %3000 => EVAL.LIMIT;
// LB00059:
// 
// END
// $IF LBUFF[IPTR+2] /= LTHAN,-> LB00047;
    if (ITYPE_ne (LBUFF[IPTR + 2], LTHAN)) goto LB00047;
// $IF TAG OF LBUFF[IPTR+1] /= 1
    if (LBUFF[IPTR + 1].TAG != 1
// OR K OF PLIST[MUTLN(IND OF LBUFF[IPTR+1]) => CV] /= KVAR
        || PLIST[(CV = MUTLN (LBUFF[IPTR + 1].IND))].K != KVAR
// OR T OF PLIST[CV] => T /= TINT
        || ((T = PLIST[CV].T) != TINT
// AND T /= TBYADDR,-> LB00052;
          && T != TBYADDR)) goto LB00052;
// IF T = TINT32 THEN
    if (T == TINT32) {
// TL.C.LIT.32(T,0);
    TL_C_LIT_32 (T, 0);
// ELSE
    } else {
// TL.C.LIT.16(T,0);
    TL_C_LIT_16 (T, 0);
// FI
    }
// TL.CV.CYCLE(CV,0,%4);
    TL_CV_CYCLE (CV, 0, 0x4);
// 2 +> IPTR;
    IPTR += 2;
// TL.CV.LIMIT(EVAL.LIMIT(T));
    TL_CV_LIMIT (EVAL_LIMIT (T));
// LB00048:
    LB00048: ;
// 
// 8 => CON.STK[1 +> CON.PTR];
    CON_STK[++CON_PTR] = 8;
// IF LBUFF[1 +> IPTR] /= DDO,-> LB00052;
    if (ITYPE_ne (LBUFF[++IPTR], DDO)) goto LB00052;
//  -> LB00050;
    goto LB00050;
// 
// LB00047:
    LB00047: ;
// 
// TL.CYCLE(EVAL.LIMIT(TINT));
    TL_CYCLE (EVAL_LIMIT (TINT));
//  -> LB00048;
    goto LB00048;
// 
// LB00052:
    LB00052: ;
// 
// MONITOR(0);
    MONITOR (0);
// LB00050:
    LB00050: ;
// 
// END
}





#ifdef PDP
   // *CODE 1; // XXX
#endif // PDP




// $PR TRANS.GOTO;
void TRANS_GOTO (void) {
// $IN DEST.LAB,ID,K;
    INTEGER DEST_LAB, ID, K;
// $IF TAG $OF LBUFF[1+>IPTR] /= 1,-> LB00069;
    if (LBUFF[++IPTR].TAG != 1) goto LB00069;
// IND $OF LBUFF[IPTR] => ID;
    ID = LBUFF[IPTR].IND;
// MUTLN(ID) => DEST.LAB;
    DEST_LAB = MUTLN (ID);
// $IF K OF PLIST[DEST.LAB] => K = KGLAB
    if ((K = PLIST[DEST_LAB].K) == KGLAB
// OR K = KVAR AND T OF PLIST[DEST.LAB] = TLAB,-> LB00067;
        || (K == KVAR && PLIST[DEST_LAB].T == TLAB)) goto LB00067;
// IF DEST.LAB < CUR.BLK,-> LB00066;
    if (DEST_LAB < CUR_BLK) goto LB00066;
// $IF K < KLAB,-> LB00069;
    if (K < KLAB) goto LB00069;
// LB00067:
    LB00067: ;
// 
// IF K = KVAR THEN
    if (K == KVAR) {
//  TL.PL(%50, DEST.LAB)
     TL_PL (0x50, DEST_LAB);
// ELSE
    } else {
//  TL.PL(%4F, DEST.LAB) FI
     TL_PL(0x4F, DEST_LAB); }
//  -> LB00068;
    goto LB00068;
// 
// LB00066:
    LB00066: ;
// 
// DECLARE.N(ID,KLAB.REF,0) => DEST.LAB;
    DEST_LAB = DECLARE_N (ID, KLAB_REF, 0);
//  -> LB00067;
    goto LB00067;
// 
// LB00069:
    LB00069: ;
// 
// MONITOR(0);
    MONITOR (0);
// LB00068:
    LB00068: ;
// 
// $EN
}




#ifdef PDP
   // *CODE 3; // XXX
#endif // PDP



// $PR TRANS.SWITCH;
void TRANS_SWITCH (void) {
// $IN SWDV,N,ID;
    INTEGER SWDV, N, ID;
// TRANS.COMP(B.REQD);
    TRANS_COMP (B_REQD);
// DUMMY() => SWDV;
    SWDV = DUMMY ();
// TL.S.DECL(NIL,%2C,-1);
    TL_S_DECL (NIL, 0x2C, (ADDR) -1); // XXX
// TL.PL(%61, SWDV);
    TL_PL (0x61, SWDV);
// TL.PL(%64,0);
    TL_PL (0x64, 0);
// TL.PL(%4F,DIND);
    TL_PL (0x4F, DIND);
// TL.ASS(SWDV,-1);
    TL_ASS (SWDV, -1);
// $IF LBUFF[1+>IPTR] /= BSLASH,-> LB00083;
    if (ITYPE_ne (LBUFF[++IPTR], BSLASH)) goto LB00083;
// LB00076:
    LB00076: ;
// 
// $IF TAG $OF LBUFF[1+>IPTR] /= 1,-> LB00083;
    if (LBUFF[++IPTR].TAG != 1) goto LB00083;
// IND $OF LBUFF[IPTR] => ID;
    ID = LBUFF[IPTR].IND;
// $IF MUTLN(ID) => N < CURBLK,-> LB00079;
    if ((N = MUTLN(ID)) < CURBLK) goto LB00079;
// $IF K $OF PLIST[N] < KLAB,-> LB00083;
    if (PLIST[N].K < KLAB) goto LB00083;
// LB00080:
    LB00080:
// 
// TL.ASS.VALUE(N,1);
    TL_ASS_VALUE (N, 1);
// $IF LBUFF[IPTR+1] = EOS,-> LB00084;
    if (ITYPE_eq (LBUFF[IPTR+1], EOS)) goto LB00084;
// $IF LBUFF[1+>IPTR] = COMMA,-> LB00076;
    if (ITYPE_eq (LBUFF[++IPTR], COMMA)) goto LB00076;
// LB00083:
    LB00083: ;
// 
// MONITOR(128);
    MONITOR (128);
// LB00084:
    LB00084: ;
// 
// TL.ASS.END();
    TL_ASS_END ();
//  -> LB00085;
    goto LB00085;
// 
// LB00079:
    LB00079: ;
// 
// DECLARE.N(ID,KLAB.REF,0) => N;
    N = DECLARE_N (ID, KLAB_REF, 0);
//  -> LB00080;
    goto LB00080;
// 
// LB00085:
    LB00085: ;
// 
// $EX;
    return;
// $EN
  }




// $PR TRANS.ALT;
void TRANS_ALT (void) {
// $IN ALTDV;
    INTEGER ALTDV;
// BEGIN.ST();
    BEGIN_ST ();
// TRANS.COMP(B.REQD);
    TRANS_COMP (B_REQD);
// $IF LBUFF[1+>IPTR] /= DFROM,-> LB00094;
    if (ITYPE_ne (LBUFF[++IPTR], DFROM)) goto LB00094;
// DUMMY() => ALTDV;
    ALTDV = DUMMY ();
// TL.S.DECL(NIL,%2C,-1);
    TL_S_DECL (NIL, 0x2C, (ADDR) -1); // XXX
// TL.PL(%61,ALTDV);
    TL_PL (0x61, ALTDV);
// TL.PL(%64,0);
    TL_PL (0x64, 0);
// TL.PL(%4F,DIND);
    TL_PL (0x4F, DIND);
// TL.LABEL.SPEC(NIL,3);
    TL_LABEL_SPEC (NIL, 3);
// ALTDV => CON.STK[1 +> CON.PTR];
    CON_STK[++CON_PTR] = ALTDV;
// DUMMY() => CON.STK[1+>CON.PTR];
    CON_STK[++CON_PTR] = DUMMY ();
// 9 => CON.STK[1 +> CON.PTR];
    CON_STK[++CON_PTR] = 9;
//  -> LB00093;
    goto LB00093;
// 
// LB00094:
    LB00094: ;
// 
// MONITOR(0);
    MONITOR (0);
// LB00093:
    LB00093: ;
// 
// $EN
}




#ifdef PDP
    // *CODE 1; // XXX
#endif // PDP








// $PR TRANS.ELSE;
void TRANS_ELSE (void) {
// $IN END.LAB
    INTEGER END_LAB;
// $IF CON.STK[CON.PTR] /= 1,-> LB00103;
    if (CON_STK[CON_PTR] != 1) goto LB00103;
// DUMMY() => END.LAB;
    END_LAB = DUMMY ();
// TL.LABEL.SPEC(NIL,3);
    TL_LABEL_SPEC (NIL, 3);
// TL.PL(%4F,END.LAB);
    TL_PL (0x4F, END_LAB);
// TL.LABEL(CON.STK[CON.PTR-1]);
    TL_LABEL (CON_STK [CON_PTR - 1]);
// END.LAB => CON.STK[CON.PTR-1];
    CON_STK[CON_PTR - 1] = END_LAB;
// 2 => CON.STK[CON.PTR];
    CON_STK[CON_PTR] = 2;
//  -> LB00102;
    goto LB00102;
// 
// LB00103:
    LB00103: ;
// 
// MONITOR(10);
    MONITOR (10);
// LB00102:
    LB00102: ;
// 
// $EN
}




// $PR TRANS.FI;
void TRANS_FI (void) {
// ::DECL & INIT;
// $IF CON.STK[CON.PTR] /= 1 /= 2,-> LB00110;
  if (CON_STK[CON_PTR] != 1 && CON_STK[CON_PTR] != 2) goto LB00110;
// TL.LABEL(CON.STK[CON.PTR -1]);
    TL_LABEL (CON_STK [CON_PTR - 1]);
// 2 -> CON.PTR;
    CON_PTR -= 2;
//  -> LB00109;
    goto LB00109;
// 
// LB00110:
    LB00110: ;
// 
// MONITOR(10);
    MONITOR (10);
// LB00109:
    LB00109: ;
// 
// $EN
}




// $PR TRANS.OD;
void TRANS_OD (void) {
// ::DECL & INIT;
// $IF CON.STK[CON.PTR] /= 3,-> LB00118;
  if (CON_STK[CON_PTR] != 3) goto LB00118;
// TL.PL(%4F, CON.STK[CON.PTR -2]);
    TL_PL (0x4F, CON_STK [CON_PTR - 2]);
// TL.LABEL(CON.STK[CON.PTR-1]);
    TL_LABEL (CON_STK [CON_PTR - 1]);
// 3 -> CON.PTR;
   CON_PTR -= 3;
//  -> LB00117;
   goto LB00117;
// 
// LB00121:
    LB00121: ;
// 
// MONITOR(10);
    MONITOR (10);
// LB00118:
    LB00118: ;
// 
// IF CON.STK[CON.PTR] /= 8,-> LB00121;
    if (CON_STK[CON_PTR] != 8) goto LB00121;
// TL.REPEAT();
    TL_REPEAT ();
// 1 -> CON.PTR;
    CON_PTR -= 1;
// LB00117:
    LB00117: ;
// 
// $EN
}
// $PR TRANS.EXIT;
void TRANS_EXIT (void) {
// $IN I;
    INTEGER I;
// IF T OF PLIST[CURPROC] =>I /= 0 THEN
    if ((I = PLIST[CURPROC].T) != 0) {
// IF I & %8000 /= 0 THEN
      if ((I & 0x8000) != 0) {
// TADPROC => I;
        I = TADPROC;
// FI
      }
// TL.PL(%46,I);
      TL_PL (0x46, I);
// TL.PL(%22,CUR.RES);
      TL_PL(0x22, CUR_RES);
// AOPD=>I;
      I = AOPD;
// FI
    }
// TL.PL(%43,I);
    TL_PL (0x43, I);
// $EN
}




// $PR TRANS.END.ALT;
void TRANS_END_ALT (void) {
// $IN I,J;
    INTEGER I, J;
// $IF CON.STK[CON.PTR] /= 10,-> LB00132;
    if (CON_STK[CON_PTR] != 10) goto LB00132;
// TL.ASS(CON.STK[CON.PTR-2],-1);
    TL_ASS (CON_STK [CON_PTR - 2], -1);
// CON.PTR => J;
    J = CON_PTR;
// $WH CON.STK[CON.PTR] >= 10 $DO
    while (CON_STK[CON_PTR] >= 10) {
// 4 -> CON.PTR;
      CON_PTR -= 4;
// $OD;
    }
// CONPTR => I;
    I = CONPTR;
// $WH I < J $DO
    while (I < J) {
// TL.ASS.VALUE(CONSTK[I+1],1);
      TL_ASS_VALUE (CONSTK[I + 1], 1);
// 4 +> I;
      I += 4;
// $OD
    }
// TL.ASS.END();
    TL_ASS_END ();
// LB00129:
    LB00129: ;
// 
// TL.LABEL(CON.STK[CON.PTR -1]);
    TL_LABEL (CON_STK[CON_PTR - 1]);
// 3 -> CON.PTR;
    CON_PTR -= 3;
//  -> LB00131;
    goto LB00131;
// 
// LB00132:
    LB00132: ;
// 
// MONITOR(128);
    MONITOR (128);
//  -> LB00129;
    goto LB00129;
// 
// LB00131:
    LB00131: ;
// 
// $EN
}



// $PR ALT.LABEL;
void ALT_LABEL (void) {
// ::DECL & INIT;
// $IF LBUFF[1] = DEND,-> LB00139;
    if (ITYPE_eq (LBUFF[1], DEND)) goto LB00139;
// $IF CON.STK[CON.PTR] /= 10,-> LB00138;
    if (CON_STK[CON_PTR] != 10) goto LB00138;
// TL.PL(%4F,CON.STK[CON.PTR -1]);
    TL_PL(0x4F,CON_STK[CON_PTR - 1]);
// LB00138:
    LB00138: ;
// 
// TL.LABEL.SPEC(NIL,3);
    TL_LABEL_SPEC (NIL, 3);
// DUMMY() => CONSTK[1+>CONPTR];
    CONSTK[++CONPTR] = DUMMY ();
// TL.LABEL(LASTMN);
    TL_LABEL (LASTMN);
// CON.STK[CON.PTR-3] => CON.STK[1+>CON.PTR];
    CON_STK[CON_PTR + 1] = CON_STK [CON_PTR - 3]; ++CON_PTR;
// CON.STK[CON.PTR-3] => CON.STK[1+>CON.PTR];
    CON_STK[CON_PTR + 1] = CON_STK [CON_PTR - 3]; ++CON_PTR;
// 10 => CON.STK[1 +> CON.PTR];
    CON_STK[++CON_PTR] = 10;
// LB00139:
    LB00139: ;
// 
// $EN
}

#ifdef PDP
    // *CODE 3; // XXX
#endif // PDP




// $PR TRANS.COND(IF.OR.UN,DEST.LAB);
static void TRANS_COND (INTEGER IF_OR_UN, INTEGER DEST_LAB) {
// ::DECL & INIT
// 0 => AP => FPTR;
    FPTR = AP = 0;
// PREPROCESS.COND();
    PREPROCESS_COND ();
// IF CWORD & %10 /= 0 THEN
    if ((CWORD & 0x10) != 0) {
// PRINT.AR()
      PRINT_AR ();
// FI
    }
// 1 -> IPTR;
    -- IPTR;
// 0 => AP => REGS.IU => ST.PTR;
    ST_PTR = REGS_IU = AP = 0;
// COMPILE.COND(IF.OR.UN,DEST.LAB,0);
    COMPILE_COND (IF_OR_UN, DEST_LAB, 0);
// $EN
}

#ifdef PDP
    // *CODE 1; // XXX
#endif // PDP
// $PS INIT.S6();
// $PR INIT.S6;
void INIT_S6 (void) {
// END
}
// *END
