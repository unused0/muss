//
// nmsl061
//

//MODULE(TRANS.COMP,TRANS.IF,TRANS.WHILE,
//TRANS.FOR,TRANS.GOTO,TRANS.SWITCH,
//TRANS.ALT,TRANS.ELSE,TRANS.FI,
//TRANS.OD,TRANS.EXIT,TRANS.END.ALT,
//ALT.LABEL,INIT.S6);

// $PS TRANS.COMP($IN)/$IN;
extern INTEGER TRANS_COMP (INTEGER);
// $PS TRANS.IF();
extern void TRANS_IF (void);
// $PS TRANS.WHILE();
extern void TRANS_WHILE (void);
// $PS TRANS.FOR();
extern void TRANS_FOR (void);
// $PS TRANS.GOTO();
extern void TRANS_GOTO (void);
// $PS TRANS.SWITCH();
extern void TRANS_SWITCH (void);
// $PS TRANS.ALT();
extern void TRANS_ALT (void);
// $PS TRANS.ELSE();
extern void TRANS_ELSE (void);
// $PS TRANS.FI();
extern void TRANS_FI (void);
// $PS TRANS.OD();
extern void TRANS_OD (void);
// $PS TRANS.EXIT();
extern void TRANS_EXIT (void);
// $PS TRANS.END.ALT();
extern void TRANS_END_ALT (void);
// $PS ALT.LABEL();
extern void ALT_LABEL (void);
// $PS INIT.S6();
extern void INIT_S6 (void);

