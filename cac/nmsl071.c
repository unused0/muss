#include <stddef.h>
#include "muss.h"
#include "kernel.h"
#include "lib.h"
#include "libm.h"
#include "mutl.h"
#include "nmsl.h"


// 
// ::COMPUTATION PROCESSING SECTION;
// ::EXTERNAL ENVIRONMENT
// $LS SELECT.OUTPUT($IN);
// $LS NEWLINES($IN);
// $LS OUTHEX($LO32,$IN);
// $LS SPACES($IN);
// $LS CAPTION(ADDR[$LO8]);
// $LS TL.ASS($IN,$IN);
// $LS TL.ASS.VALUE($IN,$IN);
// $LS TL.C.LIT.16($IN,$IN16);
// $LS TL.C.LIT.S($IN,ADDR[$LO8]);
// $LS TL.ASS.END();
// $LS TL.PL($IN,$IN);
// $LS TL.REG($IN);
// $LS TL.MAKE($IN,$IN,ADDR);
// $LS TL.D.TYPE($IN,ADDR);
// $LS TL.LABEL.SPEC(ADDR[$LO8],$IN);
// $LS TL.LABEL($IN);
// $LS TL.SELECT.VAR();
// $LS TL.SELECT.FIELD($IN,$IN,$IN);
// $LS TL.S.DECL(ADDR[$LO8],$IN,ADDR);
// $IM $LI ILOAD,LBUFFZ,SDLIST,PLISTZ,PARAMSZ;
// $TY ITYPE IS $LO8 TAG,ST $LO16 IND;
// ITYPE[LBUFFZ] LBUFF;
// $IN MSTR,MUTLSTR,IPTR,CURLEV,GLEV,TINT,TRE,TBYADDR,CURPROC,CURRES;
// $LO16[SDLIST] DLIST;
// $TY PLIST.ENT IS $LO16 INTID,PREVN,K,T $IN32 DETAIL;
// PLIST.ENT[PLISTZ] PLIST;
// $IN [PARAMSZ] PARAMS;
// $IM $LI TADPROC,KVAR,TLO0,TRE0,CLISTZ,TINT0,TINT64;
// $LO8 [CLISTZ] CLIST;
// $IM $LI KSPACE,ICOMP,TNAME,TCONST,KPROC,KFIELD,TLISTZ;
// $IN [TLISTZ] TLIST;
// $IM $LI TSTRING,STRING.TYPE,TDELIM,TLAB,TLO32,IBYTE,KPSPEC,TLO8;
// $LI/ADDR[$LO8] NIL =;
// $IN CMODE;
// $PS MONITOR($IN);
// $PS DUMMY()/$LO16;
// $PS MUTLN($LO16)/$LO16;
// $PS ADD.LSPEC($LO16)/$LO16;
// $PS COMP.TYPE()/$IN;
// $IM $LI IMIN.COMP,ISTORE,IEOS;
// ITYPE MINUS,DIF,DTHEN,DELSE,LB,RB,COMMA,LSB,DOF,RSB,EOS,DEREF,REF;
// MODULE(PREPROCESS.COMP,PREPROCESS.COND,COMPILE.COMP,COMPILE.COND,COMPILE.AND,
//       COMPILE.TEST,PRINT.AR,AR,AP,REGS.IU,REG.TYPE,FPTR,STPTR,AOPD,DIND,B.REQD);



// ::TYPE DECLARATIONS
// *GLOBAL 5;
    // *GLOBAL 5; // XXX

// $LI REF.BIT = %80,VEC =16,DREG=4,AREG=2,BREG=1,A.B.D=7,B.D=5;
#define REF_BIT 0x80
#define VEC 16
#define DREG 4
#define AREG 2
#define BREG 1
#define A_B_D 7
#define B_D 5

// $LI/ITYPE DAND= 0\0\%2B, DOR= 0\0\%2C;
// $LI B.REQD = -1,AOPD=%3000,BOPD=%2000,STK.SEL=%1005,UNSTK=%1003,DIND=%1004,CLIT=0;
#define B_OPD 0x2000
#define BOPD B_OPD
#define STK_SEL 0x1005
#define UNSTK 0x1003
#define CLIT 0

#ifdef PDP
    // *CODE 2 // XXX
#endif // PDP

    // *GLOBAL 1; // XXX

// $DA REV.OP($LO8)
// 51 52 56 54 57 53 55 58 59 60
// $EN
   static LOGICAL8 REV_OP [] = {
       51, 52, 56, 54, 57, 53, 55, 58, 59, 60
    };
// $DA TL.FN($LO8)
// 2 8 9 11 12 10 13 4 5 3 0 24 26
// 27 29 25 28 20 21 19 15 7 6
// $EN
    static LOGICAL8 TL_FN [] = {
      2, 8, 9, 11, 12, 10, 13, 4, 5, 3, 0, 24, 26,
      27, 29, 25, 28, 20, 21, 19, 15, 7, 6
    };

// $DA TL.TST.FN($LO8)
// 11 13 14 12 10  9
// $EN
    static LOGICAL8 TL_TST_FN [] = {
      11, 13, 14, 12, 10,  9
    };

// $DA REV.COMP($LO8)
//  9 10 13 14 11 12
// $EN
    static LOGICAL8 REV_CMP [] = {
      9, 10, 13, 14, 11, 12
    };

// $DA INV.TST($LO8)
// 10  9 12 11 14 13
// $EN
    static LOGICAL8 INV_TST [] = {
      10,  9, 12, 11, 14, 13
    };

    // *GLOBAL 5; // XXX

// $LO16 [256] AR;$IN AP;
    LOGICAL16 AR[256]; INTEGER AP;

// $IN REGS.IU,ATYPE,BTYPE,DTYPE,LAST.REG;
    INTEGER REGS_IU;
    static INTEGER A_TYPE, B_TYPE, D_TYPE, LAST_REG;
#define ATYPE A_TYPE
#define BTYPE B_TYPE
#define DTYPE D_TYPE
// $LO8 [16] FIELDS;$IN FPTR;
    static LOGICAL8 FIELDS [16]; INTEGER FPTR;
// $LO16 [16] STACK.TYPE;$IN ST.PTR;
    static LOGICAL16 STACK_TYPE[16]; INTEGER ST_PTR;
// $PS PREPROCESS.COMP($IN)/$IN;
    extern INTEGER PREPROCESS_COMP (INTEGER);
// $PS PREPROCESS.COND();
    extern void PREPROCESS_COND (void);
// $PS COMPILE.COMP($IN,$IN)/$IN;
    extern INTEGER COMPILE_COMP (INTEGER, INTEGER);
// $PS COMPILE.COND($IN,$IN,$IN);
    extern void COMPILE_COND (INTEGER, INTEGER, INTEGER);
// $PS COMPILE.AND($IN,$IN,$IN);
    extern void COMPILE_AND (INTEGER, INTEGER, INTEGER);
// $PS COMPILE.TEST($IN,$IN,$IN);
    extern void COMPILE_TEST (INTEGER, INTEGER, INTEGER);
// $PS STACK.REGS($IN);
    static void STACK_REGS (INTEGER);
// $PS UNSTACK.REGS($IN);
    static void UNSTACK_REGS (INTEGER);
// $PS REG.FROM.TYPE($IN,$IN)/$IN;
    static INTEGER REG_FROM_TYPE (INTEGER, INTEGER); 
// $PS CHECK.TYPE($IN,$IN,$IN)/$IN;
    static INTEGER  CHECK_TYPE (INTEGER, INTEGER, INTEGER);
// $PS PRINT.AR();
    extern void PRINT_AR (void);

#ifdef PDP
    // *CODE 3; // XXX
#endif // PDP



// $PR PREPROCESS.COMP(PTYPE);
    INTEGER PREPROCESS_COMP (INTEGER PTYPE) {
    INTEGER ret;
// ::DECL & INIT
// $IN OP,I,J,F,U,N,MSLN,MTLN;
    INTEGER OP,I,J,F,U,N,MSLN,MTLN;
// $IN OK.AP,INIT.AP,PE.AP,PS.AP,DL.OP,DL.PREV;
    INTEGER OK_AP,INIT_AP,PE_AP,PS_AP,DL_OP,DL_PREV;
// $IN REFR,USES.B,USES.D,COMP.KIND,SINGLE.OPD;
    INTEGER REFR,USES_B,USES_D,COMP_KIND,SINGLE_OPD;
// $IN CMP.TYPE,OPD.TYPE,RTYPE;
    INTEGER CMP_TYPE,OPD_TYPE,RTYPE;
// 0 => CMP.TYPE => COMP.KIND => REFR;
    REFR = COMP_KIND = CMP_TYPE = 0;
// %80 => SINGLE.OPD;
    SINGLE_OPD = 0x80;
// AP => INIT.AP + 1 => PE.AP + 1 => AP;
    AP = (PE_AP = (INIT_AP = AP) + 1) + 1;
// ILOAD => OP;
    OP = ILOAD;
// 0 => DL.OP => DL.PREV;
    DL_PREV = DL_OP = 0;
// IF LBUFF[IPTR+1] = REF,-> LB00022;
    if (ITYPE_eq (LBUFF [IPTR+1], REF)) goto LB00022;
// LB00014:
    LB00014: ;
// 
// BEGIN
    {
// ::PREPROCESS OPERAND
// 0 => USES.B => USES.D;
      USES_D = USES_B = 0;
// AP => OK.AP + 1 => AP;
      AP = (OK_AP = AP) + 1;
// IF TAG OF LBUFF[IPTR+1] => I = TNAME,-> LB00049;
      if ((I = LBUFF[IPTR+1].TAG) == TNAME) goto LB00049;
// IF REFR /= 0 OR DL.OP & %4 /= 0,-> LB00044;
      if (REFR != 0 || (DL_OP & 0x4) != 0) goto LB00044;
// IF I = TCONST,-> LB00045;
      if (I == TCONST) goto LB00045;
// IF LBUFF[IPTR+1] /= MINUS
      if (ITYPE_ne (LBUFF[IPTR + 1], MINUS)
// OR SINGLE.OPD = 0,-> LB00062;
          || SINGLE_OPD == 0) goto LB00062;
// IF TAG OF LBUFF[1+>IPTR+1] /= TCONST
      if (LBUFF[(++IPTR) + 1].TAG != TCONST
// OR ST OF LBUFF[IPTR+1] ->> 6 = 1,-> LB00047;
          || (LBUFF[IPTR+1].ST >> 6) == 1) goto LB00047;
// 2 => AR[OK.AP]
      AR[OK_AP] = 2;
// LB00046:
      LB00046: ;
// 
// ::PREPROCESS LITERAL
// ST OF LBUFF[1+>IPTR] => OPD.TYPE <<- 4 !> AR[OK.AP];
      AR[OK_AP] |= (OPD_TYPE = LBUFF[++IPTR].ST) << 4;
// IF OPD.TYPE & %C0 = TRE0 THEN
      if ((OPD_TYPE & 0xC0) == TRE0) {
// TRE => OPD.TYPE FI
        OPD_TYPE = TRE; }
// IND OF LBUFF[IPTR] => AR[AP];
      AR[AP] = LBUFF[IPTR].IND;
// 1 +> AP;
      ++ AP;
// ::END 7.1.1.1
// LB00042:
      LB00042: ;
// 
// IF OPD.TYPE & %8000 /= 0 THEN
      if ((OPD_TYPE & 0x8000) != 0) {
// TADPROC => OPD.TYPE FI
        OPD_TYPE = TADPROC; }
//  -> LB00043;
      goto LB00043;
// 
// LB00040:
      LB00040: ;
// 
// $IF LBUFF[1+>IPTR+1] = DIF,-> LB00048;
      if (ITYPE_eq (LBUFF[(++IPTR)+1], DIF)) goto LB00048;
// BEGIN
      {
// ::PREPROCESS SUBEXPRESSION
// PREPROCESS.COMP(0) => OPD.TYPE;
        OPD_TYPE = PREPROCESS_COMP (0);
// IF LBUFF[IPTR] /= RB,-> LB00209;
        if (ITYPE_ne (LBUFF[IPTR], RB)) goto LB00209;
// 5 => AR[OK.AP]
        AR[OK_AP] = 5;
//  -> LB00208;
        goto LB00208;
// 
// LB00209:
        LB00209: ;
// 
// MONITOR(0);
        MONITOR (0);
// LB00208:
        LB00208: ;
// 
// END::7.1.1.5
      }
//  -> LB00042;
      goto LB00042;
// 
// LB00044:
      LB00044: ;
// 
// MONITOR(6);
      MONITOR (6);
// LB00047:
      LB00047: ;
// 
// MONITOR(4);
      MONITOR (4);
// LB00048:
      LB00048: ;
// 
// BEGIN
      {
// ::PREPROCESS CONDITIONAL COMPUTATION
// 1 +> IPTR;
        ++IPTR;
//    PREPROCESS.COND();
        PREPROCESS_COND();
// IF LBUFF[IPTR] /= DTHEN,-> LB00202;
        if (ITYPE_ne (LBUFF[IPTR], DTHEN)) goto LB00202;
// PREPROCESS.COMP(0) => OPD.TYPE;
        OPD_TYPE = PREPROCESS_COMP (0);
// IF LBUFF[IPTR] /= DELSE,-> LB00202;
        if (ITYPE_ne (LBUFF[IPTR], DELSE)) goto LB00202;
// PREPROCESS.COMP(0) => J;
        J = PREPROCESS_COMP (0);
// IF LBUFF[IPTR] /= RB,-> LB00202;
        if (ITYPE_ne (LBUFF[IPTR], RB)) goto LB00202;
// IF OPD.TYPE /= J,-> LB00203;
        if (OPD_TYPE != J) goto LB00203;
// 6 => AR[OK.AP];
        AR[OK_AP] = 6;
//  -> LB00201;
        goto LB00201;
// 
// LB00202:
        LB00202: ;
// 
// MONITOR(0);
        MONITOR (0);
// LB00203:
        LB00203: ;
// 
// MONITOR(3);
        MONITOR (3);
// LB00201:
        LB00201: ;
// 
// END::7.1.1.4
      }
//  -> LB00042;
      goto LB00042;
// 
// LB00049:
      LB00049: ;
// 
// 3 => U;
      U = 3;
// IND OF LBUFF[IPTR+1] => MSLN;
      MSLN = LBUFF[IPTR+1].IND;
// IF LBUFF[IPTR+2] /= LB,-> LB00055;
      if (ITYPE_ne (LBUFF[IPTR+2], LB)) goto LB00055;
// LB00050:
      LB00050:
// 
// 2 +> IPTR
      IPTR += 2;
// IF REFR /= 0 OR DL.OP & %1004 /= 0,-> LB00058;
      if (REFR != 0 || (DL_OP & 0x1004) != 0) goto LB00058;
// BEGIN
      {
// ::PREPROCESS PROC CALL
// IF MUTLN(MSLN)
// => MTLN /= 0,-> LB00175;
        if ((MTLN = MUTLN (MSLN)) != 0) goto LB00175;
// IF U = 9 OR CMODE & %40 /= 0
        if (U == 9 || (CMODE & 0x40) != 0
// OR ADD.LSPEC(MSLN) => MTLN = 0,-> LB00173;
            || (MTLN = ADD_LSPEC (MSLN)) == 0) goto LB00173;
// 4 => U;
        U = 4;
// MTLN => AR[AP];
        AR[AP] = MTLN;
// LB00166:
        LB00166: ;
// 
// T OF PLIST[MTLN] => RTYPE;
        RTYPE = PLIST[MTLN].T;
// PARAMS[DETAIL OF PLIST[MTLN] => F] => N;
        N = PARAMS[(F = PLIST[MTLN].DETAIL)];
// U => AR[OK.AP];
        AR[OK_AP] = U;
// RTYPE => AR[AP+1];
        AR[AP + 1] = RTYPE;
// N => AR[3+>AP-1];
        AR[(AP += 3) - 1] = N;
// BEGIN
        {
// ::PREPROCESS ACTUAL PARAMETERS
// 1 => I;
          I = 1;
// $IF N = 0,-> LB00189;
          if (N == 0) goto LB00189;
// LB00184:
          LB00184:
// 
// PARAMS[F+I]=>J;
          J = PARAMS[F + I];
// IF J & %8000 /= 0 THEN
          if ((J & 0x8000) != 0) {
// TADPROC => J FI
            J = TADPROC; }
// PREPROCESS.COMP(J);
          PREPROCESS_COMP (J);
// IF 1 +> I > N,-> LB00190;
          if ((++I) > N) goto LB00190;
// $IF LBUFF[IPTR] = COMMA,-> LB00184;
          if (ITYPE_eq (LBUFF[IPTR], COMMA)) goto LB00184;
// LB00188:
          LB00188: ;
// 
// MONITOR(11);
          MONITOR (11);
// LB00189:
          LB00189: ;
// 
// 1 +> IPTR;
          ++ IPTR;
// LB00190:
          LB00190: ;
// 
// IF LBUFF[IPTR] /= RB,-> LB00188;
          if (ITYPE_ne (LBUFF[IPTR], RB)) goto LB00188;
// END::7.1.1.3.1
        }
// RTYPE => OPD.TYPE;
        OPD_TYPE = RTYPE;
// %170 !> COMP.KIND;
        COMP_KIND |= 0x170;
// IF RTYPE /= 0,-> LB00172;
        if (RTYPE != 0) goto LB00172;
// IF CMP.TYPE /= 0
        if (CMP_TYPE != 0
// OR LBUFF[IPTR+1] /= EOS,-> LB00174;
            || ITYPE_ne (LBUFF[IPTR + 1], EOS)) goto LB00174;
//  -> LB00172;
        goto LB00172;
// 
// LB00173:
        LB00173: ;
// 
// MONITOR(6);
        MONITOR (6);
// LB00174:
        LB00174: ;
// 
// MONITOR(10);
        MONITOR (10);
// LB00176:
        LB00176: ;
// 
// IF I /= KPROC
        if (I != KPROC
// AND I /= KPSPEC,-> LB00180;
            && I != KPSPEC) goto LB00180;
// ::OPERAND KIND SET
// ::IN MSL07.1.1 BOX 17
// MTLN => AR[AP];
        AR[AP] = MTLN;
//  -> LB00166;
        goto LB00166;
// 
// LB00180:
        LB00180: ;
// 
// MONITOR(10);
        MONITOR (10);
// LB00175:
        LB00175: ;
// 
// K OF PLIST[MTLN] => I;
        I = PLIST[MTLN].K;
// IF U = 9,-> LB00178;
        if (U == 9) goto LB00178;
//  -> LB00176;
        goto LB00176;
// 
// LB00178:
        LB00178: ;
// 
// IF I /= KVAR /= KFIELD
        if ((I != KVAR && I != KFIELD)
// OR T OF PLIST[MTLN] => I
            || ((I = PLIST[MTLN].T)
// & %C000 /= %8000,-> LB00180;
                & 0xC000) != 0x8000) goto LB00180;
// MTLN => AR[AP];
        AR[AP] = MTLN;
// I & %3FFF => MTLN;
        MTLN = I & 0x3FFF;
//  -> LB00166;
        goto LB00166;
// 
// LB00172:
        LB00172: ;
// 
// END::7.1.1.3
      }
//  -> LB00042;
      goto LB00042;
// 
// LB00052:
      LB00052: ;
// 
// IF LBUFF[IPTR+1] = LB,-> LB00040;
      if (ITYPE_eq (LBUFF[IPTR+1], LB)) goto LB00040;
// IF I /= TDELIM
      if (I != TDELIM
// OR DLIST[IND OF LBUFF[IPTR+1] => I] & %400 = 0,-> LB00059;
          || (DLIST[I = LBUFF[IPTR + 1].IND] & 0x400) == 0) goto LB00059;
// BEGIN
      {
// ::PREPROCESS BUILT IN FUNCTION
// $DA BIF.START($LO8)
// 0 2 6 9 13 15 16 19
// END
        LOGICAL8 BIF_START [] = {
         0, 2, 6, 9, 13, 15, 16, 19
        };
// $DA BIF.ACTIONS($LO8)
// 9 0       ::BYTE
// 2 10 7 0  ::MAKE
// 6 8 1     ::POSN
// 3 5 5 0   ::PART
// 4 0       ::SIZE
// 1         ::LLST
// 2 5 1     ::LPST
// 12 13 0   ::LENT
// END
        LOGICAL8 BIF_ACTIONS [] = {
          9, 0,         // BYTE
          2, 10, 7, 0,  // MAKE
          6, 8, 1,      // POSN
          3, 5, 5, 0,   // PART
          4, 0,         // SIZE
          1,            // LLST
          2, 5, 1,      // LPST
          12, 13, 0     // LENT
        };
// $DA BIF.REG.USE($LO8)
// %60 %50 %20 %70 %60 0 %60 %60
// END
        LOGICAL8 BIF_REG_USE [] = {
          0x60, 0x50, 0x20, 0x70, 0x60, 0, 0x60, 0x60
        };
// I - IBYTE => AR[AP] => U;
        U = AR[AP] = I - IBYTE;
// 1 +> AP;
        ++ AP;
// BIF.REG.USE[U] !> COMP.KIND;
        COMP_KIND |= BIF_REG_USE[U];
// IF LBUFF[1+>IPTR+1] /= LB,-> LB00218;
        if (ITYPE_ne (LBUFF[(++IPTR)+1], LB)) goto LB00218;
// 1 +> IPTR;
        ++ IPTR;
// ::PREPROCESS FUNCTION PARAMETERS
// BIF.START[U] => I;
        I = BIF_START[U];
// LB00221:
        LB00221: ;
// 
// SWITCH BIF.ACTIONS[I] => N
// \ A0,A1,A2,A3,A4,A5,A6,
// A7,A8,A9,A10,A11,A12,A13;
        switch (N = BIF_ACTIONS[I]) {
          case 0: goto A0;
          case 1: goto A1;
          case 2: goto A2;
          case 3: goto A3;
          case 4: goto A4;
          case 5: goto A5;
          case 6: goto A6;
          case 7: goto A7;
          case 8: goto A8;
          case 9: goto A9;
          case 10: goto A10;
          case 11: goto A11;
          case 12: goto A12;
          case 13: goto A13;
        }
// A0:
        A0: ;
// LB00223:
        LB00223: ;
// 
// IF LBUFF[IPTR] /= RB,-> LB00231;
        if (ITYPE_ne (LBUFF[IPTR], RB)) goto LB00231;
//  -> LB00224;
        goto LB00224;
// 
// A3:
        A3: ;
// AP => F + 2 => AP;
        AP = (F = AP) + 2;
// LB00226:
        LB00226: ;
// 
// PREPROCESS.COMP(0) => RTYPE;
        RTYPE = PREPROCESS_COMP (0);
// IF RTYPE > %4000
        if (RTYPE > 0x4000
// OR RTYPE & 3 /= 3,-> LB00232;
            || (RTYPE & 3) != 3) goto LB00232;
// IF N = 4,-> LB00230;
        if (N == 4) goto LB00230;
// RTYPE => OPD.TYPE
        OPD_TYPE = RTYPE;
// LB00230:
        LB00230: ;
// 
// -> NEXT.ACTION;
        goto NEXT_ACTION;
// LB00231:
        LB00231: ;
// 
// MONITOR(0);
        MONITOR (0);
// LB00232:
        LB00232: ;
// 
// MONITOR(3);
        MONITOR (3);
// NEXT.ACTION:
        NEXT_ACTION: ;
// IF LBUFF[IPTR] /= COMMA /= RB,-> LB00231;
        if (ITYPE_ne (LBUFF[IPTR], COMMA) && ITYPE_ne (LBUFF[IPTR], RB)) goto LB00231;
// 1 +> I;
        ++ I;
//  -> LB00221;
        goto LB00221;
// 
// A1:
        A1: ;
// 0 => OPD.TYPE;
        OPD_TYPE = 0;
// IF U = 5 THEN
        if (U == 5) {
// 1 +> IPTR FI
          ++ IPTR; }
// IF SINGLE.OPD /= 0
        if (SINGLE_OPD != 0
// AND LBUFF[IPTR+1] = EOS,-> LB00223;
             && ITYPE_eq (LBUFF[IPTR+1], EOS)) goto LB00223;
// MONITOR(10);
        MONITOR (10);
// A4:
        A4: ;
// TINT => OPD.TYPE;
        OPD_TYPE = TINT;
//  -> LB00226;
        goto LB00226;
// 
// LB00245:
        LB00245: ;
// 
// IF LBUFF[IPTR+1] /= COMMA,-> LB00231;
        if (ITYPE_ne (LBUFF[IPTR+1], COMMA)) goto LB00231;
// 1 +> IPTR;
        ++ IPTR;
// -> NEXT.ACTION;
        goto NEXT_ACTION;
// A2:
        A2: ;
// COMP.TYPE() => AR[AP] => RTYPE
// ! 3 => OPD.TYPE;
        OPD_TYPE = (RTYPE = AR[AP] = COMP_TYPE ()) | 3;
// 1 +> AP;
        ++ AP;
// IF U /= 6 AND
        if (U != 6 &&
// RTYPE & 3 /= 0,-> LB00232;
           (RTYPE & 3) != 0) goto LB00232;
//  -> LB00245;
        goto LB00245;
// 
// A5:
        A5: ;
// IF U = 3 THEN
        if (U == 3) {
// AP => AR[F];
          AR[F] = AP;
// 1 +> F;
          ++ F;
// FI
        }
// AP => N;
        N = AP;
// IF U /= 6 THEN
        if (U != 6) {
// TINT => J ELSE 0 => J FI
          J = TINT; } else { J = 0; }
// PREPROCESS.COMP(J) => RTYPE;
        RTYPE = PREPROCESS_COMP (J);
// IF U = 6 THEN
        if (U == 6) {
// %100 !> AR[N] FI
        AR[N] |= 0x100; }
// -> NEXT.ACTION;
        goto NEXT_ACTION;
// LB00224:
        LB00224:
// 
// ::END 7.1.1.6.1
// ->AROUND;
        goto AROUND;
// A10:
        A10: ;
// IF TAG OF LBUFF[IPTR+1] /= TCONST
        if (LBUFF[IPTR+1].TAG != TCONST
// OR ST OF LBUFF[IPTR+1] /= TLO8
            || LBUFF[IPTR+1].ST != TLO8
// OR CLIST[IND OF LBUFF[IPTR+1]] /= 0
            || CLIST[LBUFF[IPTR+1].IND] != 0
// OR LBUFF[IPTR+2] /= COMMA,-> LB00285;
            || ITYPE_ne (LBUFF[IPTR+2], COMMA)) goto LB00285;
// 2 +> IPTR;
        IPTR += 2;
// %80 !> AR[OK.AP+1];
        AR[OK_AP+1] |= 0x80;
// %FFFD &> OPD.TYPE;
        OPD_TYPE &= 0xfffd;
// ->NEXT.ACTION;
        goto NEXT_ACTION;
// LB00285:
        LB00285: ;
// 
// ->A5;
        goto A5;
// LB00260:
        LB00260: ;
// 
// IF U = 1,-> LB00264;
        if (U == 1) goto LB00264;
// MONITOR(7);
        MONITOR (7);
// LB00262:
        LB00262: ;
// 
// IF U = 1,-> LB00264;
        if (U == 1) goto  LB00264;
// MONITOR(0);
        MONITOR (0);
// LB00264:
        LB00264: ;
// 
// %40 !> AR[OK.AP+1];
        AR[OK_AP+1] |= 0x40;
// ->A8;
        goto A8;
// A13:
        A13: ;
// IF TAG OF LBUFF[IPTR+1] /= TCONST
        if (LBUFF[IPTR+1].TAG != TCONST
// OR ST OF LBUFF[IPTR+1] /= TLO8
            || LBUFF[IPTR+1].ST != TLO8
// OR CLIST[IND OF LBUFF[IPTR+1]] /= 0 THEN
            || CLIST[LBUFF[IPTR+1].IND] != 0) {
// COMP.TYPE() => OPD.TYPE;
          OPD_TYPE = COMP_TYPE ();
// 1 +> IPTR;
          ++ IPTR;
// ELSE
         } else {
// 0 => OPD.TYPE;
          OPD_TYPE = 0;
// 2 +> IPTR;
          IPTR += 2;
// FI
        }
// ->NEXT.ACTION;
        goto NEXT_ACTION;
// A6:
        A6: ;
// LB00252:
        LB00252: ;
// 
// IF TAG OF LBUFF[IPTR+1] /= TNAME,-> LB00262;
        if (LBUFF[IPTR+1].TAG != TNAME) goto LB00262;
// IND OF LBUFF[IPTR+1] => MSLN;
        MSLN = LBUFF[IPTR+1].IND;
// IF MUTLN(MSLN) => MTLN = 0,-> LB00259;
        if ((MTLN = MUTLN(MSLN)) == 0) goto LB00259;
// IF MTLN < CURLEV >= GLEV,-> LB00259;
        if (MTLN < GLEV  && GLEV >= CURLEV) goto LB00259;
// IF K OF PLIST[MTLN] /= KSPACE,-> LB00260;
        if (PLIST[MTLN].K != KSPACE) goto LB00260;
// IF LBUFF[2 +> IPTR]
// /= COMMA /= RB,-> LB00262;
#if 0 // XXX
        if (ITYPE_ne (LBUFF[IPTR += 2], COMMA) &&
            ITYPE_ne (LBUFF[IPTR += 2], RB)) goto LB00262;
#else
        if (ITYPE_ne (LBUFF[IPTR += 2], COMMA) &&
            ITYPE_ne (LBUFF[IPTR], RB)) goto LB00262;
#endif
// LB00257:
        LB00257: ;
// 
// MTLN => AR[AP];
        AR[AP] = MTLN;
// 1 +> AP;
        ++ AP;
// -> NEXT.ACTION;
        goto NEXT_ACTION;
//  -> LB00262;
        goto LB00262;
// 
// LB00259:
        LB00259: ;
// 
// MONITOR(2);
        MONITOR (2);
// A9:
        A9: ;
// PREPROCESS.COMP(0) => RTYPE;
        RTYPE = PREPROCESS_COMP(0);
// TBYADDR => OPD.TYPE;
        OPD_TYPE = TBYADDR;
// IF RTYPE & 3 = 0
        if ((RTYPE & 3) == 0
// AND RTYPE /= TADPROC
            && RTYPE != TADPROC
// AND RTYPE /= TLAB,-> LB00267;
            && RTYPE != TLAB) goto LB00267;
// ->NEXT.ACTION;
        goto NEXT_ACTION;
// LB00267:
        LB00267: ;
// 
// MONITOR(3);
        MONITOR (3);
// A7:
        A7: ;
// IF LBUFF[IPTR] /= RB,-> LB00252;
        if (ITYPE_ne (LBUFF[IPTR], RB)) goto LB00252;
// 0 => MTLN;
        MTLN = 0;
//  -> LB00257;
        goto LB00257;
// 
// A12:
        A12: ;
// PREPROCESS.COMP(TLO32);
        PREPROCESS_COMP (TLO32);
// ->NEXT.ACTION;
        goto NEXT_ACTION;
// A8:
        A8: ;
// PREPROCESS.COMP(TBYADDR);
        PREPROCESS_COMP (TBYADDR);
// -> NEXT.ACTION;
        goto NEXT_ACTION;
// A11:
        A11: ;
// TINT => OPD.TYPE;
        OPD_TYPE = TINT;
// 1+>IPTR;
        ++ IPTR;
// ->NEXT.ACTION;
        goto NEXT_ACTION;
// AROUND:
        AROUND: ;
// 7 => AR[OK.AP];
        AR[OK_AP] = 7;
//  -> LB00217;
        goto LB00217;
// 
// LB00218:
        LB00218: ;
// 
// MONITOR(0);
        MONITOR (0);
// LB00217:
        LB00217: ;
// 
// END::7.1.1.6
      }
//  -> LB00042;
      goto LB00042;
// 
// LB00057:
      LB00057:
// 
// BEGIN
      {
// ::PREPROCESS NAMED ITEM
// $IN INIT.FPTR,END.IPTR;
        INTEGER INIT_FPTR,END_IPTR;
// FPTR => INIT.FPTR;
        INIT_FPTR = FPTR;
// BEGIN
        {
// ::SET POINTERS TO FIELDS OF OPERAND
// LB00082:
          LB00082: ;
// 
// IPTR => FIELDS[FPTR];
          FIELDS[FPTR] = IPTR;
// 1 +> FPTR;
          ++ FPTR;
// LB00083:
          LB00083: ;
// 
// IF LBUFF[1+>IPTR+1] = REF,-> LB00083;
          if (ITYPE_eq (LBUFF[(++IPTR)+1], REF)) goto LB00083;
// IF LBUFF[IPTR+1] = LSB,-> LB00088;
          if (ITYPE_eq (LBUFF[IPTR+1], LSB)) goto LB00088;
// IF LBUFF[IPTR+1] /= DOF,-> LB00092;
          if (ITYPE_ne (LBUFF[IPTR+1], DOF)) goto LB00092;
#if 0 // flip fix
// IF TAG OF LBUFF[1+>IPTR+1] = TNAME,-> LB00087;
          if (LBUFF[(++IPTR)+1].TAG == TNAME) goto LB00087;
//  -> LB00082;
          goto LB00082;
// 
// LB00087:
          LB00087: ;
#else
// IF TAG OF LBUFF[1+>IPTR+1] = TNAME,-> LB00082;
    if (LBUFF[++IPTR+1].TAG == TNAME) goto LB00082;
#endif
// 
// MONITOR(0);
          MONITOR (0);
// LB00088:
          LB00088:
// 
// 1 => I;
          I = 1;
// LB00089:
          LB00089:
// 
// IF LBUFF[1+>IPTR+1] = LSB,-> LB00093;
          if (ITYPE_eq (LBUFF[(++IPTR)+1], LSB)) goto LB00093;
// IF LBUFF[IPTR+1] /= RSB,-> LB00094;
          if (ITYPE_ne (LBUFF[IPTR+1], RSB)) goto LB00094;
// IF I -1 => I /= 0,-> LB00089;
          if ((I = (I -1)) != 0) goto LB00089;
//  -> LB00083;
          goto LB00083;
// 
//  -> LB00092;
          goto LB00092;
// 
// LB00093:
          LB00093:
// 
// 1 +> I;
          ++ I;
//  -> LB00089;
          goto LB00089;
// 
// LB00094:
          LB00094: ;
// 
// IF LBUFF[IPTR+1] = EOS,-> LB00096;
          if (ITYPE_eq (LBUFF[IPTR+1], EOS)) goto LB00096;
// ::DONE IN BOX 9
//  -> LB00089;
          goto LB00089;
// 
// LB00096:
          LB00096: ;
// 
// MONITOR(0);
          MONITOR (0);
// LB00092:
          LB00092: ;
// 
// END::7.1.1.2.1
        }
// IPTR => END.IPTR;
        END_IPTR = IPTR;
// FIELDS[FPTR -1 => FPTR] => IPTR;
        IPTR = FIELDS[FPTR = (FPTR - 1)];
// BEGIN
        {
// ::FORM BASE OPERAND AND TYPE
// IND OF LBUFF[1+>IPTR] => MSLN;
          MSLN = LBUFF[++IPTR].IND;
// IF MUTLN(MSLN) => MTLN = 0,-> LB00113;
          if ((MTLN =  MUTLN(MSLN)) == 0) goto LB00113;
// IF MTLN < CURLEV AND MTLN >= GLEV
          if (MTLN < CURLEV &&  MTLN >= GLEV
// AND MTLN /= CURPROC,-> LB00114;
              && MTLN != CURPROC) goto LB00114;
// IF MTLN > %1000,-> LB00115;
          if (MTLN > 0x1000) goto LB00115;
// SWITCH K OF PLIST[MTLN]-1
// \K1,K2,K3,K4,K5,K6,K7,K8,K9,K10,K11,K12,K13,K14,K15;
          switch (PLIST[MTLN].K - 1) {
            case 0: goto K1;
            case 1: goto K2;
            case 2: goto K3;
            case 3: goto K4;
            case 4: goto K5;
            case 5: goto K6;
            case 6: goto K7;
            case 7: goto K8;
            case 8: goto K8;
            case 9: goto K10;
            case 10: goto K11;
            case 11: goto K12;
            case 12: goto K13;
            case 13: goto K14;
            case 14: goto K15;
          }
// K4:
          K4:
// K8:
          K8:
// LB00103:
          LB00103:
// 
// T OF PLIST[MTLN] => OPD.TYPE;
        OPD_TYPE = PLIST[MTLN].T;
// LB00104:
          LB00104:
// 
// MTLN => AR[AP];
          AR[AP] = MTLN;
// 1 +> AP;
          ++ AP;
//  -> LB00105;
          goto LB00105;
// 
// K6:
          K6: ;
// K9:
          K9: ;
// K10:
          K10: ;
// K12:
          K12: ;
// LB00107:
          LB00107: ;
// 
// MONITOR(6);
          MONITOR (6);
// K5:
          K5:
// %40 => USES.D;
          USES_D = 0x40;
//  -> LB00103;
          goto LB00103;
// 
// LB00119:
          LB00119:
// 
// CURRES => MTLN;
          MTLN = CURRES;
//  -> LB00103;
          goto LB00103;
// 
// K1:
          K1: ;
// K2:
          K2: ;
// K3:
          K3: ;
// IF REFR /= 0,-> LB00118;
          if (REFR != 0) goto LB00118;
// IF MTLN = CURPROC
          if (MTLN == CURPROC
// AND T OF PLIST[CURPROC] /= 0,-> LB00119;
              && PLIST[CURPROC].T != 0) goto LB00119;
// MONITOR(6);
          MONITOR (6);
// LB00114:
          LB00114: ;
// 
// MONITOR(2);
          MONITOR (2);
// K7:
          K7: ;
// K11:
          K11: ;
// IF DLIST[OP] & %4 /= 0,-> LB00107;
          if ((DLIST[OP] & 0x4) != 0) goto LB00107;
//  -> LB00103;
          goto LB00103;
// 
// LB00113:
          LB00113: ;
// 
// MONITOR(1);
          MONITOR(1);
// LB00118:
          LB00118: ;
// 
// %8000 ! MTLN => OPD.TYPE;
          OPD_TYPE = 0x8000 | MTLN;
//  -> LB00104;
          goto LB00104;
// 
// LB00115:
          LB00115:
// 
// TINT => OPD.TYPE;
          OPD_TYPE = TINT;
//  -> LB00104;
          goto LB00104;
// 
// K13:
          K13: ;
// K14:
          K14: ;
// K15:
          K15: ;
// IF SINGLE.OPD = 0,-> LB00107;
          if (SINGLE_OPD == 0) goto LB00107;
// REF.BIT => REFR;
          REFR = REF_BIT;
// TLAB => OPD.TYPE;
          OPD_TYPE = TLAB;
//  -> LB00104;
          goto LB00104;
// 
// LB00105:
          LB00105: ;
// 
// END::7.1.1.2.2
        }
// LB00071:
        LB00071: ;
// 
// IF LBUFF[IPTR+1] = DEREF,-> LB00078;
        if (ITYPE_eq (LBUFF[IPTR+1], DEREF)) goto LB00078;
// IF LBUFF[IPTR+1] = LSB,-> LB00079;
        if (ITYPE_eq (LBUFF[IPTR+1], LSB)) goto LB00079;
// IF FPTR /= INIT.FPTR,-> LB00080;
        if (FPTR != INIT_FPTR) goto LB00080;
// REFR ! USES.D ! USES.B => AR[OK.AP];
        AR[OK_AP] = REFR | USES_D | USES_B;
// IF REFR /= 0 AND OPD.TYPE & %8000 = 0
        if (REFR != 0 && (OPD_TYPE & 0x8000) == 0
// AND OPD.TYPE /= TLAB THEN
            && OPD_TYPE != TLAB) {
// IF OPD.TYPE & 3 /= 0 THEN
          if ((OPD_TYPE & 3) != 0) {
// CAPTION(%"MSL07.1.1.2 BOX 15");
            CAPTION((ADDR_LOGICAL8) & vec ("MSL07.1.1.2 BOX 15"));
// MONITOR(3);
            MONITOR (3);
// FI
          }
// IF OPD.TYPE > %4000 THEN
          if (OPD_TYPE > 0x4000) {
// OPD.TYPE & %3FFC ! 3 => OPD.TYPE
            OPD_TYPE = (OPD_TYPE & 0x3FFC) | 3;
// ELSE
          } else {
// OPD.TYPE ! 1 => OPD.TYPE
            OPD_TYPE = OPD_TYPE | 1;
// FI
          }
// FI
        }
// END.IPTR => IPTR;
        IPTR = END_IPTR;
//  -> LB00077;
        goto LB00077;
// 
// LB00078:
        LB00078:
// 
// 1 +> IPTR;
        ++ IPTR;
// BEGIN
        {
// ::DEREFERENCE
// %40 => USES.D;
          USES_D = 0x40;;
// IF OPD.TYPE & %4003 => I >= %4000,-> LB00131;
          if ((I = (OPD_TYPE & 0x4003)) >= 0x4000) goto LB00131;
// IF I = 0,-> LB00132;
          if (I == 0) goto LB00132;
// %7F => AR[AP];
          AR[AP] = 0x7f;
// 1 +> AP;
          ++ AP;
// IF I = 1 THEN
          if (I == 1) {
// 0 => I
            I = 0;
// ELSE
          } else {
// IF I = 3 THEN
            if (I == 3) {
// %4000 => I
              I = 0x4000;
// ELSE
            } else {
// CAPTION(%"7.1.1.2.3 - PROC ADDR!0A!");
              CAPTION((ADDR_LOGICAL8) & vec ("7.1.1.2.3 - PROC ADDR!0A!"));
// MONITOR(3);
              MONITOR (3);
// FI
            }
// FI
          }
// OPD.TYPE & %3FFC ! I => OPD.TYPE;
          OPD_TYPE = (OPD_TYPE & 0x3FFC) | I;
//  -> LB00130;
          goto LB00130;
// 
// LB00131:
          LB00131: ;
// 
// MONITOR(12);
          MONITOR (12);
// LB00132:
          LB00132: ;
// 
// MONITOR(3);
          MONITOR (3);
// LB00130:
          LB00130: ;
// 
// END::7.1.1.2.3
        }
//  -> LB00071;
        goto LB00071;
// 
// LB00079:
        LB00079:
// 
// 1 +> IPTR;
        ++ IPTR;
// BEGIN
        {
// ::SUBSCRIPT
// %40 => USES.D;
          USES_D = 0x40;
// IF OPD.TYPE & %40(3) = 0,-> LB00143;
          if ((OPD_TYPE & 0x4000) == 0) goto LB00143;
// %7E => AR[AP];
          AR[AP] = 0x7e;
// OPD.TYPE => AR[2 +> AP - 1];
          AR[(AP += 2) - 1] = OPD_TYPE;
// %10 => USES.B;
          USES_B = 0x10;
// PREPROCESS.COMP(0) => I;
          I = PREPROCESS_COMP (0);
// IF CHECK.TYPE(0,TINT,I) /= TINT
          if (CHECK_TYPE (0, TINT, I) != TINT
// AND CHECK.TYPE(0,TBYADDR,I) /= TBYADDR,-> LB00144;
              && CHECK_TYPE (0, TBYADDR, I) != TBYADDR) goto LB00144;
// IF LBUFF[IPTR] /= RSB,-> LB00145;
          if (ITYPE_ne (LBUFF[IPTR], RSB)) goto LB00145;
// OPD.TYPE & %BFFF => OPD.TYPE;
          OPD_TYPE = OPD_TYPE & 0xBFFF;
//  -> LB00142;
          goto LB00142;
// 
// LB00143:
          LB00143: ;
// 
// MONITOR(12);
          MONITOR(12);
// LB00144:
          LB00144: ;
// 
// MONITOR(9);
          MONITOR(9);
// LB00145:
          LB00145: ;
// 
// MONITOR(0);
          MONITOR(0);
// LB00142:
          LB00142: ;
// 
// END::7.1.1.2.4
        }
//  -> LB00071;
        goto LB00071;
// 
// LB00080:
        LB00080: ;
// 
// BEGIN
        {
// ::SELECT FIELD
// $IN UNION;
         INTEGER UNION;
// IF OPD.TYPE & %C000 /= 0
         if ((OPD_TYPE & 0xC000) != 0
// OR OPD.TYPE & 3 /= 0,-> LB00155;
             || (OPD_TYPE & 3) != 0) goto LB00155;
// IF OPD.TYPE < %100,-> LB00156;
         if (OPD_TYPE < 0x100) goto LB00156;
// IF TLIST[DETAIL OF PLIST[OPD.TYPE->> 2 -64] => I] => N & %8000 /= 0 THEN
         if (((N = TLIST[I = PLIST[(OPD_TYPE >> 2) - 64].DETAIL]) & 0x8000) != 0) {
// 1 => UNION;
           UNION = 1;
// N & %7FFF => N
           N = N & 0x7fff;
// ELSE
         } else {
// 0 => UNION
          UNION = 0;
// FI
          }
// 0 => F => U;
          U = F = 0;
// FIELDS[FPTR - 1 => FPTR] => IPTR;
          IPTR = FIELDS[FPTR = (FPTR - 1)];
// IND OF LBUFF[1+>IPTR] => MSLN;
          MSLN = LBUFF[++IPTR].IND;
// LB00151:
          LB00151:
// 
// IF MSLN = TLIST[2+>I],-> LB00157;
          if (MSLN == TLIST[I += 2]) goto LB00157;
// IF 1 +> F < N,-> LB00151;
          if (++F < N) goto LB00151;
// 0 => F;
          F = 0;
// 1 +> U;
          ++ U;
// IF UNION = 1 THEN
          if (UNION == 1) {
// IF TLIST[1+>I] => N & %8000 /= 0 THEN
            if (((N = TLIST[++I]) & 0x8000) != 0) {
// N & %7FFF => N
              N = N & 0x7FFF;
// ELSE
            } else {
// 2 => UNION
            UNION = 2;
// FI
            }
// ELSE
          } else {
// 0 => UNION
            UNION = 0;
// FI
          }
// IF UNION /= 0,-> LB00151;
          if (UNION != 0) goto LB00151;
// MONITOR(8);
          MONITOR (8);
// LB00157:
          LB00157: ;
// 
// IF UNION > 0 THEN
          if (UNION > 0) {
// %C0 ! U => AR[AP];
            AR[AP] = 0xc0 | U;
// 1 +> AP
            ++ AP;
// FI
          }
// %80 ! F => AR[AP];
          AR[AP] = 0x80 | F;
// 1 +> AP;
          ++ AP;
// TLIST[I - 1] => OPD.TYPE;
          OPD_TYPE = TLIST[I - 1];
// %40 => USES.D;
          USES_D = 0x40;
//  -> LB00161;
          goto LB00161;
// 
// LB00155:
          LB00155: ;
// 
// MONITOR(12);
          MONITOR (12);
// LB00156:
          LB00156: ;
// 
// MONITOR(3);
          MONITOR (3);
// LB00161:
          LB00161: ;
// 
// END::7.1.1.2.5
        }
//  -> LB00071;
        goto LB00071;
// 
// LB00077:
        LB00077: ;
// 
// END::7.1.1.2
      }
//  -> LB00042;
      goto LB00042;
// 
// LB00058:
      LB00058: ;
// 
// MONITOR(6);
      MONITOR (6);
// LB00061:
      LB00061: ;
// 
// MONITOR(0);
      MONITOR (0);
// LB00045:
      LB00045: ;
// 
// 1 => AR[OK.AP];
      AR[OK_AP] = 1;
//  -> LB00046;
      goto LB00046;
// 
// LB00059:
      LB00059:
// 
// IF I /= TSTRING,-> LB00061;
      if (I != TSTRING) goto LB00061;
// BEGIN
      {
// ::PREPROCESS STRING
// IND OF LBUFF[1+>IPTR] => I;
        I = LBUFF[++IPTR].IND;
// TL.S.DECL(NIL,TLO8,-1);
        TL_S_DECL (NIL, TLO8, (ADDR) -1); // XXX
// DUMMY() => MTLN;
        MTLN = DUMMY ();
// TL.ASS(MTLN,-1);
        TL_ASS (MTLN, -1);
// -1 => N;
        N = -1;
// WHILE CLIST[1 +> N + I] /= 0 DO OD
        while (CLIST[++N + I] != 0) { ; }
// TL.C.LIT.S(TLO8,PART(^CLIST,I,I + N -1));
        TL_C_LIT_S (TLO8, PART (& CLIST, I, I + N - 1));
// TL.ASS.VALUE(0,1);
        TL_ASS_VALUE (0, 1);
// TL.ASS.END();
        TL_ASS_END ();
// 8 => AR[OK.AP];
        AR[OK_AP] = 8;
// MTLN => AR[AP];
        AR[AP] = MTLN;
// 1+>AP;
        ++ AP;
// STRING.TYPE => OPD.TYPE;
        OPD_TYPE = STRING_TYPE;
// END::7.1.1.7
      }
//  -> LB00042;
      goto LB00042;
// 
// LB00062:
      LB00062: ;
// 
// IF DL.OP & %1000 /= 0,-> LB00058;
      if ((DL_OP & 0x1000) != 0) goto LB00058;
//  -> LB00052;
      goto LB00052;
// 
// LB00055:
      LB00055: ;
// 
// IF LBUFF[IPTR+2] /= DEREF
      if (ITYPE_ne (LBUFF[IPTR+2], DEREF)
// OR LBUFF[IPTR+3] /= LB,-> LB00057;
          || ITYPE_ne (LBUFF[IPTR+3], LB)) goto LB00057;
// 1 +> IPTR;
      ++ IPTR;
// 9 => U;
      U = 9;
//  -> LB00050;
      goto LB00050;
// 
// LB00043:
      LB00043: ;
// 
// END::7.1.1
    }
// LB00015:
    LB00015: ;
// 
// CHECK.TYPE(OP,CMP.TYPE,OPD.TYPE) => CMP.TYPE;
    CMP_TYPE = CHECK_TYPE (OP,CMP_TYPE,OPD_TYPE);
// USES.B ! USES.D !> COMP.KIND;
    COMP_KIND |= USES_B | USES_D;
// 0 => REFR;
    REFR = 0;
// IF DL.OP & %804 = 0,-> LB00023;
    if ((DL_OP & 0x804) == 0) goto LB00023;
// OPD.TYPE ! %80(3) => AR[PS.AP];
    AR[PS_AP] = OPD_TYPE | 0x8000;
// LB00018:
    LB00018: ;
// 
// CMP.TYPE ! %80(3) => AR[PE.AP];
    AR[PE_AP] = CMP_TYPE | 0x8000;
// IF DL.OP & %800 /= 0,-> LB00029;
    if ((DL_OP & 0x800) != 0) goto LB00029;
// OPD.TYPE => CMP.TYPE;
    CMP_TYPE = OPD_TYPE;
// AP => PE.AP + 1 => AP;
    AP = (PE_AP = AP) + 1;
// LB00023:
    LB00023: ;
// 
// DL.OP => DL.PREV;
    DL_PREV = DL_OP;
// IF TAG OF LBUFF[1+>IPTR] = TDELIM
    if (LBUFF[++IPTR].TAG == TDELIM
// => DL.OP & %810 /= 0,-> LB00010;
// AND DLIST[IND OF LBUFF[IPTR] => OP]
         && ((DL_OP = DLIST[OP = LBUFF[IPTR].IND]) & 0x810) != 0) goto LB00010;
// MONITOR(0);
    MONITOR (0);
// LB00010:
    LB00010: ;
// 
// IF DL.OP & %800 /= 0,-> LB00025;
    if ((DL_OP & 0x800) != 0) goto LB00025;
// IF DL.OP & %4 = 0,-> LB00013;
    if ((DL_OP & 0x4) == 0) goto LB00013;
// AP => PS.AP + 1 => AP;
    AP = (PS_AP = AP) + 1;
// LB00013:
    LB00013: ;
// 
// OP => AR[AP];
    AR[AP] = OP;
// 1 +> AP;
    ++ AP;
// 0 => SINGLE.OPD;
    SINGLE_OPD = 0;
//  -> LB00014;
    goto LB00014;
// 
// LB00025:
    LB00025: ;
// 
// IF PTYPE = 0,-> LB00027;
    if (PTYPE == 0) goto LB00027;
// AP => PS.AP + 1 => AP;
    AP = (PS_AP = AP) + 1;
// PTYPE => OPD.TYPE;
    OPD_TYPE = PTYPE;
// %100 !> COMP.KIND;
    COMP_KIND |= 0x100;
//  -> LB00015;
    goto LB00015;
// 
// LB00027:
    LB00027: ;
// 
// IF DL.PREV & %4 = 0,-> LB00018;
    if ((DL_PREV & 0x4) == 0) goto LB00018;
// 1 -> AP;
    -- AP;
// LB00029:
    LB00029: ;
// 
// OP => AR[AP];
    AR[AP] = OP;
// 1 +> AP;
    ++ AP;
// COMP.KIND ! SINGLE.OPD => AR[INIT.AP];
    AR[INIT_AP] = COMP_KIND | SINGLE_OPD;
// CMP.TYPE => PREPROCESS.COMP;
    ret = CMP_TYPE;
//  -> LB00031;
    goto LB00031;
// 
// LB00022:
    LB00022: ;
// 
// 1 +> IPTR;
    ++ IPTR;
// REF.BIT => REFR;
    REFR = REF_BIT;
//  -> LB00014;
    goto LB00014;
// 
//  -> LB00023;
    goto LB00023;
// 
// LB00031:
    LB00031: ;
// 
// END
    return ret;
    }




#ifdef PDP
// *CODE 1;
#endif // PDP

// $PR PREPROCESS.COND;
    void PREPROCESS_COND (void) {
// ::DECL & INIT
// $IN OR.AP, AND.AP, TEST.AP;
    INTEGER OR_AP, AND_AP, TEST_AP;
// $IN CTYPE,RHS.TYPE;
    INTEGER CTYPE,RHS_TYPE;
// 0 => AR[AP => OR.AP];
    AR[OR_AP = AP] = 0;
// 1 +> AP;
    ++ AP;
// LB00301:
    LB00301: ;
// 
// 0 => AR[AP => AND.AP];
    AR[AND_AP = AP] = 0;
// 1 +> AP;
    ++ AP;
// LB00302:
    LB00302: ;
// 
// BEGIN
    {
// ::PREPROCESS.TEST
// $IN I;
      INTEGER I;
// 0 => AR[AP => TEST.AP];
      AR[TEST_AP = AP] = 0;
// 1 +> AP;
      ++ AP;
// IF LBUFF[IPTR+1] = LSB,-> LB00324;
      if (ITYPE_eq (LBUFF[IPTR+1], LSB)) goto LB00324;
// ::0 => AR[AP];
// 1 +> AP;
      ++ AP;
// PREPROCESS.COMP(0) => CTYPE;
      CTYPE = PREPROCESS_COMP (0);
// %100 !> AR[TEST.AP+2];
      AR[TEST_AP+2] |= 0x100;
// IF DLIST[IND OF LBUFF[IPTR]] & %8 = 0,-> LB00323;
      if ((DLIST[LBUFF[IPTR].IND] & 0x8) == 0) goto LB00323;
// LB00314:
      LB00314: ;
// 
// AP => I;
      I = AP;
// PREPROCESS.COMP(0) => RHS.TYPE;
      RHS_TYPE = PREPROCESS_COMP (0);
// %100 !> AR[I];
      AR[I] |= 0x100;;
// 1 +> AR[TEST.AP];
      ++ AR[TEST_AP];
// IF AR[TEST.AP] = 1,-> LB00318;
      if (AR[TEST_AP] == 1) goto LB00318;
// IF CHECK.TYPE(0,CTYPE,RHS.TYPE) /= CTYPE THEN
      if (CHECK_TYPE (0, CTYPE, RHS_TYPE) != CTYPE) {
// CAPTION(%"MSL07.2.1 BOX 10!0A!");
        CAPTION ((ADDR_LOGICAL8) & vec ("MSL07.2.1 BOX 10!0A!"));
// MONITOR(3);
        MONITOR (3);
// FI
      }
// LB00318:
      LB00318: ;
// 
// CHECK.TYPE(0,CTYPE,RHS.TYPE) => CTYPE;
      CTYPE = CHECK_TYPE (0, CTYPE, RHS_TYPE);
// IF DLIST[IND OF LBUFF[IPTR]] & %8 /= 0,-> LB00314;
      if ((DLIST[LBUFF[IPTR].IND] & 0x8) != 0) goto LB00314;
// CTYPE => AR[TEST.AP+1];
      AR[TEST_AP+1] = CTYPE;
// LB00321:
      LB00321: ;
// 
// IF DLIST[IND OF LBUFF[IPTR]] & %1 = 0,-> LB00323;
      if ((DLIST[LBUFF[IPTR].IND] & 0x1) == 0) goto LB00323;
//  -> LB00322;
      goto LB00322;
// 
// LB00323:
      LB00323: ;
// 
// MONITOR(0);
      MONITOR(0);
// LB00324:
      LB00324: ;
// 
// 1 +> IPTR;
      ++ IPTR;
// ::DONE IN BOX 2
// PREPROCESS.COND();
      PREPROCESS_COND();
// IF LBUFF[IPTR] /= RSB,-> LB00329;
      if (ITYPE_ne (LBUFF[IPTR], RSB)) goto LB00329;
// IF TAG OF LBUFF[IPTR+1] /= TDELIM,-> LB00329;
      if (LBUFF[IPTR+1].TAG != TDELIM) goto LB00329;
// IND OF LBUFF[1+>IPTR] => AR[AP];
      AR[AP] = LBUFF[++IPTR].IND;
// 1 +> AP;
      ++ AP;
//  -> LB00321;
      goto LB00321;
// 
// LB00329:
      LB00329: ;
// 
// MONITOR(0);
      MONITOR(0);
// LB00322:
      LB00322: ;
// 
// END::7.2.1
    }
// IF LBUFF[IPTR] = DAND,-> LB00307;
    if (ITYPE_eq (LBUFF[IPTR], DAND)) goto LB00307;
// IF LBUFF[IPTR] = DOR,-> LB00306;
    if (ITYPE_eq (LBUFF[IPTR], DOR)) goto LB00306;
//  -> LB00305;
    goto LB00305;
// 
// LB00306:
    LB00306: ;
// 
// 1 +> AR[OR.AP];
    ++ AR[OR_AP];
//  -> LB00301;
    goto LB00301;
// 
// LB00307:
    LB00307: ;
// 
// 1 +> AR[AND.AP];
    ++ AR[AND_AP];
//  -> LB00302;
    goto LB00302;
// 
// LB00305:
    LB00305: ;
// 
// END
    }











// $PR COMPILE.COMP(INIT.OP,REQD.TYPE);
    INTEGER COMPILE_COMP (INTEGER INIT_OP, INTEGER REQD_TYPE) {
    INTEGER ret;
// :: DECL & INIT
// $IN CMP.TYPE,OPD.TYPE,OP,REFR,REG,NLOAD,
    INTEGER CMP_TYPE,OPD_TYPE,OP,REFR,REG,NLOAD,
// STACKED.REGS,CMP.KIND,OPD.KIND,OK,I,J,L,OPD,FN,PT;
    STACKED_REGS,CMP_KIND,OPD_KIND,OK,I,J,L,OPD,FN,PT;
// 0 => REFR => NLOAD;
// [rj] XXX
    OPD_TYPE = 0; /**/ /* error when processing SIZE built-in function if this is not initialised */

    NLOAD = REFR = 0;
// BEGIN
    {
// ::STACK REGISTERS
// ::IF NECESSARY ETC
// IF REQD.TYPE = 0,-> LB00359;
      if (REQD_TYPE == 0) goto LB00359;
// IF REQD.TYPE = B.REQD,-> LB00361;
      if (REQD_TYPE == B_REQD) goto LB00361;
// REQD.TYPE => CMP.TYPE;
      CMP_TYPE = REQD_TYPE;
// LB00350:
      LB00350: ;
// 
// REG.FROM.TYPE(CMP.TYPE,AR[AP] & %180 -= %80) => REG;
      REG = REG_FROM_TYPE(CMP_TYPE,(AR[AP] & 0x180) ^ 0x80);
// LB00351:
      LB00351: ;
// 
// AR[AP] => CMP.KIND;
      CMP_KIND = AR[AP];
// 2 +> AP;
      AP += 2;
// IF INIT.OP /= 0,-> LB00360;
      if (INIT_OP != 0) goto LB00360;
// ILOAD => OP;
      OP = ILOAD;
// IF REGS.IU & REG = 0,-> LB00356;
      if ((REGS_IU & REG) == 0) goto LB00356;
// STACK.REGS(REG);
      STACK_REGS(REG);
// LB00356:
      LB00356: ;
// 
// IF CMP.KIND ->> 4 & A.B.D & REGS.IU => STACKED.REGS = 0,-> LB00358;
      if ((STACKED_REGS = (((CMP_KIND >> 4) & A_B_D) & REGS_IU)) == 0) goto LB00358;
// STACK.REGS(STACKED.REGS);
      STACK_REGS(STACKED_REGS);
//  -> LB00358;
      goto LB00358;
// 
// LB00359:
      LB00359: ;
// 
// AR[AP+1] & %7FFF => CMP.TYPE;
      CMP_TYPE = AR[AP+1] & 0x7FFF;
//  -> LB00350;
      goto LB00350;
// 
// LB00361:
      LB00361: ;
// 
// AR[AP+1] & %7FFF => CMP.TYPE;
      CMP_TYPE = AR[AP+1] & 0x7FFF;
// BREG => REG;
      REG = BREG;
//  -> LB00351;
      goto LB00351;
// 
// LB00360:
      LB00360: ;
// 
// INIT.OP => OP;
      OP = INIT_OP;
// 0 => STACKED.REGS;
      STACKED_REGS = 0;
// LB00358:
      LB00358: ;
// 
// END::7.3.1
    }
// LB00334:
    LB00334: ;
// 
// :: PROCESS OPERAND
// AR[AP] => OPD.KIND & %F => OK;
   OK = (OPD_KIND = AR[AP]) & 0xF;
// 1 +> AP;
   ++ AP;
// SWITCH OK \ K0,K1,K2,K3,K4,K5,K6,K7,K8,K9;
   switch (OK) {
     case 0: goto K0;
     case 1: goto K1;
     case 2: goto K2;
     case 3: goto K3;
     case 4: goto K4;
     case 5: goto K5;
     case 6: goto K6;
     case 7: goto K7;
     case 8: goto K8;
     case 9: goto K9;
    }
// K1:
    K1: ;
// K2:
    K2: ;
// BEGIN
    {
// ::LITERALS
// OPD.KIND ->> 4 => OPD.TYPE;
      OPD_TYPE = OPD_KIND >> 4;
// IF OK = 2,-> LB00417;
      if (OK == 2) goto LB00417;
// LB00414:
      LB00414: ;
// 
// ::TEMP MOD
// $IN DMI,DMJ;AR[AP]=>DMI;OPD.TYPE->>2&%F+DMI=>DMJ;
      INTEGER DMI, DMJ; DMI = AR[AP]; DMJ = ((OPD_TYPE >> 2) & 0xF) + DMI;
// TL.C.LIT.S(OPD.TYPE,PART(^CLIST,DMI,DMJ));
      TL_C_LIT_S (OPD_TYPE, PART (& CLIST, DMI, DMJ));
// 1 +> AP;
      ++ AP;
// CLIT => OPD;
      OPD = CLIT;
//  -> LB00416;
      goto LB00416;
// 
// LB00417:
      LB00417: ;
// 
// -1 => NLOAD;
      NLOAD = -1;
//  -> LB00414;
      goto LB00414;
// 
// LB00416:
      LB00416: ;
// 
// END::7.3.2.2
    }
//  -> LB00369;
    goto LB00369;
// 
// K7:
    K7: ;
// ::BUILT IN FUNCTIONS
// SWITCH AR[1+>AP-1]=>L & %F
// \BIF0,BIF1,BIF2,BIF3,BIF4,BIF5,BIF6,BIF7;
    switch ((L = AR[(++AP) - 1]) & 0xF) {
      case 0: goto BIF0;
      case 1: goto BIF1;
      case 2: goto BIF2;
      case 3: goto BIF3;
      case 4: goto BIF4;
      case 5: goto BIF5;
      case 6: goto BIF6;
      case 7: goto BIF7;
    }
// BIF1:
    BIF1: ;
// BEGIN
    {
// ::MAKE
// AR[AP] => OPD.TYPE;
      OPD_TYPE = AR[AP];
// 1 +> AP;
      ++ AP;
// IF L & %80 /= 0,-> LB00493;
      if ((L & 0x80) == 0) goto LB00493;
// -1 => J;
      -- J;
// COMPILE.COMP(0,B.REQD);
      COMPILE_COMP(0,B_REQD);
// LB00487:
      LB00487:
// 
// IF L & %40 /= 0,-> LB00494;
      if ((L & 0x40) != 0) goto LB00494;
// AR[AP] => I;
      I = AR[AP];
// 1 +> AP;
      ++ AP;
// TL.MAKE(I,OPD.TYPE,J);
      TL_MAKE(I,OPD_TYPE, (ADDR) J); // XXX
// LB00490:
      LB00490: ;
// 
// REGS.IU ! BREG
// -= BREG => REGS.IU;
      REGS_IU = (REGS_IU | BREG) ^ BREG;
// LB00491:
      LB00491:
// 
// AREG !> REGS.IU;
      REGS_IU |= AREG;
// CMP.TYPE => A.TYPE;
      A_TYPE = CMP_TYPE;
//  -> LB00492;
      goto LB00492;
// 
// LB00493:
      LB00493:
// 
// 0 => J;
      J = 0;
//  -> LB00487;
      goto LB00487;
// 
// LB00494:
      LB00494:
// 
// COMPILE.COMP(0,0);
      COMPILE_COMP(0,0);
// IF J < 0 THEN
      if (J < 0) {
// 2 !> OPD.TYPE FI
        OPD_TYPE |= 2; }
// TL.PL(%45,OPD.TYPE ! 1);
      TL_PL(0x45, OPD_TYPE | 1);
// IF L & %80 /= 0,-> LB00491;
      if ((L & 0x80) != 0) goto LB00491;
// TL.C.LIT.16(TINT,1);
      TL_C_LIT_16(TINT,1);
// TL.PL(9,0);
      TL_PL(9,0);
// TL.PL(%27,0);
      TL_PL(0x27,0);
//  -> LB00490;
      goto LB00490;
// 
// LB00492:
      LB00492: ;
// 
// END::7.3.2.5.2
    }
// LB00467:
    LB00467: ;
// 
// ->OP.OPD.DONE;
    goto OP_OPD_DONE;
// BIF4:
    BIF4: ;
// ::SIZE
// IF OP = ILOAD,-> LB00517;
    if (OP == ILOAD) goto LB00517;
// STACK.REGS(REG);
    STACK_REGS(REG);
// LB00517:
    LB00517: ;
// 
// COMPILE.COMP(0,0);
    COMPILE_COMP(0,0);
// IF LAST.REG = DREG THEN
    if (LAST_REG == DREG) {
// TL.PL(%46,D.TYPE);
      TL_PL(0x46,D_TYPE);
// TL.PL(%21,DIND) FI
      TL_PL(0x21,DIND); }
// TL.PL(%45,CMP.TYPE => A.TYPE ! %4000);
    TL_PL (0x45, (A_TYPE = CMP_TYPE) | 0x4000);
// IF REG = BREG THEN
    if (REG == BREG) {
// TL.PL(%56,CMP.TYPE);
#if 1
  TL_PL(0x46,CMP_TYPE); /**/ /* this was coded as 0x56, but that does not exist and it looks like it might have been intended to be 0x46 */
#else
      TL_PL(0x56,CMP_TYPE);
#endif
// TL.PL(%02,AOPD) FI
      TL_PL(0x02,AOPD); }
// REGS.IU ! AREG ! DREG
// -=DREG -= AREG
// ! REG => REGS.IU;
    REGS_IU = (((REGS_IU | AREG | DREG) ^ DREG) ^ AREG) | REG;
// ->REVERSE.OP;
    goto REVERSE_OP;
//  -> LB00467;
    goto LB00467;
// 
// BIF0:
    BIF0: ;
// ::BYTE
// IF OP = ILOAD,-> LB00474;
    if (OP == ILOAD) goto LB00474;
// STACK.REGS(REG);
    STACK_REGS(REG);
// LB00474:
    LB00474: ;
// 
// COMPILE.COMP(0,0);
    COMPILE_COMP(0,0);
// IF LAST.REG = DREG THEN
    if (LAST_REG == DREG) {
// TL.PL(%46,D.TYPE);
      TL_PL(0x46,D_TYPE);
// TL.PL(%21,DIND) FI
      TL_PL(0x21,DIND); }
// TL.PL(%45,CMP.TYPE=> A.TYPE);
    TL_PL(0x45,A_TYPE = CMP_TYPE);
// IF REG /= BREG,-> LB00480;
    if (REG != BREG) goto LB00480;
// TL.PL(%56,CMP.TYPE);
    TL_PL(0x56,CMP_TYPE);
// TL.PL(%02,AOPD);
    TL_PL(0x02,AOPD);
// LB00480:
    LB00480:
// 
// REGS.IU ! AREG ! DREG
// -= AREG -= DREG
// ! REG => REGS.IU;
    REGS_IU = (((REGS_IU | AREG | DREG) ^ AREG) ^ DREG) | REG;
// ->REVERSE.OP;
    goto REVERSE_OP;
//  -> LB00467;
    goto LB00467;
// 
// BIF3:
    BIF3: ;
// ::PART
// AP => I;
    I = AP;
// AR[I+1] => AP;
    AP = AR[I+1];
// COMPILE.COMP(0,B.REQD);
    COMPILE_COMP(0,B_REQD);
// AP => J;
    J = AP;
// I + 2 => AP;
    AP = I + 2;
// COMPILE.COMP(0,0);
    COMPILE_COMP(0,0);
// IF LAST.REG = DREG THEN
    if (LAST_REG == DREG) {
// TL.PL(%46,CMP.TYPE => A.TYPE);
      TL_PL(0x46,A_TYPE = CMP_TYPE);
// TL.PL(%21,DIND);
      TL_PL(0x21,DIND);
// AREG => REG;
      REG = AREG;
// REGS.IU -= DREG ! AREG => REGS.IU FI
      REGS_IU = (REGS_IU ^ DREG) | AREG; }
// TL.PL(%27,0);
    TL_PL(0x27,0);
// BREG -=> REGS.IU;
    REGS_IU ^= BREG;
// AR[I] => AP;
    AP = AR[I];
// COMPILE.COMP(0,B.REQD);
    COMPILE_COMP(0,B_REQD);
// TL.PL(%26,1);
    TL_PL(0x26,1);
// BREG -=> REGS.IU;
    REGS_IU ^= BREG;
// J => AP;
    AP = J;
// ::END 7.3.2.5.4
//  -> LB00467;
    goto LB00467;
// 
// BIF2:
    BIF2: ;
// ::POSN
// AR[AP] => OPD;
    OPD = AR[AP];
// 1 +> AP;
    ++ AP;
// COMPILE.COMP(0,0);
    COMPILE_COMP(0,0);
// TL.PL(%20,OPD);
    TL_PL(0x20,OPD);
// ::END 7.3.2.5.2
//  -> LB00467;
    goto LB00467;
// 
// BIF5:
    BIF5: ;
// ::LLST
// TL.PL(%40,0);
    TL_PL(0x40,0);
// ::END 7.3.2.5.6
//  -> LB00467;
    goto LB00467;
// 
// BIF6:
    BIF6: ;
// BEGIN
    {
// ::LPST
// AR[AP] => OPD.TYPE;
      OPD_TYPE = AR[AP];
// 1 +> AP;
      ++ AP;
// COMPILE.COMP(0,0) => I;
      I = COMPILE_COMP(0,0);
// IF I = OPD.TYPE,-> LB00530;
      if (I == OPD_TYPE) goto LB00530;
// TL.PL(%45,OPD.TYPE);
      TL_PL(0x45,OPD_TYPE);
// LB00530:
      LB00530: ;
// 
// TL.PL(%41,AOPD);
      TL_PL(0x41,AOPD);
// END::7.3.2.5.7
    }
//  -> LB00467;
    goto LB00467;
// 
// BIF7:
    BIF7: ;
// ::LENT
// COMPILE.COMP(0,0);
    COMPILE_COMP(0,0);
// TL.PL(%42,AOPD);
    TL_PL(0x42,AOPD);
// REGS.IU -= AREG ! REG => REGS.IU;
    REGS_IU = (REGS_IU ^ AREG) | REG;
// TL.PL(%46,CMP.TYPE => A.TYPE);
    TL_PL(0x46, A_TYPE = CMP_TYPE);
// ::END 7.3.2.5.8
//  -> LB00467;
    goto LB00467;
// 
//  -> LB00369;
    goto LB00369;
// 
// K0:
    K0: ;
// BEGIN
    {
// :: NAMED ITEM
// IF OPD.KIND ->> 4 & B.D & REGS.IU => I = 0,-> LB00381;
      if ((I = ((OPD_KIND >> 4) & B_D & REGS_IU)) == 0) goto LB00381;
// STACK.REGS(I)
      STACK_REGS(I);
// LB00381:
      LB00381: ;
// 
// OPD.KIND & REF.BIT => REFR;
      REFR = OPD_KIND & REF_BIT;
// AR[AP] => OPD;
      OPD = AR[AP];
// 1 +> AP;
      ++ AP;
// IF OPD > %1000,-> LB00389;
      if (OPD > 0x1000) goto LB00389;
// T OF PLIST[OPD] => OPD.TYPE;
      OPD_TYPE = PLIST[OPD].T;
// IF K OF PLIST[OPD] = KFIELD,-> LB00390;
      if (PLIST[OPD].K == KFIELD) goto LB00390;
// IF OPD.TYPE < %100
      if (OPD_TYPE < 0x100
// OR OPD.TYPE & 3 /= 0
          || (OPD_TYPE & 3) != 0
// OR OPD.TYPE & %C000 /= 0,-> LB00388;
          || (OPD_TYPE & 0xC000) != 0) goto LB00388;
// TL.PL(%61,OPD);
      TL_PL(0x61,OPD);
// LB00387:
      LB00387: ;
// 
// DIND => OPD;
      OPD = DIND;
// LB00388:
      LB00388: ;
// 
// BEGIN
      {
// ::PROCESS OPERAND QUALIFICATIONS
// LB00392:
        LB00392:
// 
// IF AR[AP] => I = %7F,-> LB00400;
        if ((I = AR[AP]) == 0x7F) goto LB00400;
// IF I = %7E,-> LB00402;
        if (I == 0x7E) goto LB00402;
// IF I & %8000 /= 0,-> LB00397;
        if ((I & 0x8000) != 0) goto LB00397;
// IF I >= %C0,-> LB00398;
        if (I >= 0xC0) goto LB00398;
// IF I >= %80,-> LB00399;
        if (I >= 0x80) goto LB00399;
//  -> LB00397;
        goto LB00397;
// 
// LB00398:
        LB00398: ;
// 
// 1 +> AP;
        ++ AP;
// TL.PL(%65,I&%3F);
        TL_PL(0x65,I&0x3F);
//  -> LB00392;
        goto LB00392;
// 
// LB00399:
        LB00399:
// 
// 1 +> AP;
        ++ AP;
// TL.PL(%63,I&%7F);
        TL_PL(0x63,I&0x7F);
//  -> LB00392;
        goto LB00392;
// 
// LB00400:
        LB00400: ;
// 
// 1 +> AP;
        ++ AP;
// TL.PL(%62,OPD);
        TL_PL(0x62,OPD);
// DIND => OPD;
        OPD = DIND;
//  -> LB00392;
        goto  LB00392;
// 
// LB00402:
        LB00402: ;
// 
// 2 +> AP;
        AP += 2;
// IF OPD /= DIND,-> LB00404;
        if (OPD != DIND) goto LB00404;
// DREG !> REGS.IU;
        REGS_IU |= DREG;
// AR[AP-1] => D.TYPE;
        D_TYPE = AR[AP - 1];
// LB00404:
        LB00404:
// 
// COMPILE.COMP(0,B.REQD);
        COMPILE_COMP(0,B_REQD);
// IF OPD /= DIND,-> LB00409;
        if (OPD != DIND) goto LB00409;
// DREG -=> REGS.IU;
        REGS_IU ^= DREG;
// LB00407:
        LB00407: ;
// 
// TL.PL(%64,0);
        TL_PL(0x64,0);
// BREG -=> REGS.IU;
        REGS_IU ^= BREG;
//  -> LB00392;
        goto LB00392;
// 
// LB00409:
        LB00409: ;
// 
// TL.PL(%61,OPD);
        TL_PL(0x61,OPD);
// DIND => OPD;
        OPD = DIND;
//  -> LB00407;
        goto LB00407;
// 
// LB00397:
        LB00397: ;
// 
// END::7.3.2.1.1
      }
//  -> LB00389;
      goto LB00389;
// 
// LB00390:
      LB00390: ;
// 
// TL.PL(%62,OPD);
      TL_PL(0x62,OPD);
//  -> LB00387;
      goto LB00387;
// 
// LB00389:
      LB00389: ;
// 
// END::7.3.2.1
    }
//  -> LB00369;
    goto LB00369;
// 
// K5:
    K5:
// K6:
    K6:
// ::SUBEXPRESSIONS &
// ::CONDITIONAL
// ::COMPUTATIONS
// IF REG = DREG,-> LB00447;
    if (REG == DREG) goto LB00447;
// IF OK=6,-> LB00448;
    if (OK == 6) goto LB00448;
// IF REQD.TYPE = B.REQD THEN
    if (REQD_TYPE == B_REQD) {
// B.REQD => I
      I = B_REQD;
// ELSE
    } else {
// 0 => I
      I = 0;
// FI
    }
// COMPILE.COMP(0,I) => OPD.TYPE;
    OPD_TYPE = COMPILE_COMP(0,I);
// LB00438:
    LB00438: ;
// 
// IF OP = ILOAD,-> LB00449;
    if (OP == ILOAD) goto LB00449;
// IF REG /= AREG,-> LB00445;
    if (REG != AREG) goto LB00445;
// IF OPD.TYPE /= CMP.TYPE THEN
    if (OPD_TYPE != CMP_TYPE) {
// TL.PL(%45,CMP.TYPE);
      TL_PL(0x45,CMP_TYPE);
// FI
    }
// LB00441:
    LB00441: ;
// 
// REV.OP[OP-ILOAD] => OP;
    OP = REV_OP[OP-ILOAD];
// UNSTK => OPD;
    OPD = UNSTK;
// 1 -> ST.PTR;
    --ST_PTR;
// ->PLANT.OP.OPD;
    goto PLANT_OP_OPD;
// LB00447:
    LB00447: ;
// 
// MONITOR(4);
    MONITOR(4);
// LB00448:
    LB00448: ;
// 
// BEGIN
    {
// ::COMPILE CONDITIONAL COMPUTATION
// IF OP = ILOAD,-> LB00454;
      if (OP == ILOAD) goto LB00454;
// STACK.REGS(REG);
      STACK_REGS(REG);
// LB00454:
      LB00454: ;
// 
// DUMMY() => I;
      I = DUMMY ();
// DUMMY() => J;
      J = DUMMY ();
// TL.LABEL.SPEC(NIL,3);
      TL_LABEL_SPEC(NIL,3);
// TL.LABEL.SPEC(NIL,3);
      TL_LABEL_SPEC(NIL,3);
// COMPILE.COND(1,I,0);
      COMPILE_COND(1,I,0);
// IF REQD.TYPE = B.REQD THEN
      if (REQD_TYPE == B_REQD) {
// B.REQD => PT
       PT = B_REQD;
// ELSE
      } else {
// 0 => PT
        PT = 0;
// FI
      }
// COMPILE.COMP(0,PT) => OPD.TYPE;
      OPD_TYPE = COMPILE_COMP(0,PT);
// REG -=> REGS.IU;
      REGS_IU ^= REG;
// TL.PL(%4F,J);
      TL_PL(0x4F,J);
// TL.LABEL(I);
      TL_LABEL(I);
// COMPILE.COMP(0,PT);
      COMPILE_COMP(0,PT);
// TL.LABEL(J);
      TL_LABEL(J);
// END::7.3.2.4.1
    }
//  -> LB00438;
    goto LB00438;
// 
// LB00449:
    LB00449: ;
// 
// IF REG = AREG THEN
    if (REG == AREG) {
// IF CMP.TYPE => A.TYPE /= OPD.TYPE THEN
    if ((A_TYPE = CMP_TYPE) != OPD_TYPE) {
// TL.PL(%45,CMP.TYPE) FI
      TL_PL(0x45,CMP_TYPE); }
// ELSE IF REG = BREG THEN
    } else { if (REG == BREG) {
// CMP.TYPE => B.TYPE;
      B_TYPE = CMP_TYPE;
// ELSE IF REG = DREG THEN
    } else { if (REG == DREG) {
// CMP.TYPE => D.TYPE
      D_TYPE = CMP_TYPE;
// FI FI FI
    } } }
// ->OP.OPD.DONE;
    goto OP_OPD_DONE;
// REVERSE.OP:
    REVERSE_OP: ;
//  -> LB00438;
    goto LB00438;
// 
// LB00445:
    LB00445: ;
// 
// IF REG /= BREG,-> LB00441;
    if (REG != BREG) goto LB00441;
// IF OPD.TYPE /= CMP.TYPE THEN
    if (OPD_TYPE != CMP_TYPE) {
// TL.PL(%55,CMP.TYPE) FI
      TL_PL(0x55,CMP_TYPE); }
//  -> LB00441;
    goto LB00441;
// 
//  -> LB00369;
    goto LB00369;
// 
// K3:
    K3: ;
// K4:
    K4: ;
// K9:
    K9: ;
// ::PROCEDURE CALLS
// IF OP = ILOAD,-> LB00421;
    if (OP == ILOAD) goto LB00421;
// STACK.REGS(REG);
    STACK_REGS(REG);
// LB00421:
    LB00421: ;
// 
// 0 => L;
    L = 0;
// IF OK = 9,-> LB00430;
    if (OK == 9) goto LB00430;
// AR[AP] => OPD;
    OPD = AR[AP];
// LB00423:
    LB00423: ;
// 
// 1 +> AP;
    ++ AP;
// TL.PL(%40,OPD);
    TL_PL(0x40,OPD);
// AR[AP] => OPD.TYPE;
    OPD_TYPE = AR[AP];
// AR[AP+1] => I;
    I = AR[AP+1];
// 2 +> AP;
    AP += 2;
// LB00425:
    LB00425: ;
// 
// IF 1->I >= 0,-> LB00432;
    if ((--I) >= 0) goto LB00432;
// IF OK = 9 AND K OF PLIST[L] = KFIELD THEN
    if (OK == 9 && PLIST[L].K == KFIELD) {
// TL.PL(%62,L);
      TL_PL(0x62,L);
// DIND => L;
      L = DIND;
// FI
    }
// TL.PL(%42,L);
    TL_PL(0x42,L);
// IF REG = BREG,-> LB00431;
    if (REG == BREG) goto LB00431;
// LB00428:
    LB00428: ;
// 
// REG !> REGS.IU;
    REGS_IU |= REG;
// ->REVERSE.OP;
    goto REVERSE_OP;
// LB00431:
    LB00431: ;
// 
// IF OPD.TYPE /= CMP.TYPE THEN
    if (OPD_TYPE != CMP_TYPE) {
// TL.PL(%45,CMP.TYPE) FI
      TL_PL(0x45,CMP_TYPE); }
// TL.PL(%56,CMP.TYPE);
      TL_PL(0x56,CMP_TYPE);
// TL.PL(%02,AOPD);
      TL_PL(0x02,AOPD);
//  -> LB00428;
      goto LB00428;
// 
// LB00432:
    LB00432: ;
// 
// COMPILE.COMP(0,0) => PT;
    PT = COMPILE_COMP(0,0);
// TL.PL(%41,AOPD);
    TL_PL(0x41,AOPD);
// 0 => REGS.IU;
    REGS_IU = 0;
//  -> LB00425;
    goto LB00425;
// 
// LB00430:
    LB00430: ;
// 
// AR[AP] => L;
    L = AR[AP];
// T OF PLIST[L] & %3FFF => OPD;
    OPD = PLIST[L].T & 0x3FFF;
//  -> LB00423;
    goto LB00423;
// 
//  -> LB00369;
    goto LB00369;
// 
// K8:
    K8: ;
// ::STRINGS
// AR[AP] => OPD;
    OPD = AR[AP];
// 1 +> AP;
    ++ AP;
// REF.BIT => REFR;
   REFR = REF_BIT;
// ::END 7.3.2.6
// LB00369:
    LB00369: ;
// 
// ::END 7.3.2
// LB00335:
    LB00335: ;
// 
// BEGIN
    {
// ::PLANT OP
// TL.FN[OP-ILOAD] => FN;
      FN = TL_FN[OP-ILOAD];
// IF REG /= DREG,-> LB00552;
      if (REG != DREG) goto LB00552;
// $IF %60 !> FN = %62 $TH
      if ((FN |= 0x60) == 0x62) {
// CMP.TYPE => D.TYPE FI
        D_TYPE = CMP_TYPE; }
// IF REFR = 0,-> LB00548;
      if (REFR == 0) goto LB00548;
// %61 => FN;
      FN = 0x61;
// 0 => REFR;
      REFR = 0;
// LB00548:
      LB00548: ;
// 
// IF OPD /= DIND,-> LB00543;
      if (OPD != DIND) goto LB00543;
// IF FN = %62,-> LB00558;
      if (FN == 0x62) goto LB00558;
// IF FN = %61,-> LB00559;
      if (FN == 0x61) goto LB00559;
// %69 => FN;
      FN = 0x69;
// DREG !> REGS.IU;
      REGS_IU |= DREG;
// 1 -> ST.PTR;
      --ST_PTR;
// LB00543:
      LB00543: ;
// 
// IF DLIST[OP => I] & %4 /=0 OR OP = ICOMP THEN
      if ((DLIST[I = OP] & 0x4) !=0 || OP == ICOMP) {
// IF AR[AP] => I & %8000 /= 0 THEN
        if (((I = AR[AP]) & 0x8000) != 0) {
// IF AR[AP+1] => I & %8000 /= 0 THEN
          if (((I = AR[AP+1]) & 0x8000) != 0) {
// AR[AP+2] => I FI FI
            I = AR[AP+2]; } }
// IF DLIST[I] & %8 /=0
        if ((DLIST[I] & 0x8) != 0
// OR OP /= ICOMP AND I /= IEOS THEN
            ||  (OP != ICOMP && I != IEOS)) {
// TL.REG(REG)
          TL_REG(REG);
// FI FI
        } }
// LB00558:
      LB00558: ;
// 
// TL.PL(FN,OPD);
      TL_PL(FN, OPD);
//  -> LB00559;
      goto LB00559;
// 
// LB00553:
      LB00553: ;
// 
// IF REG /= AREG,-> LB00560;
      if (REG != AREG) goto LB00560;
// %20 !> FN;
      FN |= 0x20;
// IF OP /= ILOAD,-> LB00543;
      if (OP != ILOAD) goto LB00543;
// TL.PL(%46,CMP.TYPE => A.TYPE);
      TL_PL(0x46,A_TYPE = CMP_TYPE);
// IF REFR /= 0 THEN
      if (REFR != 0) {
// %21 => FN;0 => REFR FI
      FN = 0x21; REFR = 0; }
//  -> LB00558;
      goto LB00558;
// 
// LB00561:
      LB00561: ;
// 
// IF OP = ILOAD,-> LB00564;
      if (OP == ILOAD) goto LB00564;
// IF REGS.IU & BREG /= 0,-> LB00543;
      if ((REGS_IU & BREG) != 0) goto LB00543;
// UNSTACK.REGS(BREG);
      UNSTACK_REGS(BREG);
//  -> LB00543;
      goto LB00543;
// 
// LB00552:
      LB00552: ;
// 
// NLOAD +> FN;
      FN += NLOAD;
// 0 => NLOAD;
      NLOAD = 0;
//  -> LB00553;
      goto LB00553;
// 
// LB00560:
      LB00560: ;
// 
// IF REG /= BREG,-> LB00565;
      if (REG != BREG) goto LB00565;
//  -> LB00561;
      goto LB00561;
// 
// LB00565:
      LB00565: ;
// 
// MONITOR(3);
      MONITOR(3);
// LB00564:
      LB00564: ;
// 
// TL.PL(%56,CMP.TYPE => B.TYPE);
// [rj] XXX
#if 1
      TL_PL(0x46,BTYPE = CMP_TYPE); /**/ /* this was coded as 0x56, but that does not exist and it looks like it might have been intended to be 0x46 */

#else
      TL_PL(0x56,B_TYPE = CMP_TYPE);
#endif
//  -> LB00558;
      goto LB00558;
// 
// LB00559:
      LB00559: ;
// 
// END::7.3.3
    }
// IF OP /= ILOAD,-> LB00338;
    if ( OP != ILOAD) goto LB00338;
// REG !> REGS.IU;
    REGS_IU |= REG;
// LB00338:
    LB00338: ;
// 
// IF AR[AP] => OP & %8000 /= 0,-> LB00345;
    if (((OP = AR[AP]) & 0x8000) != 0) goto LB00345;
// 1 +> AP;
    ++ AP;
// IF DLIST[OP] & %800 = 0,-> LB00334;
    if ((DLIST[OP] & 0x800) == 0) goto LB00334;
// IF STACKED.REGS /=0 THEN
    if (STACKED_REGS != 0) {
// UNSTACK.REGS(STACKED.REGS)
      UNSTACK_REGS(STACKED_REGS);
// FI
    }
// CMP.TYPE => COMPILE.COMP;
    ret = CMP_TYPE;
// REG => LAST.REG;
    LAST_REG = REG;
//  -> LB00342;
    goto LB00342;
// 
// PLANT.OP.OPD:
    PLANT_OP_OPD: ;
//  -> LB00335;
    goto LB00335;
// 
// OP.OPD.DONE:
    OP_OPD_DONE: ;
//  -> LB00338;
    goto LB00338;
// 
// LB00345:
    LB00345: ;
// 
// 1 +> AP;
    ++ AP;
// BEGIN
    {
// ::PLANT CONVERSION
// IF OP & %7FFF => OP = CMP.TYPE,-> LB00572;
      if ((OP = (OP & 0x7FFF)) == CMP_TYPE) goto LB00572;
// IF REG = BREG,-> LB00573;
      if (REG == BREG) goto LB00573;
// OP & %FF => A.TYPE;
      A_TYPE = OP & 0xFF;
// TL.PL(%45,OP);
      TL_PL(0x45,OP);
// LB00571:
      LB00571: ;
// 
// OP & %FF => CMP.TYPE;
      CMP_TYPE = OP & 0xFF;
//  -> LB00572;
      goto LB00572;
// 
// LB00573:
      LB00573: ;
// 
// OP & %FF => B.TYPE;
      B_TYPE = OP & 0xFF;
// TL.PL(%55,OP);
      TL_PL(0x55,OP);
//  -> LB00571;
      goto LB00571;
// 
// LB00572:
    LB00572: ;
// 
// END::7.3.4
    }
//  -> LB00338;
    goto LB00338;
// 
// LB00342:
    LB00342: ;
// 
// END
    return ret;
  }




// $PR COMPILE.COND(IF.OR.UN,DEST.LAB,NEXT.LAB);
    void COMPILE_COND (INTEGER IF_OR_UN, INTEGER DEST_LAB, INTEGER NEXT_LAB) {
// $IN I,N,LAB,LAB.AFTER;
    INTEGER I, N, LAB, LAB_AFTER;
// AR[AP] => I => N;
    N = I = AR[AP];
// 1 +> AP;
    ++ AP;
// NEXT.LAB => LAB.AFTER;
    LAB_AFTER = NEXT_LAB;
// LB00577:
    LB00577: ;
// 
// IF 1 -> I >= 0,-> LB00581;
    if ((-- I) >= 0) goto LB00581;
// COMPILE.AND(IF.OR.UN,DEST.LAB,LAB.AFTER);
    COMPILE_AND (IF_OR_UN, DEST_LAB, LAB_AFTER);
// IF LAB.AFTER /= NEXT.LAB THEN
    if (LAB_AFTER != NEXT_LAB) {
// TL.LABEL(LAB.AFTER)
      TL_LABEL (LAB_AFTER);
// FI
    }
//  -> LB00580;
    goto LB00580;
// 
// LB00581:
    LB00581: ;
// 
// IF I + 1 /= N,-> LB00583;
    if (I + 1 != N) goto LB00583;
// IF IF.OR.UN = 0 THEN
    if (IF_OR_UN == 0) {
// DEST.LAB => LAB
      LAB = DEST_LAB;
// ELSE
    } else {
// IF NEXT.LAB = 0 THEN
      if (NEXT_LAB == 0) {
// DUMMY() => LAB.AFTER;
        LAB_AFTER = DUMMY ();
// TL.LABEL.SPEC(NIL,3)
        TL_LABEL_SPEC (NIL, 3);
// FI
      }
// LAB.AFTER => LAB
      LAB = LAB_AFTER;
// FI
    }
// LB00583:
    LB00583: ;
// 
// COMPILE.AND(0,LAB,0);
    COMPILE_AND (0, LAB, 0);
//  -> LB00577;
    goto LB00577;
// 
// LB00580:
    LB00580: ;
// 
// $EN
}



// $PR COMPILE.AND(IF.OR.UN,DEST.LAB,NEXT.LAB);
    void COMPILE_AND (INTEGER IF_OR_UN, INTEGER DEST_LAB, INTEGER NEXT_LAB) {
// $IN I,N,LAB,LAB.AFTER;
    INTEGER I, N, LAB, LAB_AFTER;
// AR[AP] => I => N;
    N = I = AR[AP];
// 1 +> AP;
    ++ AP;
// NEXT.LAB => LAB.AFTER;
    LAB_AFTER = NEXT_LAB;
// LB00586:
    LB00586: ;
// 
// IF 1 -> I >= 0,-> LB00590;
    if ((--I) >= 0) goto LB00590;
// COMPILE.TEST(IF.OR.UN,DEST.LAB,LAB.AFTER);
    COMPILE_TEST (IF_OR_UN, DEST_LAB, LAB_AFTER);
// IF LAB.AFTER /= NEXT.LAB THEN
    if (LAB_AFTER != NEXT_LAB) {
// TL.LABEL(LAB.AFTER)
      TL_LABEL (LAB_AFTER);
// FI
    }
//  -> LB00589;
    goto LB00589;
// 
// LB00590:
    LB00590: ;
// 
// IF I +1 /= N,-> LB00592;
    if ((I+1) != N) goto LB00592;
// IF IF.OR.UN = 0 THEN
    if (IF_OR_UN == 0) {
// IF NEXT.LAB = 0 THEN
      if (NEXT_LAB == 0) {
// DUMMY() => LAB.AFTER;
        LAB_AFTER = DUMMY ();
// TL.LABEL.SPEC(NIL,3)
        TL_LABEL_SPEC (NIL, 3);
// FI
      }
// LAB.AFTER => LAB
      LAB = LAB_AFTER;
// ELSE
    } else {
// DEST.LAB => LAB
      LAB = DEST_LAB;
// FI
    }
// LB00592:
    LB00592: ;
// 
// COMPILE.TEST(1,LAB,0);
    COMPILE_TEST (1, LAB, 0);
//  -> LB00586;
    goto LB00586;
// 
// LB00589:
    LB00589: ;
// 
// $EN
}



// $PR COMPILE.TEST(IF.OR.UN,DEST.LAB,NEXT.LAB);
    void COMPILE_TEST (INTEGER IF_OR_UN, INTEGER DEST_LAB, INTEGER NEXT_LAB) {
// $IN OP,TFN,COPD,CMP.TYPE,N,LTYPE;
    INTEGER OP, TFN, COPD, CMP_TYPE, N, L_TYPE;
#define LTYPE L_TYPE
// $IN I,J,REV,LHS.STORED,JUMP.LAB,LAB.AFTER;
    INTEGER I, J, REV, LHS_STORED, JUMP_LAB, LAB_AFTER;
// 0 => REV => LHS.STORED;
    LHS_STORED = REV = 0;
// NEXT.LAB => LAB.AFTER;
    LAB_AFTER = NEXT_LAB;
// IF AR[AP] => N = 0,-> LB00607;
    if ((N = AR[AP]) == 0) goto LB00607;
// AR[AP+1] => CMP.TYPE;
    CMP_TYPE = AR[AP + 1];
// 2 +> AP;
    AP += 2;
// CHECK.TYPE(0,CMP.TYPE,AR[AP+1] & %7FFF) => LTYPE;
    LTYPE = CHECK_TYPE (0, CMP_TYPE, AR[AP + 1] & 0x7FFF);
// IF COMPILE.COMP(0,L.TYPE) /= CMP.TYPE THEN
    if (COMPILE_COMP (0, L_TYPE) != CMP_TYPE) {
// TL.PL(%45,CMP.TYPE) FI
      TL_PL (0x45, CMP_TYPE); }
// LB00598:
    LB00598: ;
// 
// AR[AP-1] => OP;
    OP = AR[AP - 1];
// IF LHS.STORED /= 0,-> LB00615;
    if (LHS_STORED != 0) goto LB00615;
// IF AR[AP] ->> 4 => I & %8 =0
    if (((I = (AR[AP] >> 4)) & 0x8) == 0
// OR AR[AP+2] & %F => J > 2
         || (J = (AR[AP + 2] & 0xf)) > 2
// OR J = 0 AND I & A.B.D & REGS.IU /= 0,-> LB00609;
         || (J == 0 && ((I & A_B_D) & REGS_IU) != 0)) goto LB00609;
// COMPILE.COMP(ICOMP,CMP.TYPE);
    COMPILE_COMP (ICOMP, CMP_TYPE);
// LB00602:
    LB00602:
// 
// BEGIN
    {
// ::PLANT TEST JUMP
// TL.TST.FN[OP-IMIN.COMP] => TFN;
      TFN = TL_TST_FN[OP - IMIN_COMP];
// IF REV = 0,-> LB00623;
      if (REV == 0) goto LB00623;
// REV.COMP[TFN - 9] => TFN;
      TFN = REV_CMP[TFN - 9];
// LB00623:
      LB00623: ;
// 
// IF IF.OR.UN = 1,-> LB00631;
      if (IF_OR_UN == 1) goto LB00631;
// IF N = 1,-> LB00632;
      if (N == 1) goto LB00632;
// INV.TST[TFN-9]=>TFN;
      TFN = INV_TST[TFN - 9];
// IF LAB.AFTER /= 0,-> LB00628;
      if (LAB_AFTER == 0) goto LB00628;
// DUMMY() => LAB.AFTER;
      LAB_AFTER = DUMMY ();
// TL.LABEL.SPEC(NIL,3);
      TL_LABEL_SPEC (NIL, 3);
// LB00628:
      LB00628: ;
// 
// LAB.AFTER => JUMP.LAB;
      JUMP_LAB = LAB_AFTER; 
// LB00629:
      LB00629: ;
// 
// TL.PL(%40 ! TFN,JUMP.LAB);
      TL_PL (0x40 | TFN, JUMP_LAB);
//  -> LB00630;
      goto LB00630;
// 
// LB00631:
      LB00631: ;
// 
// INV.TST[TFN-9] => TFN;
      TFN = INV_TST[TFN - 9];
// LB00632:
      LB00632: ;
// 
// DEST.LAB => JUMP.LAB;
      JUMP_LAB = DEST_LAB;
//  -> LB00629;
      goto LB00629;
// 
// LB00630:
      LB00630: ;
// 
// END::7.6.1
    }
// IF 1 -> N /= 0,-> LB00598;
    if ((-- N) != 0) goto LB00598;
// REGS.IU ! AREG -= AREG => REGS.IU;
    REGS_IU = (REGS_IU | AREG) ^ AREG;
// IF LAB.AFTER /= NEXT.LAB THEN
    if (LAB_AFTER != NEXT_LAB) {
// TL.LABEL(LAB.AFTER)
      TL_LABEL (LAB_AFTER);
// FI
    }
//  -> LB00606;
    goto LB00606;
// 
// LB00610:
    LB00610: ;
// 
// 1 => REV;
    REV = 1;
// IF N = 1,-> LB00618;
    if (N == 1) goto LB00618;
// DUMMY() => COPD;
    COPD = DUMMY();
// TL.S.DECL(NIL,CMP.TYPE,0);
    TL_S_DECL (NIL, CMP_TYPE, 0);
// TL.PL(%20,COPD);
    TL_PL (0x20, COPD);
// 1 => LHS.STORED;
    LHS_STORED = 1;
// AREG -=> REGS.IU;
    REGS_IU ^= AREG;
// ::DONE IN BOX 20
// LB00615:
    LB00615: ;
// 
// COMPILE.COMP(0,CMP.TYPE);
    COMPILE_COMP (0, CMP_TYPE);
// TL.PL(%2F,COPD);
    TL_PL (0x2F, COPD);
// AREG -=> REGS.IU;
    REGS_IU ^= AREG;
// IF COPD = UNSTK THEN
    if (COPD == UNSTK) {
// 1 -> ST.PTR
      -- ST_PTR;
// FI
    }
//  -> LB00602;
    goto LB00602; 
// 
// LB00618:
    LB00618: ;
// 
// UNSTK => COPD;
    COPD = UNSTK;
//  -> LB00615;
    goto LB00615;
// 
// LB00607:
    LB00607: ;
// 
// 1 +> AP;
    ++ AP;
// COMPILE.COND(IF.OR.UN,DEST.LAB,NEXT.LAB);
    COMPILE_COND (IF_OR_UN, DEST_LAB, NEXT_LAB);
// 1 +> AP;
    ++ AP;
// $EX
    return;
// LB00609:
    LB00609: ;
// 
// IF CMP.TYPE & %FF03 /= 0,-> LB00617;
    if ((CMP_TYPE & 0xFF03) != 0) goto LB00617;
//  -> LB00610;
    goto LB00610;
// 
// LB00617:
    LB00617: ;
// 
// MONITOR(4);
    MONITOR (4);
// LB00606:
    LB00606: ;
// 
// $EN
}



// $PR STACK.REGS(REG.BITS);
    static void STACK_REGS (INTEGER REG_BITS) {
// :: DECL & INIT
// IF REG.BITS & AREG = 0,-> LB00638;
    if ((REG_BITS & AREG) == 0) goto LB00638;
// TL.PL(%47,A.OPD);
    TL_PL (0x47, A_OPD);
// A.TYPE => STACK.TYPE[ST.PTR];
    STACK_TYPE[ST_PTR] = A_TYPE;
// 1 +> ST.PTR;
    ++ ST_PTR;
// LB00638:
    LB00638: ;
// 
// IF REG.BITS & BREG = 0,-> LB00641;
    if ((REG_BITS & BREG) == 0) goto LB00641;
// TL.PL(%47,B.OPD);
    TL_PL (0x47,B_OPD);
// B.TYPE => STACK.TYPE[ST.PTR];
    STACK_TYPE[ST_PTR] = B_TYPE;
// 1 +> ST.PTR;
    ++ST_PTR;
// LB00641:
    LB00641: ;
// 
// IF REG.BITS & DREG = 0,-> LB00644;
    if ((REG_BITS & DREG) == 0) goto LB00644;
// TL.PL(%47,STK.SEL);
    TL_PL (0x47, STK_SEL);
// D.TYPE => STACK.TYPE[ST.PTR];
    STACK_TYPE[ST_PTR] = DTYPE;
// 1 +> ST.PTR;
    ++ST_PTR;
// LB00644:
    LB00644: ;
// 
// REG.BITS -=> REGS.IU;
    REGS_IU ^= REG_BITS;
// $EN
   }




// $PR UNSTACK.REGS(REG.BITS);
    static void UNSTACK_REGS (INTEGER REG_BITS) {
// :: DECL & INIT
// IF REG.BITS & DREG = 0,-> LB00651;
    if ((REG_BITS & DREG) == 0) goto LB00651;
// STACK.TYPE[1->ST.PTR] => D.TYPE;
    D_TYPE = STACK_TYPE[--ST_PTR];
// TL.PL(%62,STK.SEL);
    TL_PL (0x62, STK_SEL);
// LB00651:
    LB00651: ;
// 
// IF REG.BITS & BREG = 0,-> LB00654;
    if ((REG_BITS & BREG) == 0) goto LB00654;
// STACK.TYPE[1->ST.PTR] => B.TYPE;
    B_TYPE = STACK_TYPE[--ST_PTR];
// TL.PL(%56,B.TYPE);
    TL_PL (0x56, B_TYPE);
// TL.PL(%02,UNSTK);
    TL_PL (0x02, UNSTK);
// LB00654:
    LB00654: ;
// 
// IF REG.BITS & AREG = 0,-> LB00657;
    if ((REG_BITS & AREG) == 0) goto LB00657;
// STACK.TYPE[1 -> ST.PTR] => A.TYPE;
    A_TYPE = STACK_TYPE[--ST_PTR];
// TL.PL(%46,A.TYPE);
    TL_PL (0x46, A_TYPE);
// TL.PL(%22,UNSTK);
    TL_PL (0x22, UNSTK);
// LB00657:
    LB00657:
// 
// REG.BITS !> REGS.IU;
    REGS_IU |= REG_BITS;
// $EN
  }


// $PR REG.FROM.TYPE(CTYPE,KIND);
    static INTEGER REG_FROM_TYPE (INTEGER CTYPE, INTEGER KIND) {
    INTEGER ret;
// ::DECL & INIT
// IF CTYPE & 3 /= 0
    if ((CTYPE & 3) != 0
// OR CTYPE = TADPROC
        || CTYPE == TADPROC
// OR CTYPE = TLAB,-> LB00666;
        || CTYPE == TLAB) goto LB00666;
// IF CTYPE < %100 > 0,-> LB00668;
    if (CTYPE < 0x100 && CTYPE > 0) goto LB00668;
// IF CTYPE >= %4000,-> LB00669;
    if (CTYPE >= 0x4000) goto LB00669;
// IF CTYPE /= 0,-> LB00668;
    if (CTYPE != 0) goto LB00668;
// 0 => REG.FROM.TYPE;
    ret = 0;
//  -> LB00670;
    goto LB00670;
// 
// LB00667:
    LB00667: ;
// 
// DREG => REG.FROM.TYPE;
    ret = DREG;
//  -> LB00670;
    goto LB00670;
// 
// LB00668:
    LB00668: ;
// 
// AREG => REG.FROM.TYPE;
    ret = AREG;
//  -> LB00670;
    goto LB00670;
// 
// LB00669:
    LB00669: ;
// 
// VEC => REG.FROM.TYPE;
    ret = VEC;
//  -> LB00670;
    goto LB00670;
// 
// LB00666:
    LB00666: ;
// 
// IF KIND /= 0,-> LB00668;
    if (KIND != 0) goto LB00668;
//  -> LB00667;
    goto LB00667;
// 
// LB00670:
    LB00670: ;
// 
// $EN
    return ret;
    }
// $PR CHECK.TYPE(OP,OLD,NEW);
    static INTEGER CHECK_TYPE (INTEGER OP, INTEGER OLD, INTEGER NEW) {
    INTEGER ret;
// ::DECL & INIT
// IF OLD <256 AND OLD & %C3 = TLO0,-> LB00687;
    if (OLD < 256 && (OLD & 0xc3) == TLO0) goto LB00687;
// LB00688:
    LB00688: ;
// 
// IF OLD = TINT64,-> LB00689;
    if (OLD == TINT64) goto LB00689;
// LB00674:
    LB00674:
// 
// IF NEW < 256 AND NEW & %C3 = TLO0,-> LB00690;
    if (NEW < 256 && (NEW & 0xc3) == TLO0) goto LB00690;
// LB00675:
    LB00675:
// 
// IF DLIST[OP] & %1000 /= 0,-> LB00683;
    if ((DLIST[OP] & 0x1000) != 0) goto LB00683;
// IF OLD =0,-> LB00681;
    if (OLD == 0) goto LB00681;
// IF OLD = NEW,-> LB00686;
    if (OLD == NEW) goto LB00686;
// IF OLD > 256 OR NEW > 256
    if (OLD > 256 || NEW > 256
// OR OLD = TLAB
        || OLD == TLAB
// OR NEW = TLAB
        || NEW == TLAB
// OR OLD & 3 /= 0
        || (OLD & 3) != 0
// OR NEW & 3 /= 0,-> LB00684;
        || (NEW & 3) != 0) goto LB00684;
// IF NEW & %C0 => OP /= OLD & %C0,-> LB00685;
    if ((OP = (NEW & 0xc0)) != (OLD & 0xc0)) goto LB00685;
// IF OLD >= NEW,-> LB00686;
    if (OLD >= NEW) goto LB00686;
// LB00681:
    LB00681: ;
// 
// NEW => CHECK.TYPE;
    ret = NEW;
//  -> LB00682;
    goto LB00682;
// 
// LB00683:
    LB00683: ;
// 
// IF NEW <256 AND NEW & %C3 = TINT0,-> LB00686;
    if (NEW < 256 && (NEW & 0xc3) == TINT0) goto LB00686;
// LB00684:
    LB00684: ;
// 
// MONITOR(3);
    MONITOR (3);
// LB00685:
    LB00685: ;
// 
// IF OP = 0,-> LB00681;
    if (OP == 0) goto LB00681;
// LB00686:
    LB00686: ;
// 
// OLD => CHECK.TYPE;
    ret = OLD;
//  -> LB00682;
    goto LB00682;
// 
// LB00687:
    LB00687: ;
// 
// OLD -TINT0 => OLD;
    OLD = OLD - TINT0;
//  -> LB00688;
    goto LB00688;
// 
// LB00689:
    LB00689: ;
// 
// IF NEW & %3 = 0
    if ((NEW & 0x3) == 0
// OR OP /= ISTORE
        || (OP != ISTORE
// AND DLIST[OP] & %800 = 0,-> LB00674;
        && (DLIST[OP] & 0x800) == 0)) goto LB00674;
//  -> LB00681;
   goto LB00681;
// 
// LB00690:
    LB00690: ;
// 
// NEW -TINT0 => NEW;
    NEW = NEW - TINT0;
// IF NEW /= TINT64,-> LB00675;
    if (NEW != TINT64) goto LB00675;
// IF OLD & %3 = 0
    if ((OLD & 0x3) == 0
// OR OP /= ISTORE
        || (OP != ISTORE
// AND DLIST[OP] & %800 = 0,-> LB00675;
        && (DLIST[OP] & 0x800) == 0)) goto LB00675;
// OLD => CHECK.TYPE;
    ret = OLD;
// LB00682:
    LB00682:
// 
// END
    return ret;
    }




// $PR PRINT.AR;
void PRINT_AR (void) {
// $IN I;
    INTEGER I;
// SELECTOUTPUT(MSTR);
    SELECTOUTPUT (MSTR);
// NEWLINES(0);
    NEWLINES (0);
// CAPTION(%"AR");
    CAPTION ((ADDR_LOGICAL8) & vec ("AR"));
// 0 => I;
    I = 0;
// L1:
    L1: ;
// NEWLINES(0);
    NEWLINES (0);
// L2:
    L2: ;
// OUTHEX(AR[I],8);
    OUTHEX (AR [I], 8);
// IF 1 +> I > AP, -> OUT;
    if (++I > AP) goto OUT;
// IF I & 7 = 0, -> L1;
    if ((I & 7) == 0) goto L1;
// SPACES(2);
    SPACES (2);
// -> L2;
    goto L2;
// OUT:
    OUT: ;
// NEWLINES(0);
    NEWLINES (0);
// $EN
}


// $PS INIT.S7();
// PROC INIT.S7;
void INIT_S7 (void) {
// END
}

// *END
