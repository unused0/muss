#include <stddef.h>
#include "muss.h"
#include "kernel.h"
#include "lib.h"
#include "libm.h"
#include "mutl.h"
#include "nmsl.h"


// 
// $LO8[64] XNAME; ::TEMP MOD
// $LS SELECTOUTPUT($IN);
// $LS NEWLINES($IN);
// $LS CAPTION(ADDR[$LO8]);
// $LS TL.LABEL.SPEC(ADDR[$LO8],$IN);
// $PS MONITOR($IN);
// $IN MSTR,MUTLSTR;
// $IN IX;
// $IM $LI NLISTZ,PLISTZ,CHLISTZ;
// $TY NLIST.ENT IS $LO16 MUTLN,MUSLN,HASH; ::TEMP
// NLIST.ENT[NLISTZ] NLIST; ::TEMP
// $LO8[CHLISTZ] CHLIST; ::TEMP
// $TY PLIST.ENT IS $LO16 INTID,PREVN,K,T $IN32 DETAIL;
// PLIST.ENT[PLISTZ] PLIST;
// $IN CUR.BLK,LASTMN,PROCLEV;
// $IM $LI KTYPE,KDUMMY,KGLAB,KLABREF,KIMPLAB,KLAB,KTREF;
// $IM $LI KPSPEC,KPROC;
// MODULE(ADDN,GENN,DECLARE.N,DUMMY,MUTLN,EXPN,
// ::TEMP MOVE TO SECTION 2 NLIST.ENT,NLIST,CHLIST,XNAME,
// XNPTR,LASTN,LASTCH,INIT.S8);
// ::DECLARATIONS
// ::TEMP MOVE TO SECT 2 $TY NLIST.ENT IS $LO16 MUTLN,MUSLN,HASH;
// *GLOBAL 5;
// ::TEMP MOVE TO SECT 2$LO8 [CHLISTZ] CHLIST;
// ::TEMP MOVE TO SECT 2 NLIST.ENT [NLISTZ] NLIST;
// $IN LASTCH,LASTN,XNPTR;
INTEGER LASTCH, LASTN, XNPTR;
// ::TEMP MOVE TO SECT 2 $LO8[64] XNAME;
// *ifdef PDP
// *GLOBAL 0;
// *endif // PDP
// $LI/ADDR[$LO8] NIL=;
// $PS ADDN()/$LO16;
extern LOGICAL16 ADDN (void);
// $PS GENN($LO16)/ADDR [$LO8];
extern ADDR_LOGICAL8 GENN (LOGICAL16);
// $PS DECLARE.N($LO16,$IN,$IN)/$LO16;
extern LOGICAL16 DECLARE_N (LOGICAL16, INTEGER, INTEGER);
// $PS DUMMY()/$LO16;
extern LOGICAL16 DUMMY (void);
// $PS MUTLN($LO16)/$LO16;
extern LOGICAL16 MUTLN (LOGICAL16);
// $PS EXPN($LO16);
extern void EXPN (LOGICAL16);



#ifdef PDP
// *CODE 1;
#endif // PDP


// $PR ADDN;
LOGICAL16 ADDN (void) {
    LOGICAL16 ret;
// $IN I,J,K,L,HASH1,N;
    INTEGER I,J,K,L,HASH1,N;
// $LI LIT = 'A - 'a + %80;
#   define LIT ('A' - 'a' + 0x80)
// LASTCH => I + 1 => K;
    K = (I = LASTCH) + 1;
// 0 => HASH1;
    HASH1 = 0;
// $WH CHLIST[1+>I] => J /= 0 DO
    while ((J = CHLIST[++I]) != 0) {
// IF J >= 'a THEN
      if (J >= 'a') {
// J + LIT => J => CHLIST[I] FI
        CHLIST[I] = J = (J + LIT); }
// J & %1F + HASH1 & %3FF <<- 1 => HASH1 OD
      HASH1 = (((J & 0x1F) + HASH1) & 0x3FF) << 1; }
// I - LASTCH - 1 => N + (HASH1 <<- 5) => HASH1;
    HASH1 = (N = (I - LASTCH) - 1) + (HASH1 << 5);
// 1 + LASTN => I;
    I = 1 + LASTN;
// LB00010:
    LB00010: ;
// 
// IF 1 -> I < 0,-> LB00017;
    if ((--I) < 0) goto LB00017;
// IF HASH OF NLIST[I] /= HASH1,-> LB00010;
    if (NLIST[I].HASH != HASH1) goto LB00010;
// MUSLN OF NLIST[I] => J;
    J = NLIST[I].MUSLN;
// 0 => L;
    L = 0;
// LB00013:
    LB00013:
// 
// IF CHLIST[J+L]&%7F /= CHLIST[K+L]&%7F,-> LB00010;
    if ((CHLIST[J+L] & 0x7F) != (CHLIST[K+L] & 0x7F)) goto LB00010;
// IF 1 +> L /= N,-> LB00013;
    if (++ L != N) goto LB00013;
// I => ADDN;
    ret = I;
//  -> LB00016;
    goto LB00016;
// 
// LB00017:
    LB00017: ;
// 
// 0 => MUTLN OF NLIST[1 +> LASTN];
    NLIST[++ LASTN].MUTLN = 0;
// LASTCH + 1 => MUSLN OF NLIST[LASTN];
    NLIST[LASTN].MUSLN = LASTCH + 1;
// HASH1 => HASH OF NLIST[LASTN];
    NLIST[LASTN].HASH = HASH1;
// N +> LASTCH;
    LASTCH += N;
// IF LASTN >= NLISTZ OR LASTCH >= CHLISTZ THEN
    if (LASTN >= NLISTZ || LASTCH >= CHLISTZ) {
// SELECTOUTPUT(MSTR);
      SELECTOUTPUT(MSTR);
// NEWLINES(0);CAPTION(%"NLIST");MONITOR(9);FI
      NEWLINES(0);CAPTION((ADDR_LOGICAL8) & vec ("NLIST"));MONITOR(9);}
// LASTN => ADDN;
      ret = LASTN;
// LB00016:
    LB00016: ;
// 
// END
    return ret;
    }
// $PR GENN(INTID);
ADDR_LOGICAL8 GENN (LOGICAL16 INTID) {
    ADDR_LOGICAL8 ret;
// $IN FSTCH,LSTCH,I,N;
    INTEGER FSTCH,LSTCH,I,N;
// MUSLN OF NLIST[INTID] => FSTCH => LSTCH;
    LSTCH = FSTCH = NLIST[INTID].MUSLN;
// HASH OF NLIST[INTID] & %3F => N;
    N = NLIST[INTID].HASH & 0x3F;
// $IF IX & %4000 /= 0,-> LB00026;
    if ((IX & 0x4000) != 0) goto LB00026;
// N - 1 +> LSTCH;
    LSTCH += (N - 1);
// PART(^CHLIST,FSTCH,LSTCH) => GENN;
    ret = PART(& CHLIST,FSTCH,LSTCH);
//  -> LB00024;
    goto LB00024;
// 
// LB00026:
    LB00026: ;
// 
// XNPTR => I;
    I = XNPTR;
// 1 -> LSTCH;
    --LSTCH;
// FOR N DO
    { INTEGER limit = N;
      for (INTEGER i = 0; i < limit; i ++) {
// CHLIST[1+>LSTCH] => XNAME[1+>I] $OD
      XNAME[++I] = CHLIST[++LSTCH]; } }
// PART(^XNAME,0,I) => GENN;
    ret = PART(&XNAME,0,I);
// LB00024:
    LB00024: ;
// 
// $EN
    return ret;
    }


// $PR DECLARE.N(ID,KIND,TY);
LOGICAL16 DECLARE_N (LOGICAL16 ID, INTEGER KIND, INTEGER TY) {
    LOGICAL16 ret;
// $IN OLDN;
    INTEGER OLDN;
// %8000 &> IX;
    IX &= 0x8000;
// $IF MUTLN OF NLIST[ID]=>OLDN & %3FFF => DECLARE.N/=1
    if ((ret = (OLDN = NLIST[ID].MUTLN) & 0x3FFF) != 1
// $OR PROCLEV /= 0,-> LB00040;
         || PROCLEV != 0) goto LB00040;
// %4000 => IX;
    IX = 0x4000;
// LB00031:
    LB00031: ;
// 
// IF 1+>LASTMN=>DECLARE.N >= PLISTZ THEN
    if ((ret = (++LASTMN)) >= PLISTZ) {
// NEWLINES(0);CAPTION(%"PLIST");MONITOR(9);FI
      NEWLINES(0);CAPTION((ADDR_LOGICAL8) & vec ("PLIST"));MONITOR(9); }
// OLDN => PREVN OF PLIST[DECLARE.N];
    PLIST[ret].PREVN = OLDN;
// DECLARE.N => MUTLN OF NLIST[ID];
    NLIST[ID].MUTLN = ret;
// ID => INTID OF PLIST[DECLARE.N]
    PLIST[ret].INTID = ID;
// $IF KIND < KGLAB,-> LB00034;
    if (KIND < KGLAB) goto LB00034;
// $IF KIND = KGLAB $TH
    if (KIND == KGLAB) {
// TL.LABEL.SPEC(GENN(ID),IX ! 1) $EL
      TL_LABEL_SPEC(GENN(ID),IX | 1); } else {
// TL.LABEL.SPEC(GENN(ID),IX) $FI
      TL_LABEL_SPEC(GENN(ID),IX); }
// LB00034:
    LB00034: ;
// 
// $IF KIND = K.IMP.LAB $TH
    if (KIND == K_IMP_LAB) {
// KGLAB => KIND
    KIND = KGLAB;
// $EL IF KIND = KPSPEC AND IX & %8000 /= 0 $TH
    } else { if (KIND == KPSPEC && (IX & 0x8000) != 0) {
// KPROC => KIND
      KIND = KPROC;
// FI FI
    } }
// KIND => K OF PLIST[DECLARE.N];
    PLIST[ret].K = KIND;
// TY => T OF PLIST[DECLARE.N];
    PLIST[ret].T = TY;
//  -> LB00042;
    goto LB00042;
// 
#if 0
// LB00036:
    LB00036: ;
// 
// $IF KIND = KLAB AND K OF PLIST[DECLARE.N]=KLAB.REF,-> LB00034;
    if (KIND == KLAB && PLIST[ret].K == KLAB_REF) goto LB00034;
// IF KIND = KTYPE AND K OF PLIST[DECLARE.N] = KTREF,-> LB00044;
    if (KIND == KTYPE && PLIST[ret].K == KTREF) goto LB00044;
#else
LB00037: ;
#endif
// MONITOR(7);
    MONITOR(7);
// LB00040:
    LB00040: ;
// 
#if 0 // flip fix
// $IF DECLARE.N < CUR.BLK,-> LB00036;
// [rj] XXX
#if 0
    if (ret < CUR_BLK) goto LB00036;
#else
/* possible FLIP error requires translation to invert the condition below */
/**/if (ret >= CUR_BLK) goto LB00036;
#endif
//  -> LB00031;
    goto LB00031;
#else
// $IF DECLARE.N < CUR.BLK,-> LB00031;
    if (ret < CUR_BLK) goto LB00031;
// $IF KIND = KLAB AND K OF PLIST[DECLARE.N]=KLAB.REF,-> LB00034;
    if (KIND == KLAB && PLIST[ret].K == KLAB_REF) goto LB00034;
// IF KIND = KTYPE AND K OF PLIST[DECLARE.N] = KTREF,-> LB00044;
    if (KIND == KTYPE && PLIST[ret].K == KTREF) goto LB00044;
//  -> LB00037;
    goto LB00037;
#endif
// 
// LB00044:
    LB00044: ;
// 
// OLDN !> IX;
    IX |= OLDN;
//  -> LB00034;
    goto LB00034;
// 
// LB00042:
    LB00042: ;
// 
// $EN
    return ret;
    }




// $PR DUMMY;
LOGICAL16 DUMMY (void) {
// KDUMMY => K OF PLIST[1+>LASTMN];
    PLIST[++LASTMN].K = KDUMMY;
// LASTMN => DUMMY;
    return LASTMN;
// END
  }

// $PR MUTLN(INTID);
LOGICAL16 MUTLN (LOGICAL16 INTID) {
// MUTLN OF NLIST[INTID] & %3FFF => MUTLN;
    return NLIST[INTID].MUTLN & 0x3FFF;
// END
    }


// $PR EXPN(ID);
void EXPN (LOGICAL16 ID) {
// IF MUTLN OF NLIST[ID] = 0 THEN
    if (NLIST[ID].MUTLN == 0) {
// 1 => MUTLN OF NLIST[ID];
      NLIST[ID].MUTLN = 1;
// ELSE %4000 !> MUTLN OF NLIST[ID] FI
    } else { NLIST[ID].MUTLN |= 0x4000; }
// END
   }

// $PS INIT.S8();
extern void INIT_S8 (void);
// $PR INIT.S8;
void INIT_S8 (void) {
// $IN TEMP;
    INTEGER TEMP;
// -1 => LASTCH => LASTN;
    LASTN = LASTCH = -1;
// 'V => CHLIST[0];
    CHLIST[0] = 'V';
// 'S => CHLIST[1];
    CHLIST[1] = 'S';
// 'U => CHLIST[2];
    CHLIST[2] = 'U';
// 'B => CHLIST[3];
    CHLIST[3] = 'B';
// 0 => CHLIST[4];
    CHLIST[4] = '\0';
// ADDN() => TEMP;
    TEMP = ADDN ();
// %1002 => MUTLN OF NLIST[TEMP];
    NLIST[TEMP].MUTLN = 0x1002;
// END
   }

// ::END OF MODULE MSL081
// *END
