#include <stddef.h>
#include "muss.h"
#include "kernel.h"
#include "lib.h"
#include "libm.h"
#include "mutl.h"
#include "nmsl.h"


// 
// $LA RESTART;
// $IN IPTR,SPTR,CWORD;
// $LS SELECTOUTPUT($IN);
// $LS SPACES($IN);
// $LS NEWLINES($IN);
// $LS OUTLINE.NO($IN32);
// $LS OUTI($IN32,$IN);
// $LS CAPTION($AD[$LO8]);
// $LS OUTCH($IN);
// $LO8[2000] SBUFF;
// $IN MSTR,MUTLSTR;
// MODULE (MONITOR,FAULTS,LINE.NO,INIT.S9,LASTT,LASTP,PRINT.STATS,
// MAXCH,MAXN,MAXMUTLN);
// *GLOBAL 5;
// $IN FAULTS,MAXMUTLN,MAXN,MAXCH,LASTT,LASTP;
INTEGER FAULTS, MAXMUTLN, MAXN, MAXCH, LAST_T, LAST_P;
// $IN32 LINENO;
INTEGER LINE_NO;
// $IN MESS.IND;
static INTEGER MESS_IND;
// $PS MONITOR ($IN);
extern void MONITOR (INTEGER);
// $PS PRINT.STATS();
extern void PRINT_STATS (void);


#ifdef PDP
//  *CODE 1;
#endif // PDP




// $PR MONITOR(MNO);
void MONITOR (INTEGER MNO) {
// $IN QPOS,I,J,CH;
    INTEGER QPOS,I,J,CH;
// IF MNO = 200 THEN
    if (MNO == 200) {
// 4 +> MESS.IND FI
      MESS_IND += 4; }
// IF MNO > 199 AND CWORD & 32 /= 0,-> LB00015;
    if (MNO > 199 && (CWORD & 32) != 0) goto LB00015;
// SELECTOUTPUT(MSTR);
    SELECTOUTPUT(MSTR);
// NEWLINES(0);
    NEWLINES(0);
// OUTLINENO(LINE.NO);
    OUTLINENO(LINE_NO);
// SPACES(MESS.IND);
    SPACES(MESS_IND);
// $IF MNO > 199,-> LB00017;
    if (MNO > 199) goto LB00017;
// CAPTION(%">>>>*");
    CAPTION((ADDR_LOGICAL8) & vec (">>>>*"));
// $DA FMESS($LO8)
// "SYNTAX! "
// "NOTDEF! "
// "SCOPE!  "
// "TYPE!   "
// "ILLEGAL!"
// "ENDS?   "
// "INVALID!"
// "DEFINED!"
// "FIELD?  "
// "SUBS?   "
// "CONTEXT?"
// "PARAM?  "
// "ACCESS! "
// "NO FI!  "
// "LEXICAL!"
// "NO OD!  "
// $EN
    LOGICAL8 FMESS[] = {
      'S', 'Y', 'N', 'T', 'A', 'X', '!', ' ',
      'N', 'O', 'T', 'D', 'E', 'F', '!', ' ',
      'S', 'C', 'O', 'P', 'E', '!', ' ', ' ',
      'T', 'Y', 'P', 'E', '!', ' ', ' ', ' ',
      'I', 'L', 'L', 'E', 'G', 'A', 'L', '!',
      'E', 'N', 'D', 'S', '?', ' ', ' ', ' ',
      'I', 'N', 'V', 'A', 'L', 'I', 'D', '!',
      'D', 'E', 'F', 'I', 'N', 'E', 'D', '!',
      'F', 'I', 'E', 'L', 'D', '?', ' ', ' ',
      'S', 'U', 'B', 'S', '?', ' ', ' ', ' ',
      'C', 'O', 'N', 'T', 'E', 'X', 'T', '?',
      'P', 'A', 'R', 'A', 'M', '?', ' ', ' ',
      'A', 'C', 'C', 'E', 'S', 'S', '!', ' ',
      'N', 'O', ' ', 'F', 'I', '!', ' ', ' ',
      'L', 'E', 'X', 'I', 'C', 'A', 'L', '!',
      'N', 'O', ' ', 'O', 'D', '!',   
    };
// MNO & 15 * 8 => J;
    J = (MNO & 15) * 8;
// CAPTION(PART(^FMESS,J,J+7));
    CAPTION(PART(& FMESS,J,J+7));
// IPTR => QPOS;
    QPOS = IPTR;
// LB00013:
    LB00013: ;
// 
// 0 => I
    I = 0;
// $WH 1 +> I =< SPTR $DO
    while (++I <= SPTR) {
// $IF SBUFF[I] => CH < 128 $TH
    if ((CH = SBUFF[I]) < 128) {
// OUTCH(CH)
      OUTCH(CH);
// $EL $IF CH & 127 = QPOS $TH
    } else { if ((CH & 127) == QPOS) {
// OUTCH('<);
      OUTCH('<');
// OUTCH('?);
      OUTCH('?');
// 10000 => QPOS;
      QPOS = 10000;
// $FI $FI $OD
    } } }
// NEWLINES(1);
    NEWLINES(1);
// $IF MNO < 99,-> LB00019;
    if (MNO < 99) goto LB00019;
// LB00015:
    LB00015: ;
// 
// IF MNO = 201 THEN
    if (MNO == 201) {
// 4 -> MESS.IND FI
    MESS_IND -= 4; }
//  -> LB00016;
    goto LB00016;
// 
// LB00017:
    LB00017:
// 
// CAPTION(%">>>>>   ");
    CAPTION((ADDR_LOGICAL8) & vec (">>>>>   "));
// 10000 => QPOS;
    QPOS = 10000;
//  -> LB00013;
    goto LB00013;
// 
// LB00019:
    LB00019:
// 
// -1 => FAULTS;
    FAULTS = -1;
// -> RESTART;
    longjmp (RESTART, 1);
// LB00016:
    LB00016: ;
// 
// $EN
    }

// $PR PRINT.STATS;
void PRINT_STATS (void) {
// SELECTOUTPUT(MSTR);
    SELECTOUTPUT(MSTR);
// NEWLINES(0);
    NEWLINES(0);
// OUTI(MAXN,5);
    OUTI(MAXN,5);
// OUTI(MAXCH,5);
    OUTI(MAXCH,5);
// OUTI(MAXMUTLN,5);
    OUTI(MAXMUTLN,5);
// OUTI(LASTT,5);
    OUTI(LASTT,5);
// OUTI(LASTP,5);
    OUTI(LASTP,5);
// SELECTOUTPUT(MUTLSTR);
    SELECTOUTPUT(MUTLSTR);
// END
    }



// $PS INIT.S9();
extern void INIT_S9 (void);
// $PR INIT.S9;
void INIT_S9 (void) {
// 0=>MAXN=>MAXCH=>MAXMUTLN=>FAULTS;
    FAULTS = MAXMUTLN = MAXCH = MAXN = 0;
// -4=> MESS.IND;
    MESS_IND = -4;
// 0 => FAULTS => LINE.NO;
    LINE_NO = FAULTS = 0;
// END
  }
// *END
