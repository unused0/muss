//
// nmsl091
//

// MODULE (MONITOR,FAULTS,LINE.NO,INIT.S9,LASTT,LASTP,PRINT.STATS,
// MAXCH,MAXN,MAXMUTLN);
extern void MONITOR (INTEGER);
extern INTEGER FAULTS, MAXMUTLN, MAXN, MAXCH, LAST_T, LAST_P;
#define LASTT LAST_T
#define LASTP LAST_P
extern INTEGER32 LINE_NO;
#define LINENO LINE_NO
extern void INIT_S9 (void);
extern void PRINT_STATS (void);

