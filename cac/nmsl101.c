#include "muss.h"
#include "kernel.h"
#include "lib.h"
#include "libm.h"
#include "mutl.h"
#include "nmsl.h"
#include "vax.h"


// mtl121
//
// Programs in MUBL exist as files of bytes.
// Each statement is a sequence of one or more bytes in which
// the first byte identifies the kind of statement, and hence the MUTL
// procedures that are to be called, and the remaining bytes supply the
// parameters. There is a one to one correspondence between MUTL
// procedures and MUBL statements.
//
// An encoding is described by adding encoding information enclosed in
// < > brackets to the MUTL procedure specifications.
//
// For example, <C8 %04>TL.DATA.AREA(<U8>AREA), means that the
// MUBL statement for TL.DATA.AREA has two encoding fields. The first
// is one byte long and contains %04 which identifies the MUTL procedure,
// and the second is also one byte long and contains an usigned quantity
// which is the parameter.
//
// The different types of encoding fields are as follows.
//
// 1. <Cn  %hex value>
// Such encoding fields are employed to identify the MUTL procedure.
// The field width in bits is specified by n, which is followed by the
// hexadecimal value for the field.
//
// 2. <Un> The encoding field is n bits long and contains as unsigned
// integer.
// 
// 3. <UV> IThe field contains a width specifier followed by a
// value specifier, the position of the most significant zero
// bit in the field indicates the last bit of the width specifier.
// It is the number of bits in the width specifier that defines the number
// of bytes in the field. The value specifier part contains an
// unsigned integer. For example, if the first byte has the value %94,
// the encoding field is two bytes long and it contains a 14 bit
// unsigned integer value for the parameter.
//
// 4. <IV> Similar to <UV> except the value specifier contains a signed
// integer.
//
// 5. <S> The field contains a byte count encoded in <UV> form that specifies how
// many more
// bytes there are in the rest of the field. This type of encoding field
// is normally used for parameters which contain a pointer to a byte
// vector, in which case the length of the vector and the vector contents
// are put in the encoding field.
//
// Except for some of the code planting procedure encodings the width
// of parameter encoding fields is always a byte multiple.
//
// The binary representations of characters, integers and reals
// varies between machines, thus some adjustment of literals may be
// necessary when decoding MUBL.
//
// TYPE DEFINITIONS
//
// <C8 %00>TL.TYPE(<S>[SYMB.NAME],<UV>NATURE)
// <C8 %01>TL.TYPE.COMP(<UV>TYPE,<IV>DIMENSION,<S>[SYMB.NAME])
// <C8 %02>TL.END.TYPE(<U8>STATUS)
// 
// AREA SELECTION AND EQUIVALENCING
//
//
// <C8 %15>TL.SEG(<U8>SEG.NUMBER,<UV>SIZE,<IV>RUN.TIME.ADDR,
//                <IV>COMP.TIME.ADDR,<U8>KIND)
// <C8 %2C>TL.LOAD(<U8>SEG.NO,<U8>AREA.NO)
// <C8 %03>TL.CODE.AREA(<U8>AREA.NUMBER)
// <C8 %04>TL.DATA.AREA(<IV>AREA.NUMBER)
// <C8 %06>TL.COMMON(<S>[SYMB.NAME],<UV>SIZE,<U16>NATURE)
// <C8 %16>TL.END.COMMON()
// <C8 %38>TL.EQUIV.POS(<UV>POSITION)
//
// DATA DECLARATIONS
//
// 
// <C8 %07>TL.SPACE(<UV>SIZE)
// <C8 %08>TL.S.DECL(<S>[SYMB.NAME],<IV>TYPE,<IV>DIMENSION)
// <C8 %09>TL.V.DECL(<S>[SYMB.NAME],<UV>POSN,<U16>PRE.PROC,
//                   <U16>POST.PROC,<UV>TYPE,<IV>DIMENSION)
// <C8 %0A>TL.MAKE(<U16>SPACE,<U16>TYPE,<IV>DIMENSION)
// <C8 %3B>TL.SELECT.VAR()
// <C8 %2E>TL.SELECT.FIELD(<UV>BASE,<UV>ALTERNATIVE,<UV>FIELD)
// <C8 %28>TL.SET.TYPE(<UV>NAME,<UV>TYPE)
// <C8 %26>TL.ASS(<UV>MUTL.NAME.OR.TYPE,<IV>AREA.NO)
// <C8 %0F>TL.ASS.VALUE(<UV>NAME,<UV>REPEAT.CNT)
// <C8 %0D>TL.ASS.END()
// <C8 %0E>TL.ASS.ADV(<UV>NO)
//
// LITERAL DECLARATIONS
//
//
// <C8 %10>TL.C.LIT.16(<UV>BASIC.TYPE,<IV>VALUE.16)
// <C8 %35>TL.C.LIT.32(<UV>BASIC.TYPE,<IV>VALUE.32)
// <C8 %36>TL.C.LIT.64(<UV>BASIC.TYPE,<UV>VALUE.64)
// <C8 %37>TL.C.LIT.128(<UV>BASIC.TYPE,<UV>VALUE.128)
// <C8 %3C>TL.C.LIT.S(<UV>BASIC.TYPE,<S>[VALUE])
// <C8 %39>TL.C.NULL(<U16>TYPE)
// <C16 %3F00>TL.C.DATA(<UV>BASIC.TYPE,<UV>MUTL.NAME,<IV>ELEMENT.NO)
// <C8 %3A>TL.C.TYPE(<UV>BASIC.TYPE,<U8>KIND,<UV>MUTL.NAME)
// <C8 %0B>TL.LIT(<S>[SYMB.NAME],<UV>KIND)
// <C8 %2A>TL.CALC(<U8>OP.CODE,<UV>OPERAND>
//
// PROGRAM STRUCTURE DECLARATIONS
//
//
// <C8 %11>TL.PROC.SPEC(<S>[SYMB.NAME],<UV>NATURE)
// <C8 %12>TL.PROC.PARAM(<U16>TYPE,<IV>DIMENSION)
// <C8 %27>TL.PROC.RESULT(<UV>RESULT)
// <C8 %13>TL.PROC(<UV>MUTL.NAME)
// <C8 %0C>TL.PARAM.NAME(<UV>MUTL.NAME,<S>[SYMB.NAME])
// <C8 %29>TL.PROC.KIND(<U8>KIND)
// <C8 %14>TL.END.PROC()
// <C8 %31>TL.ENTRY(<U16>NAME)
// <C8 %32>TL.I.PARAM(<UV>MUTL.NAME,<UV>PARAM.NO,<U16>TYPE)
// <C8 %19>TL.BLOCK()
// <C8 %1A>TL.END.BLOCK()
//
// LABEL DECLARATION
//
//
// <C8 %1B>TL.LABEL.SPEC(<S>[SYMB.NAME],<U8>LABEL.USE)
// <C8 %1C>TL.LABEL(<U16>MUTL.NAME)
//
// CODE PLANTING
//
//
// <C1 %1>TL.PL(<U7>FN,<U0>OP) OR
// <C2 %1>TL.PL(<U7>FN,<U15>OP)
// <C8 %1D>TL.D.TYPE(<U16>TYPE,<IV>DIM)
// <C8 %34>TL.CHECK(<U16>STATUS)
// <C8 %2B>TL.INSERT(<UV>BINARY)
// <C8 %1F>TL.CYCLE(<U16>LIMIT)
// <C8 %20>TL.REPEAT()
// <C8 %3D>TL.CV.CYCLE(<U16>CV,<U16>INIT,<U8>MODE)
// <C8 %3E>TL.CV.LIMIT(<U16>LIMIT,<U8>TEST)
// <C8 %1E>TL.REG(<U8>REG.USE)
// <C8 %05>TL.RANGE(<U16>REG,<U8>KIND,<IV>L.LIMIT,<IV>U.LIMIT)
//
// Note for the TL.PL procedure the first encoding is used whenever the
// OP parameter has the value zero.
//
// MUTL INITIALISATION AND MODE CONTROL
//
//
// <C8 %22>TL(<U8>MODE,<S>FILENAME,<UV>DIRECTORY.SIZE)
// <C8 %23>TL.END()
// <C8 %24>TL.MODULE()
// <C8 %25>TL.END.MODULE(<UV>STATUS)
// <C16 %3F01>TL.GLOBAL()
// <C8 %30>TL.MODE(<U16>MODE,<UV>INFO)
//
// DIAGNOSTICS
//
//
// <C8 %21>TL.LINE(<UV>LINE.NO)
// <C8 %17>TL.BOUNDS(NO ENCODING YET DEFINED)
// <C8 %18>TL.PRINT<UV>MODE)
//
// MUBL ENCODINGS EMPLOYED OUTSIDE MANCHESTER UNIVERSITY
//
//
// <C8 %2F>TL.LANG(specification not known) - Computervision
// <C8 %2D>TL.MODNAM(specification not known) - Computervision
// <C8 %33>TL.ECB(specification not known) - Computervision
//






// 
// 
// $LS OUT.BIN.B ($IN);
// $LS SELECTOUTPUT($IN);
// $IN MUTLSTR;
// $LS IPOS ()/$IN32;
// MODULE(TL.TYPE,TL.TYPE.COMP,TL.END.TYPE,TL.SEG,TL.LOAD,
// TL.CODE.AREA,TL.DATA.AREA,TL.SPACE,TL.S.DECL,TL.V.DECL,TL.MAKE,
// TL.SELECT.VAR,TL.SELECT.FIELD,TL.ASS,TL.ASS.VALUE,TL.ASS.END,
// TL.C.LIT.16,TL.C.LIT.32 ,TL.C.LIT.64,TL.C.NULL,TL.C.LIT.S,
// TL.C.LIT.128,TL.LIT,TL.PROC.SPEC,TL.PROC.PARAM,TL.PROC.RESULT,
// TL.PROC,TL.PROC.KIND,TL.END.PROC,TL.BLOCK,TL.END.BLOCK,TL.ENTRY,
// TL.LABEL.SPEC ,TL.LABEL,TL.PL,
// TL.D.TYPE,TL.INSERT,TL.CHECK,TL.REG,TL.LINE,TL.PRINT,TL,TL.END,
// TL.MODULE,TL.END.MODULE,TL.MODE);


#ifdef PDP
// *CODE 1;
#endif // PDP

// ::PSPECS FOR MUTL
// PSPEC TL.TYPE(ADDR[$LO8],$IN);
extern void TL_TYPE (ADDR_LOGICAL8, INTEGER);
// PSPEC TL.TYPE.COMP($IN,ADDR,ADDR[$LO8]);
extern void TL_TYPE_COMP (INTEGER, ADDR, ADDR_LOGICAL8);
// PSPEC TL.END.TYPE($IN);
extern void TL_END_TYPE (INTEGER);
// PSPEC TL.SEG($IN,ADDR,$LO32,ADDR,$IN);
extern void TL_SEG (INTEGER, ADDR, LOGICAL32, ADDR, INTEGER);
// PSPEC TL.LOAD($IN,$IN);
extern void TL_LOAD (INTEGER, INTEGER);
// PSPEC TL.CODE.AREA($IN);
extern void TL_CODE_AREA (INTEGER);
// PSPEC TL.DATA.AREA($IN);
extern void TL_DATA_AREA (INTEGER);
// PSPEC TL.SPACE(ADDR);
extern void TL_SPACE (ADDR);
// PSPEC TL.S.DECL(ADDR[$LO8],$IN,ADDR);
extern void TL_S_DECL (ADDR_LOGICAL8, INTEGER, ADDR);
// PSPEC TL.V.DECL(ADDR[$LO8],$LO32,$IN,$IN,$IN,ADDR);
extern void TL_V_DECL (ADDR_LOGICAL8, LOGICAL32, INTEGER, INTEGER, INTEGER, ADDR);
// PSPEC TL.MAKE($IN,$IN,ADDR);
extern void TL_MAKE (INTEGER, INTEGER, ADDR);
// PSPEC TL.SELECT.VAR();
extern void TL_SELECT_VAR (void);
// PSPEC TL.SELECT.FIELD($IN,$IN,$IN);
extern void TL_SELECT_FIELD (INTEGER, INTEGER, INTEGER);
// PSPEC TL.ASS($IN,$IN);
extern void TL_ASS (INTEGER, INTEGER);
// PSPEC TL.ASS.VALUE($IN,$IN);
extern void TL_ASS_VALUE (INTEGER, INTEGER);
// PSPEC TL.ASS.END();
extern void TL_ASS_END (void);
// PSPEC TL.C.LIT.16($IN,$IN16);
extern void TL_C_LIT_16 (INTEGER, INTEGER16);
// PSPEC TL.C.LIT.32 ($IN,$IN32);
extern void TL_C_LIT_32 (INTEGER, INTEGER32);
// PSPEC TL.C.LIT.64($IN,$LO64);
extern void TL_C_LIT_64 (INTEGER, LOGICAL64);
// PSPEC TL.C.NULL($IN);
extern void TL_C_NULL (INTEGER);
// PSPEC TL.C.LIT.S($IN,$AD [$LO8]);
extern void TL_C_LIT_S (INTEGER, ADDR_LOGICAL8);
// PSPEC TL.C.LIT.128($IN,$RE128);
extern void TL_C_LIT_128 (INTEGER, REAL128);
// PSPEC TL.LIT(ADDR[$LO8],$IN);
extern void TL_LIT (ADDR_LOGICAL8, INTEGER);
// PSPEC TL.PROC.SPEC(ADDR[$LO8],$IN);
extern void TL_PROC_SPEC (ADDR_LOGICAL8, INTEGER);
// PSPEC TL.PROC.PARAM($IN,ADDR);
extern void TL_PROC_PARAM (INTEGER, ADDR);
// PSPEC TL.PROC.RESULT($IN);
extern void TL_PROC_RESULT (INTEGER);
// PSPEC TL.PROC($IN);
extern void TL_PROC (INTEGER);
// PSPEC TL.PROC.KIND($IN);
extern void TL_PROC_KIND (INTEGER);
// PSPEC TL.END.PROC();
extern void TL_END_PROC (void);
// PSPEC TL.BLOCK();
extern void TL_BLOCK (void);
// PSPEC TL.END.BLOCK();
extern void TL_END_BLOCK (void);
// PSPEC TL.ENTRY($IN);
extern void TL_ENTRY (INTEGER);
// PSPEC TL.LABEL.SPEC (ADDR[$LO8],$IN);
extern void TL_LABEL_SPEC (ADDR_LOGICAL8, INTEGER);
// PSPEC TL.LABEL($IN);
extern void TL_LABEL (INTEGER);
// PSPEC TL.PL($IN,$IN);
extern void TL_PL (INTEGER, INTEGER);
// PSPEC TL.D.TYPE($IN,$IN);
extern void TL_D_TYPE (INTEGER, INTEGER);
// PSPEC TL.INSERT($IN);
extern void TL_INSERT (INTEGER);
// PSPEC TL.CHECK($IN);
extern void TL_CHECK (INTEGER);
// PSPEC TL.REG($IN);
extern void TL_REG (INTEGER);
// PSPEC TL.LINE($IN32)/$IN;
extern INTEGER TL_LINE (INTEGER32);
// PSPEC TL.PRINT($IN);
extern void TL_PRINT (INTEGER);
// PSPEC TL($IN,ADDR[$LO8], $IN);
extern void TL (INTEGER, ADDR_LOGICAL8, INTEGER);
// PSPEC TL.END();
extern void TL_END (void);
// PSPEC TL.MODULE();
extern void TL_MODULE (void);
// PSPEC TL.END.MODULE($IN);
extern void TL_END_MODULE (INTEGER);
// PSPEC TL.MODE($IN,$IN);
extern void TL_MODE (INTEGER, INTEGER);
// PSPEC MUBLU16($LO);
static void MUBLU16 (LOGICAL);
// PSPEC MUBLU64($LO64)
static void MUBLU64 (LOGICAL64);
// PSPEC MUBLV($LO32,$IN);
static void MUBLV (LOGICAL32, INTEGER);
// PSPEC MUBLUV($IN32);
static void MUBLUV (INTEGER32);
// PSPEC MUBLIV($IN32);
static void MUBLIV (INTEGER32);
// PSPEC MUBLS(ADDR[$LO8]);
static void MUBLS (ADDR_LOGICAL8);
// PSPEC MUBLCODE($IN);
extern void MUBLCODE (INTEGER);
// PROC MUBLCODE(CODE);
void MUBLCODE (INTEGER CODE) {
#ifdef VAX
    mubl_code (CODE);
#else
// SELECTOUTPUT(MUTL.STR);
    SELECTOUTPUT(MUTL_STR);
// OUT.BIN.B(CODE);
    OUT_BIN_B(CODE);
#endif
// END
}



// PROC MUBLU16(HEX);
static void MUBLU16 (LOGICAL HEX) {
//    OUT.BIN.B(HEX ->>8 & %FF);
       OUT_BIN_B((HEX >>8) & 0xFF);
//    OUT.BIN.B(HEX & %FF );
       OUT_BIN_B(HEX & 0xFF );
// END;
}







// PROC MUBLU64(HEX);
static void MUBLU64 (LOGICAL64 HEX) {
//    $IN T;
    INTEGER T;
//    MUBLU16(%FF80);
    MUBLU16(0xFF80);
//    HEX ->>48 & %FFFF => T;
    T = (HEX >> 48) & 0xFFFF;
//    MUBLU16(T);
    MUBLU16(T);
//    HEX ->>32 & %FFFF => T;
    T = (HEX >> 32) & 0xFFFF;
//    MUBLU16(T);
    MUBLU16(T);
//    HEX ->>16 & %FFFF => T;
    T = (HEX >> 16) & 0xFFFF;
//    MUBLU16(T);
    MUBLU16(T);
//    HEX&%FFFF => T;
    T = HEX & 0xFFFF;
//    MUBLU16(T);
    MUBLU16(T);
// END;
}




// PROC MUBLV(N,I);
static void MUBLV (LOGICAL32 N, INTEGER I) {
//  ALTERNATIVE I FROM
    switch (I) {
//    OUT.BIN.B(N);
      case 0: OUT_BIN_B(N); break;
//    MUBLU16(N!%8000);
      case 1: MUBLU16(N|0x8000); break;
//    BEGIN
      case 2:
//     OUT.BIN.B(N->>16 ! %C0);
        OUT_BIN_B((N>>16) | 0xC0);
//     MUBLU16(N & %FFFF)
        MUBLU16(N & 0xFFFF);
//    END;
      break;
//    BEGIN
      case 3:
//     MUBLU16(N ->>16 ! %E000);
        MUBLU16((N>>16) | 0xE000);
//     MUBLU16(N & %FFFF)
        MUBLU16(N & 0xFFFF);
//    END;
      break;
//    BEGIN
      case 4:
//     OUT.BIN.B(%F0);
        OUT_BIN_B(0xF0);
//     MUBLU16(N ->>16 );
        MUBLU16(N>>16);
//     MUBLU16(N & %FFFF)
        MUBLU16(N & 0xFFFF);
//    END
      break;
//   END;
     }
// END;
}





// PROC MUBLUV(N);
static void MUBLUV (INTEGER32 N) {
//  $IN T;
    INTEGER T;
//    IF N & %F(6)80 = 0 THEN
    if ((N & 0xFFFFFF80) == 0) {
//      0 => T
      T = 0;
//    ELSE
    } else {
//    IF N & %F(4)C000 = 0 THEN
      if ((N & 0xFFFFC000) == 0) {
//      1 => T
        T = 1;
//    ELSE
      } else {
//     IF N & %FFE0(5) = 0 THEN
        if ((N & 0xFFE00000) == 0) {
//       2 => T
         T = 2;
//     ELSE
      } else {
//      IF N & %F0(7) = 0 THEN
        if ((N & 0xF0000000) == 0) {
//        3 => T
          T = 3;
//      ELSE
        } else {
//        4 => T
          T = 4;
//      FI
        }
//     FI
       }
//    FI
      }
//    FI;
     };
//    MUBLV(N,T);
    MUBLV(N,T);
// END;
}




// PROC MUBLIV(N);
static void MUBLIV (INTEGER32 N) {
// $IN T;
    INTEGER T;
// $IN32 J;
    INTEGER32 J;
// IF N & %F(6)C0 => J = 0 OR J = %F(6)C0 THEN
    if ((J = (N & 0xFFFFFFC0) == 0 || J == 0xFFFFFFC0)) {
//    0 => T;
      T = 0;
//    N & %7F => N;
      N = N & 0x7F;
// ELSE
    } else {
//  IF N & %F(4)E000 => J = 0 OR J = %F(4)E000 THEN
    if ((J = (N & 0xFFFFE000) == 0 || J == 0xFFFFE000)) {
//    1 => T;
      T = 1;
//    N & %3FFF => N;
      N = N & 0x3FFF;
//  ELSE
    } else {
//   IF N & %F(3)0(5) => J = 0 OR J = %F(3)0(5) THEN
    if ((J = (N & 0xFFF00000) == 0 || J == 0xFFF00000)) {
//     2 => T;
       T = 2;
//     N & %1F(5) => N
       N = N & 0x1FFFFF;
//   ELSE  IF N & %F80(6) => J = 0 OR J = %F80(6) THEN
     } else { if ((J = (N & 0xF8000000) == 0 || J == 0xF8000000)) {
//      3 => T;
        T = 3;
//      N & %F(6) => N
        N = N & 0xFFFFFF;
//     ELSE
       } else {
//       4 => T
         T = 4;
//     FI
       }
//   FI
     }
// FI
   }
// FI
   }
// MUBLV(N,T);
    MUBLV(N,T);
// END;
}





// PROC MUBLS(STR);
static void MUBLS (ADDR_LOGICAL8 STR) {
// $IN TEMP,I;
    INTEGER TEMP, I;
// SIZE(STR) => TEMP;
    TEMP = SIZE(STR);
//    OUT.BIN.B(TEMP);
    OUT_BIN_B(TEMP);
//    -1 => I
    I = -1;
//    WHILE 1 +> I < TEMP DO
    while (++ I < TEMP) {
//     OUT.BIN.B(STR^[I])
      OUT_BIN_B(STR[I]);
//    OD
    }
// END;
}




// PROC TL.TYPE(N,NAT);
void TL_TYPE (ADDR_LOGICAL8 N, INTEGER NAT) {
//      MUBLCODE(0); MUBLS(N) ; MUBLUV(NAT); ;
         MUBLCODE(0); MUBLS(N) ; MUBLUV(NAT); ;
// END;
}

// PROC TL.TYPE.COMP(T,D,NM);
void TL_TYPE_COMP (INTEGER T, ADDR D, ADDR_LOGICAL8 MN) {
//    MUBLCODE(1); MUBLUV(T); MUBLIV(D);MUBLS(NM); ;
       MUBLCODE(1); MUBLUV(T); MUBLIV((INTEGER32)D);MUBLS(MN); ; // XXX
// END;
}

// PROC TL.END.TYPE(STAT);
void TL_END_TYPE (INTEGER STAT) {
//    MUBLCODE(2); OUT.BIN.B(STAT); ;
       MUBLCODE(2); OUT_BIN_B(STAT); ;
// END;
}



// PROC TL.SEG(N,S,RTA,CTA,NL);
void TL_SEG (INTEGER N, ADDR S, LOGICAL32 RTA, ADDR CTA, INTEGER NL) {
// $LO32 I;RTA <<- 16 ! CTA => I;
    LOGICAL32 I; I = (RTA << 16) | (LOGICAL32) CTA; // XXX
// MUBLCODE(%15);OUT.BIN.B(N);MUBLUV(S);MUBLIV(I);
    MUBLCODE(0x15);OUT_BIN_B(N);MUBLUV((INTEGER32)S);MUBLIV(I); // XXX
// MUBLUV(NL);MUBLS(%"A");;
    MUBLUV(NL);MUBLS((ADDR_LOGICAL8) & vec ("A"));;
// END;
}



// PROC TL.LOAD(SN,AN);
void TL_LOAD (INTEGER SN, INTEGER AN) {
//    MUBLCODE(%2C); OUT.BIN.B(SN); OUT.BIN.B(AN); ;
       MUBLCODE(0x2C); OUT_BIN_B(SN); OUT_BIN_B(AN); ;
// END;
}

// PROC TL.CODE.AREA(AN);
extern void TL_CODE_AREA (INTEGER AN) {
//    MUBLCODE(3); OUT.BIN.B(AN); ;
       MUBLCODE(3); OUT_BIN_B(AN); ;
// END;
}



// PROC TL.DATA.AREA(AN);
void TL_DATA_AREA (INTEGER AN) {
//    MUBLCODE(4); OUT.BIN.B(AN); ;
       MUBLCODE(4); OUT_BIN_B(AN); ;
// END;
}



// PROC TL.SPACE(SI);
void TL_SPACE (ADDR SI) {
//    MUBLCODE(%07); MUBLUV(SI); ;
       MUBLCODE(0x07); MUBLUV((INTEGER32) SI); ; // XXX
// END;
}




// PROC TL.S.DECL(SN,T,D);
void TL_S_DECL (ADDR_LOGICAL8 SN, INTEGER T, ADDR D) {
#ifdef VAX
/*
ADDR_LOGICAL8 p = ((vector *) SN)->p;
size_t l = ((vector *) SN)->l;
char sn[l+1];
strncpy (sn, (const char *)p, l);
sn[l] = 0;
printf ("XXX TL_S_DECL sn '%s', T %x, D %d\n", sn, T, (int) D);
*/
    tl_s_decl (SN, T, D);
// 4c
// 01001100
#else
//    MUBLCODE(%08); MUBLS(SN); MUBLIV(T); MUBLUV(D); ;
       MUBLCODE(0x08); MUBLS(SN); MUBLIV(T); MUBLUV((INTEGER32) D); ; // XXX
#endif
// END;
}




// PROC TL.V.DECL(SN,SA,RS,WS,T,D);
void TL_V_DECL (ADDR_LOGICAL8 SN, LOGICAL32 SA, INTEGER RS, INTEGER WS, INTEGER T, ADDR D) {
//    MUBLCODE(%09);MUBLS(SN); MUBLUV(SA); MUBLU16(RS);
       MUBLCODE(0x09);MUBLS(SN); MUBLUV(SA); MUBLU16(RS);
// MUBLU16(WS); MUBLUV(T); MUBLIV(D); ;
    MUBLU16(WS); MUBLUV(T); MUBLIV((INTEGER32) D); ; // XXX
// END;
}




// PROC TL.SELECT.VAR;
void TL_SELECT_VAR (void) {
//     MUBLCODE(%3B);;
        MUBLCODE(0x3B);;
// END;
}







// PROC TL.SELECT.FIELD(N,F,A);
void TL_SELECT_FIELD (INTEGER N, INTEGER F, INTEGER A) {
//       MUBLCODE(%28);MUBLUV(N);MUBLUV(F);MUBLUV(A);
          MUBLCODE(0x28);MUBLUV(N);MUBLUV(F);MUBLUV(A);
// END;
}




// PROC TL.MAKE(S,T,D);
void TL_MAKE (INTEGER S, INTEGER T, ADDR D) {
//    MUBLCODE(%0A); MUBLU16(S); MUBLU16(T); MUBLIV(D);
       MUBLCODE(0x0A); MUBLU16(S); MUBLU16(T); MUBLIV((INTEGER32) D); // XXX
// END;
}




// PROC TL.ASS(VL,AN);
void TL_ASS (INTEGER VL, INTEGER AN) {
//      MUBLCODE(%26);MUBLUV(VL); MUBLIV(AN);;
         MUBLCODE(0x26);MUBLUV(VL); MUBLIV(AN);;
// END;
}



// PROC TL.ASS.VALUE(N,R);
void TL_ASS_VALUE (INTEGER N, INTEGER R) {
//     MUBLCODE(%F);MUBLUV(N);MUBLUV(R);;
        MUBLCODE(0xF);MUBLUV(N);MUBLUV(R);;
// END;
}











// PROC TL.ASS.END;
void TL_ASS_END (void) {
//     MUBLCODE(%D);;
        MUBLCODE(0xD);;
// END;
}



// PROC TL.C.LIT.16(BT,VAL);
void TL_C_LIT_16 (INTEGER BT, INTEGER16 VAL) {
//    MUBLCODE(%10); OUT.BIN.B(BT); MUBLUV(VAL); ;
       MUBLCODE(0x10); OUT_BIN_B(BT); MUBLUV(VAL); ;
// END;
}



// PROC TL.C.LIT.32(BT,VAL);
void TL_C_LIT_32 (INTEGER BT, INTEGER32 VAL) {
//    MUBLCODE(%35); OUT.BIN.B(BT); MUBLUV(VAL); ;
       MUBLCODE(0x35); OUT_BIN_B(BT); MUBLUV(VAL); ;
// END;
}


// PROC TL.C.LIT.64(BT,VAL);
void TL_C_LIT_64 (INTEGER BT, LOGICAL64 VAL) {
// $IN I;
    INTEGER I;
// VAL => I;
    I = VAL;
//    MUBLCODE(%36); OUT.BIN.B(BT); MUBLU64(VAL); ;
    MUBLCODE(0x36); OUT_BIN_B(BT); MUBLU64(VAL); ;
// END;
}





// PROC TL.C.NULL(PT);
extern void TL_C_NULL (INTEGER PT) {
//    MUBLCODE(%39);MUBLU16(PT);;
       MUBLCODE(0x39);MUBLU16(PT);;
// END;
}



// PROC TL.C.LIT.S(BT,VAL);
void TL_C_LIT_S (INTEGER BT, ADDR_LOGICAL8 VAL) {
//    MUBLCODE(%3C); OUT.BIN.B(BT); MUBLS(VAL); ;
       MUBLCODE(0x3C); OUT_BIN_B(BT); MUBLS(VAL); ;
// END;
}







// PROC TL.C.LIT.128(BT,VAL);
void TL_C_LIT_128 (INTEGER BT, REAL128 VAL) {
//    MUBLCODE(%37); OUT.BIN.B(BT); MUBLUV(VAL); ;
       MUBLCODE(0x37); OUT_BIN_B(BT); MUBLUV(VAL); ;
// END;
}




// PROC TL.LIT (SN, K);
void TL_LIT (ADDR_LOGICAL8 SN, INTEGER K) {
//    MUBLCODE(%B); MUBLS(SN); MUBLUV (K) ;
       MUBLCODE(0xB); MUBLS(SN); MUBLUV (K) ;
// END;
}




// PROC TL.PROC.SPEC(NAM, NAT);
void TL_PROC_SPEC (ADDR_LOGICAL8 NAM, INTEGER NAT) {
//    MUBLCODE(%11); MUBLS(NAM); MUBLUV(NAT); ;
       MUBLCODE(0x11); MUBLS(NAM); MUBLUV(NAT); ;
// END;
}




// PROC TL.PROC.PARAM(T, D);
void TL_PROC_PARAM (INTEGER T, ADDR D) {
//    MUBLCODE(%12); MUBLU16(T); MUBLIV(D) ;
       MUBLCODE(0x12); MUBLU16(T); MUBLIV((INTEGER32) D) ; // XXX
// END;
}



// PROC TL.PROC.RESULT(R,D);
// XXX nmsl calls are only (R)
void TL_PROC_RESULT (INTEGER R) {
//    MUBLCODE(%27); MUBLUV(R);
       MUBLCODE(0x27); MUBLUV(R);
// END;
}








// PROC TL.PROC(P);
void TL_PROC (INTEGER P) {
//    MUBLCODE(%13); MUBLUV(P); ;
       MUBLCODE(0x13); MUBLUV(P); ;
// END;
}




// PROC TL.PROC.KIND(K);
void TL_PROC_KIND (INTEGER K) {
//    MUBLCODE(%29); OUT.BIN.B(K); ;
       MUBLCODE(0x29); OUT_BIN_B(K); ;
// END;
}




// PROC TL.END.PROC;
void TL_END_PROC (void) {
//    MUBLCODE(%14); ;
       MUBLCODE(0x14); ;
// END;
}



// PROC TL.BLOCK;
void TL_BLOCK (void) {
//    MUBLCODE(%19); ;
       MUBLCODE(0x19); ;
// END;
}



// PROC TL.END.BLOCK;
void TL_END_BLOCK (void) {
//    MUBLCODE(%1A); ;
       MUBLCODE(0x1A); ;
// END;
}



// PROC TL.ENTRY(N);
void TL_ENTRY (INTEGER N) {
//       MUBLCODE(%31);MUBLU16(N);;
          MUBLCODE(0x31);MUBLU16(N);;
// END;
}


// PROC TL.LABEL.SPEC(N,U);
void TL_LABEL_SPEC (ADDR_LOGICAL8 N, INTEGER U) {
//    MUBLCODE(%1B); MUBLS(N); OUT.BIN.B(U); ;
       MUBLCODE(0x1B); MUBLS(N); OUT_BIN_B(U); ;
// END;
}



// PROC TL.LABEL(L);
void TL_LABEL (INTEGER L) {
//    MUBLCODE(%1C); MUBLU16(L); ;
       MUBLCODE(0x1C); MUBLU16(L); ;
// END;
}



// PROC TL.PL(F,N);
void TL_PL (INTEGER F, INTEGER N) {
//    IF N = 0 THEN
    if (N == 0) {
//    MUBLCODE(%80!F)
       MUBLCODE(0x80|F);
//    ELSE
    } else {
//    MUBLCODE(F->>1 ! %40);MUBLU16(F & 1 <<- 15 ! N)
       MUBLCODE((F>>1) | 0x40);MUBLU16(((F & 1) << 15) | N);
// FI
    }
// END
}



// PROC TL.D.TYPE(T,D);
void TL_D_TYPE (INTEGER T, INTEGER D) {
//      MUBLCODE(%8); MUBLU16(T); MUBLIV(D);
         MUBLCODE(0x8); MUBLU16(T); MUBLIV(D);
// END;
}



// PROC TL.INSERT(BIN);
void TL_INSERT (INTEGER BIN) {
//       MUBLCODE(%2B);MUBLUV(BIN);;
          MUBLCODE(0x2B);MUBLUV(BIN);;
// END;
}



// PROC TL.CHECK(S);
void TL_CHECK (INTEGER S) {
//    MUBLCODE(%34); MUBLU16(S); ;
       MUBLCODE(0x34); MUBLU16(S); ;
// END;
}



// PROC TL.REG(RU);
void TL_REG (INTEGER RU) {
//    MUBLCODE(%1E); OUT.BIN.B(RU); ;
       MUBLCODE(0x1E); OUT_BIN_B(RU); ;
// END;
}



// PROC TL.LINE(LN);
INTEGER TL_LINE (INTEGER32 LN) {
// IF LN = 0 THEN
    if (LN == 0) {
//   IPOS()=>LN
      LN = IPOS ();
// FI
    }
#ifdef VAX
    tl_line (LN);
#else
//    MUBLCODE(%21); MUBLUV(LN); ;
       MUBLCODE(0x21); MUBLUV(LN); ;
#endif
// END;
    return 0; // XXX
}




// PROC TL.PRINT(M);
void TL_PRINT (INTEGER M) {
//    MUBLCODE(%18); MUBLUV(M);;
       MUBLCODE(0x18); MUBLUV(M);;
// END;
}



// PROC TL(M,FN,DZ);
extern void TL (INTEGER M, ADDR_LOGICAL8 FN, INTEGER DZ) {

#ifdef VAX
/*
ADDR_LOGICAL8 p = ((vector *) FN)->p;
size_t l = ((vector *) FN)->l;
char tlfn[l+1];
strncpy (tlfn, (const char *)p, l);
tlfn[l] = 0;
tlfilename = strdup (tlfn);
//
// mtl121
// <C8 %22>TL(<U8>MODE,<S>FILENAME,<UV>DIRECTORY.SIZE)
printf ("TL:MUTL initialization\n");
printf ("%s\n", "     <C8 %22>TL(<U8>MODE,<S>FILENAME,<UV>DIRECTORY.SIZE)");
printf ("    code 0x22   MODE %02x   FILENAME '%s'   DIRECTORY.SIZE %d\n", M, tlfn, DZ);
    MUTL_STR = DEFINE_OUTPUT (-1, FN, 0, 0);
    SELECTOUTPUT(MUTL_STR);
    tlfp = current_output ();
fprintf (tlfp, "// TL: MUTL initialization\n");
fprintf (tlfp, "// TL: generating '%s'\n", tlfn);
*/
tl (M, FN, DZ);
#else
//    MUBLCODE(%22); OUT.BIN.B(M); MUBLS(FN); MUBLUV(DZ) ;
       MUBLCODE(0x22); OUT_BIN_B(M); MUBLS(FN); MUBLUV(DZ) ;
#endif
// END;
}



// PROC TL.END;
void TL_END (void) {
//    MUBLCODE(%23); ;
       MUBLCODE(0x23); ;
// END;
}



// PROC TL.MODULE;
void TL_MODULE (void) {
#ifdef VAX
    tl_module ();
#else
//    MUBLCODE(%24); ;
       MUBLCODE(0x24); ;
#endif
// END;
}



// PROC TL.END.MODULE(ST);
void TL_END_MODULE (INTEGER ST) {
//    MUBLCODE(%25); MUBLUV(ST);
       MUBLCODE(0x25); MUBLUV(ST);
// END;
}



// PROC TL.MODE(I,J);
void TL_MODE (INTEGER I, INTEGER J) {
//       MUBLCODE(%30); MUBLU16(I);MUBLUV(J);;
          MUBLCODE(0x30); MUBLU16(I);MUBLUV(J);;
// END;
}



// ::END
// *END
