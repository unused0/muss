#include "dbg.h"
#include "kernel.h"

void CREATE_SEGMENT(INTEGER SEG_NO, ADDR REQ_SIZE)
  {
    dbg ("XXX CREATE_SEGMENT (%d, %u)\n", SEG_NO, REQ_SIZE);
  }
void RELEASE_SEGMENT (INTEGER SEG_NO)
  {
    dbg ("XXX RELEASE_SEGMENT (%d)\n", SEG_NO);
  }
