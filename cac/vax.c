#include <stdio.h>
#include <stdlib.h>

#include "muss.h"
#define xlib
#include "lib.h"
#include "vax.h"

static char * musl_filename;
static FILE * tlfp;
static char * tlfilename;
static char * muslfilename;

void set_musl_filename (uint8_t * FILE)
  {
    uint8_t * p = ((vector *) FILE)->p;
    size_t l = ((vector *) FILE)->l;
    musl_filename = malloc (l + 1);
    strncpy (musl_filename, (const char *)p, l);
    musl_filename[l] = 0;
  }

void tl_s_decl (uint8_t * SN, int T, void * D)
  {
    uint8_t * p = ((vector *) SN)->p;
    size_t l = ((vector *) SN)->l;
    char sn[l+1];
    strncpy (sn, (const char *)p, l);
    sn[l] = 0;
    printf ("XXX TL_S_DECL sn '%s', T %x, D %d\n", sn, T, (int) D);
  }

void tl_line (int32_t LN)
  {
    fprintf (tlfp, "#line %d %s\n", LN, muslfilename);
  }

void tl (int M, uint8_t * FN, int DZ)
  {
    uint8_t * p = ((vector *) FN)->p;
    size_t l = ((vector *) FN)->l;
    char tlfn[l+1];
    strncpy (tlfn, (const char *)p, l);
    tlfn[l] = 0;
    tlfilename = strdup (tlfn);
//
// mtl121
// <C8 %22>TL(<U8>MODE,<S>FILENAME,<UV>DIRECTORY.SIZE)
    printf ("TL:MUTL initialization\n");
    printf ("%s\n", "     <C8 %22>TL(<U8>MODE,<S>FILENAME,<UV>DIRECTORY.SIZE)");
    printf ("    code 0x22   MODE %02x   FILENAME '%s'   DIRECTORY.SIZE %d\n", M, tlfn, DZ);
    /* 
    MUTL_STR = DEFINE_OUTPUT (-1, FN, 0, 0);
    SELECTOUTPUT(MUTL_STR);
    */
    tlfp = current_output ();
    fprintf (tlfp, "// TL: MUTL initialization\n");
    fprintf (tlfp, "// TL: generating '%s'\n", tlfn);
  }

void tl_module (void)
  {
    printf ("XXX TL_MODULE: Initialize compilation of a module\n");
  }

void mubl_code (int CODE)
  {
    printf ("XXX MUBLCODE %02x\n", CODE);
  }

