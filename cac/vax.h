#include <stdint.h>
void set_musl_filename (uint8_t * fnvec);
void tl_s_decl (uint8_t * SN, int T, void * D);
void tl_line (int32_t LN);
void tl (int M, uint8_t * FN, int DZ);
void tl_module (void);
void mubl_code (int CODE);

