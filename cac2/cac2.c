//
// cac2 infile
//

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

static FILE * infile;
static FILE * outfile;

static int line_cnt = 0;


typedef enum types_e types;
enum types_e
  { 
    ID,

    KW_ADDR, KW_AND, KW_ALTERNATIVE, KW_BEGIN,
    KW_DATAVEC, KW_DO, KW_ELSE, KW_END,
    KW_EXIT, KW_FI, KW_FOR, KW_FROM,
    KW_IF, KW_IMPORT, KW_IS,
    KW_LABEL, KW_LITERAL, KW_LSPEC,
    KW_MODULE, KW_OD, KW_OF, KW_OR,
    KW_PROC, KW_PSPEC, KW_SELECT,
    KW_SPACE, KW_SWITCH, KW_THEN, KW_TYPE,
    KW_VSTORE, KW_WHILE, KW_WITHIN,

    NUMERIC_TYPE,

    LPAREN, RPAREN,
    LBRACK, RBRACK,

    INTEGER,

    COMMA,
    SEMI,
    COLON,

    AND,            // &
    AND_STORE,      // &>
    OR,             // !
    OR_STORE,       // !>
    ADD,            // +
    ADD_STORE,      // +>
    SUB,            // -
    SUB_STORE,      // ->
    SUB_REV,        // -:
    SUB_REV_STORE,  // -:>
    XOR,            // -=
    XOR_STORE,      // -=>
    RSHIFT,         // ->>
    LSHIFT,         // <<-
    MUL,            // *
    MUL_STORE,      // *>
    DIV,            // /
    DIV_STORE,      // />
    DIV_REV,        // /:
    DIV_REV_STORE,  // /:>
    ASSIGN,         // =>

    EQ,             // = 
    NE,             // /= 
    LT,             // < 
    LE,             // =< 
    GT,             // > 
    GE,             // >= 

    WEIRD,          // >:     // Seen in nsml011: "START>:"; maybe a "main"?


    // Parser generated types
    EXPR,
    NUMERIC_TYPE_REF,
    LSPEC_REF,
  };

typedef enum subtypes_e subtypes;
enum subtypes_e
  {
    ST_INTEGER, ST_INTEGER1, ST_INTEGER8, ST_INTEGER16, ST_INTEGER24, ST_INTEGER32, ST_INTEGER64, ST_INTEGER128,
    ST_LOGICAL, ST_LOGICAL1, ST_LOGICAL8, ST_LOGICAL16, ST_LOGICAL24, ST_LOGICAL32, ST_LOGICAL64, ST_LOGICAL128,
    ST_REAL, ST_REAL1, ST_REAL8, ST_REAL16, ST_REAL24, ST_REAL32, ST_REAL64, ST_REAL128
   };

typedef struct node_s node;
struct node_s
  {
    node * left;
    node * right;
    node * parent;
    int nodeid;
    types type;
    subtypes subtype;
    char * value;
    uint64_t ivalue;
    bool parsed;
  };

static node * head = NULL;
static node * tail = NULL;
static int nodeid = 0;

static node * mknod (void)
  {
    node * p = malloc (sizeof (node));
    if (! p)
      {
        printf ("append_node malloc fail\n");
        exit (1);
      }
    p->nodeid = nodeid ++;
    return p;
  }

static void append_node (char * id, types type, subtypes subtype, uint64_t ivalue)
  {
    node * p = mknod ();
    if (head == NULL)
      {
        head = p;
      }
    p->type = type;
    p->subtype = subtype;
    p->value = strdup (id);
    p->ivalue = ivalue;
    p->parsed = false;
    p->left =  NULL;
    p->right = NULL;
    p->parent = tail;
    if (tail != NULL)
      tail->right = p;
    tail = p;
  }

struct tt_s { types t; char * str; } type_table[] =
  {
    ID, "ID",
    NUMERIC_TYPE, "NUMERIC_TYPE",
    NUMERIC_TYPE_REF, "<numeric_type_ref>",
    INTEGER, "INTEGER",
    EXPR, "<expr>",
  };
#define ttn (sizeof (type_table) / sizeof (struct tt_s))

static void print_node (node * p)
  {
    char typebuf [16];
    char * tptr = typebuf;
    sprintf (typebuf, "%d", p->type);
    int i;
    for (i = 0; i < ttn; i ++)
      {
        if (type_table[i].t == p->type)
          {
            tptr = type_table[i].str;
            break;
          }
      }
    printf ("%6d %12s%s %3d %9ld %s\n", p->nodeid, tptr, p->parsed ? "*" : " ", p->subtype, p->ivalue, p->value ? p->value : "NULL");

  }


// MUSL doesn't have reserved words, but they make parsing easier, and
// nsml doesn't rely on it.

static struct kw_s
  {
    char * name;
    types type;
    subtypes subtype;
  } kw_tab [] =
  {
    "ADDR", KW_ADDR, 0,
    "AND", KW_AND, 0,
    "ALTERNATIVE", KW_ALTERNATIVE, 0,
    "BEGIN", KW_BEGIN, 0,
    "DATAVEC", KW_DATAVEC, 0,
    "DO", KW_DO, 0,
    "ELSE", KW_ELSE, 0,
    "END", KW_END, 0,
    "EXIT", KW_EXIT, 0,
    "FI", KW_FI, 0,
    "FOR", KW_FOR, 0,
    "FROM", KW_FROM, 0,
    "IF", KW_IF, 0,
    "IMPORT", KW_IMPORT, 0,
    "INTEGER", NUMERIC_TYPE, ST_INTEGER,
    "INTEGER1", NUMERIC_TYPE, ST_INTEGER1,
    "INTEGER8", NUMERIC_TYPE, ST_INTEGER8,
    "INTEGER16", NUMERIC_TYPE, ST_INTEGER16,
    "INTEGER24", NUMERIC_TYPE, ST_INTEGER24,
    "INTEGER32", NUMERIC_TYPE, ST_INTEGER32,
    "INTEGER64", NUMERIC_TYPE, ST_INTEGER64,
    "INTEGER128", NUMERIC_TYPE, ST_INTEGER128,
    "LSPEC", KW_LSPEC, 0,
    "IS", KW_IS, 0,
    "LABEL", KW_LABEL, 0,
    "LITERAL", KW_LITERAL, 0,
    "LOGICAL", NUMERIC_TYPE, ST_LOGICAL,
    "LOGICAL1", NUMERIC_TYPE, ST_LOGICAL1,
    "LOGICAL8", NUMERIC_TYPE, ST_LOGICAL8,
    "LOGICAL16", NUMERIC_TYPE, ST_LOGICAL16,
    "LOGICAL24", NUMERIC_TYPE, ST_LOGICAL24,
    "LOGICAL32", NUMERIC_TYPE, ST_LOGICAL32,
    "LOGICAL64", NUMERIC_TYPE, ST_LOGICAL64,
    "LOGICAL128", NUMERIC_TYPE, ST_LOGICAL128,
    "LSPEC", KW_LSPEC, 0,
    "MODULE", KW_MODULE, 0,
    "OD", KW_OD, 0,
    "OF", KW_OF, 0,
    "OR", KW_OR, 0,
    "PROC", KW_PROC, 0,
    "PSPEC", KW_PSPEC, 0,
    "REAL", NUMERIC_TYPE, ST_REAL,
    "REAL1", NUMERIC_TYPE, ST_REAL1,
    "REAL8", NUMERIC_TYPE, ST_REAL8,
    "REAL16", NUMERIC_TYPE, ST_REAL16,
    "REAL24", NUMERIC_TYPE, ST_REAL24,
    "REAL32", NUMERIC_TYPE, ST_REAL32,
    "REAL64", NUMERIC_TYPE, ST_REAL64,
    "REAL128", NUMERIC_TYPE, ST_REAL128,
    "SELECT", KW_SELECT, 0,
    "SPACE", KW_SPACE, 0,
    "SWITCH", KW_SWITCH, 0,
    "THEN", KW_THEN, 0,
    "TYPE", KW_TYPE, 0,
    "VSTORE", KW_VSTORE, 0,
    "WHILE", KW_WHILE, 0,
    "WITHIN", KW_WITHIN, 0,
    NULL, 0, 0
  };



/////////////////////////////
/////////////////////////////
/////////////////////////////
//
// Lexical analysis
//
/////////////////////////////
/////////////////////////////
/////////////////////////////

typedef struct symbol_s symbol;
struct symbol_s
  {
    symbol * next;
    char * id;
    bool export;
  };

static symbol * symbols = NULL;

static void new_symbol (char * id)
  {
    symbol * p = malloc (sizeof (symbol));
    if (! p)
      {
        printf ("new_symbol malloc fail\n");
        exit (1);
      }
    p->id = strdup (id);
    p->export = false;
    p->next = symbols;
    symbols = p;
  }

static symbol * lookup_symbol (char * id)
  {
    for (symbol * p = symbols; p; p = p->next)
      if (strcmp (id, p->id) == 0)
         return p;
    return NULL;
  }

static void print_symbol_table (void)
  {
    for (symbol * p = symbols; p; p = p->next)
      printf ("%20s %s\n", p->id, p->export ? "" : "static");
  }

static void add_symbol (char * id)
  {
    if (lookup_symbol (id) != NULL)
      return;
    new_symbol (id);
  }
  
static void exp_mac (char * at, char * with)
  {
    size_t l = strlen (with);
    memmove (at + l, at + 3, strlen (at + 3) + 1);
    memcpy (at, with, l);
  }

static void expand_macro (char * buf, size_t sz, char * at)
  {
    if (strlen (buf) + 32 > sz)
      {
        printf ("expand_macro buf overflow %ld %ld\n", strlen (buf), sz);
        exit (1);
      }

    for (struct kw_s * p = kw_tab; p->name; p ++)
      {
        if (at [1] == p->name[0] && at [2] == p->name[1])
          {
            exp_mac (at, p->name);
            return;
          }
      }
    printf ("macro expansion failed at '%s'\n", at);
    exit (1);
  }







static void append_integer (char * id, uint64_t ivalue)
  {
    append_node (id, INTEGER, 0, ivalue);
  }

static void append_id (char * id)
  {
//printf ("id '%s'\n", id);
    for (struct kw_s * p = kw_tab; p->name; p ++)
      {
        if (strcmp (id, p->name) == 0)
          {
            append_node (id, p->type, p->subtype, 0);
            return;
          }
      }
//printf ("assuming '%s'\n", id);
    add_symbol (id);
    append_node (id, ID, 0, 0);
  }

static void process_line (char * buf, size_t sz)
  {
    line_cnt ++;
    //fprintf (outfile, ">>>> process_line: '%s'\n", buf);

    // nmsl doesn't have any control characters embedded in literals, so this
    // is ok for this application, and makes error reports cleaner.
#if 1
    // Clean up: remove control characters
    char * p = buf;
    for (;;)
      {
        if (! * p) break;
        if (* p < ' ')
          strcpy (p, p + 1);
        else
          p ++;
      }
#endif

    // Lexical scan

    p = buf;

scan: ;
    char ch = * p;

    if (! ch)
      goto eol;

// whitespace
    if (ch <= ' ')
      goto next;

// :: comment
    if (ch == ':' && p[1] == ':')
      goto eol;  // treat as eol

// $..  macro
    if (ch == '$')
      {
        expand_macro (buf, sz, p);
        goto scan;
      }

// ( left paren
    if (ch == '(')
      {
        append_node ("(", LPAREN, 0, 0);
        goto next;
      }

// ) right paren
    if (ch == ')')
      {
        append_node (")", RPAREN, 0, 0);
        goto next;
      }

// ( left bracket
    if (ch == '[')
      {
        append_node ("[", LBRACK, 0, 0);
        goto next;
      }

// ) right bracket
    if (ch == ']')
      {
        append_node ("]", RBRACK, 0, 0);
        goto next;
      }

// , comma
    if (ch == ',')
      {
        append_node (",", COMMA, 0, 0);
        goto next;
      }

// ; semicolon
    if (ch == ';')
      {
        append_node (";", SEMI, 0, 0);
        goto next;
      }

// : colon
    if (ch == ':')
      {
        append_node (":", COLON, 0, 0);
        goto next;
      }

// Operators

// comparators =  / =   <  >  =<  >=

// <  lt
// <<- left shift
    if (ch == '<')
      {
        if (p[1] == '<' && p[2] == '-')
          {
            append_node ("<<-", LSHIFT, 0, 0);
            p += 2;
            goto next;
          }
        append_node ("<", LT, 0, 0);
        goto next;
      }

// ! or
// !> or store
    if (ch == '&')
      {
        if (p[1] == '>')
          {
            append_node ("&>", AND_STORE, 0, 0);
            p ++;
            goto next;
          }
        append_node ("&", AND, 0, 0);
        goto next;
      }

// & and
// &> and store
    if (ch == '&')
      {
        if (p[1] == '>')
          {
            append_node ("&>", AND_STORE, 0, 0);
            p ++;
            goto next;
          }
        append_node ("&", AND, 0, 0);
        goto next;
      }

// + add
// +> add store
    if (ch == '+')
      {
        if (p[1] == '>')
          {
            append_node ("+>", ADD_STORE, 0, 0);
            p ++;
            goto next;
          }
        append_node ("+", ADD, 0, 0);
        goto next;
      }

// - sub
// -> sub store
    if (ch == '-')
      {
        if (p[1] == '>')
          {
            if (p[2] == '>')
              {
                append_node ("->>", RSHIFT, 0, 0);
                p += 2;
                goto next;
              }
            append_node ("->", SUB_STORE, 0, 0);
            p ++;
            goto next;
          }
        if (p[1] == '=')
          {
            if (p[2] == '>')
              {
                append_node ("-=>", XOR_STORE, 0, 0);
                p += 2;
                goto next;
              }
            append_node ("-=", XOR, 0, 0);
            p ++;
            goto next;
          }
        if (p[1] == ':')
          {
            if (p[2] == '>')
              {
                append_node ("-:>", SUB_REV_STORE, 0, 0);
                p += 2;
                goto next;
              }
            append_node ("-:", SUB_REV, 0, 0);
            p ++;
            goto next;
          }
        append_node ("-", SUB, 0, 0);
        goto next;
      }

// / star
    if (ch == '*')
      {
        if (p[1] == '>')
          {
            append_node ("*>", MUL_STORE, 0, 0);
            p ++;
            goto next;
          }
        append_node ("*", MUL, 0, 0);
        goto next;
      }

// / slash
    if (ch == '/')
      {
        if (p[1] == ':')
          {
            if (p[2] == '>')
              {
                append_node ("/:>", DIV_REV_STORE, 0, 0);
                p += 2;
                goto next;
              }
            append_node ("/:", DIV_REV, 0, 0);
            p ++;
            goto next;
          }
        if (p[1] == '>')
          {
            append_node ("/>", DIV_STORE, 0, 0);
            p ++;
            goto next;
          }
        if (p[1] == '=')
          {
            append_node ("/=", NE, 0, 0);
            p ++;
            goto next;
          }
        append_node ("/", DIV, 0, 0);
        goto next;
      }

// = equals
// => assign
// =< LE
    if (ch == '=')
      {
        if (p[1] == '>')
          {
            append_node ("=>", ASSIGN, 0, 0);
            p ++;
            goto next;
          }
        if (p[1] == '<')
          {
            append_node ("=<", LE, 0, 0);
            p ++;
            goto next;
          }
        append_node ("=", EQ, 0, 0);
        goto next;
      }

// >
// >= GE
// >: weird
    if (ch == '>')
      {
        if (p[1] == '=')
          {
            append_node (">=", GE, 0, 0);
            p ++;
            goto next;
          }
        if (p[1] == ':')
          {
            append_node (">:", WEIRD, 0, 0);
            p ++;
            goto next;
          }
        append_node (">", GT, 0, 0);
        goto next;
      }

// % hex literal
    if (ch == '%')
      {
        uint64_t val = 0;
        char id [4096];
        char * idp = id;
        int last_digit = 0;
        * idp = ch; // copy the %
        p ++;     // p points to the first digit
        ch = * p;
        idp ++;
        while ((ch >= '0' && ch <= '9') || (ch >= 'A' && ch <= 'F') || ch == '(')
          {
            * idp = ch;
            idp ++;
            if (ch == '(')
              {
                p ++;     // p points to repeat digit
                ch = * p;
                * idp = ch; 
                idp ++;
                if (ch < '0' || ch > '9' || p[1] != ')')
                  {
                    printf ("lost in hex const '%s'\n", p);
                    exit (1);
                  }

                p ++;  // p points to the close paren
                ch = * p;
                * idp = ch;
                idp ++;
              }
            else if (ch >= '0' && ch <= '9')
              {
                val = (val * 16) + (ch - '0');
                p ++;
                ch = * p;
              }
            else  // A-F
              {
                val = (val * 16) + (ch - 'A' + 10);
                p ++;
                ch = * p;
              }
          }
        *idp = '\0';
        append_integer (id, val);
        goto scan;
      }

             

// Identifier
    if (ch >= 'A' && ch <= 'Z')
      {
        char id [4096];
        char * idp = id;
        while ((ch >= 'A' && ch <= 'Z') || ((ch >= '0') && (ch <= '9')) || ch == '.')
          {
            if (ch != '.')
              {
                * idp = ch;
                idp ++;
              }
            p ++;
            ch = * p;
          }
        *idp = '\0';
        append_id (id);
        goto scan;
      }

// Integer
    if (ch >= '0' && ch <= '9')
      {
        uint64_t val = 0;
        char id [4096];
        char * idp = id;
        while (ch >= '0' && ch <= '9')
          {
            * idp = ch;
            idp ++;
            val = (val * 10) + (ch - '0');
            p ++;
            ch = * p;
          }
        *idp = '\0';
        append_integer (id, val);
        goto scan;
      }

    printf ("XXX Lost at '%s'\n", p);
    exit (1);

next: ;
    p ++;
    goto scan;

eol: ;
  }

static void process_eof (void)
  {
    //fprintf (outfile, ">>>> EOF\n");
  }

static void print_tree (node * p, int depth)
  {
    for (int i = 0; i < depth % 40; i ++)
      printf (" ");
    print_node (p);
    if (p->left)
      print_tree (p->left, depth + 4);
    if (p->right)
      print_tree (p->right, depth);
  }

/////////////////////////////
/////////////////////////////
/////////////////////////////
//
// Parsing
//
/////////////////////////////
/////////////////////////////
/////////////////////////////


// numbers to expressions
//
//    INTEGER   -->    EXPRESSION.left <= INTEGER
                       
static bool numbers_to_expressions (node * * p)
  {
    if ((*p)->type != INTEGER || (*p)->parsed)
      return true;

    node * exp = mknod ();
    exp->left = *p;  //
    exp->right = (*p)->right;
    (*p)->right = NULL;
    exp->type = EXPR;
    exp->subtype = 0;
    exp->value = "<expr>";
    exp->ivalue = 0;
    exp->parsed = false;

    (*p)->parsed = true;
    *p = exp;
    return false;
  }

//  INTEGERn/LOGICALn/REALn -->  NUMERIC_TYPE_REF
//  ADDR [...] --> NUMERIC_TYPE_REF

static bool parse_addr_type (node * * p)
  {
    if ((*p)->type != KW_ADDR || (*p)->parsed)
      return true;
//printf ("spotted ADDR at %d\n", (*p)->nodeid);
    node * q = (*p)->right;
    if (q->type != LBRACK)
      {
//printf ("not lbrack\n");
        return true;
      }
    q = q->right;
    if (q->type != NUMERIC_TYPE_REF && q->type != NUMERIC_TYPE)
      {
//printf ("not numeric type\n");
        return true;
      }
    q = q->right;
    if (q->type != RBRACK)
      {
//printf ("not rbrack\n");
        return true;
      }

    node * ref = mknod ();
    // move p..q to left
    ref->left = *p;  
    ref->right = q->right;
    q->right = NULL;

    ref->type = NUMERIC_TYPE_REF;
    ref->subtype = 0;
    ref->value = "<expr>";
    ref->ivalue = 0;
    ref->parsed = true;

    (*p)->parsed = true;
    *p = ref;
    return false;
  }

static bool parse_type (node * * p)
  {
    if ((*p)->type != NUMERIC_TYPE || (*p)->parsed)
      return true;

    node * indtype = NULL;

    node * ref = mknod ();
    ref->left = *p;  
    ref->right = (*p)->right;
    (*p)->right = NULL;

    ref->type = NUMERIC_TYPE_REF;
    ref->subtype = 0;
    ref->value = "<expr>";
    ref->ivalue = 0;
    ref->parsed = true;

    (*p)->parsed = true;
    *p = ref;
    return false;
  }

// LSPEC id ( type_list ) / type

static bool parse_lspec (node * * p)
  {
    if ((*p)->type != KW_LSPEC || (*p)->parsed)
      return true;

    // walk the symbols....
    node * q = (*p)-> right;
    if (q->type != ID)
      {
        printf ("parse_lspec ID lost at %d\n", q->nodeid);
        return true;
      }

    q = q->right;
    if (q->type != LPAREN)
      {
        printf ("parse_lspec LPAREN lost at %d\n", q->nodeid);
        return true;
      }

    q = q->right;
    if (q->type != RPAREN)
      {
list:
        if (q->type != NUMERIC_TYPE_REF)
          {
            printf ("parse_lspec NUMERIC_TYPE_REF lost at %d\n", q->nodeid);
            return true;
          }

        q = q->right;
        if (q->type == COMMA)
          {
            q = q->right;
            goto list;
          }
        if (q->type != RPAREN)
          {
            printf ("parse_lspec RPAREN lost at %d\n", q->nodeid);
            return true;
          }
      }
    q = q->right;
    if (q->type == DIV)
      {
        q = q->right;
        if (q->type != NUMERIC_TYPE_REF )
          {
            printf ("parse_lspec return NUMERIC_TYPE_REF lost at %d\n", q->nodeid);
            return true;
          }
        q = q->right;
      }

    if (q->type != SEMI)
      {
        printf ("parse_lspec SEMI lost at %d; %d\n", q->nodeid, q->type);
        return true;
      }

    node * ref = mknod ();
    // move p..q to left
    ref->left = *p;  
    ref->right = q->right;
    q->right = NULL;

    ref->type = LSPEC_REF;
    ref->subtype = 0;
    ref->value = "<lspec>";
    ref->ivalue = 0;
    ref->parsed = true;

    (*p)->parsed = true;
    *p = ref;
    return false;
  }

static void walk_node (node * * p, bool (* apply) (node * *))
  {
    bool down = apply (p);
    if (down && (*p)->left)
      walk_node (&((*p)->left), apply);
    if ((*p)->right)
      walk_node (&((*p)->right), apply);
  }

static void walk_tree (bool (* apply) (node * *))
  {
    walk_node (& head, apply);
  }

static void parse_tree (void)
  {
    walk_tree (numbers_to_expressions);

    walk_tree (parse_addr_type);
    walk_tree (parse_type);
    walk_tree (parse_lspec);
    //print_tree (head, 0);
  }

static void usage (void)
  {
    fprintf (stderr, "cac2 infile\n");
    exit (1);
  }

int main (int argc, char * argv [])
  {
    if (argc != 2)
      usage();
    if ((infile = fopen (argv[1], "r")) == NULL)
      {
        fprintf (stderr, "Can't open infile '%s'.\n", argv[1]);
      }
    outfile = stdout;

    while (! feof (infile))
      {
        char buf [4096];
        if (! fgets (buf, sizeof (buf), infile))
          break;
        process_line (buf, sizeof (buf));
      }
     process_eof ();

     parse_tree ();
     print_tree (head, 0);
     print_symbol_table ();
  }

